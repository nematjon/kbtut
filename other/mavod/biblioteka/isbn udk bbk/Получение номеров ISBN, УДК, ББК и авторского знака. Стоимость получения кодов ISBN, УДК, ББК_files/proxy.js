var mailru = mailru || {};
mailru.def = {
	FLASH_TRANSPORT_URL: 'http://img.imgsmail.ru/r/my/app/flash_lc.swf'
};
mailru.intercom = {flash : {}};
mailru.proxy = mailru.proxy || {
	_domain: null,
	_receiver: null,
	_req: (function(){
		var xhr = function(){
			if(window.XMLHttpRequest){
				return new XMLHttpRequest();
			} else {
				try {
					return new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {
					try {
						return new ActiveXObject("Microsoft.XMLHTTP");
					} catch(e) {}
				}
			}
		}
		return function(opts){
			var r = xhr();
			if(!opts || !opts.url || !r)
				return;
			opts.method = opts.method || 'GET';

			opts.get = opts.get || {};

			r.open(opts.method, opts.url+ '?'+ mailru.utils.makeGet(opts.get), true);
			if(opts.cb){
				r.onreadystatechange = function() {
					if (r.readyState == 4) {
						window.clearTimeout(timeout);
						if(r.status == 200) {
							if(!opts._timeout_trigged)
								opts.cb(r.responseText);
						} else if(opts.cbError){
							opts.cbError('http_error', r.status);
						}
					}
				}
			}
			if(opts.cbError){
				var timeout = window.setTimeout(function(){
					opts._timeout_trigged=1;
					opts.cbError('timeout');
				}, opts.timeout||15000);
			}
			r.send(null);
		}
	})(),
	_transmit: function(params){
		if(mailru.utils.parseGet(params.result)['app'] && mailru.utils.parseGet(params.result)['app'] === '1')
			mailru.intercomType = 'parentCall'
        if(mailru.utils.parseGet((document.URL.match(/\?(.*)/ )||[0,''])[1])['appProxy'] || window.name.indexOf('xdm') != -1)
            mailru.intercomType = 'xdm';
		var get = mailru.utils.makeGet(params);

		switch(mailru.intercomType){
            case 'xdm' :

                var xdm = new mrcXDM.Socket({
                    windowName: true,
                    onMessage: function(){}
                });
                xdm.postMessage(get);
                break;
			case 'parentCall' : 
				document.domain = 'mail.ru'
				if(parent.mailru && parent.mailru.server){
					parent.mailru.server.intercom.receiver(get);
				};
				break;
			case 'hash':
				var url = this._receiver+ '?'+ get;
				document.location = url;
				break;
			case 'event':
				var wnd;
				switch(params.relation){
					case 'opener':
						wnd = window.opener;
						break;
					case 'parent':
						wnd = window.parent;
						break;
					case 'pparent':
						wnd = window.parent.parent;
						break;
				}
				
                if(mailru.isOpera)
                    this._domain = 'http://' + this._domain.replace('http://','').split('/')[0];
				wnd.postMessage(get, this._domain);
				if(params.relation=='opener') window.close();
				break;
			case 'flash':
				mailru.intercom.flash = {
					transport: "",
					params: {},
                    sended: false,
					sendOnReady: '',
					insertFlash: function(){
						mailru.intercom.flash.transport = document.createElement('div');
						document.body.appendChild(mailru.intercom.flash.transport);
						mailru.intercom.flash.transport.id = 'flash-transport-container';
						mailru.intercom.flash.transport.innerHTML = '' +
							'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="api-lcwrapper" height="1" width="1" type="application/x-shockwave-flash" data="' + mailru.def.FLASH_TRANSPORT_URL + '">' +
								'<param value="always" name="allowScriptAccess"/>' +
								'<param value="' + mailru.def.FLASH_TRANSPORT_URL + '" name="movie"/>' +
								'<param value="' + mailru.intercom.flash.vars + '" name="FlashVars"/>' +
							'</object>';
					},
					init: function(data){
						var _get = mailru.utils.parseGet((document.URL.match(/\?(.*)/ )||[''])[0].replace(document.location.hash,  ''));
						mailru.intercom.flash.params.fcid = _get.fcid || _get.window_id;
						if(!mailru.intercom.flash.params.fcid)
							mailru.intercom.flash.params.fcid = mailru.utils.uniqid();
						mailru.intercom.flash.sendOnReady = data;
						
						mailru.intercom.flash.vars = mailru.utils.makeGet({
							CBReady: 'mailru.intercom.flash.ready',
							listenTo: '',
							connectTo: 'api',
							cid: mailru.intercom.flash.params.fcid,
							host: document.domain,
							role: 'client',
							noOpposite: 1
						});
						
						mailru.intercom.flash.insertFlash();						
					},
					request: function(params){
						params = mailru.utils.makeGet(params);
						mailru.intercom.flash.transport.send(params);
                        mailru.intercom.flash.sended = true;
					},
					ready : function(){
						mailru.intercom.flash.transport = document.getElementById('api-lcwrapper');
						
						if(params.relation=='opener'){

							mailru.intercom.flash.onSendStatus = function(){
								try{
                                    window.close();
                                }catch(e){}
							}
						}
						mailru.intercom.flash.request(params);
					},
					onSendStatus: function(){}
				}
				mailru.intercom.flash.init(get);
				
				break;
		}
	},
	init: function(domain, receiver){
		this._domain = domain;
		this._receiver = receiver;
		
		var request = (document.location.href.match(/\?(.*)/)||[0,''])[1].replace('#', '&'),
			get = mailru.utils.parseGet(request);

        request = request.replace('&fcid=' + get.fcid, '').replace('&host=' + get.host, '');        
		if(get.app && get.app === '1'){
			mailru.intercomType = 'parentCall';
		}
        
		if(get.login){
            if(!get.session_key && (!get.access_token && get.error)){
                try{
                    this._transmit({
                        event: 'connect.loginFail',
                        result: request,
                        relation: 'opener',
                        rt: 1
                    });
                } catch(e) {}
            } else {
                this._transmit({
                    event: 'connect.loginOAuth',
                    result: request,
                    relation: 'opener',
                    rt: 1
                });
            }
		} else if(get.modalWindow){
		    this._transmit({
					event: 'common.modalWindow',
					result: request,
					relation: 'pparent',
					rt: 1
			});
		} else if(get.set_widget){
		    this._transmit({
					event: 'app.widget.set',
					result: request,
					relation: 'parent',
					rt: 1
			});

		} else if(get.publish){
		    this._transmit({
					event: 'common.guestbookPublish',
					result: request,
					relation: 'parent',
					rt: 1
			});	
		} else if(get.streamPublish){
		    this._transmit({
					event: 'common.streamPublish',
					result: request,
					relation: 'parent',
					rt: 1
			});
		} else if(get.createAlbum){
		    this._transmit({
					event: 'common.createAlbum',
					result: request,
					relation: 'parent',
					rt: 1
			});
		} else if(get.uploadPhoto){
		    this._transmit({
					event: 'common.uploadPhoto',
					result: request,
					relation: 'parent',
					rt: 1
			});	
		} else if(get.permissionChanged){
		    this._transmit({
					event: 'common.permissionChanged',
					result: request,
					relation: 'parent',
					rt: 1
			});
		} else if(get.send_message){
		    this._transmit({
					event: 'common.sendMessage',
					result: request,
					relation: 'parent',
					rt: 1
			});
        } else if(get.addFriend){
		    this._transmit({
					event: 'common.friends.add',
					result: request,
					relation: 'parent',
					rt: 1
			});
		} else if(get.resource) {
			var url = {'api': '/platform/api', 'getLoginStatus': '/getLoginStatus'}[get.resource];
			if(url){
				this._req({
					get: get,
					url: url,
					cb: function(data){
						var parcel = {
							cbid: get.cbid,
							result: data,
							relation: 'parent'
						};
						if(get.resource=='getLoginStatus')
							parcel.rt = 1;
						mailru.proxy._transmit(parcel);
					}
				})
			} else if(get.resource == 'app'){
                document.domain = 'mail.ru';
				parent.parent.mailru.intercom.receiver(request);
			} 
		} 
	}
}



mailru.utils = mailru.utils || {
	uniqid: function(){
		return Math.round(Math.random(+new Date() + Math.random())*10000000);
	},
	parseGet: function (str){
		var p = str.split('&'), r = {}, di;
		for(var i=p.length; i--; ){
			di = p[i].indexOf('=');
			r[p[i].substr(0, di)] = decodeURIComponent(p[i].substr(di+1));
		}
		return r;
	},	
	makeGet: function(hash){
		var r = [];
		for(var k in hash){
			if(!hash.hasOwnProperty(k)) continue;
			r[r.length] = k+ '='+ encodeURIComponent(hash[k]);
		}
		return r.join('&');
	},
	addHandler: function(obj, name, cb){
		if (obj.addEventListener){
			obj.addEventListener(name, cb, false);
		} else if (obj.attachEvent){
			obj.attachEvent('on'+ name, cb);
		}
	}
}

mailru.loader && mailru.loader.onready('proxy');