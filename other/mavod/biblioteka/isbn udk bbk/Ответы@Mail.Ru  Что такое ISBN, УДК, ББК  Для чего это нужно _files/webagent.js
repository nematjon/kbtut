/*
    http://www.JSON.org/json2.js
    2011-10-19

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

window._JSON = (function () {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    function thisDateToJSON (key) {
        return isFinite(this.valueOf())
            ? this.getUTCFullYear()     + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate())      + 'T' +
                f(this.getUTCHours())     + ':' +
                f(this.getUTCMinutes())   + ':' +
                f(this.getUTCSeconds())   + 'Z'
            : null;
    }

    function thisValueOf (key) {
        return this.valueOf();
    }

    /* for better isolation
    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = thisDateToJSON;

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = thisValueOf;
    }
    */

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        /* for better isolation
        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }
        */

        if (value && typeof value === 'object') {
            if (value instanceof Date) {
                value = thisDateToJSON.call(value);
            } else if (value instanceof String || value instanceof Number || value instanceof Boolean) {
                value = thisValueOf.call(value);
            }
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0
                    ? '[]'
                    : gap
                    ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                    : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0
                ? '{}'
                : gap
                ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    /* for better isolation
    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {
    */
    // define public methods
    return {
        stringify: function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        },


// If the JSON object does not yet have a parse method, give it one.
    /* for better isolation
    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {
    */
        parse: function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function'
                    ? walk({'': j}, '')
                    : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        }
    }
}());

/**
 * easyXDM
 * http://easyxdm.net/
 * Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
(function (window, document, location, setTimeout, decodeURIComponent, encodeURIComponent) {
	// double inclusion safeguard
	if ("easyXDM" in window) {
		return;
	}/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, JSON, XMLHttpRequest, window, escape, unescape, ActiveXObject */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

var global = this;
var channelId = 0;
var emptyFn = Function.prototype;
var reURI = /^(http.?:\/\/([^\/\s]+))/; // returns groups for origin (1) and domain (2)
var reParent = /[\-\w]+\/\.\.\//; // matches a foo/../ expression 
var reDoubleSlash = /([^:])\/\//g; // matches // anywhere but in the protocol
var IFRAME_PREFIX = "easyXDM_";
var HAS_NAME_PROPERTY_BUG;


// http://peter.michaux.ca/articles/feature-detection-state-of-the-art-browser-scripting
function isHostMethod(object, property){
    var t = typeof object[property];
    return t == 'function' ||
    (!!(t == 'object' && object[property])) ||
    t == 'unknown';
}

function isHostObject(object, property){
    return !!(typeof(object[property]) == 'object' && object[property]);
}

// end

// http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/
function isArray(o){
    return Object.prototype.toString.call(o) === '[object Array]';
}

// end

/*
 * Cross Browser implementation for adding and removing event listeners.
 */
var on, un;
if (isHostMethod(window, "addEventListener")) {
    on = function(target, type, listener){
        target.addEventListener(type, listener, false);
    };
    un = function(target, type, listener){
        target.removeEventListener(type, listener, false);
    };
}
else if (isHostMethod(window, "attachEvent")) {
    on = function(object, sEvent, fpNotify){
        object.attachEvent("on" + sEvent, fpNotify);
    };
    un = function(object, sEvent, fpNotify){
        object.detachEvent("on" + sEvent, fpNotify);
    };
}
else {
    throw new Error("Browser not supported");
}

/*
 * Cross Browser implementation of DOMContentLoaded.
 */
var isReady = false, domReadyQueue = [];
if ("readyState" in document) {
    isReady = document.readyState == "complete";
}
else {
    // If readyState is not supported in the browser, then in order to be able to fire whenReady functions apropriately
    // when added dynamically _after_ DOM load, we have to deduce wether the DOM is ready or not.
    if (document.body) {
        // document.body is not available prior to the body being built
        // This does mean that we might fire it prematurely, but we only need the body element to be available for appending.
        isReady = true;
    }
}

function dom_onReady(){
    dom_onReady = emptyFn;
    isReady = true;
    for (var i = 0; i < domReadyQueue.length; i++) {
        domReadyQueue[i]();
    }
    domReadyQueue.length = 0;
}


if (!isReady) {
    if (isHostMethod(window, "addEventListener")) {
        on(document, "DOMContentLoaded", dom_onReady);
    }
    else {
        on(document, "readystatechange", function(){
            if (document.readyState == "complete") {
                dom_onReady();
            }
        });
        if (document.documentElement.doScroll && window === top) {
            (function doScrollCheck(){
                if (isReady) {
                    return;
                }
                // http://javascript.nwbox.com/IEContentLoaded/
                try {
                    document.documentElement.doScroll("left");
                } 
                catch (e) {
                    setTimeout(doScrollCheck, 1);
                    return;
                }
                dom_onReady();
            }());
        }
    }
    
    // A fallback to window.onload, that will always work
    on(window, "load", dom_onReady);
}
/**
 * This will add a function to the queue of functions to be run once the DOM reaches a ready state.
 * If functions are added after this event then they will be executed immediately.
 * @param {function} fn The function to add
 * @param {Object} scope An optional scope for the function to be called with.
 */
function whenReady(fn, scope){
    if (isReady) {
        fn.call(scope);
        return;
    }
    domReadyQueue.push(function(){
        fn.call(scope);
    });
}

/*
 * Methods for working with URLs
 */
/**
 * Get the domain name from a url.
 * @param {String} url The url to extract the domain from.
 * @returns The domain part of the url.
 * @type {String}
 */
function getDomainName(url){
    return url.match(reURI)[2];
}

/**
 * Returns  a string containing the schema, domain and if present the port
 * @param {String} url The url to extract the location from
 * @return {String} The location part of the url
 */
function getLocation(url){
    return url.match(reURI)[1];
}

/**
 * Resolves a relative url into an absolute one.
 * @param {String} url The path to resolve.
 * @return {String} The resolved url.
 */
function resolveUrl(url){
    
    // replace all // except the one in proto with /
    url = url.replace(reDoubleSlash, "$1/");
    
    // If the url is a valid url we do nothing
    if (!url.match(/^(http||https):\/\//)) {
        // If this is a relative path
        var path = (url.substring(0, 1) === "/") ? "" : location.pathname;
        if (path.substring(path.length - 1) !== "/") {
            path = path.substring(0, path.lastIndexOf("/") + 1);
        }
        
        url = location.protocol + "//" + location.host + path + url;
    }
    
    // reduce all 'xyz/../' to just '' 
    while (reParent.test(url)) {
        url = url.replace(reParent, "");
    }
    
    return url;
}

/**
 * Appends the parameters to the given url.<br/>
 * The base url can contain existing query parameters.
 * @param {String} url The base url.
 * @param {Object} parameters The parameters to add.
 * @return {String} A new valid url with the parameters appended.
 */
function appendQueryParameters(url, parameters){
    
    var hash = "", indexOf = url.indexOf("#");
    if (indexOf !== -1) {
        hash = url.substring(indexOf);
        url = url.substring(0, indexOf);
    }
    var q = [];
    for (var key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            q.push(key + "=" + encodeURIComponent(parameters[key]));
        }
    }
    return url + ((url.indexOf("?") === -1) ? "?" : "&") + q.join("&") + hash;
}

var query = (function(){
    var query = {}, pair, search = location.search.substring(1).split("&"), i = search.length;
    while (i--) {
        pair = search[i].split("=");
        try{
            query[pair[0]] = decodeURIComponent(pair[1]);
        }catch(e){}
    }
    return query;
}());

/*
 * Helper methods
 */
/**
 * Helper for checking if a variable/property is undefined
 * @param {Object} v The variable to test
 * @return {Boolean} True if the passed variable is undefined
 */
function undef(v){
    return typeof v === "undefined";
}

/**
 * A safe implementation of HTML5 JSON. Feature testing is used to make sure the implementation works.
 * @return {JSON} A valid JSON conforming object, or null if not found.
 */
function getJSON(){
    if (window.JSON && window.JSON.stringify(['a']) === '["a"]') {
        return window.JSON;
    } else {
        return window._JSON;
    }

    
    var cached = {};
    var obj = {
        a: [1, 2, 3]
    }, json = "{\"a\":[1,2,3]}";


    if (JSON && typeof JSON.stringify === "function" && JSON.stringify(obj).replace((/\s/g), "") === json) {
        // this is a working JSON instance
        return JSON;
    }
    if (Object.toJSON) {
        if (Object.toJSON(obj).replace((/\s/g), "") === json) {
            // this is a working stringify method
            cached.stringify = Object.toJSON;
        }
    }
    
    if (typeof String.prototype.evalJSON === "function") {
        obj = json.evalJSON();
        if (obj.a && obj.a.length === 3 && obj.a[2] === 3) {
            // this is a working parse method           
            cached.parse = function(str){
                return str.evalJSON();
            };
        }
    }
    
    if (cached.stringify && cached.parse) {
        // Only memoize the result if we have valid instance
        getJSON = function(){
            return cached;
        };
        return cached;
    }
    return null;
}

/**
 * Applies properties from the source object to the target object.<br/>
 * @param {Object} target The target of the properties.
 * @param {Object} source The source of the properties.
 * @param {Boolean} noOverwrite Set to True to only set non-existing properties.
 */
function apply(destination, source, noOverwrite){
    var member;
    for (var prop in source) {
        if (source.hasOwnProperty(prop)) {
            if (prop in destination) {
                member = source[prop];
                if (typeof member === "object") {
                    apply(destination[prop], member, noOverwrite);
                }
                else if (!noOverwrite) {
                    destination[prop] = source[prop];
                }
            }
            else {
                destination[prop] = source[prop];
            }
        }
    }
    return destination;
}

// This tests for the bug in IE where setting the [name] property using javascript causes the value to be redirected into [submitName].
function testForNamePropertyBug(){
    var el = document.createElement("iframe");
    el.name = "easyXDM_TEST";
    apply(el.style, {
        position: "absolute",
        left: "-2000px",
        top: "0px"
    });
    document.body.appendChild(el);
    HAS_NAME_PROPERTY_BUG = !(el.contentWindow === window.frames[el.name]);
    document.body.removeChild(el);
}

/**
 * Creates a frame and appends it to the DOM.
 * @param config {object} This object can have the following properties
 * <ul>
 * <li> {object} prop The properties that should be set on the frame. This should include the 'src' property.</li>
 * <li> {object} attr The attributes that should be set on the frame.</li>
 * <li> {DOMElement} container Its parent element (Optional).</li>
 * <li> {function} onLoad A method that should be called with the frames contentWindow as argument when the frame is fully loaded. (Optional)</li>
 * </ul>
 * @return The frames DOMElement
 * @type DOMElement
 */
function createFrame(config){
    if (undef(HAS_NAME_PROPERTY_BUG)) {
        testForNamePropertyBug();
    }
    var frame;
    // This is to work around the problems in IE6/7 with setting the name property. 
    // Internally this is set as 'submitName' instead when using 'iframe.name = ...'
    // This is not required by easyXDM itself, but is to facilitate other use cases 
    if (HAS_NAME_PROPERTY_BUG) {
        frame = document.createElement("<iframe name=\"" + config.props.name + "\"/>");
    }
    else {
        frame = document.createElement("IFRAME");
        frame.name = config.props.name;
    }
    
    frame.id = frame.name = config.props.name;
    delete config.props.name;
    
    if (config.onLoad) {
        on(frame, "load", config.onLoad);
    }
    
    if (typeof config.container == "string") {
        config.container = document.getElementById(config.container);
    }
    
    if (!config.container) {
        // This needs to be hidden like this, simply setting display:none and the like will cause failures in some browsers.
        frame.style.position = "absolute";
        frame.style.left = "-2000px";
        frame.style.top = "0px";
        config.container = document.body;
    }
    
    frame.border = frame.frameBorder = 0;
    config.container.insertBefore(frame, config.container.firstChild);
    
    // transfer properties to the frame
    apply(frame, config.props);
    return frame;
}

/**
 * Check whether a domain is allowed using an Access Control List.
 * The ACL can contain * and ? as wildcards, or can be regular expressions.
 * If regular expressions they need to begin with ^ and end with $.
 * @param {Array/String} acl The list of allowed domains
 * @param {String} domain The domain to test.
 * @return {Boolean} True if the domain is allowed, false if not.
 */
function checkAcl(acl, domain){
    // normalize into an array
    if (typeof acl == "string") {
        acl = [acl];
    }
    var re, i = acl.length;
    while (i--) {
        re = acl[i];
        re = new RegExp(re.substr(0, 1) == "^" ? re : ("^" + re.replace(/(\*)/g, ".$1").replace(/\?/g, ".") + "$"));
        if (re.test(domain)) {
            return true;
        }
    }
    return false;
}

/*
 * Functions related to stacks
 */
/**
 * Prepares an array of stack-elements suitable for the current configuration
 * @param {Object} config The Transports configuration. See easyXDM.Socket for more.
 * @return {Array} An array of stack-elements with the TransportElement at index 0.
 */
function prepareTransportStack(config){
    var protocol = config.protocol, stackEls;
    config.isHost = config.isHost || undef(query.xdm_p);
    
    if (!config.props) {
        config.props = {};
    }
    if (!config.isHost) {
        config.channel = query.xdm_c;
        config.secret = query.xdm_s;
        config.remote = query.xdm_e;
        protocol = query.xdm_p;
        if (config.acl && !checkAcl(config.acl, config.remote)) {
            throw new Error("Access denied for " + config.remote);
        }
    }
    else {
        config.remote = resolveUrl(config.remote);
        config.channel = config.channel || "default" + channelId++;
        config.secret = Math.random().toString(16).substring(2);
        if (undef(protocol)) {
            if (getLocation(location.href) == getLocation(config.remote)) {
                /*
                 * Both documents has the same origin, lets use direct access.
                 */
                protocol = "4";
            }
            else if (isHostMethod(window, "postMessage") || isHostMethod(document, "postMessage")) {
                /*
                 * This is supported in IE8+, Firefox 3+, Opera 9+, Chrome 2+ and Safari 4+
                 */
                protocol = "1";
            }
            else if (isHostMethod(window, "ActiveXObject") && isHostMethod(window, "execScript")) {
                /*
                 * This is supported in IE6 and IE7
                 */
                protocol = "3";
            }
            else if (navigator.product === "Gecko" && "frameElement" in window && navigator.userAgent.indexOf('WebKit') == -1) {
                /*
                 * This is supported in Gecko (Firefox 1+)
                 */
                protocol = "5";
            }
            else if (config.remoteHelper) {
                /*
                 * This is supported in all browsers that retains the value of window.name when
                 * navigating from one domain to another, and where parent.frames[foo] can be used
                 * to get access to a frame from the same domain
                 */
                config.remoteHelper = resolveUrl(config.remoteHelper);
                protocol = "2";
            }
            else {
                /*
                 * This is supported in all browsers where [window].location is writable for all
                 * The resize event will be used if resize is supported and the iframe is not put
                 * into a container, else polling will be used.
                 */
                protocol = "0";
            }
        }
    }
    
    switch (protocol) {
        case "0":// 0 = HashTransport
            apply(config, {
                interval: 100,
                delay: 2000,
                useResize: true,
                useParent: false,
                usePolling: false
            }, true);
            if (config.isHost) {
                if (!config.local) {
                    // If no local is set then we need to find an image hosted on the current domain
                    var domain = location.protocol + "//" + location.host, images = document.body.getElementsByTagName("img"), image;
                    var i = images.length;
                    while (i--) {
                        image = images[i];
                        if (image.src.substring(0, domain.length) === domain) {
                            config.local = image.src;
                            break;
                        }
                    }
                    if (!config.local) {
                        // If no local was set, and we are unable to find a suitable file, then we resort to using the current window 
                        config.local = window;
                    }
                }
                
                var parameters = {
                    xdm_c: config.channel,
                    xdm_p: 0
                };
                
                if (config.local === window) {
                    // We are using the current window to listen to
                    config.usePolling = true;
                    config.useParent = true;
                    config.local = location.protocol + "//" + location.host + location.pathname + location.search;
                    parameters.xdm_e = config.local;
                    parameters.xdm_pa = 1; // use parent
                }
                else {
                    parameters.xdm_e = resolveUrl(config.local);
                }
                
                if (config.container) {
                    config.useResize = false;
                    parameters.xdm_po = 1; // use polling
                }
                config.remote = appendQueryParameters(config.remote, parameters);
            }
            else {
                apply(config, {
                    channel: query.xdm_c,
                    remote: query.xdm_e,
                    useParent: !undef(query.xdm_pa),
                    usePolling: !undef(query.xdm_po),
                    useResize: config.useParent ? false : config.useResize
                });
            }
            stackEls = [new easyXDM.stack.HashTransport(config), new easyXDM.stack.ReliableBehavior({}), new easyXDM.stack.QueueBehavior({
                encode: true,
                maxLength: 4000 - config.remote.length
            }), new easyXDM.stack.VerifyBehavior({
                initiate: config.isHost
            })];
            break;
        case "1":
            stackEls = [new easyXDM.stack.PostMessageTransport(config)];
            break;
        case "2":
            stackEls = [new easyXDM.stack.NameTransport(config), new easyXDM.stack.QueueBehavior(), new easyXDM.stack.VerifyBehavior({
                initiate: config.isHost
            })];
            break;
        case "3":
            stackEls = [new easyXDM.stack.NixTransport(config)];
            break;
        case "4":
            stackEls = [new easyXDM.stack.SameOriginTransport(config)];
            break;
        case "5":
            stackEls = [new easyXDM.stack.FrameElementTransport(config)];
            break;
    }
    // this behavior is responsible for buffering outgoing messages, and for performing lazy initialization
    stackEls.push(new easyXDM.stack.QueueBehavior({
        lazy: config.lazy,
        remove: true
    }));
    return stackEls;
}

/**
 * Chains all the separate stack elements into a single usable stack.<br/>
 * If an element is missing a necessary method then it will have a pass-through method applied.
 * @param {Array} stackElements An array of stack elements to be linked.
 * @return {easyXDM.stack.StackElement} The last element in the chain.
 */
function chainStack(stackElements){
    var stackEl, defaults = {
        incoming: function(message, origin){
            this.up.incoming(message, origin);
        },
        outgoing: function(message, recipient){
            this.down.outgoing(message, recipient);
        },
        callback: function(success){
            this.up.callback(success);
        },
        init: function(){
            this.down.init();
        },
        destroy: function(){
            this.down.destroy();
        }
    };
    for (var i = 0, len = stackElements.length; i < len; i++) {
        stackEl = stackElements[i];
        apply(stackEl, defaults, true);
        if (i !== 0) {
            stackEl.down = stackElements[i - 1];
        }
        if (i !== len - 1) {
            stackEl.up = stackElements[i + 1];
        }
    }
    return stackEl;
}

/**
 * This will remove a stackelement from its stack while leaving the stack functional.
 * @param {Object} element The elment to remove from the stack.
 */
function removeFromStack(element){
    element.up.down = element.down;
    element.down.up = element.up;
    element.up = element.down = null;
}

/*
 * Export the main object and any other methods applicable
 */
/** 
 * @class easyXDM
 * A javascript library providing cross-browser, cross-domain messaging/RPC.
 * @version 2.4.9.102
 * @singleton
 */
global.easyXDM = {
    /**
     * The version of the library
     * @type {string}
     */
    version: "2.4.9.102",
    /**
     * This is a map containing all the query parameters passed to the document.
     * All the values has been decoded using decodeURIComponent.
     * @type {object}
     */
    query: query,
    /**
     * @private
     */
    stack: {},
    /**
     * Applies properties from the source object to the target object.<br/>
     * @param {object} target The target of the properties.
     * @param {object} source The source of the properties.
     * @param {boolean} noOverwrite Set to True to only set non-existing properties.
     */
    apply: apply,
    
    /**
     * A safe implementation of HTML5 JSON. Feature testing is used to make sure the implementation works.
     * @return {JSON} A valid JSON conforming object, or null if not found.
     */
    getJSONObject: getJSON,
    /**
     * This will add a function to the queue of functions to be run once the DOM reaches a ready state.
     * If functions are added after this event then they will be executed immediately.
     * @param {function} fn The function to add
     * @param {object} scope An optional scope for the function to be called with.
     */
    whenReady: whenReady
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global console, _FirebugCommandLine,  easyXDM, window, escape, unescape, isHostObject, undef, _trace, isReady, emptyFn */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, isHostObject, isHostMethod, un, on, createFrame, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/** 
 * @class easyXDM.DomHelper
 * Contains methods for dealing with the DOM
 * @singleton
 */
easyXDM.DomHelper = {
    /**
     * Provides a consistent interface for adding eventhandlers
     * @param {Object} target The target to add the event to
     * @param {String} type The name of the event
     * @param {Function} listener The listener
     */
    on: on,
    /**
     * Provides a consistent interface for removing eventhandlers
     * @param {Object} target The target to remove the event from
     * @param {String} type The name of the event
     * @param {Function} listener The listener
     */
    un: un,
    /**
     * Checks for the presence of the JSON object.
     * If it is not present it will use the supplied path to load the JSON2 library.
     * This should be called in the documents head right after the easyXDM script tag.
     * http://json.org/json2.js
     * @param {String} path A valid path to json2.js
     */
    requiresJSON: function(path){
        if (!isHostObject(window, "JSON")) {
            document.write('<script type="text/javascript" src="' + path + '"></script>');
        }
    }
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

(function(){
    // The map containing the stored functions
    var _map = {};
    
    /**
     * @class easyXDM.Fn
     * This contains methods related to function handling, such as storing callbacks.
     * @singleton
     * @namespace easyXDM
     */
    easyXDM.Fn = {
        /**
         * Stores a function using the given name for reference
         * @param {String} name The name that the function should be referred by
         * @param {Function} fn The function to store
         * @namespace easyXDM.fn
         */
        set: function(name, fn){
            _map[name] = fn;
        },
        /**
         * Retrieves the function referred to by the given name
         * @param {String} name The name of the function to retrieve
         * @param {Boolean} del If the function should be deleted after retrieval
         * @return {Function} The stored function
         * @namespace easyXDM.fn
         */
        get: function(name, del){
            var fn = _map[name];
            
            if (del) {
                delete _map[name];
            }
            return fn;
        }
    };
    
}());
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, chainStack, prepareTransportStack, getLocation, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.Socket
 * This class creates a transport channel between two domains that is usable for sending and receiving string-based messages.<br/>
 * The channel is reliable, supports queueing, and ensures that the message originates from the expected domain.<br/>
 * Internally different stacks will be used depending on the browsers features and the available parameters.
 * <h2>How to set up</h2>
 * Setting up the provider:
 * <pre><code>
 * var socket = new easyXDM.Socket({
 * &nbsp; local: "name.html",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
 * &nbsp; &nbsp; socket.postMessage("foo-message");
 * &nbsp; },
 * &nbsp; onMessage: function(message, origin) {
 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
 * &nbsp; }
 * });
 * </code></pre>
 * Setting up the consumer:
 * <pre><code>
 * var socket = new easyXDM.Socket({
 * &nbsp; remote: "http:&#47;&#47;remotedomain/page.html",
 * &nbsp; remoteHelper: "http:&#47;&#47;remotedomain/name.html",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
 * &nbsp; &nbsp; socket.postMessage("foo-message");
 * &nbsp; },
 * &nbsp; onMessage: function(message, origin) {
 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
 * &nbsp; }
 * });
 * </code></pre>
 * If you are unable to upload the <code>name.html</code> file to the consumers domain then remove the <code>remoteHelper</code> property
 * and easyXDM will fall back to using the HashTransport instead of the NameTransport when not able to use any of the primary transports.
 * @namespace easyXDM
 * @constructor
 * @cfg {String/Window} local The url to the local name.html document, a local static file, or a reference to the local window.
 * @cfg {Boolean} lazy (Consumer only) Set this to true if you want easyXDM to defer creating the transport until really needed. 
 * @cfg {String} remote (Consumer only) The url to the providers document.
 * @cfg {String} remoteHelper (Consumer only) The url to the remote name.html file. This is to support NameTransport as a fallback. Optional.
 * @cfg {Number} delay The number of milliseconds easyXDM should try to get a reference to the local window.  Optional, defaults to 2000.
 * @cfg {Number} interval The interval used when polling for messages. Optional, defaults to 300.
 * @cfg {String} channel (Consumer only) The name of the channel to use. Can be used to set consistent iframe names. Must be unique. Optional.
 * @cfg {Function} onMessage The method that should handle incoming messages.<br/> This method should accept two arguments, the message as a string, and the origin as a string. Optional.
 * @cfg {Function} onReady A method that should be called when the transport is ready. Optional.
 * @cfg {DOMElement|String} container (Consumer only) The element, or the id of the element that the primary iframe should be inserted into. If not set then the iframe will be positioned off-screen. Optional.
 * @cfg {Array/String} acl (Provider only) Here you can specify which '[protocol]://[domain]' patterns that should be allowed to act as the consumer towards this provider.<br/>
 * This can contain the wildcards ? and *.  Examples are 'http://example.com', '*.foo.com' and '*dom?.com'. If you want to use reqular expressions then you pattern needs to start with ^ and end with $.
 * If none of the patterns match an Error will be thrown.  
 * @cfg {Object} props (Consumer only) Additional properties that should be applied to the iframe. This can also contain nested objects e.g: <code>{style:{width:"100px", height:"100px"}}</code>. 
 * Properties such as 'name' and 'src' will be overrided. Optional.
 */
easyXDM.Socket = function(config){
    
    // create the stack
    var stack = chainStack(prepareTransportStack(config).concat([{
        incoming: function(message, origin){
            config.onMessage(message, origin);
        },
        callback: function(success){
            if (config.onReady) {
                config.onReady(success);
            }
        }
    }])), recipient = getLocation(config.remote);
    
    // set the origin
    this.origin = getLocation(config.remote);
	
    /**
     * Initiates the destruction of the stack.
     */
    this.destroy = function(){
        stack.destroy();
    };
    
    /**
     * Posts a message to the remote end of the channel
     * @param {String} message The message to send
     */
    this.postMessage = function(message){
        stack.outgoing(message, recipient);
    };
    
    stack.init();
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef,, chainStack, prepareTransportStack, debug, getLocation */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/** 
 * @class easyXDM.Rpc
 * Creates a proxy object that can be used to call methods implemented on the remote end of the channel, and also to provide the implementation
 * of methods to be called from the remote end.<br/>
 * The instantiated object will have methods matching those specified in <code>config.remote</code>.<br/>
 * This requires the JSON object present in the document, either natively, using json.org's json2 or as a wrapper around library spesific methods.
 * <h2>How to set up</h2>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; &#47;&#47; this configuration is equal to that used by the Socket.
 * &nbsp; remote: "http:&#47;&#47;remotedomain/...",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the proxy
 * &nbsp; &nbsp; rpc.foo(...
 * &nbsp; }
 * },{
 * &nbsp; local: {..},
 * &nbsp; remote: {..}
 * });
 * </code></pre>
 * 
 * <h2>Exposing functions (procedures)</h2>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {
 * &nbsp; &nbsp; nameOfMethod: {
 * &nbsp; &nbsp; &nbsp; method: function(arg1, arg2, success, error){
 * &nbsp; &nbsp; &nbsp; &nbsp; ...
 * &nbsp; &nbsp; &nbsp; }
 * &nbsp; &nbsp; },
 * &nbsp; &nbsp; &#47;&#47; with shorthand notation 
 * &nbsp; &nbsp; nameOfAnotherMethod:  function(arg1, arg2, success, error){
 * &nbsp; &nbsp; }
 * &nbsp; },
 * &nbsp; remote: {...}
 * });
 * </code></pre>

 * The function referenced by  [method] will receive the passed arguments followed by the callback functions <code>success</code> and <code>error</code>.<br/>
 * To send a successfull result back you can use
 *     <pre><code>
 *     return foo;
 *     </pre></code>
 * or
 *     <pre><code>
 *     success(foo);
 *     </pre></code>
 *  To return an error you can use
 *     <pre><code>
 *     throw new Error("foo error");
 *     </code></pre>
 * or
 *     <pre><code>
 *     error("foo error");
 *     </code></pre>
 *
 * <h2>Defining remotely exposed methods (procedures/notifications)</h2>
 * The definition of the remote end is quite similar:
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {...},
 * &nbsp; remote: {
 * &nbsp; &nbsp; nameOfMethod: {}
 * &nbsp; }
 * });
 * </code></pre>
 * To call a remote method use
 * <pre><code>
 * rpc.nameOfMethod("arg1", "arg2", function(value) {
 * &nbsp; alert("success: " + value);
 * }, function(message) {
 * &nbsp; alert("error: " + message + );
 * });
 * </code></pre>
 * Both the <code>success</code> and <code>errror</code> callbacks are optional.<br/>
 * When called with no callback a JSON-RPC 2.0 notification will be executed.
 * Be aware that you will not be notified of any errors with this method.
 * <br/>
 * <h2>Specifying a custom serializer</h2>
 * If you do not want to use the JSON2 library for non-native JSON support, but instead capabilities provided by some other library
 * then you can specify a custom serializer using <code>serializer: foo</code>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {...},
 * &nbsp; remote: {...},
 * &nbsp; serializer : {
 * &nbsp; &nbsp; parse: function(string){ ... },
 * &nbsp; &nbsp; stringify: function(object) {...}
 * &nbsp; }
 * });
 * </code></pre>
 * If <code>serializer</code> is set then the class will not attempt to use the native implementation.
 * @namespace easyXDM
 * @constructor
 * @param {Object} config The underlying transports configuration. See easyXDM.Socket for available parameters.
 * @param {Object} jsonRpcConfig The description of the interface to implement.
 */
easyXDM.Rpc = function(config, jsonRpcConfig){
    
    // expand shorthand notation
    if (jsonRpcConfig.local) {
        for (var method in jsonRpcConfig.local) {
            if (jsonRpcConfig.local.hasOwnProperty(method)) {
                var member = jsonRpcConfig.local[method];
                if (typeof member === "function") {
                    jsonRpcConfig.local[method] = {
                        method: member
                    };
                }
            }
        }
    }
	
    // create the stack
    var stack = chainStack(prepareTransportStack(config).concat([new easyXDM.stack.RpcBehavior(this, jsonRpcConfig), {
        callback: function(success){
            if (config.onReady) {
                config.onReady(success);
            }
        }
    }]));
	
    // set the origin 
    this.origin = getLocation(config.remote);
	
    
    /**
     * Initiates the destruction of the stack.
     */
    this.destroy = function(){
        stack.destroy();
    };
    
    stack.init();
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.SameOriginTransport
 * SameOriginTransport is a transport class that can be used when both domains have the same origin.<br/>
 * This can be useful for testing and for when the main application supports both internal and external sources.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote document to communicate with.
 */
easyXDM.stack.SameOriginTransport = function(config){
    var pub, frame, send, targetOrigin;
    
    return (pub = {
        outgoing: function(message, domain, fn){
            send(message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            
            if (config.isHost) {
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname,
                        xdm_c: config.channel,
                        xdm_p: 4 // 4 = SameOriginTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                easyXDM.Fn.set(config.channel, function(sendFn){
                    send = sendFn;
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                    return function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    };
                });
            }
            else {
                send = parent.easyXDM.Fn.get(config.channel, true)(function(msg){
                    pub.up.incoming(msg, targetOrigin);
                });
                setTimeout(function(){
                    pub.up.callback(true);
                }, 0);
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.PostMessageTransport
 * PostMessageTransport is a transport class that uses HTML5 postMessage for communication.<br/>
 * <a href="http://msdn.microsoft.com/en-us/library/ms644944(VS.85).aspx">http://msdn.microsoft.com/en-us/library/ms644944(VS.85).aspx</a><br/>
 * <a href="https://developer.mozilla.org/en/DOM/window.postMessage">https://developer.mozilla.org/en/DOM/window.postMessage</a>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote domain to communicate with.
 */
easyXDM.stack.PostMessageTransport = function(config){
    var pub, // the public interface
 frame, // the remote frame, if any
 callerWindow, // the window that we will call with
 targetOrigin; // the domain to communicate with
    /**
     * Resolves the origin from the event object
     * @private
     * @param {Object} event The messageevent
     * @return {String} The scheme, host and port of the origin
     */
    function _getOrigin(event){
        if (event.origin) {
            // This is the HTML5 property
            return event.origin;
        }
        if (event.uri) {
            // From earlier implementations 
            return getLocation(event.uri);
        }
        if (event.domain) {
            // This is the last option and will fail if the 
            // origin is not using the same schema as we are
            return location.protocol + "//" + event.domain;
        }
        throw "Unable to retrieve the origin of the event";
    }
    
    /**
     * This is the main implementation for the onMessage event.<br/>
     * It checks the validity of the origin and passes the message on if appropriate.
     * @private
     * @param {Object} event The messageevent
     */
    function _window_onMessage(event){
        var origin = _getOrigin(event);
        if (origin == targetOrigin && event.data.substring(0, config.channel.length + 1) == config.channel + " ") {
            var cb = function () {
                pub.up.incoming(event.data.substring(config.channel.length + 1), origin);
            };
            window.WebAgent && WebAgent.util.Mnt.wrapTryCatch(cb) || cb();
        }
    }
    
    return (pub = {
        outgoing: function(message, domain, fn){
            callerWindow.postMessage(config.channel + " " + message, domain || targetOrigin);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            un(window, "message", _window_onMessage);
            if (frame) {
                callerWindow = null;
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            if (config.isHost) {
                // add the event handler for listening
                on(window, "message", function waitForReady(event){
                    if (event.data == config.channel + "-ready") {
                        // replace the eventlistener
                        callerWindow = ("postMessage" in frame.contentWindow) ? frame.contentWindow : frame.contentWindow.document;
                        un(window, "message", waitForReady);
                        on(window, "message", _window_onMessage);
                        setTimeout(function(){
                            pub.up.callback(true);
                        }, 0);
                    }
                });
                
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host,
                        xdm_c: config.channel,
                        xdm_p: 1 // 1 = PostMessage
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
            }
            else {
                // add the event handler for listening
                on(window, "message", _window_onMessage);
                callerWindow = ("postMessage" in window.parent) ? window.parent : window.parent.document;
                callerWindow.postMessage(config.channel + "-ready", targetOrigin);
                
                setTimeout(function(){
                    pub.up.callback(true);
                }, 0);
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, apply, query, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.FrameElementTransport
 * FrameElementTransport is a transport class that can be used with Gecko-browser as these allow passing variables using the frameElement property.<br/>
 * Security is maintained as Gecho uses Lexical Authorization to determine under which scope a function is running.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote document to communicate with.
 */
easyXDM.stack.FrameElementTransport = function(config){
    var pub, frame, send, targetOrigin;
    
    return (pub = {
        outgoing: function(message, domain, fn){
            send.call(this, message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            
            if (config.isHost) {
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname + location.search,
                        xdm_c: config.channel,
                        xdm_p: 5 // 5 = FrameElementTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                frame.fn = function(sendFn){
                    delete frame.fn;
                    send = sendFn;
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                    // remove the function so that it cannot be used to overwrite the send function later on
                    return function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    };
                };
            }
            else {
                if (document.referrer && document.referrer != query.xdm_e) {
                    window.parent.location = query.xdm_e;
                }
                else {
                    if (document.referrer != query.xdm_e) {
                        // This is to mitigate origin-spoofing
                        window.parent.location = query.xdm_e;
                    }
                    send = window.frameElement.fn(function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    });
                    pub.up.callback(true);
                }
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global global, getNixProxy, easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, isHostMethod, apply, query, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.NixTransport
 * NixTransport is a transport class that uses the strange fact that in IE <8, the window.opener property can be written to and read from all windows.<br/>
 * This is used to pass methods that are able to relay messages back and forth. To avoid context-leakage a VBScript (COM) object is used to relay all the strings.<br/>
 * This transport is loosely based on the work done by <a href="https://issues.apache.org/jira/browse/SHINDIG-416">Shindig</a>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote domain to communicate with.
 * @cfg {String} secret the pre-shared secret used to secure the communication.
 */
easyXDM.stack.NixTransport = function(config){
    var pub, // the public interface
 frame, send, targetOrigin, proxy;
    
    return (pub = {
        outgoing: function(message, domain, fn){
            send(message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            proxy = null;
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            if (config.isHost) {
                try {
                    if (!isHostMethod(window, "getNixProxy")) {
                        window.execScript('Class NixProxy\n' +
                        '    Private m_parent, m_child, m_Auth\n' +
                        '\n' +
                        '    Public Sub SetParent(obj, auth)\n' +
                        '        If isEmpty(m_Auth) Then m_Auth = auth\n' +
                        '        SET m_parent = obj\n' +
                        '    End Sub\n' +
                        '    Public Sub SetChild(obj)\n' +
                        '        SET m_child = obj\n' +
                        '        m_parent.ready()\n' +
                        '    End Sub\n' +
                        '\n' +
                        // The auth string, which is a pre-shared key between the parent and the child, 
                        // and that can only be set once by the parent, secures the communication, and also serves to provide
                        // 'proof' of the origin of the messages.
                        // Before passing the message on to the recipent we convert the message into a primitive, 
                        // this mitigates modifying .toString as an attack vector.
                        '    Public Sub SendToParent(data, auth)\n' +
                        '        If m_Auth = auth Then m_parent.send(CStr(data))\n' +
                        '    End Sub\n' +
                        '    Public Sub SendToChild(data, auth)\n' +
                        '        If m_Auth = auth Then m_child.send(CStr(data))\n' +
                        '    End Sub\n' +
                        'End Class\n' +
                        'Function getNixProxy()\n' +
                        '    Set GetNixProxy = New NixProxy\n' +
                        'End Function\n', 'vbscript');
                    }
                    proxy = getNixProxy();
                    proxy.SetParent({
                        send: function(msg){
                            pub.up.incoming(msg, targetOrigin);
                        },
                        ready: function(){
                            setTimeout(function(){
                                pub.up.callback(true);
                            }, 0);
                        }
                    }, config.secret);
                    send = function(msg){
                        proxy.SendToChild(msg, config.secret);
                    };
                } 
                catch (e1) {
                    throw new Error("Could not set up VBScript NixProxy:" + e1.message);
                }
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname + location.search,
                        xdm_c: config.channel,
                        xdm_s: config.secret,
                        xdm_p: 3 // 3 = NixTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                frame.contentWindow.opener = proxy;
            }
            else {
                if (document.referrer && document.referrer != query.xdm_e) {
                    window.parent.location = query.xdm_e;
                }
                else {
                    if (document.referrer != query.xdm_e) {
                        // This is to mitigate origin-spoofing
                        window.parent.location = query.xdm_e;
                    }
                    try {
                        // by storing this in a variable we negate replacement attacks
                        proxy = window.opener;
                    } 
                    catch (e2) {
                        throw new Error("Cannot access window.opener");
                    }
                    proxy.SetChild({
                        send: function(msg){
                            // the timeout is necessary to have execution continue in the correct context
                            global.setTimeout(function(){
                                pub.up.incoming(msg, targetOrigin);
                            }, 0);
                        }
                    });
                    
                    send = function(msg){
                        proxy.SendToParent(msg, config.secret);
                    };
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                }
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, getLocation, appendQueryParameters, resolveUrl, createFrame, debug, un, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.NameTransport
 * NameTransport uses the window.name property to relay data.
 * The <code>local</code> parameter needs to be set on both the consumer and provider,<br/>
 * and the <code>remoteHelper</code> parameter needs to be set on the consumer.
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remoteHelper The url to the remote instance of hash.html - this is only needed for the host.
 * @namespace easyXDM.stack
 */
easyXDM.stack.NameTransport = function(config){
    
    var pub; // the public interface
    var isHost, callerWindow, remoteWindow, readyCount, callback, remoteOrigin, remoteUrl;
    
    function _sendMessage(message){
        var url = config.remoteHelper + (isHost ? "#_3" : "#_2") + config.channel;
        callerWindow.contentWindow.sendMessage(message, url);
    }
    
    function _onReady(){
        if (isHost) {
            if (++readyCount === 2 || !isHost) {
                pub.up.callback(true);
            }
        }
        else {
            _sendMessage("ready");
            pub.up.callback(true);
        }
    }
    
    function _onMessage(message){
        pub.up.incoming(message, remoteOrigin);
    }
    
    function _onLoad(){
        if (callback) {
            setTimeout(function(){
                callback(true);
            }, 0);
        }
    }
    
    return (pub = {
        outgoing: function(message, domain, fn){
            callback = fn;
            _sendMessage(message);
        },
        destroy: function(){
            callerWindow.parentNode.removeChild(callerWindow);
            callerWindow = null;
            if (isHost) {
                remoteWindow.parentNode.removeChild(remoteWindow);
                remoteWindow = null;
            }
        },
        onDOMReady: function(){
            isHost = config.isHost;
            readyCount = 0;
            remoteOrigin = getLocation(config.remote);
            config.local = resolveUrl(config.local);
            
            if (isHost) {
                // Register the callback
                easyXDM.Fn.set(config.channel, function(message){
                    if (isHost && message === "ready") {
                        // Replace the handler
                        easyXDM.Fn.set(config.channel, _onMessage);
                        _onReady();
                    }
                });
                
                // Set up the frame that points to the remote instance
                remoteUrl = appendQueryParameters(config.remote, {
                    xdm_e: config.local,
                    xdm_c: config.channel,
                    xdm_p: 2
                });
                apply(config.props, {
                    src: remoteUrl + '#' + config.channel,
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                remoteWindow = createFrame(config);
            }
            else {
                config.remoteHelper = config.remote;
                easyXDM.Fn.set(config.channel, _onMessage);
            }
            // Set up the iframe that will be used for the transport
            
            callerWindow = createFrame({
                props: {
                    src: config.local + "#_4" + config.channel
                },
                onLoad: function onLoad(){
                    // Remove the handler
                    un(callerWindow, "load", onLoad);
                    easyXDM.Fn.set(config.channel + "_load", _onLoad);
                    (function test(){
                        if (typeof callerWindow.contentWindow.sendMessage == "function") {
                            _onReady();
                        }
                        else {
                            setTimeout(test, 50);
                        }
                    }());
                }
            });
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.HashTransport
 * HashTransport is a transport class that uses the IFrame URL Technique for communication.<br/>
 * <a href="http://msdn.microsoft.com/en-us/library/bb735305.aspx">http://msdn.microsoft.com/en-us/library/bb735305.aspx</a><br/>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String/Window} local The url to the local file used for proxying messages, or the local window.
 * @cfg {Number} delay The number of milliseconds easyXDM should try to get a reference to the local window.
 * @cfg {Number} interval The interval used when polling for messages.
 */
easyXDM.stack.HashTransport = function(config){
    var pub;
    var me = this, isHost, _timer, pollInterval, _lastMsg, _msgNr, _listenerWindow, _callerWindow;
    var useParent, _remoteOrigin;
    
    function _sendMessage(message){
        if (!_callerWindow) {
            return;
        }
        var url = config.remote + "#" + (_msgNr++) + "_" + message;
        ((isHost || !useParent) ? _callerWindow.contentWindow : _callerWindow).location = url;
    }
    
    function _handleHash(hash){
        _lastMsg = hash;
        pub.up.incoming(_lastMsg.substring(_lastMsg.indexOf("_") + 1), _remoteOrigin);
    }
    
    /**
     * Checks location.hash for a new message and relays this to the receiver.
     * @private
     */
    function _pollHash(){
        if (!_listenerWindow) {
            return;
        }
        var href = _listenerWindow.location.href, hash = "", indexOf = href.indexOf("#");
        if (indexOf != -1) {
            hash = href.substring(indexOf);
        }
        if (hash && hash != _lastMsg) {
            _handleHash(hash);
        }
    }
    
    function _attachListeners(){
        _timer = setInterval(_pollHash, pollInterval);
    }
    
    return (pub = {
        outgoing: function(message, domain){
            _sendMessage(message);
        },
        destroy: function(){
            window.clearInterval(_timer);
            if (isHost || !useParent) {
                _callerWindow.parentNode.removeChild(_callerWindow);
            }
            _callerWindow = null;
        },
        onDOMReady: function(){
            isHost = config.isHost;
            pollInterval = config.interval;
            _lastMsg = "#" + config.channel;
            _msgNr = 0;
            useParent = config.useParent;
            _remoteOrigin = getLocation(config.remote);
            if (isHost) {
                config.props = {
                    src: config.remote,
                    name: IFRAME_PREFIX + config.channel + "_provider"
                };
                if (useParent) {
                    config.onLoad = function(){
                        _listenerWindow = window;
                        _attachListeners();
                        pub.up.callback(true);
                    };
                }
                else {
                    var tries = 0, max = config.delay / 50;
                    (function getRef(){
                        if (++tries > max) {
                            throw new Error("Unable to reference listenerwindow");
                        }
                        try {
                            _listenerWindow = _callerWindow.contentWindow.frames[IFRAME_PREFIX + config.channel + "_consumer"];
                        } 
                        catch (ex) {
                        }
                        if (_listenerWindow) {
                            _attachListeners();
                            pub.up.callback(true);
                        }
                        else {
                            setTimeout(getRef, 50);
                        }
                    }());
                }
                _callerWindow = createFrame(config);
            }
            else {
                _listenerWindow = window;
                _attachListeners();
                if (useParent) {
                    _callerWindow = parent;
                    pub.up.callback(true);
                }
                else {
                    apply(config, {
                        props: {
                            src: config.remote + "#" + config.channel + new Date(),
                            name: IFRAME_PREFIX + config.channel + "_consumer"
                        },
                        onLoad: function(){
                            pub.up.callback(true);
                        }
                    });
                    _callerWindow = createFrame(config);
                }
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.ReliableBehavior
 * This is a behavior that tries to make the underlying transport reliable by using acknowledgements.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration.
 */
easyXDM.stack.ReliableBehavior = function(config){
    var pub, // the public interface
 callback; // the callback to execute when we have a confirmed success/failure
    var idOut = 0, idIn = 0, currentMessage = "";
    
    return (pub = {
        incoming: function(message, origin){
            var indexOf = message.indexOf("_"), ack = message.substring(0, indexOf).split(",");
            message = message.substring(indexOf + 1);
            
            if (ack[0] == idOut) {
                currentMessage = "";
                if (callback) {
                    callback(true);
                }
            }
            if (message.length > 0) {
                pub.down.outgoing(ack[1] + "," + idOut + "_" + currentMessage, origin);
                if (idIn != ack[1]) {
                    idIn = ack[1];
                    pub.up.incoming(message, origin);
                }
            }
            
        },
        outgoing: function(message, origin, fn){
            currentMessage = message;
            callback = fn;
            pub.down.outgoing(idIn + "," + (++idOut) + "_" + message, origin);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug, undef, removeFromStack*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.QueueBehavior
 * This is a behavior that enables queueing of messages. <br/>
 * It will buffer incoming messages and dispach these as fast as the underlying transport allows.
 * This will also fragment/defragment messages so that the outgoing message is never bigger than the
 * set length.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration. Optional.
 * @cfg {Number} maxLength The maximum length of each outgoing message. Set this to enable fragmentation.
 */
easyXDM.stack.QueueBehavior = function(config){
    var pub, queue = [], waiting = true, incoming = "", destroying, maxLength = 0, lazy = false, doFragment = false;
    
    function dispatch(){
        if (config.remove && queue.length === 0) {
            removeFromStack(pub);
            return;
        }
        if (waiting || queue.length === 0 || destroying) {
            return;
        }
        waiting = true;
        var message = queue.shift();
        
        pub.down.outgoing(message.data, message.origin, function(success){
            waiting = false;
            if (message.callback) {
                setTimeout(function(){
                    message.callback(success);
                }, 0);
            }
            dispatch();
        });
    }
    return (pub = {
        init: function(){
            if (undef(config)) {
                config = {};
            }
            if (config.maxLength) {
                maxLength = config.maxLength;
                doFragment = true;
            }
            if (config.lazy) {
                lazy = true;
            }
            else {
                pub.down.init();
            }
        },
        callback: function(success){
            waiting = false;
            var up = pub.up; // in case dispatch calls removeFromStack
            dispatch();
            up.callback(success);
        },
        incoming: function(message, origin){
            if (doFragment) {
                var indexOf = message.indexOf("_"), seq = parseInt(message.substring(0, indexOf), 10);
                incoming += message.substring(indexOf + 1);
                if (seq === 0) {
                    if (config.encode) {
                        incoming = decodeURIComponent(incoming);
                    }
                    pub.up.incoming(incoming, origin);
                    incoming = "";
                }
            }
            else {
                pub.up.incoming(message, origin);
            }
        },
        outgoing: function(message, origin, fn){
            if (config.encode) {
                message = encodeURIComponent(message);
            }
            var fragments = [], fragment;
            if (doFragment) {
                // fragment into chunks
                while (message.length !== 0) {
                    fragment = message.substring(0, maxLength);
                    message = message.substring(fragment.length);
                    fragments.push(fragment);
                }
                // enqueue the chunks
                while ((fragment = fragments.shift())) {
                    queue.push({
                        data: fragments.length + "_" + fragment,
                        origin: origin,
                        callback: fragments.length === 0 ? fn : null
                    });
                }
            }
            else {
                queue.push({
                    data: message,
                    origin: origin,
                    callback: fn
                });
            }
            if (lazy) {
                pub.down.init();
            }
            else {
                dispatch();
            }
        },
        destroy: function(){
            destroying = true;
            pub.down.destroy();
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.VerifyBehavior
 * This behavior will verify that communication with the remote end is possible, and will also sign all outgoing,
 * and verify all incoming messages. This removes the risk of someone hijacking the iframe to send malicious messages.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration.
 * @cfg {Boolean} initiate If the verification should be initiated from this end.
 */
easyXDM.stack.VerifyBehavior = function(config){
    var pub, mySecret, theirSecret, verified = false;
    
    function startVerification(){
        mySecret = Math.random().toString(16).substring(2);
        pub.down.outgoing(mySecret);
    }
    
    return (pub = {
        incoming: function(message, origin){
            var indexOf = message.indexOf("_");
            if (indexOf === -1) {
                if (message === mySecret) {
                    pub.up.callback(true);
                }
                else if (!theirSecret) {
                    theirSecret = message;
                    if (!config.initiate) {
                        startVerification();
                    }
                    pub.down.outgoing(message);
                }
            }
            else {
                if (message.substring(0, indexOf) === theirSecret) {
                    pub.up.incoming(message.substring(indexOf + 1), origin);
                }
            }
        },
        outgoing: function(message, origin, fn){
            pub.down.outgoing(mySecret + "_" + message, origin, fn);
        },
        callback: function(success){
            if (config.initiate) {
                startVerification();
            }
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, getJSON, debug, emptyFn, isArray */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.RpcBehavior
 * This uses JSON-RPC 2.0 to expose local methods and to invoke remote methods and have responses returned over the the string based transport stack.<br/>
 * Exposed methods can return values synchronous, asyncronous, or bet set up to not return anything.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} proxy The object to apply the methods to.
 * @param {Object} config The definition of the local and remote interface to implement.
 * @cfg {Object} local The local interface to expose.
 * @cfg {Object} remote The remote methods to expose through the proxy.
 * @cfg {Object} serializer The serializer to use for serializing and deserializing the JSON. Should be compatible with the HTML5 JSON object. Optional, will default to JSON.
 */
easyXDM.stack.RpcBehavior = function(proxy, config){
    var pub, serializer = config.serializer || getJSON();
    var _callbackCounter = 0, _callbacks = {};
    
    /**
     * Serializes and sends the message
     * @private
     * @param {Object} data The JSON-RPC message to be sent. The jsonrpc property will be added.
     */
    function _send(data){
        data.jsonrpc = "2.0";
        pub.down.outgoing(serializer.stringify(data));
    }
    
    /**
     * Creates a method that implements the given definition
     * @private
     * @param {Object} The method configuration
     * @param {String} method The name of the method
     * @return {Function} A stub capable of proxying the requested method call
     */
    function _createMethod(definition, method){
        var slice = Array.prototype.slice;
        
        return function(){
            var l = arguments.length, callback, message = {
                method: method
            };
            
            if (l > 0 && typeof arguments[l - 1] === "function") {
                //with callback, procedure
                if (l > 1 && typeof arguments[l - 2] === "function") {
                    // two callbacks, success and error
                    callback = {
                        success: arguments[l - 2],
                        error: arguments[l - 1]
                    };
                    message.params = slice.call(arguments, 0, l - 2);
                }
                else {
                    // single callback, success
                    callback = {
                        success: arguments[l - 1]
                    };
                    message.params = slice.call(arguments, 0, l - 1);
                }
                _callbacks["" + (++_callbackCounter)] = callback;
                message.id = _callbackCounter;
            }
            else {
                // no callbacks, a notification
                message.params = slice.call(arguments, 0);
            }
            if (definition.namedParams && message.params.length === 1) {
                message.params = message.params[0];
            }
            // Send the method request
            _send(message);
        };
    }
    
    /**
     * Executes the exposed method
     * @private
     * @param {String} method The name of the method
     * @param {Number} id The callback id to use
     * @param {Function} method The exposed implementation
     * @param {Array} params The parameters supplied by the remote end
     */
    function _executeMethod(method, id, fn, params){
        if (!fn) {
            if (id) {
                _send({
                    id: id,
                    error: {
                        code: -32601,
                        message: "Procedure not found."
                    }
                });
            }
            return;
        }
        
        var success, error;
        if (id) {
            success = function(result){
                success = emptyFn;
                _send({
                    id: id,
                    result: result
                });
            };
            error = function(message, data){
                error = emptyFn;
                var msg = {
                    id: id,
                    error: {
                        code: -32099,
                        message: message
                    }
                };
                if (data) {
                    msg.error.data = data;
                }
                _send(msg);
            };
        }
        else {
            success = error = emptyFn;
        }
        // Call local method
        if (!isArray(params)) {
            params = [params];
        }
        try {
            var result = fn.method.apply(fn.scope, params.concat([success, error]));
            if (!undef(result)) {
                success(result);
            }
        } 
        catch (ex1) {
            error(ex1.message);
        }
    }
    
    return (pub = {
        incoming: function(message, origin){
            var data = serializer.parse(message);
            if (data.method) {
                // A method call from the remote end
                if (config.handle) {
                    config.handle(data, _send);
                }
                else {
                    _executeMethod(data.method, data.id, config.local[data.method], data.params);
                }
            }
            else {
                // A method response from the other end
                var callback = _callbacks[data.id] || {};
                if (data.error) {
                    if (callback.error) {
                        callback.error(data.error);
                    }
                }
                else if (callback.success) {
                    callback.success(data.result);
                }
                delete _callbacks[data.id];
            }
        },
        init: function(){
            if (config.remote) {
                // Implement the remote sides exposed methods
                for (var method in config.remote) {
                    if (config.remote.hasOwnProperty(method)) {
                        proxy[method] = _createMethod(config.remote[method], method);
                    }
                }
            }
            pub.down.init();
        },
        destroy: function(){
            for (var method in config.remote) {
                if (config.remote.hasOwnProperty(method) && proxy.hasOwnProperty(method)) {
                    delete proxy[method];
                }
            }
            pub.down.destroy();
        }
    });
};
})(window, document, location, window.nativeSetTimeout || window.setTimeout, decodeURIComponent, encodeURIComponent);
(function () {
    // empty comment
    var WA = window.WebAgent = window.WebAgent || {};

    var getUserLogin = function () {
        return (/Mpop=.*?:([^@:]+@[^:]+)/.exec(document.cookie.toString()) || [0, false])[1];
    };

    var ACTIVE_MAIL = getUserLogin();

    if (ACTIVE_MAIL) {
        var SAFE_ACTIVE_MAIL = ACTIVE_MAIL.replace(/[^a-z0-9]+/ig, '').toLowerCase();
    }

    var isDebug = location.href.indexOf('localhost') != -1 || location.href.indexOf('wa_debug') != -1;

    var docMode = document.documentMode;
    var ua = navigator.userAgent.toLowerCase();
    var checkUA = function (regexp) {
        return regexp.test(ua);
    };
    var isOpera = checkUA(/opera/);
    var isWebKit = checkUA(/webkit/);
    var isIE = !isOpera && checkUA(/msie/);
    var isIE8 = isIE && (checkUA(/msie 8/) && docMode != 7);
    var isGecko = !isWebKit && checkUA(/gecko/);
    var isGecko3 = isGecko && checkUA(/rv:1\.9/);

    var UID = 1;

    var apply = function (to, from, defaults) {
        if (defaults) {
            apply(to, defaults);
        }
        if (from) {
            for (var key in from) {
                to[key] = from[key];
            }
        }
        return to;
    };

    apply(WebAgent, {

        isDebug: isDebug,

        isIE: isIE,

        isIE8: isIE8,

        isFF36: isGecko3,

        isFF: isGecko,

        isWebKit: isWebKit,

        isOpera: isOpera,

        isSecure: (''+document.location).split(':')[0] == 'https',

        isLoadReduce: isIE,

        ACTIVE_MAIL: ACTIVE_MAIL,

        SAFE_ACTIVE_MAIL: SAFE_ACTIVE_MAIL,

        TEXT: {
            ERR_INFO_NETWORK: '������ �������� ����������. ��������� ������� ����������� ���������� ����� {0}. ��������, ����� ����������� ������������ ������.',
            ERR_INFO_TOO_MANY_CONNECTIONS: '������� ����� ����������� �� �������� ������������.',
            ERR_INFO_INVALID_USER: '��� ����������� ������ ���������� �������� ��������.'
        },

        resizeableLayout: true,

        apply: apply,

        applyIf: function (to, from) {
            if (from) {
                for (var key in from) {
                    if (typeof to[key] === 'undefined') {
                        to[key] = from[key];
                    }
                }
            }
            return to;
        },

        isPopup: window.__WebAgent__isPopup || false,

        namespace: function (path) {
            var arr = path.split('.');
            var o = window[arr[0]] = window[arr[0]] || {};
            WebAgent.each(arr.slice(1), function (el) {
                o = o[el] = o[el] || {};
            });
            return o;
        },

        getJSON: function () {
            // workaround for Prototype, see: https://prototype.lighthouseapp.com/projects/8886/tickets/730-prototypejs-breaks-firefox-35-native-json
            if (window.JSON && window.JSON.stringify(['a']) == '["a"]') {
                return window.JSON;
            } else {
                return window._JSON;
            }
        },

        getUserLogin: getUserLogin,

        isNumber: function (v) {
            return typeof v === 'number' && isFinite(v);
        },

        isString: function (v) {
            return typeof v === 'string';
        },

        isFunction: function (v) {
            return typeof v === 'function';
        },

        isObject: function (v) {
            return typeof v === 'object';
        },

        isBoolean: function(value) {
            return typeof value === 'boolean';
        },

        isDate: function (v) {
            return Object.prototype.toString.apply(v) === '[object Date]';
        },

        isArray: function (v) {
            // http://ajaxian.com/archives/isarray-why-is-it-so-bloody-hard-to-get-right
            return Object.prototype.toString.apply(v) === '[object Array]';
        },

        toArray: function (v) {
            if (WebAgent.isArray(v)) {
                return v;
            } else {
                return Array.prototype.slice.call(v, 0);
            }
        },

        each: function (arr, fn, scope) {
            var len = arr.length;
            for (var i = 0; i < len; ++i) {
                if (fn.call(scope || window, arr[i], i, arr) === false) {
                    return;
                }
            }
        },

        createDelegate: function (fn, scope, args, appendArgs) {
            return function () {
                var callArgs = WebAgent.toArray(arguments);
                if (appendArgs === true) {
                    callArgs = callArgs.concat(args || []);
                } else if (WebAgent.isNumber(appendArgs)) {
                    callArgs = callArgs.slice(0, appendArgs).concat(args || []);
                }
                return fn.apply(scope || window, callArgs);
            };
        },

        buildId: function (suffix) {
            return 'mailru-webagent-' + suffix;
        },

        buildIconClass: function(status, addDefaultIcon) {
            if (!status || /[<>\"\']/.test(status)) {
                status = 'online';
            }
            return (addDefaultIcon === true ? 'wa-cl-status-default ' : '') + 'wa-cl-status-' + status;
        },

        generateId: function () {
            return WebAgent.buildId('gen-' + UID++);
        },

        emptyFn: function () {
        },

        now: function () {
            return Math.floor(new Date() / 1000);
        },

        setTimeout: function (code, interval, scope) {
            var cb = function () {
                WA.util.Mnt.wrapTryCatch(code, scope);
            };
            if (window.nativeSetTimeout) {
                return window.nativeSetTimeout(cb, interval);
            } else {
                return window.setTimeout(cb, interval);
            }
        },

        setInterval: function (code, interval) {
            var cb = function () {
                WA.util.Mnt.wrapTryCatch(code);
            };
            if (window.nativeSetInterval) {
                return window.nativeSetInterval(cb, interval);
            } else {
                return window.setInterval(cb, interval);
            }
        },

        makeGet: function (hash) {
            var get = [];
            WebAgent.util.Object.each(hash, function (v, k) {
                get[get.length] = k + '=' + encodeURIComponent(v);
            });
            return get.join('&');
        },

        error: function (e) {
            if (isDebug) {
                debugger;
            }
            if(WA.Mnt) {
                WA.Mnt.log(e);
            }
            throw e;
        },

        abstractError: function () {
            WebAgent.error('Abstract method');
        },

        gstat: function (params) {
            if(typeof params == 'string') {
                var name = params;
                params = {};
                params[name] = 1;
            }
            var url = [];
            WebAgent.util.Object.each(params, function (q, n) {
                url.push('webagent.' + n + '=' + q);
            });
            new Image().src='//mail.ru/gstat?' + url.join('&') + '&rnd=' + Math.random();
        },

        makeAvatar: function (mail, type, domain) {
            domain = domain || 'avt.imgsmail.ru';
            var t = mail.match(/([^@]+)@([^\.]+)/i);
            var url = domain + '/' + t[2] + '/' + t[1] + '/' + type;
//            if(WA.isSecure) {
//                return 'https://proxy.imgsmail.ru/avt/?url717=' + url;
//            } else {
                return '//' + url;
//            }
        }

    });

})();
(function () {

    function override(source, overrides) {
        var p = source.prototype;
        WebAgent.apply(p, overrides);
        if (WebAgent.isIE && overrides.hasOwnProperty('toString')) {
            p.toString = overrides.toString;
        }
    }

    WebAgent.apply(WebAgent, {

        extend: function (superclass, overrides) {
            var oc = Object.prototype.constructor;

            var sub;
            if (overrides.constructor != oc) {
                sub = overrides.constructor;
            } else {
                sub = function () {
                    superclass.apply(this, arguments);
                };
            }

            var F = function() {
            };

            var subP;
            var superP = superclass.prototype;

            F.prototype = superP;

            subP = sub.prototype = new F();
            subP.constructor = sub;
            sub.superclass = superP;

            if (superP.constructor == oc) {
                superP.constructor = superclass;
            }

            subP.superclass = function() {
                return superP;
            };

            override(sub, overrides);

            return sub;
        }

    });

})();
(function () {

    var WA = WebAgent;

    // ---------------------------------------------------------------------------------

    var addDomListener = function (dom, eventName, fn, useCapture) {
        if (dom.addEventListener) {
            dom.addEventListener(eventName, fn, !!useCapture);
        } else if (dom.attachEvent) {
            dom.attachEvent('on' + eventName, fn);
        }
    };

    var removeDomListener = function (dom, eventName, fn, useCapture) {
        if (dom.removeEventListener) {
            dom.removeEventListener(eventName, fn, !!useCapture);
        } else if (dom.detachEvent) {
            dom.detachEvent('on' + eventName, fn);
        }
    };

    // ---------------------------------------------------------------------------------


    var EVENT_BUTTON_MAP = WA.isIE ? { b1: 0 , b4: 1, b2: 2 } : { b0: 0, b1: 1, b2: 2 };

    var DomEvent = WA.extend(Object, {

        constructor: function (e) {
            var ev = this.browserEvent = e || {}, doc;

            this.button = ev.button ? EVENT_BUTTON_MAP['b' + ev.button] : (ev.which ? ev.which - 1 : -1);
            if (/(dbl)?click/.test(ev.type) && this.button == -1) {
                this.button = 0;
            }

            this.type = ev.type;
            this.keyCode = ev.keyCode;

            this.shiftKey = ev.shiftKey;
            this.ctrlKey = ev.ctrlKey || ev.metaKey || false;
            this.altKey = ev.altKey;
            this.pageX = ev.pageX;
            this.pageY = ev.pageY;

            if (ev.pageX == null && ev.clientX != null) {
                doc = document.documentElement;
                this.pageX = ev.clientX + (doc && doc.scrollLeft || 0);
                this.pageY = ev.clientY + (doc && doc.scrollTop || 0);
            }
        },

        getKeyCode: function () {
            return this.browserEvent.keyCode;
        },

        getCharCode: function () {
            return this.browserEvent.charCode;
        },

        getTarget: function (returnElement) {
            var node = this.browserEvent.target || this.browserEvent.srcElement;
            if (!node) {
                return null;
            } else {
                var dom = node.nodeType == 3 ? node.parentNode : node;
                return returnElement ? WA.get(dom) : dom;
            }
        },

        stopPropagation: function () {
            var event = this.browserEvent;
            if (event) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else {
                    event.cancelBubble = true;
                }
            }
        },

        preventDefault: function () {
            var event = this.browserEvent;
            if (event) {
                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }
            }
        },

        stopEvent: function () {
            this.stopPropagation();
            this.preventDefault();
        }

    });

    // ---------------------------------------------------------------------------------

    var Container = {

        items: {},

        register: function (el) {
            var id = el.identify();
            if (!this.items[id]) {
                this.items[id] = new ContainerItem(el);
            }
        },

        getElement: function (id) {
            var item = this.items[id];
            if (item && item.isValid()) {
                return item.el;
            } else {
                return null;
            }
        },

        each: function (fn, scope) {
            WA.util.Object.each(this.items, fn, scope || this);
        },

        addListener: function (el, eventName, fn, scope, options) {
            this.register(el);
            var item = this.items[el.getId()];
            item.addListener(eventName, fn, scope, options);
        },

        removeListener: function (el, eventName, fn, scope) {
            var item = this.items[el.getId()];
            if (item) {
                item.removeListener(eventName, fn, scope);
            }
        },

        removeListeners: function (el, recursively, eventName) {
            var item = this.items[el.getId()];
            if (item) {
                item.removeListeners(eventName);
                if (recursively && el.dom.childNodes) {
                    el.each(function (childEl) {
                        this.removeListeners(childEl, recursively, eventName);
                    }, this);
                }
            }
        },

        remove: function (el) {
            var id = el.getId();
            var item = this.items[id];
            if (item) {
                item.destroy();
                delete this.items[id];
            }
        }

    };

    // ---------------------------------------------------------------------------------

    var ContainerItem = WA.extend(Object, {

        constructor: function (el) {
            this.el = el;
            this.events = {};
            this.domHandlers = {};
        },

        isValid: function () {
            var el = this.el;
            var dom = el.dom;
            var isInvalid = !dom || !dom.parentNode || (!dom.offsetParent && !document.getElementById(el.getId()));
            return !isInvalid || dom === document || dom === document.body || dom === window;
        },

        addListener: function (eventName, fn, scope, options) {
            var event = this.events[eventName];
            if (!event) {
                event = this.events[eventName] = new WA.util.Event();
                this.domHandlers[eventName] = WA.createDelegate(
                        function (e, ename) {
                            WA.util.Mnt.wrapTryCatch(function () {
                                this.events[ename].fire(new DomEvent(e), this.el);
                            }, this);
                        },
                        this,
                        [eventName],
                        1
                        );
                if (eventName == 'afterresize') {
                    addDomListener(this.el.dom, 'resize',
                        WA.createDelegate(function (e) {
                            if (this._timer > 0) {
                                clearTimeout(this._timer);
                            }
                            this._timer = WA.setTimeout(
                                WA.createDelegate(function() {
                                    this.domHandlers[eventName].call(window, e);
                                }, this),
                                150
                            );
                        }, this)
                    );
                } else {
                    addDomListener(this.el.dom, eventName, this.domHandlers[eventName], !!(options||{}).useCapture);
                }
            }
            event.on(fn, scope || this.el, options);
        },

        removeListener: function (eventName, fn, scope) {
            var event = this.events[eventName];
            if (event) {
                event.un(fn, scope || this.el);
            }
        },

        removeListeners: function (eventName) {
            if (eventName) {
                var event = this.events[eventName];
                if (event) {
                    event.removeAll();
                }
            } else {
                WA.util.Object.each(this.events, function (ignored, ename) {
                    this.removeListeners(ename);
                }, this);
            }
        },

        destroy: function () {
            var eventNames = WA.util.Object.getKeys(this.events);
            WA.util.Array.each(eventNames, function (eventName) {
                this.removeListeners(eventName);
                removeDomListener(this.el.dom, eventName, this.domHandlers[eventName]);
                delete this.events[eventName];
                delete this.domHandlers[eventName];
            }, this);

            this.events = null;
            this.domHandlers = null;
            this.el = null;
        }

    });

    // ---------------------------------------------------------------------------------

    var garbageCollect = function () {
        Container.each(function (item) {
            if (!item.isValid()) {
                this.remove(item.el);
            }
        });
    };

    WA.setInterval(garbageCollect, 30000);

    // ---------------------------------------------------------------------------------

    var propCache = {},
        camelRe = /(-[a-z])/gi,
        propFloat = WA.isIE ? 'styleFloat' : 'cssFloat';


    function camelFn(mchkCache, a) {
        return a.charAt(1).toUpperCase();
    }

    function chkCache(prop) {
        return propCache[prop] || (propCache[prop] = prop == 'float' ? propFloat : prop.replace(camelRe, camelFn));

    }

    var Element = WA.extend(Object, {

        constructor: function (dom) {
            this.dom = dom;
        },

        getId: function () {
            return this.dom.id;
        },

        identify: function () {
            if (!this.dom.id) {
                return this.dom.id = WA.generateId();
            } else {
                return this.dom.id;
            }
        },

        getWidth: function () {
            return Math.max(this.dom.offsetWidth, this.isVisible() ? 0 : this.dom.clientWidth) || 0;
        },

        getHeight: function () {
            return Math.max(this.dom.offsetHeight, this.isVisible() ? 0 : this.dom.clientHeight) || 0;
        },

        getSize: function () {
            return {
                width: this.getWidth(),
                height: this.getHeight()
            };
        },

        setWidth: function (width) {
            this.dom.style.width = width + 'px';
            return this;
        },

        setHeight: function (height) {
            this.dom.style.height = height + 'px';
            return this;
        },

        setSize: function (width, height) {
            if (WA.isObject(width)) {
                return this.setSize(width.width, width.height);
            } else {
                this.setWidth(width);
                this.setHeight(height);
                return this;
            }
        },

        addListener: function (eventName, fn, scope, optons) {
            Container.addListener(this, eventName, fn, scope, optons);
            return this;
        },

        on: function (eventName, fn, scope, options) {
            return this.addListener(eventName, fn, scope, options);
        },

        removeListener: function (eventName, fn, scope) {
            Container.removeListener(this, eventName, fn, scope);
            return this;
        },

        un: function (eventName, fn, scope) {
            return this.removeListener(eventName, fn, scope);
        },

        removeListeners: function (recursively, eventName) {
            Container.removeListeners(this, recursively, eventName);
            return this;
        },

        setAttribute: function (name, value) {
            this.dom.setAttribute(name, value);
        },

        getAttribute: function (name) {
            return this.dom.getAttribute(name);
        },

        removeClass: function (className) {
            var clsArray = this.dom.className.split(' ');
            this.dom.className = WA.util.Array.remove(clsArray, className).join(' ');
            return this;
        },

        addClass: function (className) {
            if (!this.hasClass(className)) {
                this.dom.className += ' ' + className;
            }
            return this;
        },

        hasClass: function (className) {
            return (' ' + this.dom.className + ' ').indexOf(' ' + className + ' ') != -1;
        },

        toggleClass: function (className, state) {
            var isBool = typeof state === "boolean";
            state = isBool ? state : !this.hasClass(className);
            this[state ? 'addClass' : 'removeClass'](className);
            return this;
        },

        getClass: function () {
            return this.dom.className;
        },

        update: function (html) {
            this.dom.innerHTML = html;
            return this;
        },

        insertFirst: function (el) {
            var newNode = WA.get(el);
            if (this.first()) {
                this.insertBefore(newNode, this.first());
                return newNode;
            }
            return newNode.appendTo(this);
        },

        appendChild: function (el) {
            return WA.get(el).appendTo(this);
        },

        appendTo: function (el) {
            WA.getDom(el).appendChild(this.dom);
            return this;
        },

        insertBefore: function (el, before) {
            WA.getDom(el).insertBefore(this.dom, before);
            return this;
        },

        createChild: function (config, atTheBeginning) {
            var el = WA.util.DomHelper.insertHtml(atTheBeginning === true ? 'afterBegin' : 'beforeEnd', this.dom, config);
            return WA.fly(el);
        },

        setVisible: function (visible) {
            this.dom.style.display = visible ? 'block' : 'none';
            return this;
        },

        isVisible: function () {
            return this.dom.style.display == 'block';
        },

        show: function () {
            return this.setVisible(true);
        },

        hide: function () {
            return this.setVisible(false);
        },

        toggle: function () {
            return this.setVisible(!this.isVisible());
        },

        each: function (fn, scope) {
            var nodes = this.dom.childNodes;
            var len = nodes.length;
            for (var i = 0; i < len; ++i) {
                var node = WA.isFunction(nodes) ? nodes(i) : nodes[i];
                if (node.nodeType == 1) {
                    var el = node.id ? WA.get(node) : WA.fly(node);
                    if (fn.call(scope || this, el, i) === false) {
                        return;
                    }
                }
            }
        },

        matchNode: function (dir, start) {
            var node = this.dom[start];
            while (node) {
                if (node.nodeType == 1) {
                    return WA.get(node);
                }
                node = node[dir];
            }
            return null;
        },

        first: function () {
            return this.matchNode('nextSibling', 'firstChild');
        },

        last: function () {
            return this.matchNode('previousSibling', 'lastChild');
        },

        prev: function () {
            return this.matchNode('previousSibling', 'previousSibling');
        },

        next: function () {
            return this.matchNode('nextSibling', 'nextSibling');
        },

        parent: function () {
            return this.matchNode('parentNode', 'parentNode');
        },

        up: function (cls) {
            var node = this;
            while (node) {
                if (node.hasClass(cls)) return node;
                node = node.parent();
            }
        },

        /* don't rely on this in old browsers, use feature detection */
        offset: function () {
            var box = {top: 0, left: 0};

            if ('getBoundingClientRect' in document.documentElement ) {
                try {
                    box = this.dom.getBoundingClientRect();
                } catch(ex) {}
            } else { /* this method doesn't support old browsers */ }

            var doc = this.dom.ownerDocument,
                docElem = doc.documentElement,
                top = box.top + (window.pageYOffset || docElem.scrollTop),
                left = box.left + (window.pageXOffset || docElem.scrollLeft);

            return {top: top, left: left};
        },

        __removeDom: (function () {
            var d = null;
            return function (node) {
                if (node.tagName != 'BODY') {
                    if (WA.isIE && !WA.isIE8) {
                        d = d || document.createElement('div');
                        d.appendChild(node);
                        d.innerHTML = '';
                    } else {
                        node.parentNode.removeChild(node);
                    }
                }
            };
        })(),

        remove: function () {
            if (this._maskEl) {
                this._maskEl.remove();
                this._maskEl = null;
            }

            Container.remove(this);
            this.__removeDom(this.dom);
            this.dom = null;
        },

        isMasked: function () {
            return this.hasClass('nwa-overlay');
        },

        mask: function () {
            return this.addClass('nwa-overlay');
        },

        unmask: function () {
            return this.removeClass('nwa-overlay');
        },

        isAncestor: function (el) {
            var parent = WA.getDom(el);
            if (parent.contains) {
                return parent.contains(this.dom);
            } else {
                var c = this.dom;
                while (c = c.parentNode) {
                    if (parent === c) {
                        return true;
                    }
                }
                return false;
            }
        },

        contains: function (el) {
            return WA.fly(el).isAncestor(this);
        },

        equals: function (el) {
            return this.dom === el.dom;
        },

        setStyle : function(prop, value){
            var tmp,
                style;
            if (!WA.isObject(prop)) {
                tmp = {};
                tmp[prop] = value;
                prop = tmp;
            }
            for (style in prop) {
                value = prop[style];
                style == 'opacity' ?
                    this.setOpacity(value) :
                    this.dom.style[chkCache(style)] = value;
            }
            return this;
        },

         setOpacity : function(opacity, animate){
            var me = this,
                s = me.dom.style;
            if (WA.isIE) {
                s.zoom = 1;
                s.filter = (s.filter || '').replace(/alpha\([^\)]*\)/gi,"") +
                           (opacity == 1 ? "" : " alpha(opacity=" + opacity * 100 + ")");
            } else {
                s.opacity = opacity;
            }
            return me;
        }

    });

    WA.apply(WA, {

        getDom: function (el) {
            if (el instanceof Element) {
                return el.dom;
            } else if (typeof el === 'string') {
                return document.getElementById(el);
            } else {
                return el;
            }
        },

        fly: function (el) {
            if (el instanceof Element) {
                return el;
            } else {
                return new Element(el);
            }
        },

        get: function (el) {
            if (el instanceof Element) {
                return el;
            } else {
                var dom = WA.getDom(el);
                if (dom) {
                    var ret = Container.getElement(dom.id);
                    if (!ret) {
                        ret = new Element(dom);
                        Container.register(ret);
                    }
                    return ret;
                } else {
                    return null;
                }
            }
        },

        getBody: function () {
            return new Element(document.body);
        },

        getDoc: function () {
            return new Element(document);
        }

    });

})();
WebAgent.namespace('WebAgent');

(function () {

    var WA = WebAgent;

    var OVERRIDES = {

        isActive: function () {
            return this.__isActive === true;
        },

        activate: function (params) {
            if (!this.isActive()) {
                this.__isActive = true;
                return this._onActivate(params) !== false;
            } else {
                return false;
            }
        },

        _onActivate: function (params) {
        },

        deactivate: function (params) {
            if (this.isActive()) {
                this.__isActive = false;
                return this._onDeactivate(params) !== false;
            } else {
                return false;
            }
        },

        _onDeactivate: function (params) {
        }

    };

    WA.Activatable = WA.extend(Object, OVERRIDES);

    WA.Activatable.extend = function (superclass, overrides) {
        var sp = WA.extend(superclass, OVERRIDES);
        return WA.extend(sp, overrides);
    };

})();
WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    var Animation = U.Animation = WA.extend(Object, {

        constructor: function () {
            this._onFrameScope = WA.createDelegate(this._onFrame, this);
            this._list = {};
            this._index = -1;
        },

        _requestFrame: function () {
            this.requestAnimationFrame.call(window, this._onFrameScope);
        },

        start: function (from, to, pxPerMs, cb, scope) {
            if(!this._started) {
                this._started = true;
                this._requestFrame();
            }

            this._index++;
            this._list[this._index] = {
                from: from,
                to: to,
                pxPerMs: pxPerMs,
                cb: cb,
                scope: scope,
                date: +new Date(),
                first: true
            };
            return this._index;
        },

        clear: function (index) {
            delete this._list[index];
        },

        _onFrame: function () {
            var count = 0;
            U.Object.each(this._list, function (v, index) {
                count++;
                var value;
                var d = (+new Date() - v.date) * v.pxPerMs;
                var lastFrame = false;
                if(v.from < v.to) {
                    value = v.from + d;
                    if(value > v.to) {
                        lastFrame = true;
                        value = v.to;
                    }
                } else {
                    value = v.from - d;
                    if(value < v.to) {
                        lastFrame = true;
                        value = v.to;
                    }
                }

                v.cb.call(v.scope || window, value, lastFrame, v.first);
                
                v.first = false;
                if(lastFrame) {
                    this.clear(index);
                }
            }, this);
            
            if(count) {
                this._requestFrame();
            } else {
                this._started = false;
            }
        },

        requestAnimationFrame: (function(){
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function(callback, element){
                    WA.setTimeout(callback, 1000 / 60);
                };
        })()

    });

    U.animation = new U.Animation();

})();WebAgent.namespace('WebAgent.util');

(function () {

    var A = WebAgent.util.Array = {

        each: function (arr, fn, scope) {
            WebAgent.each(arr, fn, scope);
        },
        
        eachReverse: function (arr, fn, scope) {
            var j = 0;
            for (var i = arr.length; i--;) {
                if (fn.call(scope || window, arr[i], j, arr) === false) {
                    return;
                }
                j++;
            }            
        },

        transform: function (arr, fn, scope) {
            var ret = [];
            A.each(arr, function () {
                var el = fn.apply(this, arguments);
                ret.push(el);
            }, scope);
            return ret;
        },

        filter: function (arr, fn, scope) {
            var ret = [];
            A.each(arr, function (el, index, allItems) {
                if (fn.call(this, el, index, allItems)) {
                    ret.push(el);
                }
            }, scope);
            return ret;
        },

        indexOf: function (arr, el) {
            var fn = null;
            if (el instanceof RegExp) {
                fn = function (entry) {
                    return el.test(entry);
                };
            } else {
                fn = function (entry) {
                    return el === entry;
                };
            }

            return A.indexOfBy(arr, fn);
        },

        indexOfBy: function (arr, fn, scope) {
            var ret = -1;
            A.each(arr, function (el, index) {
                if (fn.call(scope || window, el, index) === true) {
                    ret = index;
                    return false;
                }
            });
            return ret;
        },

        findBy: function (arr, fn, scope) {
            var index = A.indexOfBy(arr, fn, scope);
            if (index != -1) {
                return arr[index];
            } else {
                return null;
            }
        },

        removeAt: function (arr, index) {
            arr.splice(index, 1);
            return arr;
        },

        remove: function (arr, el) {
            var index = A.indexOf(arr, el);
            if (index != -1) {
                return A.removeAt(arr, index);
            } else {
                return arr;
            }
        },

        removeBy: function (arr, fn, scope) {
            var index = A.indexOfBy(arr, fn, scope);
            if (index != -1) {
                return A.removeAt(arr, index);
            } else {
                return null;
            }
        },

        clone: function (arr) {
            return Array.prototype.slice.call(arr);
        }

    };

})();
WebAgent.namespace('WebAgent.util');

(function () {

    WebAgent.util.Date = {

        getElapsed: function (date1, date2) {
            date2 = date2 || new Date();
            return date2.getTime() - date1.getTime();
        }

    };

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;

    WA.util.DelayedTask = WA.extend(Object, {

        constructor: function (config) {
            WA.apply(this, config);

            this.id = null;

            if (this.fn) {
                this._initFn(this.fn, this.scope);
            }
        },

        _initFn: function (fn, scope) {
            this.scope = scope || this.scope || this;
            this.fn = WA.createDelegate(fn, this.scope);
        },

        isStarted: function () {
            return this.id != null;
        },

        start: function (interval, fn, scope) {
            if (this.isStarted()) {
                this.stop();
            }

            if (interval > 0) {
                this.interval = interval;
            }

            if (fn) {
                this._initFn(fn, scope);
            }

            this.id = WA.setTimeout(this.fn, this.interval);
        },

        stop: function () {
            if (this.isStarted()) {
                clearTimeout(this.id);
                this.id = null;
            }
        }

    });

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    var tableRe = /^(?:table|tbody|tr|td)$/i;
    var tableElRe = /^(?:td|tr|tbody)$/i;
    var emptyTags = /^(?:br|frame|hr|img|input|link|meta|range|spacer|wbr|area|param|col)/i;

    var tempTableEl = null;
    var afterbegin = 'afterbegin',
            afterend = 'afterend',
            beforebegin = 'beforebegin',
            beforeend = 'beforeend',
            ts = '<table>',
            te = '</table>',
            tbs = ts + '<tbody>',
            tbe = '</tbody>' + te,
            trs = tbs + '<tr>',
            tre = '</tr>' + tbe;

    function ieTable(depth, s, h, e) {
        tempTableEl.innerHTML = [s, h, e].join('');

        var i = -1;
        var el = tempTableEl;
        var ns;

        while (++i < depth) {
            el = el.firstChild;
        }

        if (ns = el.nextSibling) {
            var df = document.createDocumentFragment();
            while (el) {
                ns = el.nextSibling;
                df.appendChild(el);
                el = ns;
            }
            el = df;
        }

        return el;
    }

    function insertIntoTable(tag, where, el, html) {
        var node, before;

        tempTableEl = tempTableEl || document.createElement('div');

        if (tag == 'td' && (where == afterbegin || where == beforeend) ||
                !tableRe.test(tag) && (where == beforebegin || where == afterend)) {
            return null;
        }
        before = where == beforebegin ? el :
                where == afterend ? el.nextSibling :
                        where == afterbegin ? el.firstChild : null;

        if (where == beforebegin || where == afterend) {
            el = el.parentNode;
        }

        if (tag == 'td' || (tag == 'tr' && (where == beforeend || where == afterbegin))) {
            node = ieTable(4, trs, html, tre);
        } else if ((tag == 'tbody' && (where == beforeend || where == afterbegin)) ||
                (tag == 'tr' && (where == beforebegin || where == afterend))) {
            node = ieTable(3, tbs, html, tbe);
        } else {
            node = ieTable(2, ts, html, te);
        }
        el.insertBefore(node, before);
        return node;
    }

    function createHtml(o) {
        var html = '';
        if (WA.isString(o)) {
            html = o;
        } else if (WA.isArray(o)) {
            U.Array.each(o, function (entry) {
                html += createHtml(entry);
            });
        } else {
            var tag = o.tag || 'div';
            html = '<' + tag;
            var attrs = [];
            U.Object.each(o, function (val, attr) {
                if (U.Array.indexOf(['tag', 'children', 'html'], attr) == -1) {
                    attrs.push(('cls' === attr ? 'class' : attr) + '="' + val + '"');
                }
            });
            if (attrs.length > 0) {
                html += ' ' + attrs.join(' ');
            }

            if (emptyTags.test(tag)) {
                html += '/>';
            } else {
                html += '>';
                if (o.children) {
                    html += createHtml(o.children);
                } else if (o.html) {
                    html += o.html;
                }
                html += '</' + tag + '>';
            }
        }
        return html;
    }

    var DH = U.DomHelper = {

        append: function (el, html, returnElement) {
            return DH.insertHtml('beforeEnd', el, html, returnElement);
        },

        insertHtml: function (where, el, html, returnElement) {
            var hash = {},
                    hashVal,
                    setStart,
                    range,
                    frag,
                    rangeEl,
                    rs;

            where = where.toLowerCase();
            html = createHtml(html);

            hash[beforebegin] = ['BeforeBegin', 'previousSibling'];
            hash[afterend] = ['AfterEnd', 'nextSibling'];

            if (el.insertAdjacentHTML) {
                if (tableRe.test(el.tagName) && (rs = insertIntoTable(el.tagName.toLowerCase(), where, el, html))) {
                    return returnElement ? WA.get(rs) : rs;
                }

                hash[afterbegin] = ['AfterBegin', 'firstChild'];
                hash[beforeend] = ['BeforeEnd', 'lastChild'];
                if ((hashVal = hash[where])) {
                    el.insertAdjacentHTML(hashVal[0], html);
                    rs = el[hashVal[1]];
                    return returnElement ? WA.get(rs) : rs;
                }
            } else {
                range = el.ownerDocument.createRange();
                setStart = 'setStart' + (/end/i.test(where) ? 'After' : 'Before');
                if (hash[where]) {
                    range[setStart](el);
                    frag = range.createContextualFragment(html);
                    el.parentNode.insertBefore(frag, where == beforebegin ? el : el.nextSibling);
                    rs = el[(where == beforebegin ? 'previous' : 'next') + 'Sibling'];
                    return returnElement ? WA.get(rs) : rs;
                } else {
                    rangeEl = (where == afterbegin ? 'first' : 'last') + 'Child';
                    if (el.firstChild) {
                        range[setStart](el[rangeEl]);
                        frag = range.createContextualFragment(html);
                        if (where == afterbegin) {
                            el.insertBefore(frag, el.firstChild);
                        } else {
                            el.appendChild(frag);
                        }
                    } else {
                        el.innerHTML = html;
                    }
                    rs = el[rangeEl];
                    return returnElement ? WA.get(rs) : rs;
                }
            }
            WA.error('Illegal insertion point -> "' + where + '"');
        },

        whenReady: function (fn, scope) {
            easyXDM.whenReady(fn, scope);
        },

        rapidHtmlInsert: function (html) {
            var before = document.body.getElementsByTagName('*')[0];
            if (document.body && before) {
                document.body.insertBefore(document.createElement('div'), before).innerHTML = createHtml(html);
            } else {
                WA.setTimeout(function () {
                    DH.rapidHtmlInsert(html);
                }, 10);
            }
        },

        htmlEntities: function (str) {
            return (str || '').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }

    };

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    // -----------------------------------------------------------------------------------------------

    var BaseListener = WA.extend(Object, {

        constructor: function (fn, scope) {
            this.fn = fn;
            this.scope = scope;
        },

        equals: function (fn, scope) {
            if (this.fn === fn) {
                if (scope) {
                    if (this.scope === scope) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        },

        invoke: function (args) {
            if (this.fn) {
                return this.fn.apply(this.scope || window, args);
            }
        },

        destroy: function () {
            delete this.fn;
            delete this.scope;
        }

    });

    // -----------------------------------------------------------------------------------------------

    var SingleListener = WA.extend(BaseListener, {

        constructor: function (fn, scope, owner) {
            SingleListener.superclass.constructor.call(this, fn, scope);
            this.owner = owner;
        },

        invoke: function (args) {
            var ret = SingleListener.superclass.invoke.call(this, args);
            this.owner.removeListener(this.fn, this.scope);
            
            return ret;
        },

        destroy: function () {
            SingleListener.superclass.destroy.call(this);
            delete this.owner;
        }

    });

    // -----------------------------------------------------------------------------------------------

    U.Event = WA.extend(Object, {

        constructor: function () {
            this.suspended = false;
            this.listeners = [];
        },

        hasListeners: function () {
            return this.listeners.length > 0;
        },

        addListener: function (fn, scope, options) {
            options = options || {};

            var listener = null;

            if (options.single === true) {
                listener = new SingleListener(fn, scope, this);
            } else {
                listener = new BaseListener(fn, scope);
            }

            this.listeners.push(listener);

            return this;
        },

        on: function (fn, scope, options) {
            return this.addListener(fn, scope, options);
        },

        findListener: function (fn, scope) {
            return U.Array.indexOfBy(this.listeners, function (listener) {
                return listener.equals(fn, scope);
            });
        },

        removeListener: function (fn, scope) {
            var index = this.findListener(fn, scope);
            if (index != -1) {
                var listener = this.listeners[index];
                listener.destroy();
                if (this.firing) {
                    // TODO: lolwut?
                    this.__listeners = this.listeners.slice(0);
                }
                this.listeners.splice(index, 1);
            }
            return this;
        },

        un: function (fn, scope) {
            return this.removeListener(fn, scope);
        },

        fire: function () {
            var ret = true;
            if (!this.suspended) {
                var args = arguments;
                this.firing = true;
                var listeners = this.listeners.slice(0);
                U.Array.each(listeners, function (listener, i) {
                    if (listener.invoke(args) === false) {
                        return ret = false;
                    }
                }, this);
                this.firing = false;
                if (this._needRemoveAll) {
                    delete this._needRemoveAll;
                    this.removeAll();
                }
            }
            return ret;
        },

        removeAll: function () {
            if (!this.firing) {
                while (this.listeners.length > 0) {
                    var listener = this.listeners.shift();
                    listener.destroy();
                }
            } else {
                this._needRemoveAll = true;
            }
        },

        relay: function (event) {
            event.on(function () {
                return this.fire.apply(this, arguments);
            }, this);
            return this;
        },

        suspend: function () {
            this.suspended = true;
        },

        resume: function () {
            this.suspended = false;
        }

    });

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    U.EventConfirm = WA.extend(U.Event, {
        fire: function () {
            var ret = true;
            if (!this.suspended) {
                this.firing = true;
                
                var args = Array.prototype.slice.call(arguments, 0);
                var readyCb = args.shift();
                if(readyCb && readyCb.success) {
                    readyCb = WA.createDelegate(readyCb.success, readyCb.scope || window);
                }

                var confirmCount = 0;
                var checkReady = function () {
                    if(confirmCount == 0) {
                        readyCb && readyCb();
                    }
                };

                args.push(function () {
                    confirmCount--;
                    checkReady();
                });

                var listeners = this.listeners.slice(0);
                U.Array.each(listeners, function (listener, i) {
                    var r = listener.invoke(args);
                    if (r === false) {
                        return ret = false;
                    } else  if(r === true) {
                        confirmCount++;
                    }
                }, this);
                checkReady();
                
                this.firing = false;
            }
            return ret;
        }
    });

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    U.EventDispatcher = WA.extend(Object, {

        constructor: function () {
            this._listeners = {};
        },

        addEventListener: function (type, cb) {
            if(!this._listeners[type]) {
                this._listeners[type] = [];
            }

            this._listeners[type].push(cb);
        },

        dispatchEvent: function (type, event) {
            if(this['on'+ type]) {
                this['on'+ type](event);
            }

            var listeners = this._listeners[type];
            if(listeners) {
                for(var i = 0; i<listeners.length; i++) {
                    listeners[i].call(window, event);
                }
            }
        },

        removeEventListener: function (type, cb) {
            var listeners = this._listeners[type];
            if(listeners) {
                var res = [];
                for(var i = 0; i<listeners.length; i++) {
                    if(listeners[i] !== cb) {
                        res.push(listeners[i])
                    }
                }
                this._listeners[type] = res;
            }
            
        },

        destroy: function () {
            // not implemented yet
        }

    });


})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;

    var srcChar = "qwertyuiopasdfghjklzxcvbnm[{]};:',<.> ��������������������������������";
    var mapChar = "������������������������������������� qwertyuiop[]asdfghjkl;'zxcvbnm,.";
    var delimPos = 37;

    WA.util.KeyMapping = {

        translate: function (word) {
            var alterWord = '',
                len = 0,
                lang = null,
                pos = -1,
                char = '',
                i = 0;

            word = (word || '').toLowerCase();
            len = word.length;

            while (i < len) {
                char = word[i++];
                pos = srcChar.indexOf(char);
                if (!lang) {
                    // detect base language
                    if (pos > -1 && pos < delimPos) {
                        lang = 'en';
                    } else if (pos > delimPos) {
                        lang = 'ru';
                    }
                }

                if (lang == 'ru' && pos > delimPos || lang == 'en' && pos < delimPos && pos > -1) {
                    char = mapChar[pos];
                }

                alterWord += char;
            }

            return alterWord;
        }
    };

})();WebAgent.namespace('WebAgent.util');

(function () {
    var WA = WebAgent;
    var U = WA.util;
    var JSON = WA.getJSON();
    var S;

    var browser = window.opera && 'op' ||
        window.chrome && 'ch' ||
        window.WebKitAnimationEvent && 'sf' ||
        document.attachEvent && 'ie' ||
        'mozInnerScreenX' in window && 'ff' ||
        'na';

    var DAY = 1000 * 60 * 60 * 24;

    var usedBranch = WA.usedBranch;
    if(!usedBranch) {
        usedBranch = (document.location.search.match(/&usedBranch=([^=&]+)/) || []) [1];
    }
    var shortDomain = document.location.host.replace(/(?:^|\.)(\w)[^\.]+/g, '$1');
    var FLUSH_DELAY = 10 * 60000;
    var FLUSH_DIALOGS_MAX_INTERVAL = DAY;
    //var PREFIX = ('UA-' + browser + '_D-' + shortDomain + '_B-' + WA.usedBranch).substr(0, 20);
    var PREFIX = ('B-' + usedBranch).substr(0, 20);
    //var GRAPHITE_PREFIX = 'webagent.' + shortDomain + '.' + WA.usedBranch + '.' + browser + '.';
    var GRAPHITE_PREFIX = 'webagent.' + usedBranch + '.';

    var M = WA.extend(Object, {

        constructor: function () {
            var now = this._timeStart = +new Date(),
                elapsed = now - (localStorage.waMonitoringFlushDate || 0),
                timeleft = FLUSH_DELAY - elapsed;

            this.onBeforeFlushEvent = new U.Event();

            if (elapsed > FLUSH_DELAY - 5000) {
                localStorage.waMonitoringFlushDate = now;
                timeleft = 5000;
            }

            this._flushTask = new U.DelayedTask({
                fn: this._flush,
                scope: this
            });
            this._flushTask.start(timeleft);
            
            this._storeTask = new U.DelayedTask({
                interval: 0,
                fn: this._store,
                scope: this
            });

            this._flushDialogsMaxTask = new U.DelayedTask({
                fn: this._flushDialogsMaxFn,
                scope: this
            });
            new U.DelayedTask().start(10, this._flushDialogsMaxInit, this);

            WA.fly(window).un('beforeunload', this._flush, this);
            WA.fly(window).un('unload', this._flush, this);
        },

        activeUserCount: function () {
            if(!this._userCounted && WA.Storage) {
                this._userCounted = true;
                S || (S = WA.Storage);

                S.load(['activeUserCountDate'], {
                    success: function (storage) {
                        var now = +new Date();
                        var date = +storage['activeUserCountDate'];
                        if(!date || now - date > 1000*60*60*24) {
                            this.count('activeUser');
                            S.save({
                                activeUserCountDate: now
                            });
                        }
                    },
                    scope: this
                })
            }
        },

        _flushDialogsMaxInit: function () {
            if(!WA.Storage) { // in rpc.html do nothing
                return;
            }

            WA.Storage.whenReady(function () {
                S = WA.Storage;
                S.load(['dialogsStat'], {
                    success: function (storage) {
                        var now = +new Date();
                        var stat = JSON.parse(storage['dialogsStat']||0) || {d: now, c: 0};
                        var d = FLUSH_DIALOGS_MAX_INTERVAL - (now - stat.d);
                        this._flushDialogsMaxTask.start(d<0 ? 0 : d);
                    },
                    scope: this
                })
            }, this);
        },

        _flushDialogsMaxFn: function () {
            S.load(['dialogsStat'], {
                success: function (storage) {
                    var now = +new Date();
                    var stat = JSON.parse(storage['dialogsStat']||0);
                    if(stat && stat.c) {
                        this.count('dialogsMaxUniq' + (stat.c > 6 ? 'M' : stat.c));
                    }
                    S.save({
                        'dialogsStat': JSON.stringify({
                            d: now,
                            c: 0
                        })
                    });
                },
                scope: this
            });
        },

        increaseDialogMax: function (count) {
            S || (S = WA.Storage);

            S.load(['dialogsStat'], {
                success: function (storage) {
                    var stat = JSON.parse(storage['dialogsStat']||0) || {c: 0};

                    if(count > stat.c) {
                        S.save({
                            'dialogsStat': JSON.stringify({
                                d: +new Date(),
                                c: count
                            })
                        });
                        this._flushDialogsMaxTask.start(FLUSH_DIALOGS_MAX_INTERVAL);
                    }
                },
                scope: this
            }, this);
        },

        _ts: {},
        _intervals: [],
        _counters: {},
        _timeSum: 0,
        _timeStart: 0,

        log: function (params) {
            try {
                if(typeof params == 'object') {
                    params =  WA.makeGet(params)
                }
            } catch(e) {}
            
            new Image().src='//mrilog.mail.ru/empty.gif?' + params + '&WALOG&resPath=' + WebAgent.resPath + '&location=' + document.location;
        },

        wrapTryCatch: function (cb, scope) {
            var t1 = +new Date();
            
            if(WA.isProduction) {
                try {
                    cb.call(scope || window);
                } catch (e) {
                    U.Mnt.count('errorsJS');
                    
                    e = e || {};
                    if (typeof e == 'string') {
                        e = {
                            message: e
                        };
                    }
                    try {
                        var err = {
                            login: WA.ACTIVE_MAIL,
                            type: e.name,
                            message: e.message,
                            line: e.line || e.lineNumber || e.number,
                            file: e.fileName,
                            stack: e.stack
                        };
                        this.log(WA.makeGet(err));
                    } catch (e) {
                        this.log('error on stringify error: ' + e);
                    }
                }
            } else {
                cb.call(scope || window);
            }

            var t2 = +new Date();
            this._timeSum += t2 - t1;
        },

        count: function (param, q) {
            if(!this._counters[param]) {
                this._counters[param] = 0;
            }
            this._counters[param] += (q === undefined ? 1 : q);
            this._storeTask.start();

            if(param == 'submitByButton' || param == 'submitByEnter') {
                var id = ({
                    'otvet': '790314',
                    'webagent': '726182',
                    'e': '726182',
                    'my': '726184',
                    'foto': '726184',
                    'video': '726184',
                    'news': '726185',
                    'maps': '726193',
                    'pogoda': '827834',
                    'health': '827835'
                })[location.host.split('.')[0]];
                if (id) {
                    new Image().src = '//rs.mail.ru/d' + id + '.gif?' + Math.random();
                }
            }
        },

        begin: function (name) {
            this._ts[name] = +new Date();

        },

        end: function (name) {
            if(this._ts[name]) {
                this._intervals.push(name.substr(0, 20) + ':' + (+new Date() - this._ts[name]));
                this._storeTask.start();
            }
        },

        _store: function () {
            if( this._intervals.length ) {
                localStorage['waMonitoringI_' + (+new Date())] = this._intervals.join(',');
                this._intervals = [];
            }

            var isCounterExist = false;
            U.Object.each(this._counters, function () {
                isCounterExist = true;
                return false;
            });
            if( isCounterExist ) {
                localStorage['waMonitoringC_' + (+new Date())] = JSON.stringify(this._counters);
                this._counters = {};
            }
        },

        setStartOccupancy: function () {
            this._isFirstFlush = true;
        },

        _flush: function () {
            if(!usedBranch) {
                return;
            }

            if( this._intervals.length ) {
                this._flushTask.start(5000);
                return;
            }
            
            this.onBeforeFlushEvent.fire();

            var now = +new Date();
            var occupancy = this._timeSum / (now - this._timeStart) * 100;
            this._timeSum = 0;
            this._timeStart = now;


            var parts = [(this._isFirstFlush ? 'occupancyStart:' : 'occupancy:') + occupancy.toFixed(2)];
            this._isFirstFlush = false;
            var intervalsNotEmpty = false;
            
            var deleteList = [];
            var counters = {};
            var dump = [];
            for(var i=0, len = localStorage.length; i < len; i++) {
                var key = localStorage.key(i);
                if(!key) {
                    continue;
                }
                var value = localStorage[key];
                
                if(key.indexOf('waMonitoringI_') == 0) {
                    intervalsNotEmpty = true;
                    var s = parts[parts.length-1] + ',' + value;
                    if(s.length > 1000) {
                        parts.push(value);
                    } else {
                        parts[parts.length-1] = s;
                    }

                    deleteList.push(key);
                } else if(key.indexOf('waMonitoringC_') == 0) {
                    U.Object.each(JSON.parse(value), function (v, k) {
                        if( !counters[k] ) {
                            counters[k] = 0;
                        }
                        counters[k] += v;
                    }, this);

                    deleteList.push(key);
                } else {
                    dump.push([key, value]);
                }
            }

            if(dump.length < deleteList.length) {
                localStorage.clear();
                U.Array.each(dump, function (v) {
                    localStorage[v[0]] = v[1];
                });
            } else {
                U.Array.each(deleteList, function (key) {
                    localStorage.removeItem(key);
                });
            }

            var reqcount = 0, graphiteCount = 0, radarCount = 0;
            if(intervalsNotEmpty) {
                U.Array.each(parts, function (part) {
                    new Image().src = '//webagent.radar.imgsmail.ru/update?p=webagent&t=' + PREFIX + '&v=0&i=' + part + '&rnd=' + Math.random();
                    reqcount++;
                    radarCount++;
                });
            }

//            var buff = GRAPHITE_PREFIX + 'sum=1';
            var buff = '';
            U.Object.each(counters, function (v, k) {
                buff += (buff != '' ? '&' : '') + GRAPHITE_PREFIX + k + '=' + v;
                if(buff.length > 1000) {
                    new Image().src = '//mail.ru/gstat?' + buff + '&rnd=' + Math.random();
                    buff = '';
                    reqcount++;
                    graphiteCount++;
                }
            }, this);
            if(buff != '') {
                new Image().src = '//mail.ru/gstat?' + buff + '&rnd=' + Math.random();
                reqcount++;
                graphiteCount++;
            }

            if(reqcount) {
                this.count('monitoringReqCount', reqcount);
                this.count('monitoringReqCountGraphite', graphiteCount);
                this.count('monitoringReqCountRadar', radarCount);
            }

            this._flushTask.start(FLUSH_DELAY);
        },

        rbCountOpen: function () {
            this._countRB(706711);
        },

        rbCountAction: function () {
            this._countRB(706784);
        },

        _countRB: function (id) {
            var url = '//rs.mail.ru/d' + id + '.gif';
            WA.setTimeout(function(){
                new Image().src = url + '?rnd=' + Math.random();
            }, 0);
        }

    });

    U.Mnt = new M();

})();

WebAgent.namespace('WebAgent.util');

(function () {

    var O = WebAgent.util.Object = {

        each: function (obj, fn, scope) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (fn.call(scope || window, obj[key], key) === false) {
                        return;
                    }
                }
            }
        },

        pair: function (key, value) {
            var o = {};
            o[key] = value;
            return o;
        },

        getKeys: function (obj) {
            var keys = [];
            O.each(obj, function (ignored, key) {
                keys.push(key);
            });
            return keys;
        }

    };

})();WebAgent.namespace('WebAgent.util');

(function () {

    var S = WebAgent.util.String = {

        format: function () {
            var args = WebAgent.toArray(arguments);
            var format = args.shift();
            return format.replace(/\{(\d+)\}/g, function (ignored, i) {
                return args[i];
            });
        },

        capitalize: function (value) {
            return value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
        },

        parseTemplate: function (template, data) {
            return template.replace(/\{([^\}]+)\}/g, function (ignored, p) {
                return data[p];
            });
        },

        pluralize: function (num, zero, one, two) {
            return (num % 10 == 1 && num % 100 !== 11 ? one : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? two : zero));
        },
        
        htmlEntity: function (text) {
            return (text || '').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        },

        entityDecode: function (text) {
            return (text || '').replace(/&lt;/ig, '<').replace(/&gt;/ig, '>').replace(/&quot;/ig, '"');
        },

        ellipsis: function (str, len) {
            if (str.length > len) {
                return str.substr(0, len - 3) + '...';
            } else {
                return str;
            }
        },

        formatDate: function (ts) {
            
            // TODO: COPYPASTA!!!
            
            var tso = new Date(ts-0);
            var msg_date = tso.toLocaleTimeString().replace(/:\d\d(\sgmt.*)?$/i, '');
            if( ts + 86400000 < (new Date()).valueOf() )	{	// 86400000 = 1000*60*60*24
                var splitter = '.';
                var den = tso.getDate();
                var mes = tso.getMonth()+1;
                mes+='';
                if (mes.length==1)
                    mes='0'+mes;
                var god = tso.getYear()+'';
                god = god.slice(1,3);
                msg_date =  den + splitter + mes + splitter + god +' '+ msg_date;
            }
            return msg_date

        },

        quote: (function () {

            var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
                escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
                gap,
                indent,
                meta = {    // table of character substitutions
                    '\b': '\\b',
                    '\t': '\\t',
                    '\n': '\\n',
                    '\f': '\\f',
                    '\r': '\\r',
                    '"' : '\\"',
                    '\\': '\\\\'
                },
                rep;


            return function (string) {
                escapable.lastIndex = 0;
                return escapable.test(string) ?
                    '"' + string.replace(escapable, function (a) {
                        var c = meta[a];
                        return typeof c === 'string' ? c :
                            '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                    }) + '"' :
                    '"' + string + '"';
            }
        })(),

        trim: function (str) {
            return str.replace(/^\s+|\s+$/g, '');
        }

    };

})();WebAgent.namespace('WebAgent.debug');

WebAgent.debug.State = (function () {

    var WA = WebAgent;

    var State = WA.extend(Object, {

        constructor: function () {
            this.items = {};
            this.isReady = false;

            WA.util.DomHelper.whenReady(this._onReady, this);
        },

        _onReady: function () {
            this.isReady = true;

            this.el = WA.getBody().createChild({
                id: 'mailru-webagent-debug-state'
            });

            WA.util.Object.each(this.items, function (item, id) {
                item.el = this._createEl(id, item.value);
            }, this);
        },

        _createEl: function (id, value) {
            return this.el.createChild({
                id: this.id + '-' + id,
                cls: 'wa-debug-state-item',
                children: [
                    {
                        tag: 'strong',
                        html: id + ':&nbsp;'
                    },
                    {
                        tag: 'span',
                        html: value || '&nbsp;'
                    }
                ]
            });
        },

        _addItem: function (id, value) {
            if (this.items[id]) {
                WA.error('Duplicate item: ' + id);
            } else {
                this.items[id] = {
                    id: id,
                    value: value,
                    el: this.isReady ? this._createEl(id, value) : null
                };
            }
        },

        set: function (id, value) {
            var item = this.items[id];
            if (item) {
                item.value = value;
                if (item.el) {
                    item.el.last().update(value);
                }
            } else {
                this._addItem(id, value);
            }
        },

        remove: function (id) {
            var item = this.items[id];
            if (item) {
                item.id = null;
                item.el.remove();
                item.el = null;
                item.value = null;
                this.items[id] = null;
            } else {
                WA.error('Invalid item: ' + id);
            }
        }

    });

    var state = null;

    if (WA.isDebug) {
        state = new State();
    }

    return {

        set: function (id, value) {
            if (state) {
                state.set(id, value);
            }
            return this;
        },

        remove: function (id) {
            if (state) {
                state.remove(id);
            }
            return this;
        }

    };

})();WebAgent.namespace('WebAgent.debug');

(function () {

    var WA = WebAgent;

    var print = function (level, args) {
        return;
        if (WA.isDebug && typeof console !== 'undefined') {
            var m = console[level] || console.log;
            var entries = WebAgent.toArray(args).join(' ');
            if (m && m.call) {
                m.call(console, entries);
            } else {
                if (console[level]) {
                    console[level](entries);
                } else {
                    console.log(entries);
                }
            }
        }
    };

    var Console = WA.debug.Console = {

        log: function () {
            return;
            print.call(Console, 'log', arguments);
        },

        debug: function () {
            print.call(Console, 'debug', arguments);
        },

        info: function () {
            print.call(Console, 'info', arguments);
        },

        warn: function () {
            print.call(Console, 'warn', arguments);
        },

        error: function () {
            print.call(Console, 'error', arguments);
        }

    };

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var U = WA.util;
    var UI = WA.ui;

    UI.Component = WA.extend(Object, {

        isComponent: true,

        constructor: function (config) {
            this.initialConfig = config || {};

            this.id = this.initialConfig.id;

            this.rendered = false;
            this.autoRender = this.initialConfig.autoRender || false;
            this.beforeShowEvent = new WA.util.Event();
            this.beforeShowEvent.on(this._onBeforeShow, this);

            this.beforeHideEvent = new WA.util.Event();
            this.beforeHideEvent.on(this._onBeforeHide, this);

            this.showEvent = new WA.util.Event();
            this.showEvent.on(this._onShow, this);

            this.hideEvent = new WA.util.Event();
            this.hideEvent.on(this._onHide, this);

            this.renderEvent = new WA.util.Event();

            this.initComponent();
        },

        getId: function () {
            if (!this.id) {
                if (this.el) {
                    var id = this.el.getId();
                    if (id) {
                        this.id = id;
                    } else {
                        this.id = this.el.dom.id = WA.generateId();
                    }
                } else {
                    this.id = WA.generateId();
                }
            }
            return this.id;
        },

        getEl: function () {
            return this.el;
        },

        initComponent: function () {
        },

        render: function (ct) {
            this.container = WA.get(ct);
            if (!this.el) {
                if (this.autoEl) {
                    this.el = this.container.createChild(this.autoEl);
                } else {
                    this._onRender(this.container);
                }
            }

            var id = this.el.getId();
            if (id) {
                if (this.id && this.id !== id) {
                    WA.error('Different ids: ' + this.id + ', ' + id);
                } else {
                    this.id = id;
                }
            } else if (this.id) {
                this.el.dom.id = this.id;
            }

            if (this.initialConfig.cls) {
                this.el.addClass(this.initialConfig.cls);
            }

            if (this.initialConfig.fadeIn && !WA.isIE) {
                this.el.addClass('nwa_fadein');
            }

            this.rendered = true;

            this._onAfterRender();

            this.renderEvent.fire(this);
        },

        _onRender: function (ct) {
            WA.abstractError();
        },

        _onAfterRender: function () {
        },

        isVisible: function () {
            return this.rendered && this.el.isVisible();
        },

        setVisible: function (visible) {
            if (!this.rendered && this.autoRender !== false && visible) {
                this.render(WA.isBoolean(this.autoRender) ? WA.getBody() : this.autoRender);
            }
            if (visible !== this.isVisible()) {
                var p = visible ? 'show' : 'hide';

                var beforeEvent = this['before' + U.String.capitalize(p) + 'Event'];
                if (beforeEvent.fire(this) !== false) {
                    this.el.setVisible(visible);

                    if (this.initialConfig.fadeIn && !WA.isIE) {
                        if (visible) {
                            WA.setTimeout(WA.createDelegate(function(){
                                this.el.setStyle('opacity', 1)
                            },this), 10);
                        } else {
                            this.el.setStyle('opacity', 0)
                        }
                    }

                    var event = this[p + 'Event'];
                    event.fire(this);
                }

                return this;
            }
        },

        toggle: function () {
            this.setVisible(!this.isVisible());
        },

        show: function () {
            return this.setVisible(true);
        },

        hide: function () {
            return this.setVisible(false);
        },

        _onBeforeShow: function (me) {
        },

        _onBeforeHide: function (me) {
        },

        _onShow: function (me) {
            if (this.initialConfig.modal) {
                if (WA.isIE) {
                    this.overlayEl = (this.initialConfig.overlay || this.container).createChild({
                        tag: 'div',
                        cls: 'nwa-overlay-div'
                    }, true);
                } else {
                    (this.initialConfig.overlay || this.container).addClass('nwa-overlay');
                }
            }
            if (this.initialConfig.hideOnEscape) {
                WA.getDoc().on('keydown', this._onEscape, this);
            }
        },

        _onHide: function (me) {
            if (this.initialConfig.modal) {
                (this.initialConfig.overlay || this.container).removeClass('nwa-overlay');
                if (this.overlayEl) {
                    this.overlayEl.remove();
                }
            }
            WA.getDoc().un('keydown', this._onEscape, this);
        },

        _onEscape: function (e) {
            if (e.keyCode == 27) {
                this.hide();
            }
        },

        destroy: function () {
            this.initialConfig = null;

            this.beforeShowEvent.removeAll();
            this.beforeShowEvent = null;

            this.beforeHideEvent.removeAll();
            this.beforeHideEvent = null;

            this.showEvent.removeAll();
            this.showEvent = null;

            this.hideEvent.removeAll();
            this.hideEvent = null;

            this.renderEvent.removeAll();
            this.renderEvent = null;

            this.container = null;

            if (this.rendered) {
                this.el.remove();
                this.el = null;
            }
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    UI.Container = WA.extend(UI.Component, {

        defaultType: null,

        initComponent: function () {
            UI.Container.superclass.initComponent.call(this);

            this._initItems();
        },

        _initItems: function () {
            var items = this.initialConfig.items || [];
            delete this.initialConfig.items;

            this.items = [];

            WA.util.Array.each(items, function (item) {
                this.items.push(this._lookupItemComponent(item));
            }, this);
        },

        _lookupItemComponent: function (cmp) {
            return this._lookupComponent(cmp, this.defaultType);
        },

        _lookupComponent: function (cmp, defaultType) {
            if (defaultType && !WA.isFunction(cmp.render)) {
                return new defaultType(cmp);
            } else {
                return cmp;
            }
        },

        _onAfterRender: function () {
            UI.Container.superclass._onAfterRender.call(this);

            WA.util.Array.each(this.items, function (item, index) {
                this._renderItem(this.el, item, index);
            }, this);
        },

        _renderItem: function (container, item, index) {
            item.render(container);
        },

        _destroyItem: function (item) {
            item.destroy();
        },

        destroy: function () {
            while (this.items.length > 0) {
                this._destroyItem(this.items.shift());
            }
            this.items = null;

            UI.Container.superclass.destroy.call(this);
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    UI.Window = WA.extend(UI.Container, {

        initComponent: function () {
            UI.Window.superclass.initComponent.call(this);

            this.closeAction = this.closeAction || this.initialConfig.closeAction || 'destroy';

            this.closeBtn = new WA.ui.Button({
                cls: 'nwa-window__close',
                handler: function () {
                    this.close();
                },
                scope: this
            });

            this._initToolbar('bbar');
        },

        _initToolbar: function (key) {
            var config = this.initialConfig[key];
            if (config) {
                var defaultType = UI.Toolbar;
                if (WA.isArray(config)) {
                    this[key] = this._lookupComponent({ items: config }, defaultType);
                } else {
                    this[key] = this._lookupComponent(config, defaultType);
                }
                this[key].renderEvent.on(function (bar) {
                    bar.el.addClass('nwa-window-' + key);
                });
            }
        },

        _onRender: function (container) {
            var id = this.getId();

            var ids = {
                header: id + '-header',
                body: id + '-body',
                footer: id + '-footer'
            };

            var cssPrefix = 'nwa-window';

            var items = [
                {
                    id: ids.header,
                    cls: cssPrefix + '__header',
                    html: this.initialConfig.title
                },
                {
                    id: ids.body,
                    cls: cssPrefix + '__body'
                }
            ];

            if (this.bbar) {
                items.push({
                    id: ids.footer,
                    cls: cssPrefix + '__footer'
                });
            }

            this.el = container.createChild({
                cls: cssPrefix,
                style: 'display: none',
                children: items
            });

            this.header = WA.get(ids.header);

            if (this.closeBtn) {
                this.closeBtn.render(ids.header);
            }

            this.body = WA.get(ids.body);

            if (this.bbar) {
                this.bbar.render(ids.footer);
            }
        },

        _renderItem: function (container, item, index) {
            UI.Window.superclass._renderItem.call(this, this.body, item, index);
        },

        close: function () {
            this[this.closeAction]();
        },

        destroy: function () {
            if (this.bbar) {
                this.bbar.destroy();
                this.bbar = null;
            }

            UI.Window.superclass.destroy.call(this);
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    UI.Button = WA.extend(UI.Component, {

        initComponent: function () {
            UI.Button.superclass.initComponent.call(this);

            var menu = this.initialConfig.menu;
            if (menu) {
                if (menu.render) {
                    this.menu = menu;
                } else if (WA.isArray(menu)) {
                    this.menu = new UI.menu.Menu({ items: menu });
                } else {
                    this.menu = new UI.menu.Menu(menu);
                }
                delete this.initialConfig.menu;
            }
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'button',
                cls: 'nwa-button',
                title: this.initialConfig.tooltip || '',
                html: this.initialConfig.text
            });
            if (this.menu) {
                this.menu.setControlElement(this.el);
            }
        },

        _onAfterRender: function () {
            UI.Button.superclass._onAfterRender.call(this);

            this.el.on(this.initialConfig.clickEvent || 'click', this._onClick, this);
        },

        _onClick: function (e) {
            if (e.button === 0) {
                if (e && this.initialConfig.preventDefault !== false) {
                    e.preventDefault();
                }

                var handler = this.initialConfig.handler;
                if (handler) {
                    handler.call(this.initialConfig.scope || this, this, e);
                }

                if (this.menu) {
                    if (!this.menu.rendered) {
                        this.menu.render(this.container);
                    }
                    this.menu.toggle();
                }
            }
        },

        setText: function (text) {
            this.el.update(this.initialConfig.text = text);
        },

        getText: function () {
            return this.initialConfig.text;
        },

        destroy: function () {
            this.el.un(this.initialConfig.clickEvent || 'click', this._onClick, this);

            if (this.menu) {
                this.menu.destroy();
                this.menu = null;
            }

            UI.Button.superclass.destroy.call(this);
        }

    });

    UI.ToggleButton = WA.extend(UI.Button, {

        _pressedClassName: 'nwa-button_pressed',
        state: false,

        _onRender: function (ct) {
            UI.ToggleButton.superclass._onRender.call(this, ct);
            if (this.state) {
                this.el.addClass(this._pressedClassName);
            }
        },

        _onClick: function (e) {
            UI.ToggleButton.superclass._onClick.call(this, e);

            var newState = !this.state;
            var stateHandler = this.initialConfig.stateHandler;
            if (stateHandler && stateHandler.call(this.initialConfig.scope || this, newState, this) !== false) {
                this.setState(newState);
            }
        },

        setState: function (newState) {
            this.state = newState;

            this.el.removeClass(this._pressedClassName);

            if (this.state) {
                this.el.addClass(this._pressedClassName);
            }
        }
    });

    UI.LinkButton = WA.extend(UI.Button, {

        _onRender: function (ct) {
            var children = [];

            if (this.initialConfig.iconCls) {
                children.push({
                    tag: 'img',
                    cls: 'nwa-img-button ' + this.initialConfig.iconCls,
                    src: '//img1.imgsmail.ru/0.gif',
                    align: 'top',
                    title: this.initialConfig.tooltip || ''
                });
            }

            children.push({
                tag: 'span',
                html: this.initialConfig.text || ''
            });

            var linkConfig = {
                tag: 'a',
                cls: 'nwa-button',
                href: 'javascript:void(0)',
                children: children
            };

            if (this.initialConfig.linkAttributes) {
                WA.apply(linkConfig, this.initialConfig.linkAttributes);
            }

            this.el = ct.createChild(linkConfig);
        },

        setText: function (text) {
            this.initialConfig.text = text;
            if(this.rendered) {
                var lastEl = this.el.last();
                lastEl.update(this.initialConfig.text);
            }
        },

        setIconCls: function (cls) {
            if(this.rendered) {
                this.el.first().removeClass(this.initialConfig.iconCls).addClass(cls);
            }
            this.initialConfig.iconCls = cls;
        }

    });

    UI.ImageButton = WA.extend(UI.Button, {

        initComponent: function () {
            UI.ImageButton.superclass.initComponent.call(this);

            this.initialConfig.iconCls = this.initialConfig.iconCls || ' ';
        },

        _onRender: function (ct) {
            UI.ImageButton.superclass._onRender.call(this, ct);

            this.el.createChild({
                tag: 'span',
                cls: 'nwa-img-button ' + (this.initialConfig.iconCls || ''),
                title: this.initialConfig.tooltip || '',
                html: '&nbsp;'
            });
        },

        setIconCls: function (cls) {
            if (this.rendered) {
                this.el.first().removeClass(this.initialConfig.iconCls).addClass(cls);
            }
            this.initialConfig.iconCls = cls;
        }

    });

    UI.TabButton = WA.extend(UI.Button, {

        _onRender: function (ct) {

            var buttonConfig = {
                tag: 'div',
                cls: 'nwa-button-tab',
                children: [
                    {
                        tag: 'div',
                        cls: 'nwa-button-tab__inner ',
                        title: this.initialConfig.title || '',
                        children: [
                            {
                                tag: 'span',
                                cls: 'nwa-button-tab__icon ' + (this.initialConfig.defaultIconCls || '') + ' ' + (this.initialConfig.iconCls || '')
                            },
                            {
                                tag: 'span',
                                cls: 'nwa-button-tab__nick',
                                html: (this.initialConfig.text || '')
                            }
                        ]
                    },
                    {
                        tag: 'div',
                        cls: 'nwa-msg-counter'
                    }
                ]
            };

            if (this.initialConfig.closable) {
                buttonConfig.children.push({
                    tag: 'div',
                    cls: 'nwa-button-tab__close nwa_action_close',
                    children: [{
                        tag: 'span'
                    }]
                });
            }

            if (this.initialConfig.fader) {
                buttonConfig.children[0].children.push({tag: 'div',cls: 'nwa-button-tab__nick-fadeout'});
            }

            if (this.initialConfig.tabAttributes) {
                WA.apply(buttonConfig, this.initialConfig.tabAttributes);
            }

            this.el = ct.createChild(buttonConfig);
        },

        // for compatibility
        setState: function(){},

        setText: function (text) {
            this.initialConfig.text = text;
            if (this.rendered) {
                var lastEl = this.el.first().first().next();
                lastEl.update(this.initialConfig.text);
            }
        },

        setTitle: function (title) {
            if(this.rendered) {
                this.el.first().setAttribute('title', title);
            }
        },

        setCount: function (val) {
            if (this.rendered) {
                var countEl = this.el.first().next();
                countEl.update(val);
            }
        },

        setIconCls: function (cls) {
            if(this.rendered) {
                this.el.first().first().removeClass(this.initialConfig.iconCls).addClass(cls);
            }
            this.initialConfig.iconCls = cls;
        },

        setAttr: function (name, value) {
            this.el.setAttribute(name, value);
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;
    var U = WA.util;

    UI.TextField = WA.extend(UI.Component, {

        initComponent: function () {
            UI.TextField.superclass.initComponent.call(this);

            this.hintText = this.initialConfig.hintText || '';
            this.hintClass = this.initialConfig.hintClass || 'nwa_hint_msg';

            this.focusEvent = new WA.util.Event();
            this.blurEvent = new WA.util.Event();
        },

        _onRender: function (ct) {
            var config = this.initialConfig;

            var o;

            if (config.multiline) {
                o = {
                    tag: 'textarea',
                    cls: 'nwa-textfield'
                };
            } else {
                o = {
                    tag: 'input',
                    type: 'text',
                    cls: 'nwa-textfield'
                };
            }

            if (config.maxLength) {
                o.maxlength = config.maxLength;
            }

            this.el = ct.createChild(o);
        },

        val: function (value, preventHint) {
            //get
            if (typeof value === 'undefined') {
                if (this.rendered) {
                    return this.el.dom.value === this.hintText ? '' : this.el.dom.value;
                }
                return null;
            }

            //set
            if (this.rendered) {
                this.el.dom.value = value || '';
                if (preventHint !== true || this.el.dom.value != '') {
                    this._checkHintValue();
                }
            }
        },

        _onAfterRender: function () {
            this.el.dom.value = this.initialConfig.value || '';

            this._checkHintValue();

            if (this.hintText && this.el.dom.value == this.hintText) {
                this.el.addClass(this.hintClass);
            }

            this.el.on('focus', this._onFocus, this);
            this.el.on('blur', this._onBlur, this);
        },

        focus: function () {
            try {
                this.el.dom.focus();
            } catch(e) {}
        },

        blur: function () {
            try {
                this.el.dom.blur();
            } catch(e) {}
        },

        select: function () {
            this.el.dom.select();
        },

        _onFocus: function () {
            if (this.hintText && this.el.dom.value === this.hintText) {
                this.el.dom.value = '';
            }
            this.el.removeClass(this.hintClass);
            this.focusEvent.fire(this);
        },

        _onBlur: function () {
            this._checkHintValue();
            this.blurEvent.fire(this);
        },

        _checkHintValue: function () {
            var domValue = U.String.trim(this.el.dom.value);
            if (this.hintText) {
                if (domValue === '' || domValue == this.hintText) {
                    this.el.addClass(this.hintClass);
                    this.el.dom.value = this.hintText;
                } else {
                    this.el.removeClass(this.hintClass);
                }
            }
        },

        destroy: function () {
            this.focusEvent.removeAll();
            this.focusEvent = null;

            this.blurEvent.removeAll();
            this.blurEvent = null;

            UI.TextField.superclass.destroy.call(this);
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    UI.Layer = WA.extend(UI.Component, {
        constructor: function () {
            UI.Layer.superclass.constructor.apply(this, arguments);
            this.initialConfig.hideOnEscape = true;
        },

        autoEl: {
            tag: 'div',
            style: 'display: none'
        },

        _onShow: function () {
            UI.Layer.superclass._onShow.call(this);

            WA.getBody().on('mousedown', this._onClick, this);
        },

        _onHide: function () {
            UI.Layer.superclass._onHide.call(this);

            WA.getBody().un('mousedown', this._onClick, this);
        },

        _onAncestorClick: function (el, isSelf, e) {
        },

        _onClick: function (e) {
            if (!this.isVisible() || e.button !== 0) {
                return;
            }

            var isAncestor = false;
            var isSelf = false;

            var target = e.getTarget(true);
            if (target) {
                isSelf = this.el.equals(target);
                isAncestor = isSelf || this.el.contains(target);
            }

            if (isAncestor) {
                this._onAncestorClick(target, isSelf, e);
                if (this.initialConfig.hideOnClick) {
                    this.hide();
                }
            } else {
                this.hide();
            }
        }

    });

})();WebAgent.namespace('WebAgent.ui.menu');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;
    var M = UI.menu;

    var Layer = WA.extend(UI.Layer, {

        constructor: function (menu, config) {
            this.menu = menu;
            this.controlEl = config.controlEl || [];
            if (this.controlEl && this.controlEl.length == null) {
                this.controlEl = [this.controlEl];
            }
            Layer.superclass.constructor.call(this, config);
        },

        _onAncestorClick: function (el, isSelf, e) {
            if (!isSelf) {
                var c = el;
                var index = null;
                while (c && !c.equals(this.menu.el)) {
                    index = parseInt(c.getAttribute('menu_index'));
                    if (WA.isNumber(index)) {
                        this.menu.itemClickEvent.fire(this, this.menu.items[index], e);
                        break;
                    } else {
                        c = c.parent();
                    }
                }
            }
        },

        _onClick: function (e) {
            if (this.initialConfig.autoHide === false) {
                return true;
            }

            var target = e.getTarget(true);
            // prevent closing menu by mousedown on control button
            for (var i = 0, len = this.controlEl.length; i < len; i++) {
                if (this.controlEl[i].contains(target)) {
                    return false;
                }
            }

            Layer.superclass._onClick.call(this, e);
        }


    });

    var Menu = M.Menu = WA.extend(UI.Container, {

        initComponent: function () {
            Menu.superclass.initComponent.call(this);

            this.controlEl = this.initialConfig.controlEl || null;
            this.itemClickEvent = new WA.util.Event();
        },

        _onRender: function (container) {
            this.layer = new Layer(this, {
                hideOnClick: this.initialConfig.hideOnClick !== false,
                fadeIn: this.initialConfig.fadeIn === true,
                controlEl: this.controlEl,
                autoHide: this.initialConfig.autoHide !== false
            });
            this.layer.hideEvent.on(function () {
                this.hideEvent.fire();
            },this);

            this.layer.render(container);

            this.el = this.layer.el.createChild({
                cls: 'nwa-menu'
            });
        },

        _lookupItemComponent: function (cmp) {
            if (cmp === '-') {
                return new M.Separator(this, cmp);
            } else if (cmp instanceof M.Item) {
                return cmp;
            } else {
                var defaultType = this.defaultType || M.Button;
                return this._lookupComponent(cmp, defaultType);
            }
        },

        _renderItem: function (container, item, index) {
            var ct = this._getItemRenderContainer();
            Menu.superclass._renderItem.call(this, ct, item, index);
            item.el.setAttribute('menu_index', index);
        },

        _getItemRenderContainer: function () {
            return this.el;
        },

        isVisible: function () {
            return this.layer.isVisible();
        },

        // set control button element
        setControlElement: function (el) {
            this.controlEl = el;
        },

        _onShow: function (me) {
            Menu.superclass._onShow.call(this, me);
            this.layer.show();

            if (WA.isIE) { //fixing bug ":hover state sticks in IE"
                this._reflowItems();
            }
        },

        _reflowItems: function () {
            WA.util.Array.each(this.items, function (item, index) {
                if (item.reflow) item.reflow();
            }, this);
        },

        _onHide: function (me) {
            Menu.superclass._onHide.call(this, me);
            this.layer.hide();
        },

        destroy: function () {
            this.itemClickEvent.removeAll();
            this.itemClickEvent = null;

            this.layer.destroy();
            this.layer = null;

            Menu.superclass.destroy.call(this);
        }

    });

    // ---------------------------------------------------------------------------------

    M.Item = WA.extend(Object, {

        constructor: function (config) {
            this.initialConfig = config;
        },

        render: function (container) {
            this._onRender(container);
            this.el.addClass('nwa-menu-item');
        },

        _onRender: function (container) {
            WA.abstractError();
        },

        destroy: function () {
            this.initialConfig = null;

            if (this.el) {
                this.el.remove();
                this.el = null;
            }
        }

    });

    // ---------------------------------------------------------------------------------

    M.Separator = WA.extend(M.Item, {

        _onRender: function (container) {
            this.el = container.createChild({
                cls: 'nwa-menu-separator'
            });
        }

    });

    // ---------------------------------------------------------------------------------

    M.Button = WA.extend(M.Item, {

        constructor: function (config) {
            var cfg = WA.applyIf({
                clickEvent: 'mousedown'
            }, config);

            this.button = new UI.LinkButton(cfg);

            M.Button.superclass.constructor.call(this, cfg);
        },

        _onRender: function (container) {
            this.el = container.createChild({
                cls: 'nwa-menu-button'
            });

            this.button.render(this.el);
        },

        reflow: function (container) {
            if (this.button) {
                this.button.destroy();
                this.button = new UI.LinkButton(this.initialConfig);
                this.button.render(this.el);
            }
        },

        destroy: function () {
            this.button.destroy();
            this.button = null;

            M.Button.superclass.destroy.call(this);
        }

    });

    M.Image = WA.extend(M.Item, {

        _onRender: function (container) {
            var config = this.initialConfig;

            var o = {
                tag: 'div',
                title: config.title
            };

            if (config.path) {
                o.style = 'background-image: url(' + config.path + ')';
            }

            if (config.cls) {
                o.cls = config.cls;
            }

            this.el = container.createChild(o);
        }

    });

})();WebAgent.namespace('WebAgent.ui.menu');

(function () {

    var WA = WebAgent;
    var M = WA.ui.menu;

    M.ImageMenu = WA.extend(M.Menu, {

        defaultType: M.Image,

        _onAfterRender: function () {
            M.ImageMenu.superclass._onAfterRender.call(this);

            this.el.addClass('nwa-image-menu');

            var ct = this._getItemRenderContainer();
            ct.createChild({ cls: 'nwa-clear-both' });
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    UI.Toolbar = WA.extend(UI.Container, {

        defaultType: UI.Button,

        _onRender: function (ct) {
            this.el = ct.createChild({
                cls: 'wa-toolbar',
                tag: 'table',
                cellspacing: 0,
                children: [
                    {
                        tag: 'tr',
                        id: this.getId() + '-row'
                    }
                ]
            });

            this.rowEl = WA.get(this.getId() + '-row');
        },

        _lookupItemComponent: function (cmp) {
            if (WA.isString(cmp)) {
                return cmp;
            } else {
                return UI.Toolbar.superclass._lookupItemComponent.call(this, cmp);
            }
        },

        _renderItem: function (container, item, index) {
            var cls = ['wa-toolbar-item'];
            if (index === 0) {
                cls.push('wa-toolbar-item-first');
            } else if (index === this.items.length - 1) {
                cls.push('wa-toolbar-item-last');
            }

            var ct = this.rowEl.createChild({
                tag: 'td',
                cls: cls.join(' ')
            });

            if (item === '->') {
                ct.addClass('wa-toolbar-spacer');
            } else {
                UI.Toolbar.superclass._renderItem.call(this, ct, item, index);
            }
        },

        _destroyItem: function (item) {
            if (item.isComponent) {
                item.container.remove();
                UI.Toolbar.superclass._destroyItem.call(this, item);
            }
        }

    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    var MP3_URL = '//img.imgsmail.ru/r/webagent/images/message.mp3';
    var OGG_URL = '//img.imgsmail.ru/r/webagent/images/message.ogg';
    var SWF_URL = '//img.imgsmail.ru/r/webagent/images/message2.swf';

    var Beep = WA.extend(WA.ui.Component, {

        _onRender: function (ct) {
            this.el = ct.createChild({
                style: 'position:absolute; left: -10000px',
                children: [
                    {
                        tag: 'audio',
                        preload: 'auto',
                        autobuffer: 'true',
                        children: [
                            {
                                tag: 'source',
                                src: MP3_URL,
                                type: 'audio/mp3'
                            },
                            {
                                tag: 'source',
                                src: OGG_URL,
                                type: 'audio/ogg'
                            }
                        ]
                    }
                ]
            });
            this._audio = this.el.first();
        },

        play: function () {
            if(this._audio && this._audio.dom.play) {
                this._audio.dom.play();
            }
        }

    });

    var BeepViaFlash = WA.extend(Beep, {

        _onRender: function (ct) {
            this.el = ct.createChild({
                style: 'position:absolute; left: -10000px',
                children: [
                    {
                        tag: 'object',
                        width: '1',
                        height: '1',
                        type: 'application/x-shockwave-flash',
                        data: SWF_URL,
                        children: [
                            {
                                tag: 'param',
                                name: 'allowScriptAccess',
                                value: 'always'
                            },
                            {
                                tag: 'param',
                                name: 'movie',
                                value: SWF_URL
                            }
                        ]
                    }
                ]
            });
            this._obj = this.el.first();
        },

        _play: function () {
            try {
                if(this._obj) {
                    this._obj.dom.beep();
                }
            } catch(e) {
            }
        },

        play: function () {
            WA.setTimeout(this._play, 1, this);
        }

    });

    if (window.Audio) {
        UI.Beep = Beep;
    } else {
        UI.Beep = BeepViaFlash;
    }

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;
    var U = WA.util;

    UI.Collection = WA.extend(UI.Component, {

        initComponent: function () {
            this.children = this.initialConfig;
        },

        append: function (name, component) {
            this.children[name] = component;
        },

        _onRender: function (ct) {
            this.el = ct;
            U.Object.each(this.children, function (component) {
                component.render(ct);
            }, this)
        },

        each: function (fn, scope) {
            U.Object.each(this.children, fn, scope);
        }
    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var UI = WA.ui;

    UI.Label = WA.extend(UI.Component, {

        initComponent: function () {
            this.title = this.initialConfig.title;
            this.child = this.initialConfig.child;
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                cls: 'nwa-twocol__row',
                children: [
                    {
                        cls: 'nwa-twocol__label',
                        children: this.title
                    },
                    {
                        cls: 'nwa-twocol__value'
                    }
                ]
            });
            this.child.render(this.el.first().next());

        }
    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent;
    var U = WA.util;
    var UI = WA.ui;

    UI.Select = WA.extend(UI.Component, {

        initComponent: function () {
            this.options = this.initialConfig.options || [];
            this.changeEvent = new U.Event();
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'select'
            });
            //this.el.parent().on('change', this._onChangeEvent, this);
            this._renderOptions();
            this.setValue(this.initialConfig.defaultValue || '-1');
            
            if(this.initialConfig.attributes) {
                U.Object.each(this.initialConfig.attributes, function (value, key) {
                    this.el.setAttribute(key, value);
                }, this)
            }
        },

        _onChangeEvent: function () {
            var value = this.getValue();
            if(this._value != value) {
                this._value = value;
                this.changeEvent.fire(value);
            }
        },

        __renderList: function (list) {
            var html = [];
            if(list.length && WA.isIE) {
                html.push(U.String.format(
                    '<option value="{1}" style="display:none">{0}</option>',
                    list[0][0],
                    list[0][1]
                ));
            }
            U.Array.each(list, function (option) {
                html.push(U.String.format(
                    '<option value="{1}">{0}</option>',
                    option[0],
                    option[1]
                ));
            }, this);
            return html;
        },

        _renderOptions: function () {
            var html = this.__renderList(this.options);
            this.el.update(html.join(''));

            //hotfix for ie
            this.el.un('change', this._onChangeEvent, this);
            var parent = this.el.parent();
            html = parent.dom.innerHTML;
            this.el.remove();
            this.el = parent.createChild(html);
            this.el.on('change', this._onChangeEvent, this);
        },

        update: function (list) {
            this.options = list;
            this._renderOptions();
        },

        setValue: function (value) {
            if(this.getValue != value) {
                this.el.dom.value = value;
                this._value = value;
                return true;
            }
            return false;
        },

        getValue: function () {
            return this.el.dom.value;
        },

        val: function () {
            return this.getValue();
        },

        getTitle: function () {
            return (this.el.dom[this.el.dom.selectedIndex] || {innerHTML: ''}).innerHTML;
        },

        disable: function () {
            this.el.update('').dom.disabled = true;
        },

        enable: function () {
            this.el.dom.disabled = false;
        }
    });

})();WebAgent.namespace('WebAgent.ui');

(function () {

    var WA = WebAgent,
        UI = WA.ui,

        MIN_SLIDER_SIZE = 30, //px
        WHEEL_STEP = 40; //px

    UI.ScrollBar = WA.extend(UI.Component, {
        initComponent: function () {
            UI.ScrollBar.superclass.initComponent.call(this);

            this.sourceEl = this.initialConfig.source || null;
            this.sliderSize = 0;
            this.sliderStart = 0;
            this.contentSpace = 0;
            this.mouseStart = 0;
            this.trackSize = 0;
            this.sliderSpace = 0;
            this.rate = 1;
            this.deferredUpdate = false;
        },

        render: function (ct) {
            if (!UI.ScrollBar.hasNativeSupport()) {
                this.sourceEl = null;
                return; //fallback to native scroll
            }

            UI.ScrollBar.superclass.render.call(this, ct);
        },

        _onRender: function (ct) {
            var config = this.initialConfig;

            this.container = ct;
            this.sourceEl = this.sourceEl || ct;

            this.sourceEl.addClass('nwa-scrollbar__source');

            this.el = ct.createChild({
                tag: 'div',
                style: 'display:none',
                cls: 'nwa-scrollbar',
                children: [{
                    tag: 'div',
                    cls: 'nwa-scrollbar__view',
                    children: {
                        tag: 'div',
                        cls: 'nwa-scrollbar__slider',
                        children: {
                            tag: 'div',
                            cls: 'nwa-scrollbar__slider-view'
                        }
                    }
                }]
            }, true);

            this.track = this.el.first();
            this.slider = this.track.first();

            this._initEvents();
        },

        _initEvents: function() {
            this.slider.on('mousedown', this._start, this);
            this.track.on('mousedown', this._drag, this);

            this.container.on('DOMMouseScroll', this._wheel, this);
            this.container.on('mousewheel', this._wheel, this);

            if (this.initialConfig.watchResize) {
                WA.get(window).on('afterresize', this.sync, this);
            }
        },

        _start: function (e) {
            e.preventDefault();
            this.dragging = true;
            this.el.addClass('nwa-scrollbar_captured');
            this.mouseStart = e.pageY;
            this.sliderStart = parseInt(this.slider.dom.style.top);
            WA.getDoc().on('mousemove', this._drag, this);
            WA.getDoc().on('mouseup', this._end, this);
            this.slider.on('mouseup', this._end, this);

            return false;
        },

        _drag: function(e) {
            if (e.type == 'mousedown') {
                if (this.dragging) return false;
                this.sliderStart = - Math.floor(this.sliderSize / 2);
                this.mouseStart = this.track.offset()['top'];
            } else {
                e.preventDefault();
            }
            if (this.rate < 1) {
                var sliderTop = Math.min(this.sliderSpace, Math.max(0, this.sliderStart + e.pageY - this.mouseStart));
                this._scrollContent(sliderTop * this.scrollRatio);
                this._moveSlider(sliderTop);
            }

            return false;
        },

        _end: function (e) {
            WA.getDoc().un('mousemove', this._drag, this);
            WA.getDoc().un('mouseup', this._end, this);
            this.slider.un('mouseup', this._end, this);
            this.el.removeClass('nwa-scrollbar_captured');
            this.dragging = false;
            if (this.deferredUpdate) {
                this._onSourceUpdate();
            }

            return false;
        },

        _wheel: function (e) {
            if (this.rate >= 1) return false;

            e.preventDefault();
            e = e.browserEvent;
            var delta = e.wheelDelta ? e.wheelDelta / 120 : -e.detail / 3;
            var deltaScroll = Math.min(this.contentSpace, Math.max(0, this.sourceEl.dom.scrollTop - delta * WHEEL_STEP));
            this._scrollContent(deltaScroll);
            this._onSourceUpdate();
        },

        sync: function () {
            if (this.dragging) {
                this.deferredUpdate = true;
                return;
            }
            this._onSourceUpdate();
        },

        setSource: function (sourceEl) {
            if (this.sourceEl) {
                this.sourceEl.removeClass('nwa-scrollbar__source');
            }
            this.sourceEl = sourceEl;
            this.sourceEl.addClass('nwa-scrollbar__source');
            this._onSourceUpdate();
        },

        _onSourceUpdate: function () {
            this.deferredUpdate = false;

            if (!this.sourceEl) {
                return;
            }

            var contentView = this.sourceEl.dom.offsetHeight,
                contentSize = this.sourceEl.dom.scrollHeight;

            this.rate = contentView / contentSize;

            if (this.rate >= 1) {
                this.hide();
                return;
            }

            this.show();

            this.trackSize = this.track.dom.clientHeight;

            if (this.trackSize < MIN_SLIDER_SIZE) {
                this.hide();
                return;
            }

            this.sliderSize = Math.max(MIN_SLIDER_SIZE, Math.floor(this.trackSize * this.rate));
            this.sliderSpace = this.trackSize - this.sliderSize;
            this.contentSpace = contentSize - contentView;
            this.scrollRatio = this.contentSpace / this.sliderSpace;
            var sliderTop = Math.min(Math.floor(this.sourceEl.dom.scrollTop * this.sliderSpace / this.contentSpace), this.sliderSpace);

            this.slider.setHeight(this.sliderSize);
            this._moveSlider(sliderTop);
        },

        _moveSlider: function (delta) {
            this.slider.setStyle('top', delta + 'px');
        },

        _scrollContent: function (delta) {
            this.sourceEl.dom.scrollTop = delta;
        },

        destroy: function () {
            UI.ScrollBar.superclass.destroy.call(this);
        }

    });

    UI.ScrollBar.hasNativeSupport = function () {
        return !!("getBoundingClientRect" in document.documentElement );
    }

})();WebAgent.namespace('WebAgent.data');

(function () {

    var WA = WebAgent;
    var U = WA.util;
    var ArrayUtils = U.Array;

    WA.data.Record = WA.extend(Object, {

        constructor: function (fields, data) {
            this.fields = fields;
            this.data = [];

            ArrayUtils.each(this.fields, function (fieldName) {
                this.data.push(data[fieldName]);
            }, this);
        },

        hasField: function (fieldName) {
            return ArrayUtils.indexOf(this.fields, fieldName) != -1;
        },

        get: function (fieldName) {
            var index = ArrayUtils.indexOf(this.fields, fieldName);
            if (index != -1) {
                return this.getAt(index);
            } else {
                return null;
            }
        },

        getAt: function (index) {
            return this.data[index];
        }

    });

})();WebAgent.namespace('WebAgent.data');

(function () {

    WebAgent.data.SmileSets = ['set03', 'set01', 'animated_set', 'static_png', 'set04', 'set06', 'set05'];

    WebAgent.data.SmileData =
/******* FETCHED DATA START ****/
    {"set03":[{"id":"400","name":"set03_obj01","alt":":���������:","tip":"���������"},{"id":"401","name":"set03_obj02","alt":":������:","tip":"������"},{"id":"402","name":"set03_obj03","alt":":������_��_����:","tip":"������_��_����"},{"id":"403","name":"set03_obj04","alt":":��������:","tip":"��������"},{"id":"404","name":"set03_obj05","alt":":��������:","tip":"��������"},{"id":"405","name":"set03_obj06","alt":":�����:","tip":"�����"},{"id":"406","name":"set03_obj07","alt":":������������:","tip":"������������"},{"id":"407","name":"set03_obj08","alt":":������:","tip":"������"},{"id":"408","name":"set03_obj09","alt":":���������_�����:","tip":"���������_�����"},{"id":"409","name":"set03_obj10","alt":":�_�����:","tip":"�_�����"},{"id":"410","name":"set03_obj11","alt":":����:","tip":"����"},{"id":"411","name":"set03_obj12","alt":":��_���!:","tip":"��_���!"},{"id":"412","name":"set03_obj13","alt":":����_��������:","tip":"����_��������"},{"id":"413","name":"set03_obj14","alt":":��!:","tip":"��!"},{"id":"414","name":"set03_obj15","alt":":�����:","tip":"�����"},{"id":"415","name":"set03_obj16","alt":":����������:","tip":"����������"},{"id":"416","name":"set03_obj17","alt":":�������:","tip":"�������"},{"id":"417","name":"set03_obj18","alt":":������ ������:","tip":"������_������"},{"id":"418","name":"set03_obj19","alt":":��-�:","tip":"��-�"},{"id":"419","name":"set03_obj20","alt":":������:","tip":"������"},{"id":"420","name":"set03_obj21","alt":":����� �� �����:","tip":"�����_��_�����"},{"id":"421","name":"set03_obj22","alt":":������:","tip":"������"},{"id":"422","name":"set03_obj23","alt":":(","tip":"� ������"},{"id":"423","name":"set03_obj24","alt":":���� ��������:","tip":"���� ��������"},{"id":"424","name":"set03_obj25","alt":"o_O","tip":"��, �"},{"id":"425","name":"set03_obj26","alt":":�����:","tip":"�����"},{"id":"426","name":"set03_obj27","alt":":)","tip":"��������"},{"id":"427","name":"set03_obj28","alt":":�� ����:","tip":"�� ����"},{"id":"428","name":"set03_obj29","alt":"%)","tip":"� �����������"},{"id":"429","name":"set03_obj30","alt":";)","tip":"����������"},{"id":"430","name":"set03_obj31","alt":":�����!:","tip":"�����!"},{"id":"431","name":"set03_obj32","alt":":�����:","tip":"�����"},{"id":"432","name":"set03_obj33","alt":":������:","tip":"������"}],"set01":[{"id":"010","name":"set01_obj01","alt":"<###20###img010>","tip":":)"},{"id":"011","name":"set01_obj02","alt":"<###20###img011>","tip":";)"},{"id":"012","name":"set01_obj03","alt":"<###20###img012>","tip":":-))"},{"id":"013","name":"set01_obj04","alt":"<###20###img013>","tip":";-P"},{"id":"014","name":"set01_obj05","alt":"<###20###img014>","tip":"8-)"},{"id":"015","name":"set01_obj06","alt":"<###20###img015>","tip":":-D"},{"id":"016","name":"set01_obj07","alt":"<###20###img016>","tip":"}:o)"},{"id":"017","name":"set01_obj08","alt":"<###20###img017>","tip":"$-)"},{"id":"018","name":"set01_obj09","alt":"<###20###img018>","tip":":-'"},{"id":"019","name":"set01_obj10","alt":"<###20###img019>","tip":":-("},{"id":"020","name":"set01_obj11","alt":"<###20###img020>","tip":"8-("},{"id":"021","name":"set01_obj12","alt":"<###20###img021>","tip":":'("},{"id":"022","name":"set01_obj13","alt":"<###20###img022>","tip":":''()"},{"id":"023","name":"set01_obj14","alt":"<###20###img023>","tip":"S:-o"},{"id":"024","name":"set01_obj16","alt":"<###20###img024>","tip":"(:-o"},{"id":"025","name":"set01_obj17","alt":"<###20###img025>","tip":"8-0"},{"id":"026","name":"set01_obj18","alt":"<###20###img026>","tip":"8-[o]"},{"id":"027","name":"set01_obj19","alt":"<###20###img027>","tip":"):-p"},{"id":"028","name":"set01_obj20","alt":"<###20###img028>","tip":"):-("},{"id":"029","name":"set01_obj21","alt":"<###20###img029>","tip":"):-$"},{"id":"030","name":"set01_obj22","alt":"<###20###img030>","tip":"):-D"},{"id":"031","name":"set01_obj24","alt":"<###20###img031>","tip":":-E"},{"id":"032","name":"set01_obj25","alt":"<###20###img032>","tip":"��������"},{"id":"033","name":"set01_obj26","alt":"<###20###img033>","tip":"���������"},{"id":"034","name":"set01_obj27","alt":"<###20###img034>","tip":":-]["},{"id":"035","name":"set01_obj28","alt":"<###20###img035>","tip":":-|"},{"id":"036","name":"set01_obj29","alt":"<###20###img036>","tip":"B-j"},{"id":"037","name":"set01_obj30","alt":"<###20###img037>","tip":":~o"},{"id":"038","name":"set01_obj31","alt":"<###20###img038>","tip":"(_I_)"},{"id":"039","name":"set01_obj32","alt":"<###20###img039>","tip":"������"},{"id":"040","name":"set01_obj33","alt":"<###20###img040>","tip":":-*"},{"id":"041","name":"set01_obj34","alt":"<###20###img041>","tip":"����"},{"id":"000","name":"set01_obj41","alt":"<###20###img000>","tip":"�������!"},{"id":"001","name":"set01_obj42","alt":"<###20###img001>","tip":"Peace!"},{"id":"002","name":"set01_obj43","alt":"<###20###img002>","tip":"OK"},{"id":"003","name":"set01_obj44","alt":"<###20###img003>","tip":"����� \"����\""},{"id":"004","name":"set01_obj45","alt":"<###20###img004>","tip":"������ \"����\""},{"id":"005","name":"set01_obj46","alt":"<###20###img005>","tip":"�����"},{"id":"006","name":"set01_obj47","alt":"<###20###img006>","tip":"��������!"},{"id":"007","name":"set01_obj48","alt":"<###20###img007>","tip":"����"},{"id":"008","name":"set01_obj49","alt":"<###20###img008>","tip":"�����"},{"id":"009","name":"set01_obj50","alt":"<###20###img009>","tip":"������!"}],"animated_set":[{"id":"200","name":"animated_set_obj01","alt":":���������:","tip":"���������"},{"id":"201","name":"animated_set_obj02","alt":":������������:","tip":"������������"},{"id":"202","name":"animated_set_obj03","alt":":��������:","tip":"��������"},{"id":"203","name":"animated_set_obj04","alt":":����:","tip":"����"},{"id":"204","name":"animated_set_obj05","alt":":�����:","tip":"�����"},{"id":"205","name":"animated_set_obj06","alt":":̸����:","tip":"̸����"},{"id":"206","name":"animated_set_obj07","alt":":�����:","tip":"�����"},{"id":"207","name":"animated_set_obj08","alt":":������:","tip":"������"},{"id":"208","name":"animated_set_obj09","alt":":��������� �����:","tip":"���������_�����"},{"id":"209","name":"animated_set_obj10","alt":":���:","tip":"���"},{"id":"210","name":"animated_set_obj11","alt":":�����:","tip":"�����"},{"id":"211","name":"animated_set_obj12","alt":":������:","tip":"������"},{"id":"212","name":"animated_set_obj13","alt":":���� ��������:","tip":"���� ��������"},{"id":"213","name":"animated_set_obj14","alt":":������:","tip":"������"},{"id":"214","name":"animated_set_obj15","alt":":������ � �����:","tip":"������ � �����"},{"id":"215","name":"animated_set_obj16","alt":":�������:","tip":"�������"},{"id":"216","name":"animated_set_obj17","alt":":�����:","tip":"�����"},{"id":"217","name":"animated_set_obj18","alt":":�����:","tip":"�����"},{"id":"218","name":"animated_set_obj19","alt":":�����:","tip":"�����"},{"id":"219","name":"animated_set_obj20","alt":":��������:","tip":"��������"},{"id":"220","name":"animated_set_obj21","alt":":����� ���:","tip":"�����_���"},{"id":"221","name":"animated_set_obj22","alt":":���������:","tip":"���������"},{"id":"222","name":"animated_set_obj23","alt":":��������:","tip":"��������"},{"id":"223","name":"animated_set_obj24","alt":":�������������:","tip":"�������������"},{"id":"224","name":"animated_set_obj25","alt":":���:","tip":"���"},{"id":"225","name":"animated_set_obj26","alt":":������:","tip":"������"},{"id":"226","name":"animated_set_obj27","alt":":�������:","tip":"�������"},{"id":"227","name":"animated_set_obj28","alt":":��������:","tip":"��������"},{"id":"228","name":"animated_set_obj29","alt":":��������� ����:","tip":"���������_����"},{"id":"229","name":"animated_set_obj30","alt":":Peace!:","tip":"Peace!"},{"id":"230","name":"animated_set_obj31","alt":":���������:","tip":"���������"},{"id":"231","name":"animated_set_obj32","alt":":������:","tip":"������"}],"static_png":[{"id":"300","name":"static_png_obj01","alt":":��������:","tip":"��������"},{"id":"301","name":"static_png_obj02","alt":":�����������:","tip":"�����������"},{"id":"302","name":"static_png_obj03","alt":":�������:","tip":"�������"},{"id":"303","name":"static_png_obj04","alt":":��������:","tip":"��������"},{"id":"304","name":"static_png_obj05","alt":":�����������:","tip":"�����������"},{"id":"305","name":"static_png_obj06","alt":":�������:","tip":"�������"},{"id":"306","name":"static_png_obj07","alt":":��������� ����:","tip":"��������� ����"},{"id":"307","name":"static_png_obj08","alt":":�����:","tip":"�����"},{"id":"308","name":"static_png_obj09","alt":":���������:","tip":"���������"},{"id":"309","name":"static_png_obj10","alt":":�����:","tip":"�����"},{"id":"310","name":"static_png_obj11","alt":":���������:","tip":"���������"},{"id":"311","name":"static_png_obj12","alt":":��������� �����:","tip":"��������� �����"},{"id":"312","name":"static_png_obj13","alt":":�������������:","tip":"�������������"},{"id":"313","name":"static_png_obj14","alt":":����:","tip":"����"},{"id":"314","name":"static_png_obj15","alt":":����� �� �����:","tip":"����� �� �����"},{"id":"315","name":"static_png_obj16","alt":":����������:","tip":"����������"},{"id":"316","name":"static_png_obj17","alt":":�����:","tip":"�����"},{"id":"317","name":"static_png_obj18","alt":":�����:","tip":"�����"},{"id":"318","name":"static_png_obj19","alt":":��������:","tip":"��������"},{"id":"319","name":"static_png_obj20","alt":":�����:","tip":"�����"},{"id":"320","name":"static_png_obj21","alt":":������ ����������:","tip":"������ ����������"},{"id":"321","name":"static_png_obj22","alt":":�����:","tip":"�����"},{"id":"322","name":"static_png_obj23","alt":":������:","tip":"������"},{"id":"323","name":"static_png_obj24","alt":":������������:","tip":"������������"},{"id":"324","name":"static_png_obj25","alt":":�����:","tip":"�����"},{"id":"325","name":"static_png_obj26","alt":":����������:","tip":"����������"},{"id":"326","name":"static_png_obj27","alt":":��������:","tip":"��������"},{"id":"327","name":"static_png_obj28","alt":":���������:","tip":"���������"},{"id":"328","name":"static_png_obj29","alt":":�������������:","tip":"�������������"},{"id":"329","name":"static_png_obj30","alt":":���:","tip":"���"},{"id":"330","name":"static_png_obj31","alt":":��������:","tip":"��������"},{"id":"331","name":"static_png_obj32","alt":":������:","tip":"������"}],"set04":[{"id":"501","name":"set04_obj01","alt":":���!:","tip":"���!"},{"id":"502","name":"set04_obj02","alt":":������!:","tip":"������!"},{"id":"503","name":"set04_obj03","alt":":�����:","tip":"�����"},{"id":"504","name":"set04_obj04","alt":":������:","tip":"������"},{"id":"505","name":"set04_obj05","alt":":�����:","tip":"�����"},{"id":"506","name":"set04_obj06","alt":":����!:","tip":"����!"},{"id":"507","name":"set04_obj07","alt":":������ ������:","tip":"������_������"},{"id":"508","name":"set04_obj08","alt":":������:","tip":"������"},{"id":"509","name":"set04_obj09","alt":":�����!:","tip":"�����!"},{"id":"510","name":"set04_obj10","alt":":��!:","tip":"��!"},{"id":"511","name":"set04_obj11","alt":":������� �� �����:","tip":"�������_��_�����"},{"id":"512","name":"set04_obj12","alt":":� �����!:","tip":"�_�����!"},{"id":"513","name":"set04_obj13","alt":":��������:","tip":"��������"},{"id":"514","name":"set04_obj14","alt":":���!:","tip":"���!"},{"id":"515","name":"set04_obj15","alt":":���!:","tip":"���!"},{"id":"516","name":"set04_obj16","alt":":���������:","tip":"���������"}],"set06":[{"id":"702","name":"set06_obj02","alt":":�������:","tip":"�������"},{"id":"703","name":"set06_obj03","alt":":� �������:","tip":"� �������"},{"id":"704","name":"set06_obj04","alt":":������� � ���������:","tip":"������� � ���������"},{"id":"708","name":"set06_obj08","alt":":������ �����:","tip":"������ �����"},{"id":"709","name":"set06_obj09","alt":":��������:","tip":"��������"},{"id":"710","name":"set06_obj10","alt":":�������:","tip":"�������"},{"id":"711","name":"set06_obj11","alt":":��������:","tip":"��������"},{"id":"712","name":"set06_obj12","alt":":������:","tip":"������"},{"id":"713","name":"set06_obj13","alt":":�������:","tip":"�������"},{"id":"714","name":"set06_obj14","alt":":��������:","tip":"��������"},{"id":"715","name":"set06_obj15","alt":":���:","tip":"���"},{"id":"716","name":"set06_obj16","alt":":ϸ�:","tip":"ϸ�"},{"id":"717","name":"set06_obj17","alt":":���� � �������:","tip":"���� � �������"},{"id":"718","name":"set06_obj18","alt":":�������:","tip":"�������"},{"id":"719","name":"set06_obj19","alt":":�����:","tip":"�����"},{"id":"720","name":"set06_obj20","alt":":�����:","tip":"�����"}],"set05":[{"id":"601","name":"set05_obj01","alt":":������ �� �����:","tip":"�����_��_�����"},{"id":"602","name":"set05_obj02","alt":"������","tip":"���������_�����"},{"id":"603","name":"set05_obj03","alt":":������:","tip":"������"},{"id":"604","name":"set05_obj04","alt":":������ ��������:","tip":"� �����������"},{"id":"605","name":"set05_obj05","alt":":������:","tip":"������"},{"id":"606","name":"set05_obj06","alt":":�����:","tip":"�����"},{"id":"607","name":"set05_obj07","alt":":��������:","tip":"�����"},{"id":"608","name":"set05_obj08","alt":":�����:","tip":"�����"},{"id":"609","name":"set05_obj09","alt":":����������:","tip":"����������"},{"id":"610","name":"set05_obj10","alt":":�������:","tip":"�������"},{"id":"611","name":"set05_obj11","alt":":�������:","tip":"�������"},{"id":"612","name":"set05_obj12","alt":":������:","tip":"������"},{"id":"613","name":"set05_obj13","alt":":�������������:","tip":"�������������"},{"id":"614","name":"set05_obj14","alt":":����:","tip":"����"},{"id":"615","name":"set05_obj15","alt":":��, �!:","tip":"��, �"},{"id":"616","name":"set05_obj16","alt":":������:","tip":"������"},{"id":"617","name":"set05_obj17","alt":":������:","tip":"������"},{"id":"618","name":"set05_obj18","alt":":������:","tip":"������"},{"id":"619","name":"set05_obj19","alt":":���� ��������:","tip":"����_��������"},{"id":"620","name":"set05_obj20","alt":":����������:","tip":"����������"}]}
/******* FETCHED DATA END ******/
;

})();WebAgent.namespace('WebAgent.rpc');

(function () {

    var WA = WebAgent;

    var rpc;
    var baseRemoteEvent = new WA.util.Event();

    // -----------------------------------------------------------------------------------------------

    var onError = function (e) {
        WA.error(e);
    };

    // -----------------------------------------------------------------------------------------------

    var Rpc = WA.extend(Object, {

        constructor: function (id) {
            this.id = id;

            this.remoteEvent = new WA.util.Event();

            baseRemoteEvent.on(this._onBaseRemoteEvent, this);
        },

        _onBaseRemoteEvent: function (options) {
            var id = options.id;
            if (id === this.id) {
                this.remoteEvent.fire(options.method, options.params);
                return false;
            }
        },

        invoke: function (method, params, successFn, errorFn) {
            if (rpc) {
                var options = {
                    id: this.id,
                    method: method,
                    params: params
                };

                rpc.invoke(options, successFn || WA.emptyFn, errorFn || onError);
            } else if (WA.isFunction(errorFn)) {
                try {
                    WA.error('RPC isn\'t ready yet');
                } catch (e) {
                    errorFn(e);
                }
            }
        }

    });

    // -----------------------------------------------------------------------------------------------

    var inited = false;
    var onReadyEvent = new WA.util.Event();
    var isReady = false;

    WA.rpc.Local = {

        createRpc: function (id) {
            return new Rpc(id);
        },

        init: function () {
            if (!inited) {
                inited = true;

                var path;
                if(false && (WA.isLocalhost || WA.testServer)) {    // local or relate branch storage scope
                    path = WA.resPath + '/rpc.html?' + WA.resProps;
                } else {
                    path = (''+document.location).match(/^[^:]+/)[0] + '://' + WA.SAFE_ACTIVE_MAIL + '.webagent.mail.ru/ru/images/webagent/rpc.html?' + WA.resProps;
                }

                var config = {
                    remote: path
                };

                rpc = new easyXDM.Rpc(config, {

                    local: {

                        invoke: function (options) {
                            baseRemoteEvent.fire(options);
                        }

                    },

                    remote: {
                        invoke: {
                        }
                    }

                });

                easyXDM.whenReady(function () {
                    isReady = true;
                    onReadyEvent.fire();
                });
            }
        },

        whenReady: function (fn, scope) {
            if (isReady) {
                fn.call(scope || window);
            } else {
                onReadyEvent.on(fn, scope, { single: true });
            }
        }

    };

})();(function () {

    var WA = WebAgent;
    var Storage = WA.extend(Object, {

        constructor: function (invoker) {
            this.rpc = WA.rpc.Local.createRpc(invoker || 'Storage');
        },

        init: function () {
            WA.rpc.Local.init();
        },

        whenReady: function (fn, scope) {
            WA.setTimeout(function () {
                WA.rpc.Local.whenReady(fn, scope);
            }, 1, this);
        },

        _onError: function (e) {
            WA.error(e);
        },

        _createDelegatedCallbacks: function (callback) {
            var createDelegate = WA.createDelegate;
            var onError = createDelegate(this._onError, this);
            var cb;

            callback = callback || {};

            if (WA.isFunction(callback)) {
                cb = {
                    success: callback,
                    failure: onError
                };
            } else {
                cb = {
                    success: callback.success || WA.emptyFn,
                    failure: callback.failure || onError,
                    scope: callback.scope
                };
            }

            var scope = callback.scope || window;

            var successFn = createDelegate(cb.success, scope);
            var errorFn = createDelegate(cb.failure, scope);

            return {
                successFn: successFn,
                errorFn: errorFn
            };
        },

        _callRpc: function (method, params, callback) {
            var cb = this._createDelegatedCallbacks(callback);

            this.rpc.invoke(method, params, cb.successFn, cb.errorFn);
        },

        save: function (data, callback) {
            this._callRpc('save', data, callback);
        },

        load: function (key, callback) {
            this._callRpc('load', key, callback);
        },

        remove: function (key) {
            this._callRpc('remove', key);
        },

        clear: function () {
            this._callRpc('clear');
        },

        destroy: function () {
        }

    });

    WebAgent.Storage = new Storage();
//    WebAgent.Storage = new Storage(WA.isLoadReduce ? 'Cookie' : 'Storage');
    WebAgent.HugeStorage = new Storage('HugeStorage');


    var SessionStorage = WA.extend(Storage, {

        _ifSessionExist: function (cb, elseCb, scope) {
            if (WA.conn && WA.conn.Connection) {
                WA.conn.Connection.getSessionId(function (session) {
                    if (session === null) {
                        elseCb.call(scope || window);
                    } else {
                        cb.call(scope || window, session);
                    }
                }, this);
            } else {
                if (WA.isDebug) {
                    WA.error('OH SHI~');
                }

                WA.setTimeout(function () {
                    cb.call(scope || window, session);
                }, 1);
            }
        },

        save: function (data, callback) {
            this._ifSessionExist(function (session) {

                WA.util.Object.each(data, function (value, key) {
                    data[key] = session + ';' + value;
                });

                SessionStorage.superclass.save.call(this, data, callback);

            }, function () {
                var cb = this._createDelegatedCallbacks(callback);
                cb.errorFn();
            }, this);
        },

        load: function (key, callback) {
            this._ifSessionExist(function (session) {

                var cb = this._createDelegatedCallbacks(callback);

                SessionStorage.superclass.load.call(this, key, {
                    success: function (storage) {
                        if (WA.isObject(storage)) {

                            var res = {};
                            WA.util.Object.each(storage, function (value, key) {
                                var i = value.indexOf(';');
                                if (value.substr(0, i) == session) {
                                    res[key] = value.substr(i + 1);
                                } else {
                                    res[key] = '';
                                }
                            });

                            cb.successFn(res);
                        } else {
                            var i = storage.indexOf(';');
                            if (storage.substr(0, i) == session) {
                                cb.successFn(storage.substr(i + 1));
                            } else {
                                cb.successFn('');
                            }
                        }
                    },
                    failure: cb.errorFn,
                    scope: this
                });


            }, function () {
                var cb = this._createDelegatedCallbacks(callback);

                if (WA.isObject(key)) {
                    var res = {};
                    WA.util.Object.each(key, function (value) {
                        res[value] = '';
                    });
                    cb.successFn(res);
                } else {
                    cb.successFn('');
                }
            }, this);
        }

    });

    WebAgent.SessionStorage = new SessionStorage();

    WebAgent.HugeSessionStorage = new SessionStorage('HugeStorage');

})();(function () {

    var WA = WebAgent;
    var U = WA.util;

    var FM = WA.extend(WA.Activatable, {

        constructor: function () {
            FM.superclass.constructor.call(this);

            this.focused = false;
            this.focusing = false;

            this.focusEvent = new U.Event();
            this.blurEvent = new U.Event();

            this.rpc = WA.rpc.Local.createRpc('FocusManager');
            this.rpc.remoteEvent.on(this._onRemoteEvent, this);
        },

        _listenUserEvents: function () {
            if (!this.userEventsInited) {
                this.userEventsInited = true;
                WA.fly(window).on('focus', this._onWindowFocus, this);
                WA.getBody().on('mousemove', this._onMouseMove, this);
            }
        },

        _onWindowFocus: function () {
            //WA.debug.State.set('FM.windowFocus', new Date());
            this._tryToFocus();
        },

        _onMouseMove: function () {
            this._tryToFocus();
        },

        _focusingDone: function () {
            this.focusing = false;
        },

        _tryToFocus: function () {
            if (!this.focused && !this.focusing && this.isActive()) {
                //WA.debug.State.set('FM.tryToFocus', 'trying...');
                this.focusing = true;
                this.rpc.invoke('tryToFocus');
            }
        },

        _onRemoteEvent: function (method, params) {
            //WA.debug.State.set('FM.remoteEvent', method);
            if ('focus' === method) {
                this._focus();
            } else if ('blur' === method) {
                this._blur();
            } else if ('focusingDone' === method) {
                this._focusingDone();
            }
        },

        _focus: function () {
            if (!this.focused && this.isActive()) {
                this.focused = true;
                this.focusEvent.fire();
            }
        },

        _blur: function () {
            if (this.focused) {
                this.focused = false;
                this.blurEvent.fire();
            }
        },

        activate: function (onActivate, scope, forceFocus) {
            this._listenUserEvents();

            var fn = WA.isFunction(onActivate) ? WA.createDelegate(onActivate, scope || this) : WA.emptyFn;

            return FM.superclass.activate.call(this, {
                fn: fn,
                forceFocus: forceFocus
            });
        },

        _onActivate: function (params) {
            this.rpc.invoke('activate', params.forceFocus, params.fn, params.fn);

            WA.debug.State.set('FM.activated', true);
        },

        _onDeactivate: function () {
            this._blur();
            this.rpc.invoke('deactivate');

            WA.debug.State.set('FM.activated', false);
        },

        ifFocused: function (trueFn, elseFn, scope) {
            trueFn = trueFn || WA.emptyFn;
            elseFn = elseFn || WA.emptyFn;
            scope = scope || window;

            /*if (this.focused) {
             trueFn.call(scope);
             } else {
             elseFn.call(scope);
             }*/

            var successFn = WA.createDelegate(
                    function (focused) {
                        if (focused) {
                            trueFn.call(this);
                        } else {
                            elseFn.call(this);
                        }
                    },
                    scope
                    );

            var errorFn = function (e) {
                WA.debug.Console.debug('FocusManager.ifFocused() error: ' + e);
            };

            this.rpc.invoke('ifFocused', null, successFn, errorFn);
        }

    });

    WA.FocusManager = new FM();

})();WebAgent.XDRequest = (function () {

    var WA = WebAgent;
    var extend = WA.extend;
    var JSON = WA.getJSON();

    // -----------------------------------------------------------------------------------------------

    var XDXHR = extend(Object, {
        constructor: function (domain) {
            
        },
        request: function (opts, cb, cbError) {
            
        }
    });
    
    // -----------------------------------------------------------------------------------------------

    var XHRViaEasyXDM = extend(Object, {
        constructor: function (domain) {
            this._domain = domain;
		    this._rpc = new easyXDM.Rpc({
                remote: (''+document.location).match(/^[^:]+/)[0] + '://'+ domain+ '/communicate.html?' + WA.resProps
            }, {
                remote: {
                    request: {}
                }
            });
        },
        request: function (opts, cb, cbError, scope) {
            cb = WA.createDelegate(cb || function () {}, scope);

            var log = function (resp) {
//                new Image().src = 'http://log.foto.mail.ru/jserr/webagent?' + WA.makeGet({
//                    date: new Date().toString(),
//                    login: WA.ACTIVE_MAIL,
//                    error: (resp||{}).status,
//                    url: opts.url,
//                    data: JSON.stringify(opts.data)
//                });
            };

            var cbErrorWrapped = WA.createDelegate(function (resp) {
                cbError && cbError.call(scope || window, resp.data);
                log(resp.data);
            }, scope);
            
            this._rpc.request(opts, function (resp) {
                cb.apply(window, arguments);
                if(resp.status != 200) {
                    log(resp);
                }
            }, cbErrorWrapped);
        }
    });
    
    // -----------------------------------------------------------------------------------------------
    
    
    
    var XDR = function () {
        this._XHR = XHRViaEasyXDM;
    };
    XDR.prototype = {
        _XHR: false,
        _domains: {},
        getInstance: function (domain) {
            if(!this._domains[domain]) {
			    this._domains[domain] = new this._XHR(domain);
            }
            
            return this._domains[domain];
        }
    };
    
    return new XDR();
})();
    WebAgent.namespace('WebAgent.conn');

WebAgent.conn.Socket = (function () {

    var WA = WebAgent;
    var U = WA.util;
    var S = WA.Storage;
    var FM = WA.FocusManager;

    var NO_ACK = -1;
    var PAGE_UNIQ = Math.round(Math.random() * 100000);

    var Socket = WA.extend(Object, {

        connected: false,
        _shuttingDown: false,

        isReady: false,
        beforeResponseEvent: new U.Event(),
        afterResponseEvent: new U.Event(),
        connectedEvent: new U.Event(),
        errorEvent: new U.Event(),
        triggerEvent: new U.Event(),
        readyEvent: new U.Event(),
        requestReadyEvent: new U.Event(),
        successReconnectEvent: new U.Event(),


        constructor: function () {
            FM.focusEvent.on(this.restore, this);
            FM.blurEvent.on(this._shutdown, this);

            this.triggerEvent.on(this._onEvents, this);
        },

        _onEvents: function (type, value) {
            switch(type) {
                case 'stream':
                    S.save({
                        'socket.seq': value.segment,
                        'socket.date': +new Date()
                    });

                    this._continueConnection();
                    break;
                case 'serviceUnavailable':
                    this._onHttpError(500);
                    return false;
                    break;
            }
        },

        _restore: function (now) {
            this._continueConnection();
            S.save({'socket.restoreDate': +new Date()});
        },

        __setReady: function () {

            this.isReady = true;
            this.readyEvent.fire();
        },

        restore: function () {
            if (this.connected) {
                this._shuttingDown = false;
                this.__setReady();
            } else if (WA.enabled === false) {
                this.__setReady();
            } else {
                this.isValid(function () {
                    S.load(['socket.restoreDate'], {
                        success: function (storage) {
                            var now = +new Date();

                            var restoreInterval = now - (storage['socket.restoreDate'] || 0);
                            if (restoreInterval >= 5000) {
                                this._restore();
                            } else {
                                WA.setTimeout(this._ifActual(this._restore, this), 5000 - restoreInterval);
                            }

                            this.connected = true;
                            this.__setReady();
                        },
                        scope: this
                    });
                }, function () {
                    this.__setReady();
                }, this);

            }
        },

        isValid: function (cb, elseCb, scope) {
            S.load(['socket.date'], {
                success: function (storage) {
                    var now = +new Date();
                    if (now - (storage['socket.date'] || 0) < 70000) {
                        cb.call(scope || window);
                    } else {
                        elseCb.call(scope || window);
                    }
                },
                scope: this
            });
        },

        _shutdown: function () {
            this.requestable = false;
            if (this.connected) {
                this._shuttingDown = true;
            }
            WA.debug.Console.log('SOCK::_shutdown');
        },

        disconnect: function (onresponse) {
            this._shutdown();
            if(onresponse) {
                this.connected = false;
                this._shuttingDown = false;
            }

            WA.debug.Console.log('SOCK::disconnect');
            S.save({'socket.date': 0});
        },

        _continueConnection: function () {
            S.load(['socket.session', 'socket.seq'], {
                success: function (storage) {

                    S.save({
                        'socket.date': +new Date()
                    });

                    var opts = {
                        session: storage['socket.session'],
                        r: Math.round(Math.random() * 100000)
                    };
                    if(storage['socket.seq'] != NO_ACK) {
                        opts.stream_segment_ack = storage['socket.seq'];
                    }
                    this._connect(opts);

                    // wait for http errors
                    clearTimeout(this._connectFailTimeout);
                    this._connectFailTimeout = WA.setTimeout(WA.createDelegate(function (){
                        if(this.connected && !this._shuttingDown && !this.requestable) {
                            this.requestable = true;
                            this.requestReadyEvent.fire();
                        }

                        this.successReconnectEvent.fire();
                    }, this), 300);

                },
                scope: this
            });
        },

        _connectSession: function (domain, status, title, session) {
            S.save({
                'socket.domain': domain,
                'socket.date': +new Date(),
                'socket.session': session,
                'socket.seq': NO_ACK
            });

            var options = {
                session: session,
                with_login: 1,
                status: status,
                show_xstatus: 1 // ������� �������� ������ ��� ����� ��������
            };
/*

            // TODO: ����� ���� ���������� title
            var title = WA.XStatuses.getCurrentText(status);
*/

            if (title) {
                options.status_title = title;
            }

            this._connect(options);

        },

        _beginConnection: function (domain, status, title) {
            this.isValid(function () {
                S.load(['socket.session'], {
                    success: function (storage) {
                        this._connectSession(domain, status, title, storage['socket.session']);
                    },
                    scope: this
                });
            }, function () {
                this._connectSession(domain, status, title, Math.round(Math.random() * 100000))
            }, this);
        },

        _connect: function (opts) {
            if(!this._polled) {
                this._polled = true;
                S.load(['socket.domain'], {
                    success: function (storage) {
                        if(!opts) {
                            opts = {};
                        }
                        opts.page_uniq = PAGE_UNIQ;
                        opts.realm = location.hostname;

                        WA.XDRequest.getInstance(storage['socket.domain']).request({
                            url: '/connect',
                            data: opts,
                            timeout: 70000
                        }, this._ifActual(this._dispatch, this), this._ifActual(this._dispatch, this));
                    },
                    scope: this
                });
            }
        },

        connect: function (domain, status, title) {
            if (!this.connected && domain && status && status != 'offline') {
                this.connected = true;

                this._beginConnection(domain, status, title);
                this.connectedEvent.fire();
            }
        },

        _discardEventsState: 'discard',

        discardEvents: function () {
            this._discardEventsState = 'discard';
            WA.debug.Console.log('SOCK.discardEvents()');
        },

        continueEvents: function () {
            WA.debug.Console.log('SOCK.continueEvents() "'+ this._discardEventsState+ '"');
            if(this._discardEventsState == 'discarded') {
                this._continueConnection();
            }

            if(this._discardEventsState == 'discarded' || this._discardEventsState == 'discard') {
                this._discardEventsState = 'normal';
            }
        },

        _onRedirect: function (jimServer) {
            WA.conn.UserState.getStatus(function (status) {
                this._beginConnection(jimServer, status);
            }, this);
        },

        _validateResponse: function (resp) {
            if (!resp) {
                this._onError('connectTimeout');
            } else if (resp.status != 200) {
                U.Mnt.count('errorsHTTP');
                this._onHttpError(resp.status);
            } else if (!resp.data.match(/\["stream",\s{"segment":\s\d+}\][\w\W]*\]/)) {
                this._onError('brokenResponse');
            } else {
                try {
                    var events = eval('(' + resp.data + ')');
                } catch(e) {
                    this._onError('invalidSyntaxResponse');
                    if (WA.isDebug) {
                        throw e.message;
                    }
                    return;
                }
                if(events[0][0] == 'redirect') {
                    var jimServer = WA.conn.UserState.getForcedJmp() || events[0][1].jimServer;
                    this._onRedirect(jimServer);
                } else {
                    return events;
                }
            }
        },

        _dispatch: function (resp) {
            delete this._polled;
            WA.debug.Console.log('_dispatch ', resp);
            var events = this._validateResponse(resp);
            if(events) {
                if(!this.requestable) {
                    this.requestable = true;
                    this.requestReadyEvent.fire();
                }

                if(this._discardEventsState == 'normal') {
                    U.Mnt.begin('dispatch');

                    this.beforeResponseEvent.fire();
                    for (var i = 0; i < events.length; i++) {
                        if (this.triggerEvent.fire(events[i][0], events[i][1]) === false) {
                            break;
                        }
                    }
                    this.afterResponseEvent.fire();
                    
                    U.Mnt.end('dispatch');
                } else {
                    this._discardEventsState = 'discarded';
                    WA.debug.Console.log('SOCK: events discarded');
                }
            } else {
                clearTimeout(this._connectFailTimeout);
            }
        },

        _ifActual: function (cb, scope) {
            return WA.createDelegate(function () {
                if (this._shuttingDown) {
                    this._setDisconnected();
                } else if(this.connected) {
                    var args = arguments;
                    FM.ifFocused(function () {
                        cb.apply(scope || window, args);
                    }, function () {
                        this._setDisconnected();
                    }, this);
                } else {
                }

            }, this);
        },

        _setDisconnected: function () {
            delete this._polled;
            this.requestable = false;
            this.connected = false;
            this._shuttingDown = false;

            WA.debug.Console.log('SOCK::_setDisconnected');
        },

        _onTimeoutError: function () {
            this._ifActual(this._continueConnection, this);
        },

        explainHttpError: function (errno) {
            WA.debug.Console.log('explainHttpError ', errno);
            
            if(errno == 0) {
                return 'connectInterrupt';
            } else if (errno == 503) {
                return 'invalidUser';
            } else if(errno >= 500 && errno < 599) {
                return 'serviceUnavailable';
            } else if(errno >= 400 && errno < 499) {
                switch (errno) {
                    case 410:
                        return 'sessionLost';
                    case 400:
                    case 403:
                    case 404:
                        return 'invalidUser';
                    case 413:
                        return 'tooManyConnections';
                    default:
                        return 'serverNotUnderstand';
                }
            } else {
                return 'otherErrors';
            }
        },

        _onHttpError: function (errno) {
            var err = this.explainHttpError(errno);
            if(err == 'connectInterrupt') {
                WA.setTimeout(WA.createDelegate(function () {
                    FM.ifFocused(function () {
                        this._continueConnection();
                    }, function () {}, this);
                }, this), 3000);
            } else {
                this._setDisconnected();
                this.errorEvent.fire(err);
            }
        },

        _onError: function (type, param) {
            switch (type) {
                case 'brokenResponse':
                    FM.ifFocused(function () {
                        WA.setTimeout(WA.createDelegate(this._continueConnection, this), 1000);
                    }, function () {
                    }, this);
                    break;
                case 'invalidSyntaxResponse':
                    WA.setTimeout(WA.createDelegate(this._continueConnection, this), 10000);
                    break;
                case 'connectTimeout':
                    
                    break;
            }
        },

        send: function (method, params, cb, scope) {
            params || (params = {});
            params.domain || (params.domain = location.hostname);

            S.load(['socket.domain', 'socket.session'], {
                success: function (storage) {
                    var get = {
                        session: storage['socket.session'],
                        r: Math.round(Math.random() * 100000)
                    };

                    var req = {
                        method: 'POST',
                        url: '/' + method + '?' + WA.makeGet(get),
                        data: params,
                        page_uniq: PAGE_UNIQ
                    };

                    WA.XDRequest.getInstance(storage['socket.domain']).request(req, function (resp) {
                        WA.debug.Console.log('send: ', method, ', response: ', resp);
                        cb && cb.call(scope || window, true, resp);
                    }, function (resp) {
                        cb && cb.call(scope || window, false, resp);
                    }, this);
                },
                scope: this
            });
        },
        
        getSessionId: function (cb, scope) {
            S.load(['socket.session'], function (storage) {
                cb.call(scope || window, storage['socket.session']);
            });
        }
    });

    return new Socket();

})();WebAgent.namespace('WebAgent.conn');

WebAgent.conn.Connection = (function () {

    var WA = WebAgent;
    var U = WA.util;
    var SOCK = WA.conn.Socket;
    var JSON = WA.getJSON();

    var Conn = WA.extend(Object, {

        constructor: function () {
            this.triggerEvent = new U.Event();
            
            this.beforeResponseEvent = new U.Event().relay(SOCK.beforeResponseEvent);
            this.afterResponseEvent = new U.Event().relay(SOCK.afterResponseEvent);

            SOCK.afterResponseEvent.on(this._onAfterResponseEvent, this);

            SOCK.triggerEvent.on(this._onEvent, this);
        },

        _onEvent: function (type, value) {
            var res;

            if(type == 'contactListState') {
                WA.debug.Console.log('contactListState ', value);
            }

            
            switch (type) {
                case 'contactList':
                    for(var i = value.length; i-- ;) {
                        if(value[i][0].indexOf('@uin.icq') != -1 && value[i][3].indexOf('status_') == -1) {
                            value[i][3] = 'icq_'+ value[i][3];
                        }
                    }
                    break;
                case 'contact':
                    if (value['micblog::status']) {
                        type = 'contactMicblogStatus';
                        WA.apply(value, value['micblog::status'], {from: value.email});
                    } else if (value.status) {
                        type = 'contactStatus';
                        if(!('nickname' in value) || typeof value.nickname != 'string') {
                            value.nickname = value.email;
                        }
                        if(value.email.indexOf('@uin.icq') != -1) {
                            value.status.type = 'icq_' + value.status.type;
                        }
                    } else if (value.history) {
                        type = 'contactHistory';
                        U.Array.each(value.history, function (el) {
                            if(!('text' in el[1])) {
                                el[1].text = '';
                            }
                            
                            if('to' in el[1]) {
                                el[1].from = WA.ACTIVE_MAIL;
                                delete el[1].to;
                            }

                            if('sender' in el[1]) {
                                el[1].text = el[1].sender + ':\n' + el[1].text;
                            }
                        });
                    } else if (WA.isDebug) {
                        throw 'unknow server event';
                    }
                    break;
                case 'iq':
                    if (value.body && value.body.contact) {
                        res = value.body.contact;

                        if (res.unignore && res.unignore.success) {
                            type = 'contactUnignoreSuccess';
                            value = res.email;
                        } else if (res.unignore && res.unignore.error) {
                            type = 'contactUnignoreFail';
                            value = {
                                message: res.add.error,
                                mail: res.email
                            };
                        } else if (res.add && res.add.success) {
                            type = 'contactAddSuccess';
                            value = res.email;
                        } else if (res.add && res.add.error) {
                            type = 'contactAddFail';
                            value = {
                                message: res.add.error,
                                mail: res.email
                            };
                        }
                    } else if (value.body && value.body.username) {
                        type = 'contactMail';
                        value = {
                            id: value.id,
                            mail: value.body.username
                        };
                    }
                    break;
                case 'message':
                    if(value.composing) {
                        type = 'typingNotify';
                    } else if (value.request && value.request.authorize) {
                        type = 'authRequest';

                        if(!WA.SA.get(value.from).nick) {
                            var uniq = Math.round(Math.random()*999999);

                            this._modifyAuthWaitingList(function (list) {
                                list[uniq] = value;
                                return list;
                            }, this);

                            var params = {
                                email: value.from,
                                uniq: uniq
                            };
                            WA.conn.Socket.send('wp', params);
                            return;
                        }
                    } else  if (value.delivered) {
                        type = 'messageDelivered';
                    } else if(!('text' in value)) {
                        value.text = '';
                    }
                    break;
                case 'wp':
                    if(value.uniq) {
                        if(value.error) {
                            //todo
                        } else if(value.uniq){
                            this._modifyAuthWaitingList(function (list) {
                                if(list[value.uniq]) {
                                    var contact = value.result[0];
                                    var status = WA.SA.get(contact.email);
                                    this.triggerEvent.fire('contactStatus', {
                                        email: contact.email,
                                        nickname: WA.MainAgent.WP2Nick(contact),
                                        status: {
                                            type: status.status || 'gray',
                                            title: status.statusTitle || '�� �����������'
                                        }
                                    });
                                    
                                    this.triggerEvent.fire('authRequest', list[value.uniq]);
                                    SOCK.afterResponseEvent.fire();
                                    delete list[value.uniq];
                                }
                                return list;
                            }, this);
                        }
                    }
                    break;
                case 'userInfo':
                    if(this._preventUserInfo) {
                        return;
                    }
                    this._preventUserInfo = true;
                    break;
            }

            WA.debug.Console.log(type, JSON.stringify(value));
            
            //U.Mnt.begin('d_' + type);
            this.triggerEvent.fire(type, value);
            //U.Mnt.end('d_' + type);
        },

        _modifyAuthWaitingList: function (cb, scope) {
            WA.Storage.load(['authWaitingList'], function (storage) {
                var list = JSON.parse(storage['authWaitingList'] || '{}');
                if('length' in list) {
                    list = {};
                }
                list = cb.call(scope || window, list);
                WA.Storage.save({
                    authWaitingList: JSON.stringify(list)
                });
            });
        },

        getSessionId: function (cb, scope) {
            SOCK.isValid(function () {
                SOCK.getSessionId(cb, scope);
            }, function () {
                cb.call(scope || window, null);
            }, this)
        },

        getClist: function (onSuccess, onError, scope) {
            SOCK.send('contactlist', {}, function (success) {
                if(success) {
                    onSuccess && onSuccess.call(scope || window);
                } else {
                    onError && onError.call(scope || window);
                }
            }, this);
        },

        getMailById: function (id) {
            SOCK.send('username', {
                'verify-uid': id,
                'iq-id': id
            }, function (success) { }, this);
        },

        _offlineMessages: [],

        confirmOfflineDelivery: function (id) {
            this._offlineMessages.push(id);
        },

        _onAfterResponseEvent: function () {
            if(this._preventUserInfo) {
                this._preventUserInfo = false;
            }
            
            if(this._offlineMessages.length) {
                SOCK.send('message', {
                    uidl: this._offlineMessages.join(';'),
                    delivered: 1
                });
                this._offlineMessages = [];
            }
        }

    });

    return new Conn();

})();WebAgent.namespace('WebAgent.conn');

WebAgent.conn.UserState = (function () {

    var WA = WebAgent;
    var S = WA.Storage;
    var SOCK = WA.conn.Socket;
    var U = WA.util;
    var FM = WA.FocusManager;
    var JSON = WA.getJSON();

    var AUTO_AWAY_TIMEOUT = 600 * 1000; //10 min

    var AutoAway = WA.extend(WA.ui.Component, {

        constructor: function () {
            FM.focusEvent.on(this._onFocus, this);
            FM.blurEvent.on(this._onBlur, this);

            this.checker = new U.DelayedTask({
                interval: 1000,
                fn: this._onCheck,
                scope: this
            });

            WA.getBody().on('mousemove', this._onUserEvents, this)
                .on('mousedown', this._onUserEvents, this)
                .on('keydown', this._onUserEvents, this);
        },

        _onFocus: function (isTimeout) {
            WA.SessionStorage.load(['autoaway'], {
                success: function (storage) {
                    this.isAway = !!storage['autoaway'];
                    this._focused = true;
                    if(!isTimeout && !this.isAway) {
                        this._onUserEvents();
                    }
                    this.checker.start();

                    WA.debug.Console.log('AUTOAWAY: focus, isAway=', this.isAway);
                },
                scope: this
            });
        },

        _onBlur: function () {
            this._focused = false;
            this.checker.stop();
            WA.debug.Console.log('AUTOAWAY: blur');
        },

        _onUserEvents: function () {
            if(this._focused) {
                this._date = +new Date();

                if(this.isAway) {
                    this._onComeback();
                }
            }
        },

        _onCheck: function () {
            if(!this.isAway && +new Date() - this._date > AUTO_AWAY_TIMEOUT) {
                this._onAway();
            }
            this.checker.start();
        },

        _onComeback: function () {
            this.isAway = false;
            WA.SessionStorage.save({
                'autoaway': ''
            });

            WA.conn.UserState.getStatus(function (status, statusTitle) {
                WA.debug.Console.log('AUTOAWAY: comeback, status=', status);
                WA.conn.UserState._setStatus(status, statusTitle);
            }, this);
        },

        _onAway: function () {
            this.isAway = true;
            WA.SessionStorage.save({
                'autoaway': '1'
            });

            WA.conn.UserState._setStatus('away');
            WA.debug.Console.log('AUTOAWAY: away');
        }

    });
    new AutoAway();


    var UserState = WA.extend(Object, {

        constructor: function () {

            this.permanentShutdownEvent = new U.Event();
            this.networkErrorEvent = new U.Event();
            this.readyEvent = new U.Event();
            this.connectedEvent = new U.Event();
            this.disconnectedEvent = new U.Event();
            this.statusChangedEvent = new U.EventConfirm();
            this.statusChangedEvent.on(function (status) {
                WA.debug.Console.log('statusChangedEvent ', status)
            });

            SOCK.readyEvent.on(this._onSocketReady, this);
            SOCK.triggerEvent.on(this._onServerEvent, this);
            SOCK.requestReadyEvent.on(this._onSocketRequestable, this);
            SOCK.errorEvent.on(this._onNetworkError, this);
            SOCK.successReconnectEvent.on(function () {
                if(this.serviceUnavailableDelay != 2000) {
                    this.serviceUnavailableDelay = 2000;
                }
            }, this);

            /*
            this.email = this.getInfo().email;
            this.nick = this.email;
            S.whenReady(function () {
                S.load(['selfnick'], {
                    success: function (storage) {
                        if (storage['selfnick']) {
                            this.nick = storage['selfnick'];
                        }
                    },
                    scope: this
                })
            }, this);
            */
        },

        init: function () {
            this.sync = WA.Synchronizer.registerComponent('user_state', ['data'], false);
            this.sync.triggerEvent.on(this._onSync, this);
        },

        _onSync: function (data) {
            if(data.isChanged('data')) {
                data = JSON.parse(data.get('data') || 'false');
                if(data) {
                    if(data.connectionsLimit) {
                        this.permanentShutdownEvent.fire({
                            isShowInfo: true,
                            info: WA.TEXT.ERR_INFO_TOO_MANY_CONNECTIONS
                        });
                    }
                }
            }
        },

        _saveState: function (state) {
            this.sync.write('date', JSON.stringify(state));

        },

        _clearState: function () {
            this.sync.write('date', '');
        },

        isValidUser: function () {
            if(WA.getUserLogin()) {
                return true;
            } else {
                this._onInvalidUser();
                return false;
            }
        },

        _onNetworkError: function (err) {
            WA.debug.Console.log('net err ', err);
            switch(err) {
                case 'connectInterrupt':// do nothing
                    WA.setTimeout(WA.createDelegate(function () {
                        FM.ifFocused(function () {
                            this._checkConnectivity();
                        }, function () {}, this);
                    }, this), 2000);
                    break;
                case 'invalidUser':
                    this._onInvalidUser(WA.TEXT.ERR_INFO_INVALID_USER);
                    break;
                case 'connectTimeout':// todo: show info and reconnect from user/status
                case 'otherErrors':// todo: try to connect
                case 'sessionLost':// check user by cookie, if (an other user) {shutdown, hide all, close popup} else {reconnect from request user/status}
                    if(WA.getUserLogin() != WA.ACTIVE_MAIL) {
                        this._onInvalidUser(WA.TEXT.ERR_INFO_INVALID_USER);
                    } else {
                        this._checkConnectivity();
                    }
                    break;
                case 'serviceUnavailable':// delay and reconnect from user/status
                    WA.setTimeout(WA.createDelegate(function () {
                        FM.ifFocused(function () {
                            this._checkConnectivity();
                        }, function () {}, this);
                    }, this), this.serviceUnavailableDelay);
                    this.serviceUnavailableDelay = 10000;
                    /* TODO
                    this.sync.read('data', function (data) {
                        data = JSON.parse(data.get('data') || 'false');
                        if(!data) {
                            data = {};
                        }

                        var delay = 1000;
                        if(data.serviceUnavailable) {
                            delay = (data.delay || delay) * 2;
                        }

                        this.networkErrorEvent.fire(delay);

                        this._saveState({
                            serviceUnavailable: +new Date(),
                            delay: delay
                        });
                    }, this);
                    */
                    break;
                case 'serverNotUnderstand':// errors in client, show info about it, and shutdown.
                    // TODO
                    break;
                case 'tooManyConnections':// show info, delay 1 hour till connect

                    this.permanentShutdownEvent.fire({
                        isShowInfo: true,
                        info: WA.TEXT.ERR_INFO_TOO_MANY_CONNECTIONS
                    });
                    break;
            }
        },

        _onInvalidUser: function (info) {
            //return (window.open('http://swa.mail.ru/cgi-bin/auth?Page=http%3A//webagent.mail.ru/webagent&FailPage=http%3A//webagent.mail.ru/auth', 'wa_popup', 'width=600, height=500, resizable=false')&&false)
            this.invalidUser = true;
            if(info) {
                this.permanentShutdownEvent.fire({
                        reason: 'noAuth',
                        isShowInfo: true,
                        info: info
                    });
            }

            if(WA.isPopup) {
                document.location = 'http://swa.mail.ru/cgi-bin/auth?Page=http%3A//webagent.mail.ru/webagent&FailPage=http%3A//webagent.mail.ru/auth';
            }
        },

        _onSocketRequestable: function () {
            S.load(['status.forced', 'status.forced_title'], {
                success: function (storage) {
                    if (storage['status.forced']) {
                        this._setStatus(storage['status.forced'], storage['status.forced_title']);
                    }
                },
                scope: this
            })
        },

        isReady: false,

        _onServerEvent: function (type, value) {
            switch(type) {
                case 'logout':
                    this.connected = false;
                    this.statusChangedEvent.fire({
                        success: function () {
                            SOCK.disconnect(true);
                            this.disconnectedEvent.fire();
                        },
                        scope: this
                    }, 'offline', null);
                    return false;
                    break;
                /*
                case 'userInfo':
                    this.nick = value['Mrim.NickName'];
                    S.save({
                        'selfnick': this.nick
                    });
                    break;
                 */
            }
        },

        getStatus: function (cb, scope) {
            S.load(['status.requested', 'status.forced', 'status.requested_title', 'status.forced_title'], {
                success: function (storage) {
                    var st = storage['status.forced'] || storage['status.requested'] || 'online';
                    var st_title = storage['status.forced_title'] || storage['status.requested_title'] || '';
                    cb.call(scope || window, st, st_title);
                },
                scope: this
            })
        },

        getActualStatus: function (cb, scope) {
            S.load(['status.otherClientDate', 'status.disabled'], {
                success: function (storage) {
                    var now = +new Date();
                    if (storage['status.disabled']) {
                        cb.call(scope || window, 'disabled');
                    } else if (now - (storage['status.otherClientDate'] || 0) < 60 * 60000) {
                        cb.call(scope || window, 'offline');
                    } else {
                        this.getStatus(cb, scope);
                    }
                },
                scope: this
            })
        },

        getState: function (cb, scope) {
            S.load(['status.disabled', 'status.disabled_requested'], {
                success: function (storage) {
                    WA.setTimeout(function () {
                        if('enabled' in WA && (WA.enabled ^ !storage['status.disabled'])) {
                            storage['status.disabled'] = ['1', ''][+WA.enabled];
                            S.save(storage);
                        }

                        if (storage['status.disabled']) {
                            cb.call(scope || window, 'disabled');
                        } else {
                            SOCK.isValid(function () {
                                cb.call(scope || window, 'online');
                            }, function () {
                                this.getActualStatus(function (status) {
                                    if(status == 'offline' && storage['status.disabled_requested']) {
                                        cb.call(scope || window, 'offline');
                                    } else {
                                        cb.call(scope || window);
                                    }
                                }, this);
                            }, this);
                        }
                    }, 1, this);
                },
                scope: this
            })
        },

        _onSocketReady: function () {
            WA.debug.Console.log('_onSocketReady, SOCK.connected: ', SOCK.connected);
            if (SOCK.connected) {
                this.connected = true;
                this.isReady = true;
                this.readyEvent.fire();
            } else {
                this._checkConnectivity();
            }
        },

        _commitEnabledStatus: function (storage, enabled) {
            var props = {}, saveProps = false;
            if(typeof enabled == 'boolean' && (enabled ^ !storage['status.disabled'])) {
                props['status.disabled'] = ['1', ''][+enabled];
                saveProps = true;
            }
            if(!storage['status.disabled_requested']) {
                props['status.disabled_requested'] = 1;
                saveProps = true;
            }
            if(saveProps) {
                S.save(props);
            }
        },

        _jimDomain: 'jim.mail.ru',

        _checkConnectivity: function () {
            S.load(['status.otherClientDate', 'status.requested', 'status.forced', 'status.forced_title', 'status.requested_title', 'status.disabled', 'status.disabled_requested'], {
                success: function (storage) {
                    var now = +new Date();
                    var st = storage['status.forced'] || storage['status.requested'] || 'online';
                    var st_title = storage['status.forced_title'] || storage['status.requested_title'] || '';

                    if(storage['status.forced'] && storage['status.forced'] != 'offline') {
                        storage['status.otherClientDate'] = 0;
                        S.save({'status.otherClientDate': 0});
                    }

                    if (st != 'offline' && (now - (storage['status.otherClientDate'] || 0) > 60 * 60000)) {
                        this._getUserStatus(function (status) {
                            this._commitForcedStatus(storage['status.forced'], storage['status.forced_title'], !status.jimOnline);

                            this._commitEnabledStatus(storage, status.enabled);
                            if(!status.enabled) {
//                                this.statusChangedEvent.fire(null, 'offline');
//                                this.readyEvent.fire();
                                this.permanentShutdownEvent.fire();
                                return;
                            }

                            if (status.jimOnline) {
                                SOCK.connect(status.jimServer, status.type);
                                this.connected = true;
                            } else if(storage['status.forced'] && storage['status.forced'] != 'offline') {
                                SOCK.connect(status.jimServer, storage['status.forced'], storage['status.forced_title']);
                                this.connected = true;
                            } else if (!status.type || status.type == 'offline') {
                                if (st != 'offline') {
                                    SOCK.connect(status.jimServer, st, st_title);
                                    this.connected = true;
                                } else {
                                    this.connected = false;
                                }

                            } else {
                                S.save({'status.otherClientDate': +new Date()});
                                this.connected = false;

                            }

                            this.isReady = true;
                            if (this.connected) {
                                this.statusChangedEvent.fire(null, st, st_title);
                                U.Mnt.setStartOccupancy();
                                U.Mnt.rbCountOpen(); //RB
                            } else {
                                this.statusChangedEvent.fire(null, 'offline', null);
                            }

                            this.readyEvent.fire(this.connected);

                        }, this);
                    } else {
                        this._commitForcedStatus(storage['status.forced'], storage['status.forced_title'], true);

                        if(!storage['status.disabled_requested']) {
                            this._getUserStatus(function (status) {
                                this._commitEnabledStatus(storage, status.enabled);
                                if(!status.enabled) {
                                    this.permanentShutdownEvent.fire();
                                } else {
                                    this.connected = false;
                                    this.isReady = true;
                                    this.statusChangedEvent.fire(null, 'offline', null);
                                    this.readyEvent.fire();
                                }
                            }, this);
                        } else {
                            this.connected = false;
                            this.isReady = true;
                            this.statusChangedEvent.fire(null, 'offline', null);
                            this.readyEvent.fire();
                        }
                    }
                },
                scope: this
            })
        },

        _commitForcedStatus: function (forcedStatus, forcedTitle, saveRequested) {
            if (forcedStatus) {
                S.save({
                    'status.forced': '',
                    'status.forced_title': ''
                });
                if (saveRequested) {
                    S.save({
                        'status.requested': forcedStatus,
                        'status.requested_title': forcedTitle || ''
                    });
                }
            }
        },

        getForcedJmp: function () {
            var m = (''+document.location).match(/wa_force_jmp=([^&]+)/i);
            if(m) {
                return m[1];
            } else if(false) {
                return 'jmptest.mail.ru';
            }

        },

        _getUserStatus: function (cb, scope) {
            this.getUserStatus(cb, function (status, explain) {
                this._onNetworkError( explain );
            }, this);
        },

        getUserStatus: function (cbSuccess, cbFail, scope) {
            var respCb = WA.createDelegate(function (response) {
                if (response.status != 200) {
                    cbFail.call(scope || window, response.status, SOCK.explainHttpError(response.status));
                } else {
                    var status = eval('('+ response.data+ ')')[0][1].body.user.status;
                    var jmp = this.getForcedJmp();
                    if(jmp) {
                        status.jimServer = jmp;
                    }
                    cbSuccess.call(scope || window, status);
                }
            }, this);

            WA.XDRequest.getInstance(this._jimDomain).request({
                url: '/user/status'
            }, respCb, respCb);
        },

        _setStatus: function (status, title) {
            WA.debug.Console.log('_setStatus: ', status, ' connected:', this.connected);

            if (this.connected && status == 'offline') {
                this.statusChangedEvent.fire({
                    success: function () {
                        SOCK.disconnect();
                        SOCK.send('logout', false, this._checkChangeSuccess, this);
                        this.connected = false;
                        this.disconnectedEvent.fire();
                    },
                    scope: this
                }, status, null);
            } else if (this.connected && status != 'offline') {   // a little bit hindu
                this.statusChangedEvent.fire(null, status, title);
                var params = { type: status };
                //var title = WA.XStatuses.getCurrentText(status);
                if (title) {
                    params.title = title;
                }
                SOCK.send('user/status', params, this._checkChangeSuccess, this);
            } else if (!this.connected && status != 'offline') {
                this.statusChangedEvent.fire(null, status, title);
            }
        },

        setStatus: function (status, title) {
            S.save({
                'status.forced': status,
                'status.forced_title': title
            });

            this._setStatus(status, title);
        },

        _checkChangeSuccess: function (success, response) {
            if (success) {
                S.load(['status.forced', 'status.forced_title'], {
                    success: function (storage) {
                        this._commitForcedStatus(storage['status.forced'], storage['status.forced_title'], true);
                    },
                    scope: this
                });
            } else {
                if (WA.isDebug) {
                    WA.error('Unable to change status');
                }
            }
        }

    });

    return new UserState();

})();WebAgent.namespace('WebAgent.conn');

(function () {
    
    var WA = WebAgent;
    var U = WA.util;
    var SOCK = WA.conn.Socket;

    WebAgent.conn.ConnectionRoster = WA.extend(Object, {

        constructor: function () {
            WA.conn.Connection.triggerEvent.on(this._onConnectionTriggerEvent, this);
            WA.conn.Connection.beforeResponseEvent.on(this._onBeforeConnectionResponse, this);
            WA.conn.Connection.afterResponseEvent.on(this._onAfterConnectionResponse, this);
        },

        _onBeforeConnectionResponse: function () {

        },

        _onAfterConnectionResponse: function () {

        },

        _onConnectionTriggerEvent: function (type, value) {
            var method = '_onConnection' + type.substr(0, 1).toUpperCase() + type.substr(1);
            if (this[method]) {
                this[method].call(this, value);
            }
        },

        destroy: function () {
            WA.conn.Connection.triggerEvent.un(this._onConnectionTriggerEvent, this);
            WA.conn.Connection.beforeResponseEvent.un(this._onBeforeConnectionResponse, this);
            WA.conn.Connection.afterResponseEvent.un(this._onAfterConnectionResponse, this);
        }


    });

})();
WebAgent.Invalidator = (function () {

    var WA = WebAgent;

    var Invalidator = WA.extend(WA.Activatable, {

        constructor: function () {
            this.invalidateEvent = new WA.util.Event();

            this.activate();
        },

        invalidate: function () {
            if (this.isActive()) {
                WA.debug.State.set('invalidate_on', new Date());
                this.invalidateEvent.fire();
            }
        }

    });

    return new Invalidator();

})();WebAgent.Synchronizer = (function () {

    var WA = WebAgent;
    var U = WA.util;

    // -----------------------------------------------------------------------------------------------

    var RequestGroup = WA.extend(Object, {

        constructor: function () {
            this.count = 0;
            this.items = [];
            this.allData = {};
        },

        add: function (id, fn, scope) {
            this.count++;

            this.items.push({
                id: id,
                fn: fn,
                scope: scope
            });

            return this;
        },

        _onItemReady: function (data, item) {
            this.count--;

            this.allData[item.id] = data;

            if (this.count == 0) {
                this.readyFn(this.allData);
                this._destroy();
            }
        },

        _invokeItem: function (item) {
            var ctx = {
                ready: WA.createDelegate(this._onItemReady, this, [item], 1)
            };
            item.fn.call(item.scope || window, ctx);
        },

        request: function (readyFn, scope) {
            if (this.count > 0) {
                this.readyFn = WA.createDelegate(readyFn, scope || window);
                U.Array.each(this.items, this._invokeItem, this);
            } else {
                WA.error('Add items!');
            }
        },

        _destroy: function () {
            this.readyFn = null;
            this.allData = null;
            this.items = null;
        }

    });

    // -----------------------------------------------------------------------------------------------

    var SYNC_RECORD_UNCHANGED = 'UNCHANGED_' + new Date().getTime();

    var SyncRecord = WA.extend(WA.data.Record, {

        isChanged: function (fieldName) {
            return this.hasField(fieldName) && this.get(fieldName) !== SYNC_RECORD_UNCHANGED;
        }

    });

    // -----------------------------------------------------------------------------------------------

    var ComponentSynchronizer = WA.extend(Object, {

        constructor: function (owner, id, keys, sessional, isHuge) {
            this.owner = owner;

            this.id = id;

            this._initKeys(keys);

            this.sessional = sessional || false;
            this.hugeStorage = isHuge || false;

            this.triggerEvent = new U.Event();
            this.destroyEvent = new U.Event();

            this.canShowDebugState = false;
        },

        _initKeys: function (keys) {
            this.keys = WA.isArray(keys) ? keys : [keys];

            this.localData = {};

            U.Array.each(this.keys, function (k) {
                this.localData[k] = '';
            }, this);
        },

        getId: function () {
            return this.id;
        },

        getKeys: function () {
            return this.keys;
        },

        setData: function (storageData) {
            var data = {};
            var canFire = false;

            var keys = this.getKeys();
            U.Array.each(keys, function (key) {
                var value = storageData[key];
                if (this.localData[key] !== value) {
                    this.localData[key] = data[key] = value;
                    canFire = true;
                } else {
                    data[key] = SYNC_RECORD_UNCHANGED;
                }
            }, this);

            if (this.canShowDebugState) {
                var debugData = [];
                U.Object.each(this.localData, function (v, k) {
                    debugData.push(k + ': ' + v);
                });
                WA.debug.State.set('Sync.' + this.getId(), debugData.join(', '));
            }

            if (canFire) {
                this.triggerEvent.fire(new SyncRecord(keys, data));
            }
        },

        write: function (key, value, cb, scope) {
            this.owner.write(this, key, value, cb, scope);
        },

        read: function (cb, scope) {
            this.owner.read(this, cb, scope);
        },

        isSessional: function () {
            return this.sessional;
        },

        isHugeStorage: function () {
            return this.hugeStorage;
        },

        whenReady: function (fn, scope) {
            this.owner.whenReady(this, fn, scope);
        },

        showDebugState: function () {
            this.canShowDebugState = true;
        },

        destroy: function () {
            this.id = null;
            this.owner = null;
            this.keys = null;
            this.localData = null;

            this.triggerEvent.removeAll();
            this.triggerEvent = null;

            this.destroyEvent.fire(this);
            this.destroyEvent.removeAll();
            this.destroyEvent = null;
        }

    });

    // -----------------------------------------------------------------------------------------------

    var Synchronizer = WA.extend(Object, {

        _prefix: 'sync',

        constructor: function () {
            this.dataChecker = new U.DelayedTask({
                interval: 500,
                fn: this._onDataCheck,
                scope: this
            });

            this.syncs = [];
        },

        registerComponent: function (id, keys, sessional, isHuge) {
            var index = U.Array.indexOfBy(this.syncs, function (sync) {
                return id === sync.getId();
            });
            if (index != -1) {
                WA.error('Duplicate synchronizers: ' + id);
            }

            var sync = new ComponentSynchronizer(this, id, keys, sessional !== false, !!isHuge);
            this.syncs.push(sync);
            sync.destroyEvent.on(this._onDestroySync, this);
            return sync;
        },

        _onDestroySync: function (sync) {
            U.Array.removeBy(this.syncs, function (entry) {
                return entry.getId() === sync.getId();
            }, this);
        },

        startUp: function () {
            this.dataChecker.start();
            WA.debug.State.set('Sync.state', 'working');
        },

        stop: function () {
            this.dataChecker.stop();
            WA.debug.State.set('Sync.state', 'stopped');
        },

        _generateStorageKey: function (sync, key) {
            return [this._prefix, sync.getId(), key].join('$');
        },

        _generateStorageKeys: function (sync) {
            return U.Array.transform(sync.getKeys(), function (id) {
                return this._generateStorageKey(sync, id);
            }, this);
        },

        _onDataCheck: function () {
            WA.FocusManager.ifFocused(
                    function () {
                        this.dataChecker.start();
                    },
                    function () {
                        var prepareFn = function (isSessional, isHugeStorage) {
                            var keys = [];
                            U.Array.each(this.syncs, function (sync) {
                                if (sync.isSessional() === isSessional && sync.isHugeStorage() == isHugeStorage) {
                                    keys = keys.concat(this._generateStorageKeys(sync));
                                }
                            }, this);
                            var Storage = this._getSyncStorageParams(isSessional, isHugeStorage);

                            return function (ctx) {
                                Storage.load(keys, {
                                    success: function (data) {
                                        ctx.ready(data);
                                    },
                                    failure: function (e) {
                                        WA.error(e);
                                    },
                                    scope: this
                                });
                            };
                        };

                        var onReady = function (data) {
                            U.Array.each(this.syncs, function (sync) {
                                var p = sync.isSessional() ? 'sessional' : 'usual';
                                if (sync.isHugeStorage()) {
                                    p = 'huge' + p;
                                }
                                var syncData = this._getSyncData(sync, data[p]);
                                sync.setData(syncData);
                            }, this);

                            this.dataChecker.start();
                        };

                        new RequestGroup()
                                .add('sessional', prepareFn.call(this, true, false), this)
                                .add('usual', prepareFn.call(this, false, false), this)
                                .add('hugesessional', prepareFn.call(this, true, true), this)
                                .add('hugeusual', prepareFn.call(this, false, true), this)
                                .request(onReady, this);
                    },
                    this
                    );
        },

        _getSyncData: function (sync, storageData) {
            var keys = sync.getKeys();
            var storageKeys = this._generateStorageKeys(sync);
            var data = {};
            U.Array.each(storageKeys, function (storageKey, index) {
                data[keys[index]] = storageData[storageKey];
            });
            return data;
        },

        _getSyncStorageParams: function (isSessional, isHugeStorage) {
            var storageType = isSessional ? 'SessionStorage' : 'Storage';
            if (isHugeStorage) {
                storageType = 'Huge' + storageType;
            }
            return WA[storageType];
        },

        _getSyncStorage: function (sync) {
            return this._getSyncStorageParams(sync.isSessional(), sync.isHugeStorage());
        },

        write: function (sync, key, value, cb, scope) {
            WA.FocusManager.ifFocused(
                    function () {
                        var data = U.Object.pair(this._generateStorageKey(sync, key), value);
                        this._getSyncStorage(sync).save(data, {
                            success: function () {
                                if (WA.isFunction(cb)) {
                                    cb.call(scope || window);
                                }
                            },
                            failure: WA.createDelegate(
                                    function (e, errorInfo) {
                                        // TODO: smth
                                        //WA.error(e + ', info: [' + errorInfo + ']');
                                    },
                                    this,
                                    [
                                        [sync.getId(), key, value].join(' ')
                                    ],
                                    1
                                    ),
                            scope: this
                        });
                    },
                    null,
                    this
                    );
        },

        read: function (sync, cb, scope) {
            var keys = this._generateStorageKeys(sync);
            this._getSyncStorage(sync).load(keys, {
                success: function (storageData) {
                    var data = this._getSyncData(sync, storageData);
                    cb.call(scope || window, new SyncRecord(sync.getKeys(), data));
                },
                failure: WA.createDelegate(
                        function (e, errorInfo) {
                            WA.error(e + ', info: [' + errorInfo + ']');
                        },
                        this,
                        [
                            [sync.getId(), keys].join(' ')
                        ],
                        1
                        ),
                scope: this
            });
        },

        whenReady: function (sync, fn, scope) {
            this._getSyncStorage(sync).whenReady(fn, scope);
        }

    });

    return new Synchronizer();

})();WebAgent.ChangeLog = (function () {

    var WA = WebAgent;
    var U = WA.util;
    var JSON = WA.getJSON();

    var readyEvent = new U.Event();

    var ChangeLog = WA.extend(WA.Activatable, {

        constructor: function () {
            this.sequenceId = -2;

            this.data = [];

            this.sync = WA.Synchronizer.registerComponent('changelog', ['data']);

            this.updateEvent = new U.Event();

            this.updateEvent.on(function (data) {
                WA.debug.State
                        .set('changelog-id', this.sequenceId)
                        .set('changelog-data', JSON.stringify(data))
                        ;
            }, this);
        },

        _onReady: function () {
            this.sync.read(function (syncRecord) {
                this._onSyncRestore(syncRecord);

                if (this._isReady()) {
                    readyEvent.fire();
                } else {
                    WA.error('Initialization failed');
                }

                this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);

                WA.conn.Connection.triggerEvent.on(this._onConnectionTriggerEvent, this);

                WA.conn.Socket.afterResponseEvent.on(this._onSocketAfterResponseEvent, this);

                WA.Invalidator.invalidateEvent.on(this._onInvalidate, this);
            }, this);
        },

        _isReady: function () {
            return this.sequenceId >= 0;
        },

        whenReady: function (fn, scope) {
            if (this._isReady()) {
                fn.call(scope);
            } else {
                readyEvent.on(fn, scope, { single: true });
            }
        },

        _onActivate: function () {
            if (!this._isReady()) {
                if (this.sequenceId === -2) {
                    this.sync.whenReady(this._onReady, this);
                } else {
                    this.sync.read(this._onSyncRestore, this);
                }
            }
        },

        _onDeactivate: function () {
            this._clear();
        },

        _onInvalidate: function () {
            if (this.isActive()) {
                this._clear();
                this.sync.read(this._onSyncRestore, this);
            }
        },

        getSequenceId: function () {
            return this.sequenceId;
        },

        get: function (mail) {
            var index = this._logIndexOf(mail);
            return this.data[index];
        },

        _onConnectionTriggerEvent: function (type, value) {
            if (type === 'contactStatus' && this.data) {
                this._log(value);
            }
        },

        _onSocketAfterResponseEvent: function () {
            this._commit();
        },

        _logIndexOf: function (mail) {
            return U.Array.indexOfBy(this.data, function (logItem) {
                return logItem.email === mail;
            });
        },

        logAction: function (action, data) {
            var o = WA.applyIf({ action: action }, data);

            WA.applyIf(o, { status: { type: null } });

            this._log(o);
            this._commit(); // forced commit
        },

        _log: function (data) {
            if (!this.isActive()) {
                return;
            }

            var index = this._logIndexOf(data.email);

            if (index != -1) {
                this.data[index].nick = data.nickname;
                this.data[index].status = data.status.type;
                this.data[index].statusTitle = data.status.title;
            } else {
                this.data.push({
                    email: data.email,
                    status: data.status.type,
                    statusTitle: data.status.title || null,
                    nick: data.nickname,
                    action: data.action
                });
            }
            //WA.debug.Console.info(data.email, data.status.type);
        },

        _commit: function () {
            if (!this.isActive()) {
                return;
            }

            if (this._isReady()) {
                if (this.sequenceId === Number.MAX_VALUE) {
                    this.sequenceId = 0;
                } else {
                    this.sequenceId += 1;
                }

                var newData = JSON.stringify({
                    id: this.sequenceId,
                    data: this.data
                });

                this.sync.write('data', newData, function () {
                    this.updateEvent.fire(this.data);
                    this.data = [];
                }, this);
            }
        },

        _clear: function () {
            this.sequenceId = -1;
            this.data = [];
        },

        _parseData: function (str) {
            return JSON.parse(str || 'null');
        },

        _parseSeqId: function (v) {
            return parseInt(v || 0);
        },

        _onSyncRestore: function (syncRecord) {
            var o = this._parseData(syncRecord.get('data'));
            if (!o) {
                this.sequenceId = 0;
            } else {
                this.sequenceId = this._parseSeqId(o.id);
            }
            WA.debug.State.set('changelog-id', this.sequenceId);
        },

        _onSyncTriggerEvent: function (syncRecord) {
            if (!this.isActive()) {
                return;
            }

            if (syncRecord.isChanged('data')) {
                var o = this._parseData(syncRecord.get('data'));

                if (o) {
                    var sequenceId = this._parseSeqId(o.id);

                    if (sequenceId !== 0 && this.sequenceId !== -1
                            && Math.abs(sequenceId - this.sequenceId) > 1) {
                        WA.Invalidator.invalidate();
                    } else {
                        this.sequenceId = sequenceId;
                    }

                    this.data = o.data;
                    this.updateEvent.fire(this.data, true);
                }
            }
        }

    });

    return new ChangeLog();

})();(function () {

    var WA = WebAgent;
    var JSON = WA.getJSON();
    var U = WA.util;


    var TYPING_STATE = 'typingNotify';
    var MESSAGE_STATE = 'message';

    var TYPING_INTERVAL = 15000;

    var CS = WA.extend(WA.Activatable, {

        constructor: function () {
            this.data = {};

            this.typings = {};

            this.updateEvent = new WA.util.Event();

            this.sync = WA.Synchronizer.registerComponent('constates', ['data'], true);
            this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);

            WA.conn.Connection.triggerEvent.on(this._onConnectionTriggerEvent, this);
            WA.conn.UserState.statusChangedEvent.on(function (status) {
                if(status == 'offline') {
                    this.data = {};
                    U.Object.each(this.typings, function (task) {
                        task.stop();
                    }, this);
                    this.typings = {};
                }
            }, this);
        },

        _fireUpdateEvent: function () {
            if (this.isActive()) {
                var data = WA.apply({}, this.data);
                this.updateEvent.fire(data);
            }
        },

        getStates: function (email) {
            var o = this.data[email];
            return o ? o.states : null;
        },

        getUnread: function (email) {
            var o = this.data[email];
            return o ? o.unread : null;
        },

        _saveSyncData: function () {
            if (this.isActive()) {
                this.sync.write('data', JSON.stringify(this.data));
            }
        },

        removeState: function (email, state) {
            this._removeState(email, state);
        },

        removeStates: function (emails, state) {
            if (WA.isArray(emails) && emails.length > 0) {
                var statefulEmails = [];

                WA.util.Array.each(emails, function (m) {
                    if (this.getStates(m)) {
                        statefulEmails.push(m);
                    }
                }, this);

                var len = statefulEmails.length;
                if (len > 0) {
                    var silentOptions = {
                        suppressEvent: true,
                        sync: false
                    };

                    WA.util.Array.each(statefulEmails, function (m, index) {
                        if (len - 1 === index) {
                            this.removeState(m, state);
                        } else {
                            this._removeState(m, state, silentOptions);
                        }
                    }, this);
                }
            }
        },

        _removeState: function (email, state, options) {
            var states = this.getStates(email);
            if (states) {
                WA.util.Array.remove(states, state);

                options = options || {};

                if (options.suppressEvent !== true) {
                    this._fireUpdateEvent();
                }

                if (options.sync !== false) {
                    this._saveSyncData();
                }
            }
        },

        saveUnread: function (email, count) {
            if (this.data[email]) {
                this.data[email].unread = count;
            } else {
                this.data[email] = {
                    states: [],
                    unread: count
                };
            }
            this._saveSyncData();
        },

        _saveState: function (email, state) {
            var states = this.getStates(email);

            if (!states) {
                this.data[email] = {
                    states: [state]
                };
                this._saveState(email, state);
                return;
            }

            if (state === MESSAGE_STATE) {
                WA.util.Array.remove(states, TYPING_STATE);
                WA.util.Array.remove(states, MESSAGE_STATE);
            }

            if (WA.util.Array.indexOf(states, state) == -1) {
                states.push(state);
            }

            if (state === TYPING_STATE) {
                this._processTyping(email);
            }

            this._fireUpdateEvent();
            this._saveSyncData();
        },

        _processTyping: function (email, interval) {
            if (this.data[email]) {
                var typing = this.typings[email];
                if (!typing) {
                    typing = this.typings[email] = new WA.util.DelayedTask({
                        interval: interval || TYPING_INTERVAL,
                        fn: WA.createDelegate(function (m) {
                            delete this.typings[m];
                            delete this.data[m].typing;
                            this._removeState(m, TYPING_STATE, {
                                sync: false
                            });
                        }, this, [email], 0)
                    });
                }
                typing.start();
                this.data[email].typing = new Date().getTime();
            }
        },

        _onConnectionTriggerEvent: function (type, value) {
            if (WA.util.Array.indexOf([TYPING_STATE, MESSAGE_STATE], type) != -1) {
                this._saveState(value.from, type);
            }
        },

        _onActivate: function () {
            this.sync.whenReady(function () {

                this.sync.read(this._onSyncTriggerEvent, this);

            }, this);
        },

        _onSyncTriggerEvent: function (syncRecord) {
            if (syncRecord.isChanged('data')) {
                var data = JSON.parse(syncRecord.get('data') || 'null');
                if (data) {
                    this.data = data;

                    WA.util.Object.each(data, function (entry, email) {
                        if (WA.util.Array.indexOf(this.getStates(email) || [], TYPING_STATE) != -1) {
                            var typing = parseInt(this.data[email].typing) || TYPING_INTERVAL;
                            var delta = new Date().getTime() - typing;
                            if (delta >= 0 && delta < TYPING_INTERVAL) {
                                this._processTyping(email, TYPING_INTERVAL - delta);
                            } else {
                                this._removeState(email, TYPING_STATE, {
                                    suppressEvent: true,
                                    sync: false
                                });
                            }
                        }
                    }, this);

                    this._fireUpdateEvent();
                }
            }
        }

    });

    WA.ContactStates = new CS();

    WA.ContactStates.TYPING_STATE = TYPING_STATE;
    WA.ContactStates.MESSAGE_STATE = MESSAGE_STATE;

})();
(function () {

    var ALL = {
        status_5: '����',
        status_18: '�� ������',
        status_19: '�� �������',
        status_7: '��� �?!',
        status_10: '�����',
        status_47: '� ������',
        status_22: '�������',
        status_26: '� ���������',
        status_24: '�������',
        status_27: '� �����',
        status_23: '����',
        status_4: '�����',
        status_9: '�������',
        status_6: '��',
        status_21: '��� ����',
        status_20: '����',
        status_17: '����',
        status_8: '������!',
        status_15: '������ ���������',
        status_16: '�����',
        status_28: '�� �������� �������',
        status_51: '�����',
        status_52: '������',
        status_46: '�����',
        status_12: '� ��������',
        status_13: '���������',
        status_11: '� ��������',
        status_14: '����� � ���',
        status_48: '������� �����',
        status_53: '������',
        status_29: '������',
        status_30: '�������',
        status_32: '���������',
        status_33: '�������',
        status_40: '������',
        status_41: '����',
        status_34: '�����',
        status_35: '�����',
        status_36: '�����!',
        status_37: '����',
        status_38: '�����',
        status_39: '�����',
        status_42: '�������!',
        status_43: '������!',
        status_49: '�����!',
        status_44: '�����',
        status_45: '����!',
        status_50: '��!'
    };

    var DEFAULTS = ['status_52', 'status_17', 'status_21', 'status_37', 'status_26'];

    //-----------------------------------------------------------------------------------------------------------------

    var WA = WebAgent;
    var U = WA.util;
    var Storage = WA.Storage;
    var UI = WA.ui;
    var JSON = WA.getJSON();


    //-----------------------------------------------------------------------------------------------------------------

    var STORAGE_SCHEMA = ['type', 'text'];

    var XS = WA.extend(Object, {

        constructor: function () {
            this.currentStatuses = U.Array.transform(DEFAULTS, function (entry) {
                if (ALL.hasOwnProperty(entry)) {
                    return {
                        type: entry,
                        text: ALL[entry]
                    };
                } else {
                    WA.error('Invalid default status: ' + entry);
                }
            });

            this.dialogInstance = null;

            this.sync = WA.Synchronizer.registerComponent('xstatuses', ['data'], false);
            this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);

            this.updateEvent = new U.Event();

            this.sync.whenReady(function () {
                this.sync.read(this._onSyncTriggerEvent, this);
            }, this);
        },

        _onSyncTriggerEvent: function (syncRecord) {
            if (syncRecord.isChanged('data')) {
                var data = syncRecord.get('data');
                if (data) {
                    U.Array.each(JSON.parse(data), function (xs, index) {
                        U.Array.each(xs, function (value, i) {
                            var key = STORAGE_SCHEMA[i];
                            this.currentStatuses[index][key] = value;
                        }, this);
                    }, this);
                    this.updateEvent.fire(this.currentStatuses);
                }
            }
        },

        update: function (data) {
            if (data.length != this.currentStatuses.length) {
                WA.error('Invalid data');
            }
            var storageData = [];
            U.Array.each(data, function (entry, index) {
                storageData[index] = U.Array.transform(STORAGE_SCHEMA, function (k) {
                    return entry[k];
                });
            });
            this.sync.write('data', JSON.stringify(storageData), function () {
                this.currentStatuses = U.Array.clone(data);
                this.updateEvent.fire(this.currentStatuses);
            }, this);
        },

        getCurrents: function () {
            return this.currentStatuses;
        },

        getCurrentText: function (status) {
            var xs = U.Array.findBy(this.currentStatuses, function (entry) {
                if (entry.type === status) {
                    return true;
                }
            });
            if (xs) {
                return xs.text;
            } else {
                return null;
            }
        },

        getAll: function () {
            return ALL;
        },

        openDialog: function (container) {
            var dialog = this.dialogInstance;
            if (!dialog) {
                dialog = this.dialogInstance = new Dialog(this);
                dialog.render(container);
            }
            return dialog.show();
        }

    });

    //-----------------------------------------------------------------------------------------------------------------

    var DialogItem = WA.extend(UI.Component, {

        constructor: function (window, xs) {
            this.window = window;

            DialogItem.superclass.constructor.call(this);

            this.textField = new UI.TextField({
                maxLength: 32,
                cls: 'nwa-rename-statuses__item-field'
            });

            this.clearBtn = new WA.ui.Button({
                cls: 'nwa-rename-statuses__item-clear',
                handler: function(){
                    this.textField.val('');
                    this.textField.focus();
                },
                scope: this
            });

            var items = [];
            U.Object.each(ALL, function (text, type) {
                items.push(
                        new UI.menu.Image({
                            cls: WA.buildIconClass(type),
                            title: text,
                            statusType: type
                        })
                        );
            });

            var menu = new UI.menu.ImageMenu({
                items: items
            });

            this.picker = new UI.ImageButton({
                menu: menu,
                cls: 'nwa-rename-statuses__picker'
            });

            menu.itemClickEvent.on(this._onMenuItemClick, this);

            this.setValue(xs);
        },

        _onRender: function (container) {
            this.el = container.createChild({
                cls: 'nwa-rename-statuses__item',
                children: [
                    {
                        cls: 'nwa-rename-statuses__item-picker'
                    }
                ]
            });

            this.picker.render(this.el.first());
            this.textField.render(this.el);
            this.clearBtn.render(this.el);
        },

        _onMenuItemClick: function (menu, item) {
            var config = item.initialConfig;
            var status = config.statusType;
            this.setValue({
                type: status,
                //text: this.window.xsObject.getCurrentText(status) || config.title
                text: this.value.text || ''
            });
        },

        setValue: function (value) {
            this.value = WA.apply({}, value);
            this.picker.setIconCls(WA.buildIconClass(value.type));
            this.textField.val(value.text);
        },

        getValue: function () {
            this.value.text = this.textField.val();
            return this.value;
        },

        destroy: function () {
            this.picker.destroy();
            this.picker = null;

            this.textField.destroy();
            this.textField = null;

            this.window = null;

            DialogItem.superclass.destroy.call(this);
        }

    });

    //-----------------------------------------------------------------------------------------------------------------

    var Dialog = WA.extend(UI.Window, {

        closeAction: 'hide',

        constructor: function (xsObject) {
            this.xsObject = xsObject;

            var items = [];

            this.__eachCurrentStatus(function (xs) {
                items.push(new DialogItem(this, xs));
            });

            Dialog.superclass.constructor.call(this, {
                id: WA.buildId('xstatus-dlg'),
                cls: 'nwa-rename-statuses',
                fadeIn: true,
                items: items,
                title: '�������������� ��������',
                bbar: [
                    '->',
                    {
                        cls: 'nwa-button_blue',
                        text: '���������',
                        handler: function () {
                            this._saveStatuses();
                        },
                        scope: this
                    },
                    {
                        cls: 'nwa-button_blue',
                        text: '��������',
                        handler: function () {
                            this.close();
                        },
                        scope: this
                    }
                ]
            });
        },

        __eachCurrentStatus: function (fn, scope) {
            U.Array.each(this.xsObject.getCurrents(), fn, scope || this);
        },

        _saveStatuses: function () {
            var data = U.Array.transform(this.items, function (item) {
                return item.getValue();
            });
            this.xsObject.update(data);
            this.close();
        },

        _onBeforeShow: function (me) {
            Dialog.superclass._onBeforeShow.call(this, me);

            this.__eachCurrentStatus(function (xs, index) {
                this.items[index].setValue(xs);
            }, this);
        },

        _onHide: function (me) {
            Dialog.superclass._onHide.call(this, me);
        },

        destroy: function () {
            this.xsObject = null;

            Dialog.superclass.destroy.call(this);
        }

    });

    //-----------------------------------------------------------------------------------------------------------------

    WA.XStatuses = new XS();

})();(function () {

    var WA = WebAgent;
    var U = WA.util;
    var UI = WA.ui;
    var ConnectionRoster = WA.conn.ConnectionRoster;

    var FRIENDSHIP_TYPES = [
        '', '��������, ��� ��� ������������', '��������, ��� ��� ������������',
        '��������, ��� ��� �������', '� ��� ���� ����� ������ � ���� ����', '���� ������� �������� �� ��� ����',
        '������� �� ������ ������ ���������', '�� � �������� � ������ ���������',
        '�� ��������� �� ���� ����� ��������', '', '�� ������������ �������� � ���� ���������',
        '�� ������������ �������� � ���� ���������', '�� ������������ �������� � ���� ���������',
        '�� ������������ �������� � ���� ���������', '��� ��� ���� � ���� ����', '��� ���� ������ ����� � ���� ����'
    ];

    var TAB_HTML = '<div class="nwa-button_blue nwa-add__tab" wa-tabindex="{1}"><span class="nwa-button" wa-tabindex="{1}">{0}</span></div>';
    var ACTIVE_TAB_HTML = '<div class="nwa-white-plate nwa-add__tab_active" wa-tabindex="{1}"><span class="nwa-button" wa-tabindex="{1}">{0}</span></div>';
    var SEARCH_RESULT_HTML = '<div class="nwa-suggest-item nwa-add__search-item">\
                                        <div class="nwa-add__search-item__userpic" wa-action="userpic" wa-mail="{mail}" style="background-image:url({avatar});"></div>\
                                        <div class="nwa-add__search-item__info">\
                                            <div class="nwa-add__search-item__name {status}">{name}<div class="nwa-add__search-item__name-fader"></div></div>{relation}\
                                            <div class="nwa-add__search-item__buttons">\
                                                <span class="nwa-add__search-item__button nwa-dialog__tools-profile" wa-action="profile" wa-mail="{mail}">������</span>\
                                                <span class="nwa-add__search-item__button nwa-add__btn-add" wa-action="add" wa-mail="{mail}" wa-name="{name}">��������</span>{nofriends}\
                                            </div>\
                                        </div>\
                                    </div>';
    var SEARCH_RESULT_TOP = '<div class="nwa-add__title2">�� ������ <span>{title}</span><div class="nwa-add__title2-fader"></div></div></div><div class="nwa-add__return"><a wa-action="back" href="javascript: void(0);">��������� � ���������� ������</a></div>';
    var SEARCH_EMPTY = '<div class="nwa-add__title2">������ �� �������</div>';

    var Tabs = WA.extend(UI.Component, {
        constructor: function () {
            Tabs.superclass.constructor.apply(this, arguments);
            this.tabChangeEvent = new U.Event();
            this._list = [];
            this._index = 0;
        },
        
        _onRender: function (el) {
            this.el = el.createChild({
                cls: 'nwa-add__tabs'
            });
            this.el.on('click', this._onClickEvent, this);
            this._redraw();
        },

        _onClickEvent: function (el) {
            var trg = el.getTarget(true);
            var index = +trg.getAttribute('wa-tabindex');
            if(!isNaN(index)) {
                if(this.setFocus(index)) {
                    this.tabChangeEvent.fire(index);
                }
            }
        },

        _redraw: function () {
            var html = [];
            U.Array.each(this._list, function (title, i) {
                html.push(U.String.format(
                    (i == this._index ? ACTIVE_TAB_HTML : TAB_HTML),
                    title,
                    i
                ));
            }, this);
            this.el.update(html.join(''));
        },

        addTab: function (title) {
            this._list.push(title);
        },

        setFocus: function (index) {
            if(this._index != index) {
                this._index = index;
                this._redraw();
                return true;
            }
        }

    });

    var PlateSearch = WA.extend(UI.Component, {
        constructor: function () {
            PlateSearch.superclass.constructor.apply(this, arguments);
            this.searchEvent = new U.Event();

            
            this._emailField = new UI.TextField({
                hintText: '������� e-mail ��� ����� ICQ',
                cls: 'nwa-textfield nwa-textfield_thin nwa-addcontact__search-field nwa_hint_msg'
            });

            this._emailSearch = new UI.Button({
                cls: 'nwa-button nwa-button_orange nwa-addcontact__search-button',
                text: '�����'
            });

            this._params = new UI.Collection({
                firstname: new UI.Label({
                    title: '���:',
                    child: new UI.TextField({
                        cls: 'nwa-textfield nwa-addcontact__search-field'
                    })
                }),
                lastname: new UI.Label({
                    title: '�������:',
                    child: new UI.TextField({
                        cls: 'nwa-textfield nwa-addcontact__search-field'
                    })
                }),
                sex: new UI.Label({
                    title: '���:',
                    child: new UI.Component()
                }),
                age: new UI.Label({
                    title: '�������',
                    child: new UI.Component()
                }),
                country: new UI.Label({
                    title: '������:',
                    child: new UI.Select({
                        cls: 'nwa-textfield nwa-addcontact__search-select'
                    })
                }),
                region: new UI.Label({
                    title: '������:',
                    child: new UI.Select({
                        cls: 'nwa-textfield nwa-addcontact__search-select'
                    })
                }),
                city: new UI.Label({
                    title: '�����:',
                    child: new UI.Select({
                        cls: 'nwa-textfield nwa-addcontact__search-select'
                    })
                })
            });
            this._country = this._params.children.country.child;
            this._country.changeEvent.on(this._onCountryChange, this);

            this._region = this._params.children.region.child;
            this._region.changeEvent.on(this._onRegionChange, this);

            this._city = this._params.children.city.child;

            this._paramsSearch = new UI.Button({
                cls: 'nwa-button nwa-button_orange nwa-addcontact__search-button',
                text: '�����'
            });
        },

        _onRender: function (el) {
            this.el = el.createChild({
                style: 'display:block',
                children: [
                    {
                        cls: 'nwa-white-plate nwa-add__white1',
                        children: [
                            {
                                cls: 'nwa-add__search-button'
                            },
                            {
                                cls: 'nwa-add__search-input'
                            },
                            {
                                cls: 'nwa-add__error',
                                children: '������������ e-mail ��� ����� ICQ'
                            }
                        ]
                    },
                    {
                        cls: 'nwa-white-plate nwa-add__white2',
                        children: [
                            {
                                cls: 'nwa-add__title',
                                children: '����� �� ������:'
                            },
                            {
                                cls: 'nwa-twocol'
                            },
                            {
                                cls: 'nwa-add__map',
                                children: [
                                    {
                                        tag: 'a',
                                        href: 'http://maps.mail.ru/?units&origin=webagent',
                                        target: '_blank',
                                        children: '����� �� �����'
                                    }
                                ]
                            },
                            {
                                cls: 'nwa-add__search-button',
                                children: []
                            }
                        ]
                    }
                ]
            });

            this.el.first().on('keyup', function (ev) {
                if(ev.keyCode == 13) {
                    this._searchEmail();
                }
            }, this);

            this._emailSearchErrorEl =  this.el.first().last();

            this.el.last().on('keyup', function (ev) {
                if(ev.keyCode == 13) {
                    this._searchParams();
                }
            }, this);

            this._paramsSearch.render(this.el.first().next().last());
            this._paramsSearch.el.on('click', this._searchParams, this);
            
            this._emailSearch.render(this.el.first().first());
            this._emailSearch.el.on('click', this._searchEmail, this);

            this._params.children.sex.child.autoEl = [
                {
                    tag: 'input',
                    type: 'radio',
                    'wa-name': 'sex',
                    'wa-value': '1',
                    name: 'waSearchSex',
                    id: 'nwaSearchSexMale'
                },
                {
                    tag: 'label',
                    'for': 'nwaSearchSexMale',
                    children: ' �������'
                },
                {
                    tag: 'input',
                    type: 'radio',
                    'wa-name': 'sex',
                    'wa-value': '2',
                    name: 'waSearchSex',
                    id: 'nwaSearchSexFemale'
                },
                {
                    tag: 'label',
                    'for': 'nwaSearchSexFemale',
                    children: ' �������'
                }
            ];
            
            this._params.children.age.child.autoEl = [
                {
                    tag: 'input',
                    cls: 'nwa-textfield nwa-addcontact__search-field nwa-add__input-short',
                    type: 'text',
                    'wa-name': 'ageFrom',
                    id: 'nwaSearchAgeFrom'
                },
                {
                    tag: 'label',
                    cls: 'nwa-add__search-txt',
                    children: '��',
                    'for': 'nwaSearchAgeUntil'
                },
                {
                    tag: 'input',
                    cls: 'nwa-textfield nwa-addcontact__search-field nwa-add__input-short',
                    type: 'text',
                    'wa-name': 'ageUntil',
                    id: 'nwaSearchAgeUntil'
                }
            ];

            this._params.render(this.el.first().next().first().next());
            this._params.children.sex.child.el.parent().addClass('nwa-twocol__value__label');
            this.reset();

            this._emailField.render(this.el.first().first().next());
        },

        _searchEmail: function () {
            var val = U.String.trim(this._emailField.val());
            if(val) {
                if( this.searchEvent.fire({ email: val }) === false ) {
                    this._emailSearchErrorEl.show();
                } else {
                    this._emailSearchErrorEl.hide();
                }
            } else {
                this._emailSearchErrorEl.show();
            }
        },

        _searchParams: function () {
            var opts = {};
            var isEmpty = true;
            var location = [];
            this._params.each(function (item, key) {
                if(item.child.val) {
                    if(item.child.val()) {
                        opts[key] = item.child.val();
                        isEmpty = false;
                        
                        if(item.child.getTitle) {
                            location.push(item.child.getTitle());
                        }
                    }
                } else {
                    item.child.el.parent().each(function (el) {
                        var name = el.getAttribute('wa-name');
                        var type = el.getAttribute('type');
                        var value = el.getAttribute('wa-value');
                        if(name && type == 'radio') {
                            if(el.dom.checked) {
                                opts[name] = value;
                                isEmpty = false;
                            }
                        } else if (name && el.dom.value) {
                            opts[name] = el.dom.value;
                            isEmpty = false;
                        }
                    });
                }
                
            }, this);

            if(isEmpty) {
                opts.isEmpty = true;
            }
            this.searchEvent.fire(opts, location.join(', '));
        },

        reset: function () {
            this._country.setValue('');
            this._region.disable();
            this._city.disable();
            var list = this.el.dom.getElementsByTagName('input');
            U.Array.each(list, function (el) {
                if(el.getAttribute('type') == 'text') {
                    el.value = '';
                }
                if(el.getAttribute('type') == 'radio') {
                    el.checked = false;
                }
            });
            this._emailField.val('');
            this._emailSearchErrorEl.hide();
        },

        _onAfterRender: function () {
            if(!this._base && !this._baseRequested) {
                this._baseRequested = true;
                WA.setGeobase = WA.createDelegate(this._setGeobase, this);
                var script = document.createElement('script');
                script.src = WA.resDomain + WA.resPath.replace(/(\/[^\/]+){2}$/, '/') + 'geobase.js?' + Math.random();
                script.type = 'text/javascript';
                WA.getBody().appendChild(script);
            }
        },

        __update: function (select, t, prefix) {
            var list = t.slice(0);
            list.unshift([prefix, '']);
            select.update(list);
        },

        _setGeobase: function (data) {
            this._base = data;
            this._country.enable();
            this.__update(this._country, this._base[0], '����� ������');
        },

        _onCountryChange: function (id) {
            this._city.disable();

            if(this._base[id]) {
                this._region.enable();
                if(this._base[this._base[id][0][1]]) {
                    this.__update(this._region, this._base[id], '����� ������');
                } else {
                    this._region.disable();
                    this._onRegionChange(id);
                }
            } else {
                this._region.disable();
            }
        },

        _onRegionChange: function (id) {
            if(this._base[id]) {
                this._city.enable();
                this.__update(this._city, this._base[id], '����� �����');
            } else {
                this._city.disable();
            }
        }
    });

    var PlateResult = WA.extend(UI.Component, {
        constructor: function () {
            PlateResult.superclass.constructor.apply(this, arguments);
            this.showProfileEvent = new U.Event();
            this.showUserpicEvent = new U.Event();
            this.noFriendsEvent = new U.Event();
            this.addEvent = new U.Event();
            this.backEvent = new U.Event();
        },

        _onRender: function (el) {
            this.el = el.createChild({
                style: 'display: none;',
                cls: 'nwa-white-plate nwa-add__white3 nwa-add__search-result',
                children:[
                    {
                        cls: 'nwa-add__search-result-inner'
                    }
                ]
            });

            this._contEl = this.el.first();
            this.scrollBar = new WA.ui.ScrollBar({source: this._contEl});
            this.scrollBar.render(this.el);
        },

        _onAfterRender: function () {
            this._contEl.on('click', this._onClickEvent, this);
        },

        _onClickEvent: function (el) {
            var trg = el.getTarget(true);
            var action = trg.getAttribute('wa-action');
            var mail = trg.getAttribute('wa-mail');
            switch(action) {
                case 'back':
                    this.backEvent.fire();
                    break;
                case 'userpic':
                    this.showUserpicEvent.fire(mail);
                    break;
                case 'profile':
                    this.showProfileEvent.fire(mail, this._isSuggest);
                    break;
                case 'add':
                    this.addEvent.fire(mail, false, this._isSuggest);
                    break;
                case 'nofriends':
                    this.noFriendsEvent.fire(mail, trg);
                    break;
            }
        },

        setTitle: function (txt) {
            this._contEl.update(U.String.parseTemplate(SEARCH_RESULT_TOP, {
                title: U.String.htmlEntity(txt)
            }));
            this.scrollBar.sync();
        },

        update: function (list) {
            if (this.rendered) {
                if(!list || list.length == 0) {
                    this.el.removeClass('nwa-preloader');
                    this._contEl.createChild(SEARCH_EMPTY);
                } else {
                    var html = [];
                    U.Array.each(list, function (contact) {
                        var t = contact.email.match(/([^@]+)@([^\.]+)/);
                        var isUIN = contact.email.indexOf('@uin.icq') != -1;
                        html.push(U.String.parseTemplate(SEARCH_RESULT_HTML, {
                                avatar: WA.makeAvatar(contact.email, '_avatarsmall'),
                                name: U.String.htmlEntity(WA.MainAgent.WP2Nick(contact)),
                                relation: '',
                                nofriends: '',
                                mail: contact.email,
                                status: WA.buildIconClass(isUIN ? 'icq_online' : 'online')
                        }));
                    });

                    this.el.removeClass('nwa-preloader');
                    this._contEl.createChild(html.join(''));
                }
                this.scrollBar.sync();
            }
        },

        reset: function () {
            this._contEl.update('');
            this.el.addClass('nwa-preloader');
        }

    });

    var PlateSuggest = WA.extend(PlateResult, {
        constructor: function () {
            PlateSuggest.superclass.constructor.apply(this, arguments);
            this.requestContactsInfoEvent = new U.Event();
            this._isSuggest = true;
        },

        _onShow: function () {
            if(!this._xdr) {
                this._xdr = WA.XDRequest.getInstance('mrimraker1.mail.ru');
            }

            var data = WA.apply({});

            this.el.addClass('nwa-preloader');
            this._contEl.update('');
            this.scrollBar.sync();

            var dispatch = WA.createDelegate(function (data) {
                var nick = data.data.match(/[^\[]+(?=\]\]\>\<\/Nick\>)/g);
                var email = data.data.match(/[^\[]+(?=\]\]\>\<\/Email\>)/g);
                var sex = data.data.match(/[^\[]+(?=\]\]\>\<\/Sex\>)/g);
                var type = data.data.match(/[^\[]+(?=\]\]\>\<\/FriendshipType\>)/g);
                var html = [];

                if(!email) {
                    this.el.removeClass('nwa-preloader');
                    this._contEl.update(html.join(''));
                } else {
                    this.requestContactsInfoEvent.fire(email, function (contacts) {
                        var contactsHash = {};
                        U.Array.indexOfBy(contacts, function (contact) {
                            contactsHash[contact[0]] = 1;
                        });

                        for(var i=0; i<nick.length; i++) {
                            if(!contactsHash[email[i]]) {
                                var t = email[i].match(/([^@]+)@([^\.]+)/);
                                var isUIN = email[i].indexOf('@uin.icq') != -1;
                                html.push(U.String.parseTemplate(SEARCH_RESULT_HTML, {
                                        avatar: WA.makeAvatar(email[i], '_avatarsmall'),
                                        name: U.String.htmlEntity(nick[i]),
                                        relation: '<div class="nwa-add__search-item__relation">' + (FRIENDSHIP_TYPES[+type[i]] || '������� ���� �� ������') + '<div class="nwa-add__search-item__name-fader"></div></div>',
                                        nofriends: '<span class="nwa-add__search-item__button nwa-add__btn-remove" wa-action="nofriends" wa-mail="' + email[i] + '">�� �������</span>',
                                        mail: email[i],
                                        status: WA.buildIconClass(isUIN ? 'icq_online' : 'online')
                                }));
                            }
                        }
                        this.el.removeClass('nwa-preloader');
                        this._contEl.update(html.join(''));
                        this.scrollBar.sync();
                    }, this);
                }
            }, this);

            this._xdr.request({
                url: '/agent_suggest',
                data: data
            }, dispatch, WA.createDelegate(function() {
                this._xdr.request({
                    url: '/agent_suggest',
                    data: data
                }, dispatch, WA.createDelegate(function () {
                    this.el.removeClass('nwa-preloader');
                    this._contEl.update('');
                }, this));
            }, this));
        }
    });

    var PlateCollection = WA.extend(UI.Component, {
        constructor: function () {
            PlateCollection.superclass.constructor.apply(this, arguments);
            this._collection = [];
        },

        add: function (plate) {
            this._collection.push(plate);
        },

        show: function (index) {
            if(index < this._collection.length) {
                U.Array.each(this._collection, function (plate, i) {
                    plate.hide();
                }, this);
                this._collection[index].show();
            }
        }
    });

    var Abstraction = WA.extend(ConnectionRoster, {

        constructor: function () {
            Abstraction.superclass.constructor.apply(this, arguments);
            this.updateEvent = new U.Event();
            this.addSuccessEvent = new U.Event();
            this._profiles = {};
            this._waiting = {};
            this._addHistory = {};
        },

        search: function (opts) {
            opts.uniq = this._uniq = opts.uniq || Math.round(Math.random()*999999);
            WA.conn.Socket.send('wp', opts);
            this.__search = opts;
        },

        get: function (mail, cb, scope) {
            if(this._profiles[mail]) {
                cb.call(scope || window, this._profiles[mail]);
            } else {
                if(!this._waiting[mail]) {
                    this._waiting[mail] = [];
                    this.search({email: mail});
                }
                this._waiting[mail].push([cb, scope]);
            }
        },

        _invokeWaitings: function () {
            var dellist = [];
            U.Object.each(this._waiting, function (cbs, mail) {
                if(this._profiles[mail]) {
                    U.Array.each(cbs, function (cb) {
                        cb[0].call(cb[1] || window, this._profiles[mail]);
                    }, this);
                    dellist.push(mail);
                }
            }, this);

            U.Array.each(dellist, function (mail) {
                delete this._waiting[mail];
            }, this);
        },

        _addSuccess: function (mail) {
            var nick = this.__getNick(mail);
            this.addSuccessEvent.fire(mail, nick);
            delete this._addHistory[mail];
        },

        _onConnectionContactAddFail: function (value) {
            if (this._addHistory[value.mail]) {
                switch(value.message) {
                    case 'exists':
                        this._addSuccess(value.mail);
                        break;
                    case 'ignored':
                        WA.conn.Socket.send('contact', {
                            operation: 'unignore',
                            email: value.mail
                        });
                        this._addSuccess(value.mail);
                        break;
                }
            }
        },

        _onConnectionContactAddSuccess: function (value) {
            this._addSuccess(value);
            
            if(this._addHistory[value]) {
                U.Mnt.count('addSuccess');
            }
        },

        _collectProfiles: function (value) {
            U.Array.each(value, function (contact) {
                this._profiles[contact.email] = contact;
            }, this);
        },

        _onConnectionWp: function (value) {
            if(value.uniq && value.uniq == this._uniq) {
                if(value.result) {
                    delete this.__search;
                    this._collectProfiles(value.result);
                    this.updateEvent.fire(value.result, value.uniq);
                    this._invokeWaitings();
                } else if(value.error == 'unknow') {
                    WA.setTimeout((function(){
                        return WA.createDelegate(function () {
                            this.search(this.__search);
                        }, this);
                    }).call(this), 5000);
                } else {
                    this.updateEvent.fire([]);
                }
            }
        },

        __getNick: function (mail) {
            return ( this._addHistory[mail] || {nick: mail} ).nick;
        },

        add: function (mail, nick) {
            this._addHistory[mail] = {
                nick: nick
            };
            WA.MainAgent.sendAddContact(mail, nick, function (success) {
                //todo handle errors
            }, this);
        }

    });

    WA.AddSearchDialog = WA.extend(UI.Layer, {
        constructor: function () {
            WA.AddSearchDialog.superclass.constructor.apply(this, arguments);
            this.autoEl = null;

            this.openContactEvent = new U.Event();
            this.addSuccessEvent = new U.Event();
            this.contactInfoRequestEvent = new U.Event();

            this._tabs = new Tabs();
            this._tabs.addTab('�����');
            this._tabs.addTab('�������� �� �������');
            this._tabs.setFocus(0);
            this._tabs.tabChangeEvent.on(this._onTabChangeEvent, this);

            this._search = new PlateSearch();
            this._search.searchEvent.on(this._onSearchEvent, this);

            this._result = new PlateResult();
            this._result.showUserpicEvent.on(this._onShowUserpicEvent, this);
            this._result.showProfileEvent.on(this._onShowProfileEvent, this);
            this._result.noFriendsEvent.on(this._onNoFriendsEvent, this);
            this._result.addEvent.on(this._onAddEvent, this);
            this._result.backEvent.on(function () {
                this._plates.show(0);
            }, this);

            this._suggest = new PlateSuggest();
            this._suggest.showUserpicEvent.on(this._onShowUserpicEvent, this);
            this._suggest.showProfileEvent.on(this._onShowProfileEvent, this);
            this._suggest.noFriendsEvent.on(this._onNoFriendsEvent, this);
            this._suggest.addEvent.on(this._onAddEvent, this);
//            this._suggest.requestContactsInfoEvent.on(function (maillist, cb, scope) {
//                this.contactInfoRequestEvent.fire(maillist, cb, scope);
//            }, this);
            this.contactInfoRequestEvent.relay(this._suggest.requestContactsInfoEvent);

            this._plates = new PlateCollection();
            this._plates.add(this._search);
            this._plates.add(this._result);
            this._plates.add(this._suggest);


            this._abstraction = new Abstraction();
//            this._abstraction.updateEvent.on(this._result.update, this._result);
            this._abstraction.updateEvent.on(this._onUpdate, this);
            this._abstraction.addSuccessEvent.on(this._onAddSuccess, this);

        },

        _showProfile: function (mail, profile, isSuggest) {
            if(!this._profile) {
                this._profile = new WA.Profile(true, {
                    modal: true,
                    fadeIn: true,
                    overlay: this._mainEl.parent()
                });
                this._profile.addEvent.on(this._onAddEvent, this);
                this._profile.render(this._mainEl);
            }
            this._profile.show(mail, profile, isSuggest);
        },

        _hideProfile: function () {
            if(this._profile) {
                this._profile.hide();
            }
        },

        _onRender: function (el) {
            this.el = el.createChild({
                cls: 'nwa-add nwa_fadein',
                style: 'opacity: 1',
                children: [
                    {
                        cls: 'nwa-add__plate',
                        children: [
                            {
                                cls: 'nwa-add__nick',
                                children: [
                                    {
                                        tag: 'span',
                                        children: '����� � ���������� ���������'
                                    },
                                    {
                                        cls: 'nwa-header__nick-fadeout'
                                    }
                                ]
                            },
                            {
                                cls: 'nwa-addcontact__close'
                            }
                        ]
                    }
                ]
            });

            this._mainEl = this.el.first();
            this._mainEl.last().on('click', this.hide, this);
            this._tabs.render(this._mainEl);
            this._search.render(this._mainEl);
            this._result.render(this._mainEl);
            this._suggest.render(this._mainEl);
            this._photo = new WA.PhotoPreview({
                autoRender: this._mainEl,
                modal: true
            });
        },

        _onTabChangeEvent: function (index) {
            if(index == 1) {
                index = 2;
            }

            this._plates.show(index);
        },

        _onShowUserpicEvent: function (mail) {
            this._photo.show(mail);
        },

        _onShowProfileEvent: function (mail, isSuggest) {
            this._abstraction.get(mail, function (profile) {
                this._showProfile(mail, profile, isSuggest);
            }, this);
        },

        __addContactAndNick: function (mail, nick) {
            this._abstraction.add(mail, nick);
        },

        _onAddEvent: function (mail, nick, isSuggest) {
            this.contactInfoRequestEvent.fire([mail], function (contacts) {
                var contact = contacts[0];
                if(!contact) {
                    if(!nick) {
                        this._abstraction.get(mail, function (contact) {
                            this.__addContactAndNick(mail, WA.MainAgent.WP2Nick(contact));
                        }, this);
                    } else {
                        this.__addContactAndNick(mail, nick);
                    }
                } else {
                    this.openContactEvent.fire.apply(this.openContactEvent, contact);
                    this.hide();
                }
            }, this);

            if(isSuggest) {
                U.Mnt.count('addBySuggest');
            } else {
                U.Mnt.count('addByEmail');
            }
        },

        _onAddSuccess: function (mail, nick) {
            this.openContactEvent.fire(mail, nick, 'gray');
            this.addSuccessEvent.fire(mail, nick);
            this.hide();

        },

        _onNoFriendsEvent: function (mail, el) {
            el.up('nwa-suggest-item').remove();

            if (mail.indexOf('@uin.icq') != -1) {
                return;
            }

            this.el.mask();

            if (!this._xdr) {
                this._xdr = WA.XDRequest.getInstance('mrimraker1.mail.ru');
            }

            var data = {
                exclude: mail
            };

            this._xdr.request({
                    url: '/agent_suggest',
                    data: data
                },
                WA.createDelegate(this.el.unmask, this.el),
                WA.createDelegate(this.el.unmask, this.el));

            U.Mnt.count('removeBySuggest');
        },

        __isIcq: function (uin) {
            var digits = (uin.match(/\d+/g) || []).join('');
            if(!!(uin.indexOf('@') == -1 && digits.length > 4)) {
                return digits;
            } else {
                return false;
            }
        },

        __isValidMail: function (email) {
            return !!email.match(/^[^@]+@([a-z0-9][a-z0-9-]+\.){1,}([a-z0-9][a-z0-9-]{1,})$/i);
        },

        searchMail: function (mail, openProfile) {
            if(openProfile) {
                this._openProfileUniq = Math.round(Math.random() * 999999);
            }
            this._onSearchEvent({
                email: mail,
                uniq: this._openProfileUniq
            });
        },

        _onUpdate: function (value, uniq) {
            this._result.update(value);

            if (uniq && uniq == this._openProfileUniq) {
                if(value && value.length) {
                    this._onShowProfileEvent(value[0].email);
                }
                delete this._openProfileUniq;
            }
        },

        _onSearchEvent: function (opts, location) {
            var isValid = false;
            if(opts.email) {
                var email = opts.email, uin;
                if(this.__isValidMail(email)) {
                    isValid = true;
                } else if (uin = this.__isIcq(email)) {
                    opts.email = uin + '@uin.icq';
                    isValid = true;
                }
            } else {
                isValid = true;
            }
            
            if(isValid) {
                var searchTitle = email;
                if(!searchTitle) {
                    searchTitle = U.String.trim((opts.firstname||'') + ' ' + (opts.lastname||''));
                    var params = [];
                    if(searchTitle) {
                        params.push(searchTitle);
                    }
                    if(opts.sex) {
                        params.push((['', '�������', '�������'])[+opts.sex]);
                    }
                    if(opts.ageFrom) {
                        params.push('�� ' + opts.ageFrom);
                    }
                    if(opts.ageUntil) {
                        params.push('�� ' + opts.ageUntil);
                    }
                    if(location) {
                        params.push(location);
                    }
                    if(opts.isEmpty) {
                        params = ['��� ����������'];
                    }
                    searchTitle = params.join(', ');
                }

                this._abstraction.search(opts);
                this._result.reset();
                this._result.setTitle(searchTitle);
                this._plates.show(1);
            } else {
                return false;
            }
        },

        _onHide: function () {
            WA.AddSearchDialog.superclass._onHide.apply(this);
            this._plates.show(0);
            this._search.reset();
            this._tabs.setFocus(0);
            this._hideProfile();
            this._photo.hide();
        },

        _onShow: function () {
            WA.AddSearchDialog.superclass._onShow.apply(this);
            U.Mnt.count('openAddDialog');
            U.Mnt.activeUserCount();
            U.Mnt.rbCountAction();//RB open add dialog
        }
        
    });

})();(function () {

    var WA = WebAgent;
    var U = WA.util;
    var JSON = WA.getJSON();


    // ---------------------------------------------------------------------------------

    var AbstractData = WA.extend(WA.Activatable, {

        constructor: function () {
            this.data = null;

            this.loadEvent = new U.Event();

            WA.Invalidator.invalidateEvent.on(this._onInvalidate, this);

            WA.ChangeLog.updateEvent.on(this._onChangeLogUpdate, this);
        },

        _onActivate: function () {
            this.loadEvent.resume();
        },

        _onDeactivate: function () {
            this.data = null;
            this.loadEvent.suspend();
        },

        getRange: function (from, to) {
            this.requestParams = {
                from: from,
                to: to
            };

            //WA.debug.State.set('clist.request', [from, to].join(', '));

            if (this.isActive() && WA.isNumber(from) && WA.isNumber(to)) {
                WA.ChangeLog.whenReady(this._onGetRange, this);
            }
        },

        _onGetRange: function () {
            WA.abstractError();
        },

        _sortContactList: function (data) {
            var len = data.length;
            var a = [], gr, n;
            for (var i = 0; i < len; i++) {
                if (WA.MainAgent.isChat(data[i][0])) {
                    gr = 4;
                } else if (data[i][2]) {
                    gr = 3;
                } else if (data[i][3] == 'offline' || data[i][3] == 'icq_offline') {
                    gr = 2;
                } else {
                    gr = 1;
                }
                //gr = data[i][2] && 3 || data[i][3].length == 7 && 2 || 1;
                n = data[i][1] || data[i][0];
                a[i] = gr + n.toLowerCase() + '_' + i;
            }
            a = a.sort();

            var ret = [];
            U.Array.each(a, function (entry) {
                var tmp = entry.split('_');
                var index = tmp[tmp.length - 1];
                ret.push(data[index]);
            });
            return ret;
        },

        _applyChangeLogTo: function (data, log) {
            var updated = false;

            if (log.length > 0) {
                U.Array.each(log, function (logItem) {
                    if (logItem.action === 'remove') {
                        var index = U.Array.indexOfBy(data, function (dataItem) {
                            return dataItem[0] === logItem.email;
                        });
                        if (index != -1) {
                            data.splice(index, 1);
                            updated = true;
                        }
                    }
                });

                U.Array.each(data, function (dataItem) {
                    var email = dataItem[0];

                    var index = U.Array.indexOfBy(log, function (logItem) {
                        var isFound = logItem.email === email;
                        if (isFound) {
                            if (logItem.action) {
                                if (logItem.action === 'rename') {
                                    dataItem[1] = logItem.nick || logItem.email;
                                    updated = true;
                                }
                                return false;
                            }
                        }
                        return isFound;
                    });

                    if (index != -1) {
                        var newStatus = log[index].status;
                        var t = newStatus + dataItem[3];
                        if(t.indexOf('offline') != -1 && newStatus != dataItem[3]) {
                            updated = true;
                        }

                        dataItem[1] = log[index].nick;
                        dataItem[2] = 0;
                        dataItem[3] = newStatus;
                        dataItem[4] = log[index].statusTitle;
                    }
                }, this);

                U.Array.each(log, function (logItem) {
                    if (logItem.action === 'add') {
                        data.push([
                            logItem.email,
                            logItem.nick || logItem.email,
                            1,
                            logItem.status || 'offline',
                            logItem.statusTitle
                        ]);
                        updated = true;
                    }
                });

            }

            return updated;
        },

        _fireLoadEvent: function (data, length) {
            return this.loadEvent.fire(this, data, length);
        },

        _onInvalidate: function () {
            this.data = null;

            /*var reqParams = this.requestParams;
             if (reqParams) {
             this.getRange(0, reqParams.to - reqParams.from);
             }*/
        },

        _onChangeLogUpdate: function (log) {
            if (this.data) {
                if (this._applyChangeLogTo(this.data, log)) {
                    this.data = this._sortContactList(this.data);
                }
                this._initVisibleData(this.data, this.data.length);
            }
        },

        _sliceListData: function (data, from, to) {
            to = Math.min(to, data.length);
            return data.slice(from, to);
        },

        _extractVisibleArea: function (data) {
            var reqParams = this.requestParams;
            if (reqParams) {
                return this._sliceListData(data, 0, reqParams.to - reqParams.from);
            } else {
                return [];
            }
        },

        _initVisibleData: function (data, length) {
            var reqParams = this.requestParams;
            if (reqParams) {
                var visibleData = this._sliceListData(data, reqParams.from, reqParams.to);
                this._fireLoadEvent(visibleData, length);
            }
        },

        destroy: function () {
            this.loadEvent.removeAll();
            this.loadEvent = null;

            WA.Invalidator.invalidateEvent.un(this._onInvalidate, this);
            WA.ChangeLog.updateEvent.un(this._onChangeLogUpdate, this);
        }

    });

    // ---------------------------------------------------------------------------------

    var RemoteData = WA.extend(AbstractData, {

        _prefix: 'clist_',

        constructor: function () {
            RemoteData.superclass.constructor.call(this);

            this._clistReceivedEvent = new U.Event();

            this.data = null;

            this.fullLength = null;

            this.storageListReader = new WA.util.DelayedTask();

            this.canShowOffline = true;

            WA.conn.Connection.triggerEvent.on(this._onConnectionTriggerEvent, this);
            WA.conn.Socket.successReconnectEvent.on(this._onSuccessReconnect, this);

            this.searchReadyEvent = new U.Event();

            this.sync = WA.Synchronizer.registerComponent('clist', ['varea_version']);
            this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);

            WA.ContactStates.updateEvent.on(this._onContactStatesUpdate, this);

        },

        _onActivate: function () {
            this.searchReadyEvent.resume();

            RemoteData.superclass._onActivate.call(this);
        },

        _onDeactivate: function () {
            this.fullLength = null;

            this.searchReadyEvent.suspend();

            RemoteData.superclass._onDeactivate.call(this);
        },

        _isReady: function () {
            return WA.isArray(this.data) && WA.isNumber(this.fullLength);
        },

        _isFullData: function () {
            return this._isReady() && this.data.length === this.fullLength;
        },

        _getListFromServer: function () {
            if (!this.__loadingFromServer && this.requestParams) {
                this.__loadingFromServer = true;
                WA.conn.Connection.getClist(null, function () {
                    this.__loadingFromServer = false;
                    this.__loadingFail = true;
                }, this);
            }
        },

        _onSuccessReconnect: function () {
             if(this.__loadingFail) {
                 this.__loadingFail = false;
                 this._getListFromServer();
             }
        },

        _onConnectionTriggerEvent: function (type, value) {
            if (type === 'contactList') {
                var data = this._sortContactList(value);
                this._saveListToStorage(
                        this._extractVisibleArea(data),
                        data.length,
                        function () {
                            this._saveLocalListData(data, data.length);

                            var query = this.__searchQuery;
                            if (query) {
                                //this.__searchQuery = null;
                                this.search(query);
                            }

//                            if(this.__getContact) {
//                                this.getContactsInfo.apply(this, this.__getContact);
//                            }
                            this._clistReceivedEvent.fire();

                            this.__loadingFromServer = false;

                            WA.debug.State.set('clist.status', 'loaded from <strong>server</strong>');
                        }
                        );
            }
        },

        _getListFromStorage: function (cb) {
            var keys = [
                this._prefix + 'list',
                this._prefix + 'list_length'
            ];
            WA.SessionStorage.load(keys, {
                success: function (data) {
                    var list = data[this._prefix + 'list'];
                    var length = data[this._prefix + 'list_length'];
                    cb.call(this, list ? this._parseListFromStorage(list) : null, parseInt(length));
                },
                failure: function (e) {
                    //debugger
                },
                scope: this
            });
        },

        _saveListToStorage: function (data, fullLength, cb) {
            /*
             var tmp = [];
             U.Array.each(data, function (contactData) {
             tmp.push([
             '[',
             U.Array.transform(contactData, function (el) {
             if (WA.isString(el)) {
             return U.String.quote(el);
             } else {
             return el;
             }
             }).join(','),
             ']'
             ].join(''));
             });
             var serialized = '[' + tmp.join(',') + ']';*/
            var serialized = JSON.stringify(data);

            var pfx = this._prefix;
            var params = {};
            params[pfx + 'list'] = serialized;
            params[pfx + 'list_length'] = fullLength;

            WA.SessionStorage.save(params, {
                success: function () {
                    this.sync.write('varea_version', new Date().getTime());
                    if (WA.isFunction(cb)) {
                        cb.call(this);
                    }
                },
                scope: this
            });
        },

        _initVisibleData: function (data, length) {
            if (this.canShowOffline) {
                RemoteData.superclass._initVisibleData.call(this, data, length);
            } else {
                var onlineContacts = [];

                U.Array.indexOfBy(data, function (contact) {
                    if (!contact[2] && contact[3] !== 'offline' && contact[3] !== 'icq_offline') {
                        onlineContacts.push(contact);
                    }
                });

                if (onlineContacts.length > 0) {
                    RemoteData.superclass._initVisibleData.call(this, onlineContacts, onlineContacts.length);
                } else {
                    RemoteData.superclass._initVisibleData.call(this, data, length);
                }
            }
        },

        _saveLocalListData: function (data, fullLength) {
            this.data = data;
            this.fullLength = fullLength;
            this._initVisibleData(this.data, this.fullLength);
        },

        _parseListFromStorage: function (storageData) {
            return eval(storageData);
        },

        _onGetRange: function () {
            this.storageListReader.stop();

            WA.debug.State.set('clist.status', 'loading...');

            if (!this._isReady()) {
                WA.debug.State.set('clist.status', 'not ready yet: loading...');
                WA.FocusManager.ifFocused(
                        function () {
                            WA.debug.State.set('clist.status', 'loading from storage...');
                            this._getListFromStorage(function (storageData, fullLength) {
                                if (!this._isReady()) {
                                    if (!storageData || storageData.length === 0
                                            || this.requestParams.from > 0
                                            || this.requestParams.to - this.requestParams.from > storageData.length) {
                                        WA.debug.State.set('clist.status', 'loading from server...');
                                        this._getListFromServer();
                                    } else {
                                        WA.debug.State.set('clist.status', 'loaded from <strong>storage</strong>');
                                        this._saveLocalListData(storageData, fullLength);
                                    }
                                }
                            });
                        },
                        function () {
                            WA.debug.State.set('clist.status', 'inactive window; loading...');
                            this.storageListReader.start(1000, function () {
                                this._getListFromStorage(function (storageData, fullLength) {
                                    if (!this.data) {
                                        if (storageData) {
                                            this._saveLocalListData(storageData, fullLength);
                                        } else {
                                            this.storageListReader.start();
                                        }
                                    }
                                });
                            }, this);
                        },
                        this
                        );
            } else {
                if (this._isFullData()) {
                    this._initVisibleData(this.data, this.data.length);
                } else {
                    this.data = null;
                    var reqParams = this.requestParams;
                    this.getRange(reqParams.from, reqParams.to);
                }
            }
        },

        _onInvalidate: function () {
            RemoteData.superclass._onInvalidate.call(this);
            this.fullLength = null;
            this._trySaveCurrentListToStorage();
        },

        _trySaveCurrentListToStorage: function () {
            WA.FocusManager.ifFocused(
                    function () {
                        this._saveListToStorage(
                                this._extractVisibleArea(this.data || []),
                                this.fullLength || 0
                                );
                    },
                    null,
                    this
                    );
        },

        _onChangeLogUpdate: function (log) {
            if (this._isReady()) {
                if (this._applyChangeLogTo(this.data, log)) {
                    this.data = this._sortContactList(this.data);
                }

                U.Array.each(log, function (logItem) {
                    if (logItem.action === 'remove') {
                        this.fullLength--;
                    } else if (logItem.action === 'add') {
                        this.fullLength++;
                    }
                }, this);

                this._initVisibleData(this.data, this.fullLength);
                this._trySaveCurrentListToStorage();
            }
        },

        _onContactStatesUpdate: function (contactStates) {
            if (this._isReady()) {
                var isAffected = false;

                var data = this._extractVisibleArea(this.data);
                U.Array.each(data, function (entry) {
                    if (WA.ContactStates.getStates(entry[0])) {
                        isAffected = true;
                        return false;
                    }
                }, this);

                if (isAffected) {
                    this._initVisibleData(this.data, this.fullLength);
                }
            }
        },

        _onSyncTriggerEvent: function (syncRecord) {
            if (syncRecord.isChanged('varea_version')) {
                var isStorageReaderStarted = this.storageListReader.isStarted();
                this.storageListReader.stop();
                this._getListFromStorage(function (storageData, fullLength) {
                    var reqParams = this.requestParams;
                    //                    if (storageData && reqParams && reqParams.from === 0
                    //                            && (this.data ? this.data.length < fullLength : true)) {
                    if (storageData && !this._isFullData() && reqParams && reqParams.from === 0) {
                        this._saveLocalListData(storageData, fullLength);
                    } else if (isStorageReaderStarted) {
                        this.storageListReader.start();
                    }
                });
            }
        },

        getContactsInfo: function (maillist, cb, scope) {
            if (this._isFullData()) {
//                delete this.__getContact;
                this._clistReceivedEvent.removeAll();
                var contacts = [];
                U.Array.each(this.data, function (entry) {
                    var index = U.Array.indexOf(maillist, entry[0]);
                    if(index != -1) {
                        contacts.push([
                            entry[0],
                            entry[1],
                            WA.MainAgent.isChat(entry[0]) && 'chat' || entry[2] && 'gray' || entry[3],
                            entry[4] || entry[3]
                        ]);
                    }
                });
                
                cb.call(scope || window, contacts);
            } else {
//                this.__getContact = [maillist, cb, scope];
                this._clistReceivedEvent.on(function () {
                    this.getContactsInfo(maillist, cb, scope);
                }, this, {single: true});
                if (!this.requestParams) {
                    this.getRange(0, 0);
                }
                this._getListFromServer();
            }
        },

        search: function (query) {
            if (this._isFullData()) {
                var MAX = 12;

                var filteredData = [];

                var q = (query || '').toLowerCase();
                var alterQuery = WA.util.KeyMapping.translate(q);

                U.Array.each(this.data, function (entry) {
                    if (MAX > 0 && filteredData.length === MAX) {
                        return false;
                    }
                    var mail = entry[0].toLowerCase();
                    var nick = (entry[1] || '').toLowerCase();

                    if (mail.indexOf(q) != -1 || nick.indexOf(q) != -1 || mail.indexOf(alterQuery) != -1 || nick.indexOf(alterQuery) != -1) {
                        filteredData.push(entry);
                    }
                });

                this.searchReadyEvent.fire(filteredData, query);
            } else {
                this.__searchQuery = query;
                this._getListFromServer();
            }
        },

        cancelSearch: function () {
            this.__searchQuery = null;
        },

        setOfflineContactsVisibility: function (canShow) {
            if (this.canShowOffline !== canShow) {
                this.canShowOffline = canShow;
                if (this._isReady()) {
                    this._initVisibleData(this.data, this.fullLength);
                }
            }
        },

        destroy: function () {
            WA.error('It is horrible!');
        }

    });

    // ---------------------------------------------------------------------------------

    var SearchData = WA.extend(AbstractData, {

        _onDeactivate: function () {
            SearchData.superclass._onDeactivate.call(this);

            this.cancelSearch();
        },

        setData: function (data, query) {
            this.data = data;
            this.__searchQuery = query;
            this.getRange(0, this.data.length);
        },

        _onGetRange: function () {
            this._initVisibleData(this.data, this.data.length);
        },

        _fireLoadEvent: function (data, length) {
            if (this.__searchQuery) {
                return SearchData.superclass._fireLoadEvent.call(this, {
                    query: this.__searchQuery,
                    data: data
                }, length);
            }
        },

        cancelSearch: function () {
            this.data = null;
            this.__searchQuery = null;
        }

    });

    // ---------------------------------------------------------------------------------

    WA.ContactListAbstraction = WA.extend(WA.Activatable, {

        constructor: function () {
            this.loadEvent = new U.Event();
            this.countEvent = new U.Event();
            this.microBlogChangeEvent = new U.Event();

            this.remoteData = new RemoteData();
            this.remoteData.loadEvent.on(this._onRemoteDataLoad, this);
            this.remoteData.searchReadyEvent.on(this._onSearchReady, this);

            this.searchData = new SearchData();
            this.searchReadyEvent = new U.Event().relay(this.searchData.loadEvent);

            this.sync = WA.Synchronizer.registerComponent('clist_abstr', ['micblog']);
            this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);

            WA.conn.Connection.triggerEvent.on(this._onConnectionTriggerEvent, this);
        },

        _onActivate: function () {
            this.loadEvent.resume();
            this.countEvent.resume();
            this.searchReadyEvent.resume();

            if (this.__onlineCount__) {
                this.countEvent.fire(this.__onlineCount__);
                delete this.__onlineCount__;
            }

            this.remoteData.activate();
            this.searchData.activate();
        },

        _onDeactivate: function () {
            this.loadEvent.suspend();
            this.countEvent.suspend();
            this.searchReadyEvent.suspend();

            this.remoteData.deactivate();
            this.searchData.deactivate();
        },

        _onSyncTriggerEvent: function (syncRecord) {
            if (syncRecord.isChanged('micblog')) {
                var msg = syncRecord.get('micblog') || '';
                this.microBlogChangeEvent.fire(msg);
            }
        },

        _onRemoteDataLoad: function (ignored, data, length) {
            this.loadEvent.fire(data, length);
        },

        _onConnectionTriggerEvent: function (type, value) {
            if (!this.isActive()) {

                // TODO: hack {{{
                if (type === 'contactListState') {
                    this.__onlineCount__ = parseInt(value.onlineCount);
                }
                // TODO: }}}

                return;
            }

            if (type === 'contactListState') {
                this.countEvent.fire(parseInt(value.onlineCount));
            }

            if (type === 'micblog::status') {
                this.sync.write('micblog', value.text);
                this.microBlogChangeEvent.fire(value.text);
            }
        },

        getList: function (from, to) {
            this.remoteData.getRange(from, to);
        },

        _onSearchReady: function (data, query) {
            this.searchData.setData(data, query);
        },

        search: function (query) {
            if (query) {
                this.remoteData.search(query);
            }
        },

        cancelSearch: function () {
            this.remoteData.cancelSearch();
            this.searchData.cancelSearch();
        },

        setOfflineContactsVisibility: function (canShow) {
            this.remoteData.setOfflineContactsVisibility(canShow);
        },

        removeContact: function (email, callback) {
            callback = callback || {};

            var cb = function (success, response) {
                if (success) {
                    WA.ChangeLog.logAction('remove', { email: email });
                    if (WA.isFunction(callback.success)) {
                        callback.success.call(callback.scope || this, email, response);
                    }
                } else {
                    if (WA.isFunction(callback.failure)) {
                        callback.failure.call(callback.scope || this, email, response);
                    }
                }
            };

            WA.conn.Socket.send('contact', {
                operation: 'remove',
                email: email
            }, cb, this);
        },

        renameContact: function (email, newNick, callback) {
            callback = callback || {};

            var cb = function (success, response) {
                if (success) {
                    WA.ChangeLog.logAction('rename', { email: email, nickname: newNick });
                    if (WA.isFunction(callback.success)) {
                        callback.success.call(callback.scope || this, email, response);
                    }
                } else {
                    if (WA.isFunction(callback.failure)) {
                        callback.failure.call(callback.scope || this, email, response);
                    }
                }
            };

            WA.conn.Socket.send('contact', {
                operation: 'modify',
                email: email,
                nickname: newNick
            }, cb, this);
        },

        addContact: function (email, nick) {
            WA.ChangeLog.logAction('add', { email: email, nickname: nick });
        },

        getContactsInfo: function (maillist, cb, scope) {
            this.remoteData.getContactsInfo(maillist, cb, scope);
        },

        postMicroBlog: function (msg) {
            var cb = function (success, response) {
                if (success) {
                    this.sync.write('micblog', msg);
                } else {
                    //??
                }
            };
            //send broadcast msg
            WA.conn.Socket.send('micblog/status', {
                text: msg
            }, cb, this);

        }

    });

})();
(function () {

    var WA = WebAgent;
    var U = WA.util;
    var DH = U.DomHelper;
    var SOCK = WA.conn.Socket;

    var LINE_HEIGHT = 25;

    var CONTACT_STATE_STATUS_MAP = {};
    CONTACT_STATE_STATUS_MAP[WA.ContactStates.TYPING_STATE] = 'typing';
    CONTACT_STATE_STATUS_MAP[WA.ContactStates.MESSAGE_STATE] = 'message';

    WA.ContactListAgent = WA.Activatable.extend(WA.ui.Component, {

        initComponent: function () {
            WA.ContactListAgent.superclass.initComponent.call(this);

            this.contactSelectEvent = new U.Event();
            this.playSoundEvent = new U.Event();
            this.micblogAlertsOptionEvent = new U.Event();
            this.popupChangeVisibleEvent = new U.Event();
            this.statusMenuToggleEvent = new U.Event();

            this.abstraction = new WA.ContactListAbstraction();
            this.abstraction.loadEvent.on(this._renderList, this);
            this.abstraction.microBlogChangeEvent.on(this._onMicroBlogChange, this);

            this.addContactDialog = new WA.AddSearchDialog();
            this.addContactDialog.addSuccessEvent.on(this.abstraction.addContact, this.abstraction);
            this.addContactDialog.openContactEvent.on(this._openContact, this);
            this.addContactDialog.contactInfoRequestEvent.on(this.abstraction.getContactsInfo, this.abstraction);
            this.addContactDialog.hideEvent.on(function(){this.btnAddContact.setState(false)}, this);
            this.addDialogOpenEvent = new U.Event().relay(this.addContactDialog.showEvent);

            this.visible = false;
            this.searchMode = false;
            this.soundOff = false;
            this.showOffline = false;
            this.micblogAlertsOff = false;
            this.sync = WA.Synchronizer.registerComponent('clist_agent', ['hide_offline', 'play_sound', 'microblog', 'micblog_alerts_off'], false);

//            this.sync.read(function (data) {
//                var status = data.get('microblog') || '';
//
//            }, this);

            this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);

            this.offsetChecker = new U.DelayedTask({
                interval: 50,
                fn: function () {
                    this._checkOffset();
                    this.offsetChecker.start();
                },
                scope: this
            });

            this.searchField = new WA.ContactListAgent.SearchField(this.abstraction, {
                cls: 'nwa-search__field',
                hintText: '����� ��������',
                hintClass: 'nwa_hint_msg'
            });

            this.searchField.selectEvent.on(this._openContact, this);
            this.searchField.searchStartEvent.on(this._onSearchStart, this);
            this.searchField.searchEndEvent.on(this._onSearchEnd, this);

            WA.Invalidator.invalidateEvent.on(this._onInvalidate, this);

            this.onlineConfirmSuccessEvent = new U.Event();
            this.onlineConfirmDeclineEvent = new U.Event();
        },

        _onSearchStart: function () {
            this.spacerEl.hide();
            this.clistEl.addClass('nwa-clist_search_mode');
            this.scrollBar.setSource(this.searchField.searchResults.getEl());
            this.searchMode = true;
        },

        _onSearchEnd: function () {
            this.clistEl.removeClass('nwa-clist_search_mode');
            this.spacerEl.show();
            this.scrollBar.setSource(this.clistEl);
            this.searchMode = false;
        },

        _onRender: function (ct) {
            var mail = WA.ACTIVE_MAIL;
            var iconUrl = WA.makeAvatar(mail, '/_avatar32');

            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-agent',
                children: [
                    {
                        tag: 'div',
                        cls: 'nwa-agent__shadow',
                        children: [
                            {
                                tag: 'div',
                                id: 'nwaHeader',
                                cls: 'nwa-header',
                                children: [
                                    {
                                        tag: 'div',
                                        cls: 'nwa-header__avatar',
                                        style: 'background-image: url(' + iconUrl + ')'
                                    },
                                    {
                                        tag: 'div',
                                        id: 'nwaNick',
                                        cls: 'nwa-header__nick',
                                        children: [
                                            {
                                                tag: 'span'
                                            },
                                            {
                                                tag: 'div',
                                                cls: 'nwa-header__nick-fadeout'
                                            }
                                        ]
                                    },
                                    {
                                        tag: 'div',
                                        cls: 'nwa-header__mini',
                                        title: '��������'
                                    },
                                    {
                                        tag: 'div',
                                        cls: 'nwa-header__popup',
                                        title: WA.isPopup ? '������� ����' : '������� � ��������� ����'
                                    }
                                ]
                            },
                            {
                                tag: 'div',
                                cls: 'nwa-search',
                                children: [
                                    {
                                        tag: 'div',
                                        id: 'nwaSearch',
                                        cls: 'nwa-search__wrap'
                                    }
                                ]
                            },
                            {
                                tag: 'div',
                                id: 'nwaClist',
                                cls: 'nwa-clist',
                                children: [
                                    {
                                        tag: 'div',
                                        id: 'nwaClistWrap',
                                        cls: 'nwa-clist__wrap'
                                    }
                                ]
                            },
                            {
                                tag: 'div',
                                id: 'nwaToolbar',
                                cls: 'nwa-toolbar'
                            }
                        ]
                    }
                ]
            });

            var buttonsCt = WA.get('nwaToolbar');
            var headerCont = WA.get('nwaHeader');

            this.broadcastMsg = new BroadcastMsg();
            this.broadcastMsg.render(headerCont);
            this.broadcastMsg.editOk.on(function(msg){
                this.abstraction.postMicroBlog(msg);
            }, this);

            // add contact
            this.btnAddContact = new WA.ui.ToggleButton({
                cls: 'nwa-toolbar__add-contact',
                handler: this._onAddContactClick,
                stateHandler: function(){return false},
                scope: this,
                text: '<span class="nwa-toolbar__add-contact-label">�������� �������</span>'
            });

            this.btnAddContact.render(buttonsCt);

            this.spacerEl = WA.get('nwaClistWrap');
            this.clistEl = WA.get('nwaClist');

            this.scrollBar = new WA.ui.ScrollBar({source: this.clistEl, watchResize: WA.isPopup});
            this.scrollBar.render(this.el.first());

            this.spacerEl.on('click', this._onContactClick, this);

            // search
            this.searchField.render(WA.get('nwaSearch'));
            this.searchField.resultTo(this.clistEl);

            var optionsMenu = this.optionsMenu = new WA.ui.menu.Menu({
                cls: 'nwa-toolbar__options-menu',
                fadeIn: true,
                items: [
                    {
                        text: '������ ������ ��������',
                        id: 'optOffline',
                        iconCls: 'nwa-toolbar__options-menu__offline',
                        handler: this._onToggleOfflineClick,
                        scope: this
                    },
                    {
                        text: '�� ��������� � �����������',
                        id: 'optMicblogAlerts',
                        iconCls: 'nwa-toolbar__options-menu__micblog wa-micblog-alerts-off',
                        handler: this._onToggleMicblogAlertsClick,
                        scope: this
                    },
                    {
                        id: 'optSound',
                        text: '��������� �����',
                        iconCls: 'nwa-toolbar__options-menu__sound',
                        handler: this._onToggleSoundClick,
                        scope: this
                    },
                    {
                        id: 'optSettings',
                        text: '���������',
                        iconCls: 'nwa-toolbar__options-menu__settings',
                        handler: function () {
                            window.open('//e.mail.ru/cgi-bin/editprofile');
                        },
                        scope: this
                    }
                ]
            });

            optionsMenu.showEvent.on(function(){
                WA.get('optMicblogAlerts').last().update(this.micblogAlertsOff ? '��������� � �����������' : '�� ��������� � �����������');
                WA.get('optMicblogAlerts').toggleClass('wa-micblog-alerts-off', !this.micblogAlertsOff);
                WA.get('optOffline').last().update(this.showOffline ? '������ ������ ��������' : '��� ��������');
                WA.get('optOffline').toggleClass('wa-show-offline-contacts', this.showOffline);
                WA.get('optSound').last().update(this.soundOff ? '��������� �����' : '�������� �����');
                WA.get('optSound').toggleClass('wa-no-play-sound', this.soundOff);
                this.openOptionsBtn.setState(true);
            }, this);

            optionsMenu.hideEvent.on(function(){
                this.openOptionsBtn.setState(false);
            }, this);

            // options
            this.openOptionsBtn = new WA.ui.ToggleButton({
                cls: 'nwa-toolbar__options',
                menu: optionsMenu,
                tooltip: '���������',
                text: '<span>&nbsp;</span>'
            });

            this.openOptionsBtn.render(buttonsCt);

            // display nickname
            // TODO: set real name
            this._nickBtn = WA.get('nwaNick').first();
            this._nickBtn.update(WA.ACTIVE_MAIL);

            // status
            this.statusToggleBtn = new WA.ui.Button({
                cls: 'nwa-toolbar__status',
                scope: this,
                text: '<span class="nwa-button__l"></span><span class="nwa-button__r"></span><span class="nwa-button__fadeout"></span>',
                handler: function(){
                    this.statusMenuToggleEvent.fire();
                    U.Mnt.count('settingsButton');
                }
            });

            this.statusToggleBtn.render(buttonsCt);

            headerCont.on('click', this._onHeaderClick, this);
            this.el.on('DOMMouseScroll', function(e){e.stopEvent();});

        },

        setSelfNick: function (nick) {
            this._nickBtn.update(nick);
        },

        setStatus: function (status, statusTitle) {
            this.statusToggleBtn.el.first().dom.className = WA.buildIconClass(status);
            this.statusToggleBtn.el.first().next().update(U.String.htmlEntity(statusTitle));
        },

        _onAfterRender: function () {
            this._onlineConfirmDialog = new WA.DialogsAgent.RemoveContactDialog({
                title: '������������?',
                okButton: '��',
                autoRender: this.el
            });
            var wasOk;
            this._onlineConfirmDialog.showEvent.on(function () {
                wasOk = false;
            }, this);
            this._onlineConfirmDialog.hideEvent.on(function () {
                this.deactivate();
                if(!wasOk) {
                    this.onlineConfirmDeclineEvent.fire();
                }
            }, this);
            this._onlineConfirmDialog.okEvent.on(function () {
                wasOk = true;
                this.onlineConfirmSuccessEvent.fire();
            }, this);

            this.sync.whenReady(function () {
                this.sync.read(this._onSyncTriggerEvent, this);
            }, this);
        },

        _onHeaderClick: function (e) {
            if (e.getTarget(true).hasClass('nwa-header__popup')) {
                this.popupChangeVisibleEvent.fire();
                U.Mnt.count('popupButton');
                return;
            }

            if (e.getTarget(true).up('nwa-header__msg')) {
                return;
            }
            this.setVisible(WA.isPopup);
        },

        _onContactClick: function (e) {
            var el = e.getTarget(true);
            var mail = el.getAttribute('mail');
            if(!mail) {
                el = el.parent();
                mail = el.getAttribute('mail');
            }
            if (mail) {
                var status = el.getAttribute('status');
                var statusTitle = el.getAttribute('title');
                var nick = el.getAttribute('nick');
                this._openContact(mail, nick, status, statusTitle);
            }
        },

        _openContact: function (mail, nick, status, statusTitle) {
            this.contactSelectEvent.fire(mail, nick || mail, status, statusTitle);
        },

        _onToggleMicblogAlertsClick: function (button) {
            var val = !this.micblogAlertsOff;
            this.sync.write('micblog_alerts_off', val ? '1' : '');
            this._setMicblogAlertsVisibility(val);
        },

        _setMicblogAlertsVisibility: function (val) {
            this.micblogAlertsOptionEvent.fire(val);
            this.micblogAlertsOff = val;
        },

        _onToggleOfflineClick: function (button) {
            var canShow = !this.showOffline;
            this.sync.write('hide_offline', canShow ? '' : '1');
            this._setOfflineContactsVisibility(canShow);
        },

        _setOfflineContactsVisibility: function (canShow) {
            this.abstraction.setOfflineContactsVisibility(canShow);
            this.showOffline = canShow;
        },

        _onToggleSoundClick: function (button) {
            var canPlay = !this.soundOff;
            this.sync.write('play_sound', canPlay ? '' : '1');
            this._setSoundPlayToggle(canPlay);
        },

        _setSoundPlayToggle: function (canPlay) {
            this.playSoundEvent.fire(canPlay);
            this.soundOff = canPlay;
        },

        _onSyncTriggerEvent: function (syncRecord) {
            var val;

            if (syncRecord.isChanged('micblog_alerts_off')) {
                val = syncRecord.get('micblog_alerts_off') === '1';
                this._setMicblogAlertsVisibility(val);
            }

            if (syncRecord.isChanged('hide_offline')) {
                var hideOffline = syncRecord.get('hide_offline') === '1';
                this._setOfflineContactsVisibility(!hideOffline);
            }

            if (syncRecord.isChanged('play_sound')) {
                var playSound = syncRecord.get('play_sound') === '1';
                this._setSoundPlayToggle(!playSound);
            }

            if (syncRecord.isChanged('microblog')) {
                var msg = syncRecord.get('microblog') || '';
                this.broadcastMsg.setValue(msg);
            }
        },

        _checkOffset: function () {
            var top = this.clistEl.dom.scrollTop;
            var height = this.clistEl.dom.parentNode.scrollHeight;

            if (height != this._height || top != this._top) {
                var changed = false;
                if (WA.resizeableLayout && height != this._height) {
                    this._height = height;

                    this._blockSize = Math.ceil(height / LINE_HEIGHT);
                    this._blockHeight = this._blockSize * LINE_HEIGHT;
                    changed = true;
                }

                if (top !== this._top) {
                    this._top = top;
                }

                var bottom = top + height;
                var from = Math.ceil(bottom / this._blockHeight) - 1;
                var to = from + 2;
                if (from != 0)
                    from--;

                if (this._from != from || this._to != to) {
                    this._from = from;
                    this._to = to;

                    changed = true;
                }

                if (changed) {
                    this._contactsOffset = from * this._blockHeight;
                    this.abstraction.getList(from * this._blockSize, to * this._blockSize);
                }
            }
        },

        __getContactStatus: function (contact) {
            var email = contact[0];

            var states = WA.ContactStates.getStates(email);
            if (states && states.length > 0) {
                var lastState = states[states.length - 1];
                var ret = CONTACT_STATE_STATUS_MAP[lastState];
                if (ret) {
                    return ret;
                }
            }

            if (WA.MainAgent.isChat(email)) {
                return 'chat';
            }

            return contact[2] ? 'gray' : contact[3];
        },

        __getContactUnread: function (contact) {
            var unread = WA.ContactStates.getUnread(contact[0]);
            return unread || 0;
        },

        _renderList: function (visibleArea, listLength) {
            if (!WA.isNumber(this._clistLength) && (listLength > 10 || WA.isDebug)) { // first rendering
                this.el.removeClass('wa-mdf-shortclist'); //vd ????
            }

            if (this._clistLength !== listLength) {
                this._clistLength = listLength;
                this.spacerEl.dom.style.height = LINE_HEIGHT * this._clistLength + 'px';
            }

            var format = [
                '<div',
                ' style="top: {0}px"',
                ' class="nwa-cl-contact {9}"',
                ' status="{1}"',
                ' mail="{2}"',
                ' id="{4}"',
                ' nick="{3}"',
                ' title="{5}"'
                ,'>',
                '<span class="nwa-cl-contact-icon {7}" title="{5}"></span>',
                '<span class="nwa-cl-contact-nick" title="{6}">{3}</span><div class="nwa-header__nick-fadeout"></div>',
                '<span class="nwa-msg-counter">{8}</span>',
                '</div>'
            ].join('');

            var html = [];
            U.Array.each(visibleArea, function (contact, i) {
                var email = contact[0];
                var icon = this.__getContactStatus(contact);
                var status = WA.MainAgent.isChat(contact[0]) && 'chat' || contact[2] && 'gray' || contact[3] ;
                var statusTitle = contact[4] && U.String.htmlEntity(contact[4]) || status.replace(/^icq_/, '');
                statusTitle = U.String.htmlEntity(WA.MainAgent.statuses[statusTitle] || statusTitle);
                var emailTitle = status == 'chat' ? statusTitle : email.replace("@uin.icq", '');
                var unread = this.__getContactUnread(contact);

                html[html.length] = U.String.format(
                        format,
                        this._contactsOffset + i * LINE_HEIGHT,
                        status,
                        email,
                        DH.htmlEntities(contact[1] || email),
                        this._generateItemId(email),
                        //*WA.XStatuses.getAll()[status]*/U.String.htmlEntity(contact[4] || '')
                        statusTitle,
                        emailTitle,
                        WA.buildIconClass(icon, true),
                        unread,
                        (unread > 0 ? 'nwa-cl-contact_unread' : '')
                    );
            }, this);
            html = html.join('');

            if (this.spacerEl.dom.innerHTML != html) {
                this.spacerEl.update(html);
                this.scrollBar.sync();
            }
        },

        _onMicroBlogChange: function (msg) {
            this.broadcastMsg.setValue(msg);
        },

        _onBeforeShow: function () {
            return this.isActive();
        },

        _onShow: function () {
            this.visible = true;
            this._checkOffset();
            this.offsetChecker.start();
            this.scrollBar.sync();
        },

        _onHide: function () {
            this.visible = false;
            this.offsetChecker.stop();
            this._height = -1; this._top = -1; // artamonova fix
            this.searchField.stop();
            this.addContactDialog.hide();
        },

        _resetScroll: function () {
            this.clistEl.dom.scrollTop = 0;
        },

        _onInvalidate: function () {
            this._resetScroll();
            delete this._height; // forces offset check
            if (this.visible) {
                this._checkOffset();
            }
        },

        _onAddContactClick: function () {
            var dlg = this.addContactDialog;
            if (!dlg.rendered) {
                dlg.render(this.el);
            }
            this.btnAddContact.setState(!dlg.isVisible());
            dlg.setVisible(!dlg.isVisible());
        },

        searchContact: function (mail, openProfile) {
            this._onAddContactClick();
            this.addContactDialog.show();
            this.addContactDialog.searchMail(mail, openProfile);
            this.show();
        },

        _generateItemId: function (email) {
            return 'wa-clist-spacer-item-' + email.replace(/@|\./g, '_');
        },

        activate: function (cb, scope) {
            return WA.ContactListAgent.superclass.activate.call(this, {
                cb: cb,
                scope: scope
            });
        },

        _onActivate: function (params) {
            this.abstraction.activate();

            if (WA.isFunction(params.cb)) {
                params.cb.call(params.scope || window);
            }
        },

        _onDeactivate: function () {
            this.abstraction.deactivate();
            this.searchField.abort();
            this.hide();
            this._resetScroll();
            delete this._height; // forces offset check
        },

        actualize: function (cb, scope) {
            cb && cb.call(scope || window);
            // TODO
        },

        enableUserEvents: function () {
            // TODO
        },

        disableUserEvents: function () {
            // TODO
        },

        onlineConfirm: function (title) {
            this._renderList([], 0);
            this.activate();
            this.show();
            this._onlineConfirmDialog.show(title);
        }

    });

    var BroadcastMsg = WA.extend(WA.ui.Component, {
        constructor: function (config) {
            BroadcastMsg.superclass.constructor.call(this, config);
            this._editMode = false;
            this._value = '';
            this._hintText = '������� ���� �������';
            this.editOk = new U.Event();
        },

        setValue: function (val) {
            val = val || '';
            this._value = val;
            this.editField.val(val);
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-header__msg',
                id: 'nwaBroadcast',
                children: {
                    tag: 'div',
                    cls: 'nwa-header__msg-wrap'
                }
            });

            this.editField = new WA.ui.TextField({
                multiline: true,
                cls: 'nwa-header__msg-field',
                hintText: this._hintText,
                hintClass: 'nwa_hint_msg'
            });

            this.editField.render(this.el.first());
        },

        _onAfterRender: function () {
            BroadcastMsg.superclass._onAfterRender.call(this);

            this.editField.getEl().on('mousedown', function (e) {
                    if (this._editMode) return;
                    this._editOn();
                    e.stopEvent();
                }, this
            );
            this.editField.blurEvent.on(this._editOff, this);
            this.editField.getEl().on('keydown', function (e) {
                if (!e.shiftKey && e.keyCode == 13) {
                    var newValue = U.String.trim(this.editField.val());
                    if (this._value !== newValue) {
                        this._value = newValue;
                        this.editOk.fire(newValue);
                    }
                    this._editOff();
                    e.stopEvent();
                } else if (e.keyCode == 27) {
                    this.editField.val(this._value);
                    this._editOff();
                }
            }, this);
        },

        _editOn: function () {
            if (this._editMode) return;
            this._editMode = true;
            this.el.addClass('nwa-header__msg_expanded');
            this.el.removeClass('nwa-header__msg_nohint');
            this.editField.focus();
            this.editField.select();
        },

        _editOff: function () {
            if (!this._editMode) return;
            this._editMode = false;
            this.editField.blur();
            this.el.removeClass('nwa-header__msg_expanded');
            this.editField.getEl().dom.scrollTop = 0;
            if(this.editField.val() != '') {
                this.el.addClass('nwa-header__msg_nohint');
            }
        }

    });

})();
(function () {

    var WA = WebAgent;
    var U = WA.util;

    // ---------------------------------------------------------------------------------

    var SELECTED_ITEM_CLS = 'nwa-search-results__item_selected';

    var SearchResults = WA.extend(WA.ui.Component, {

        autoEl: {
            tag: 'div',
            style: 'display: none'
        },

        initComponent: function () {
            SearchResults.superclass.initComponent.call(this);

            this.__cursor = 0;

            this.selectEvent = new U.Event();
        },

        setData: function (data) {
            if (!this.isVisible()) {
                this.show();
            }

            if (data.data.length > 0) {
                var html = U.Array.transform(data.data, function (contact, index) {
                    return this.__renderSearchResult(contact, index, data.query);
                }, this).join('');

                this.el.update(html);
            } else {
                this.el.update('&nbsp;���������� �����������');
            }
        },

        _onAfterRender: function () {
            SearchResults.superclass._onAfterRender.call(this);

            this.el.on('mouseover', this._onMouseOver, this);
            this.el.on('click', this._onClick, this);
        },

        __renderSearchResult: function (contact, index, query) {
            var mail = contact[0];
            var nick = U.DomHelper.htmlEntities(contact[1] || mail);

            var hlMail = mail.replace('@uin.icq', '');
            var inMail = hlMail.indexOf(query);
            if (inMail != -1) {
                hlMail =  hlMail.substr(0, inMail) + '<strong>' + hlMail.substr(inMail, query.length) + '</strong>' + hlMail.substr(inMail + query.length);
            }

            var hlNick = contact[1] ? nick : hlMail;
            var inNick = nick.indexOf(query);
            if (contact[1] && inNick != -1) {
                hlNick =  nick.substr(0, inNick) + '<strong>' + nick.substr(inNick, query.length) + '</strong>' + nick.substr(inNick + query.length);
            }

            var status = WA.MainAgent.isChat(mail) && 'chat' || contact[2] && 'gray' || contact[3]

            return U.String.format(
                    '<div id="{4}" class="nwa-search-results__item {3}" nick="{0}" mail="{1}" status="{2}" idx="{5}">'
                        + '<span class="nwa-cl-contact-icon {8}"></span><span>{6} ({7})</span></div>',
                    nick,
                    mail,
                    status,
                    index === this.__cursor ? SELECTED_ITEM_CLS : '',
                    this.__generateItemId(index),
                    index,
                    hlNick,
                    hlMail,
                    WA.buildIconClass(status, true)
                    );
        },

        __generateItemId: function (index) {
            return this.getId() + '-item-' + index;
        },

        __getItemAt: function (index) {
            return WA.get(this.__generateItemId(index));
        },

        __getCurrentEl: function () {
            return this.__getItemAt(this.__cursor);
        },

        __setCursor: function (newEl) {
            var index = parseInt(newEl.getAttribute('idx'));
            if (index >= 0 && this.__cursor !== index) {
                var oldEl = this.__getCurrentEl();
                if (oldEl) {
                    oldEl.removeClass(SELECTED_ITEM_CLS);
                    newEl.addClass(SELECTED_ITEM_CLS);
                    this.__cursor = index;
                }
            }
        },

        moveCursor: function (direction) {
            var oldEl = this.__getCurrentEl();
            if (oldEl) {
                var newEl = oldEl[direction]();
                if (newEl) {
                    this.__setCursor(newEl);
                }
            }
        },

        _onMouseOver: function (e) {
            var el = e.getTarget(true);
            if (el) {
                this.__setCursor(el);
            }
        },

        _onHide: function () {
            this.el.update('');
            this.__cursor = 0;
        },

        __selectEl: function (el) {
            this.selectEvent.fire(
                    el.getAttribute('mail'),
                    el.getAttribute('nick'),
                    el.getAttribute('status')
                    );
        },

        _onClick: function (e) {
            if (!this.isVisible() || e.button !== 0) {
                return;
            }

            var target = e.getTarget(true);

            if (target && this.el.contains(target)) {
                e.stopPropagation();
                var c = target;
                while (c && !c.equals(this.el)) {
                    var mail = c.getAttribute('mail');
                    if (mail) {
                        this.__selectEl(c);
                        break;
                    } else {
                        c = c.parent();
                    }
                }
            }
        },

        selectCurrentItem: function () {
            var el = this.__getCurrentEl();
            if (el) {
                this.__selectEl(el);
            }
        }

    });

    // ---------------------------------------------------------------------------------

    var SearchField = WA.ContactListAgent.SearchField = WA.extend(WA.ui.TextField, {

        constructor: function (abstraction, config) {
            SearchField.superclass.constructor.call(this, config);

            this.abstraction = abstraction;
            this.abstraction.searchReadyEvent.on(this._onSearchReady, this);

            this.searchStartEvent = new U.Event();
            this.searchEndEvent = new U.Event();

            this.searchDT = new U.DelayedTask();
            this.pasteDT = new U.DelayedTask({
                fn: function () {
                    this._doSearch();
                    this.pasteDT.start();
                },
                scope: this,
                interval: 200
            });

            if (config.closeOnBlur) {
                WA.FocusManager.blurEvent.on(function () {
                    this._cancelSearch();
                }, this);
            }

            this.searchResults = new SearchResults({
                cls: 'nwa-search-results'
            });

            this.selectEvent = new U.Event();
            this.searchResults.selectEvent.on(this._onSearchResultSelect, this);
/*
            this.searchResults.hideEvent.on(function () {
                this._cancelSearch();
            }, this);
*/
        },

        resultTo: function (container) {
            this.resultContainer = container;
        },

        abort: function () {
            this._cancelSearch();
        },

        _onSearchResultSelect: function (mail, nick, status) {
            WA.setTimeout(this._cancelSearch, 400, this);
            this.selectEvent.fire(mail, nick, status);
        },

        _onRender: function (ct) {
            SearchField.superclass._onRender.call(this, ct);

            this.clearBtn = new WA.ui.Button({
                cls: 'nwa-search__clear',
                handler: function () {
                    this._cancelSearch();
                },
                scope: this
            });
            this.clearBtn.render(ct);
        },

        _onAfterRender: function () {
            SearchField.superclass._onAfterRender.call(this);

            this.el.on('keyup', this._onSearchFieldKeyUp, this);
            this.el.on('paste', function(e) {
                WA.setTimeout(this._doSearch, 200, this);
            }, this);
            if (WA.isOpera) {
                this.el.on('mouseup', function (e){
                    if (e.button == 2) {
                        this.pasteDT.start();
                    }
                }, this);
            }

        },

        _cancelSearch: function (stayFocused) {
            this.clearBtn.hide();
            if (stayFocused !== true) {
                this.val('');
                try {
                    this.el.dom.blur();
                } catch(e) {}
                this._onBlur(); // forcing hint message to appear when not focused
                this.stop();
            }
            this.abstraction.cancelSearch();
            if (this.searchResults.rendered) {
                this.searchResults.hide();
            }
            this.searchEndEvent.fire();
        },

        _onSearchReady: function (ignored, data) {
            var sr = this.searchResults;
            if (!sr.rendered) {
                sr.renderEvent.on(
                        WA.createDelegate(this._onSearchReady, this, [ignored, data], 0)
                        );
                sr.render(this.resultContainer || this.container);
            } else {
                sr.setData(data);
                this.searchStartEvent.fire();
            }
        },

        _onSearchFieldKeyUp: function (e) {
            var keyCode = e.getKeyCode();

            if (keyCode === 38) {
                this.searchResults.moveCursor('prev');
            } else if (keyCode === 40) {
                this.searchResults.moveCursor('next');
            } else if (keyCode === 13) {
                var selectedItem = this.searchResults.selectCurrentItem();
                if (selectedItem) {
                    this._onSearchResultSelect(selectedItem.mail, selectedItem.nick, selectedItem.status);
                }
            } else if (keyCode === 27) {
                e.preventDefault();
                this._cancelSearch();
            } else {
                this.searchDT.start(150, this._doSearch, this);
            }
        },

        _doSearch: function () {
            var query = this.val();
            if (query) {
                this.clearBtn.show();
                this.abstraction.search(query);
            } else {
                this._cancelSearch(true);
            }
        },

        stop: function () {
            this.pasteDT.stop();
            this.searchDT.stop();
        },

        destroy: function () {
            this.searchResults.destroy();
            if (this.clearBtn) {
                this.clearBtn.destroy();
            }
            this.pasteDT.stop();
            this.searchDT.stop();

            this.selectEvent.removeAll();
            this.selectEvent = null;

            SearchField.superclass.destroy.call(this);
        }

    });

})();
WebAgent.UserAttend = (function () {
    var WA = WebAgent;
    var U = WA.util;

    var UserAttend = WA.extend(Object, {

        constructor: function () {

            this.attendEvent = new U.Event();
            /*
            window.addEventListener('focus', WA.createDelegate(this._onIntermediateBlur, this), true)
            window.addEventListener('blur', WA.createDelegate(this._onIntermediateBlur, this), true)
            */
            WA.fly(window).on('blur', this._onIntermediateBlur, this);
            WA.fly(window).on('focus', this._onIntermediateBlur, this);
            WA.getBody().on('mousemove', this._onMouseMove, this);
            this._attendChecker = new U.DelayedTask({
                interval: 1000,
                fn: this._checkAttend,
                scope: this
            });

        },

        isAttend: false,
        
        _userActivityDate: +new Date(),

        _onMouseMove: function () {
            this._userActivityDate = +new Date();

            if(this.__focusTmr) {
                clearTimeout(this.__focusTmr);
                this.__focusTmr = false;
            }
        },

        _checkAttend: function () {
            var now = +new Date();
            if(now - this._userActivityDate > 1000*60) {
                this._onBlur();
            }
        },

        __focusTmr: false,

        _onIntermediateBlur: function () {
            clearTimeout(this.__focusTmr);
            this.__focusTmr = WA.setTimeout(WA.createDelegate(this._onBlur, this), 10);
        },

        _onBlur: function () {
            if(this.isAttend) {
                this.__focusTmr = false;
                this.isAttend = false;
                //WA.debug.Console.log('attend ', this.isAttend);
            }
        },

        _onActivity: function () {
            if(!this.isAttend) {
                this.isAttend = true;
                this.attendEvent.fire();
                //WA.debug.Console.log('attend ', this.isAttend);
            }
        },

        activate: function () {
            WA.debug.Console.log('UserAttend.activate')
            WA.get('wa-root').on('click', this._onActivity, this).on('keydown', this._onActivity, this);
            WA.FocusManager.ifFocused(this._onActivity, null, this);
        },

        deactivate: function () {
            WA.debug.Console.log('UserAttend.deactivate')
            WA.get('wa-root').un('click', this._onActivity, this).un('keydown', this._onActivity, this);
            this.isAttend = false;
        }

    });
    
    return new UserAttend();
})();
(function () {

    var WA = WebAgent;
    var U = WA.util;
    var SOCK = WA.conn.Socket;
    var US = WebAgent.conn.UserState;
    var FM = WA.FocusManager;
    var ConnectionRoster = WA.conn.ConnectionRoster;
    var JSON = WA.getJSON();
    var S = WA.Storage;
//    var S = WA.HugeStorage;
    var UNDELIVERED_TIMEOUT = 10; //in seconds

    var StatusAccumulator = WA.extend(ConnectionRoster, {

        constructor: function () {
            StatusAccumulator.superclass.constructor.apply(this, arguments);

            this._log = {};
        },

        _onConnectionContactStatus: function (value) {
            if (!this._log[value.email]) {
                this._log[value.email] = {};
            }

            if (value.status) {
                this._log[value.email].status = value.status.type;
                this._log[value.email].statusTitle = value.status.title;
            }

            this._log[value.email].nick = value.nickname || value.email;
        },

        _onConnectionContactMicblogStatus: function (value) {
            this._onConnectionContactStatus(value);
        },

        get: function (mail) {
            if (this._log[mail]) {
                return {
                    nick: this._log[mail].nick,
                    status: this._log[mail].status,
                    statusTitle: this._log[mail].statusTitle
                };
            } else {
                return {};
            }
        },

        destroy: function () {
            delete this._log;
            StatusAccumulator.superclass.destroy.apply(this, arguments);
        }

    });
    var SA = WA.SA = new StatusAccumulator();

    var IncomingMessage = WA.extend(Object, {

        constructor: function (value) {
            var mail = value.from;
            var message = SA.get(mail) || {};

            WA.applyIf(message, {
                nick: value.nickname
            });

            message.from = mail;
            message.date = value.timestamp;
            message.text = value.text;
            message.unreaded = 1;

            if (value.request && value.request.delivery) {
                if (value.offline) {
                    WA.conn.Connection.confirmOfflineDelivery(value.uidl);
                } else if (value.message_id){
                    SOCK.send('message', {
                        to: mail,
                        message_id: value.message_id,
                        delivered: 1
                    });
                }
            }

            this.data = message;
        }

    });

    var AuthMessage = WA.extend(IncomingMessage, {

        constructor: function (value) {
            AuthMessage.superclass.constructor.call(this, value);

            WA.applyIf(this.data, {
                nick: this.data.from,
                status: 'gray'
            });

            this.data.message_id = value.uidl || value.message_id;
            this.data.auth = 1;
        }

    });

    var MicblogMessage = WA.extend(IncomingMessage, {
        constructor: function (value) {
            MicblogMessage.superclass.constructor.call(this, value);

            this.data.micblog = 1;
            this.data.message_id = value.id;
        }
    });

    var OutgoingMessage = WA.extend(ConnectionRoster, {

        constructor: function (msg) {
            OutgoingMessage.superclass.constructor.apply(this, arguments);

            var now = WA.now();
            this.mail = msg.mail;
            this.text = msg.text;
            this.resend = msg.resend;
            this.data = {
                text: this.resend ? msg.text : WA.DialogsAgent.Smiles.processText(msg.text),
                date: msg.date || now,
                from: WA.ACTIVE_MAIL,
                message_id: (Math.random() * 10000000).toFixed(0),
                undelivered: 1,
                unreaded: 1,
                deliverExpired: 0
            };

            var timeout = msg.date ? msg.date + UNDELIVERED_TIMEOUT - now  : UNDELIVERED_TIMEOUT;
            this._timer = WA.setTimeout(WA.createDelegate(function() {
                this.data.deliverExpired = 1;
                this.updateEvent.fire(this.data.message_id);
            },this), timeout > 0 ? timeout * 1000 : 0);

            this.updateEvent = new U.Event();
        },

        send: function () {
            var text = this.data.text;
            if(this.mail.indexOf('@uin.icq') != -1) {
                text = WA.DialogsAgent.Smiles.processTextIcq(this.text);
            }
            SOCK.send('message', {
                to: this.mail,
                message_id: this.data.message_id,
                text: text
            });
        },

        _onConnectionMessageDelivered: function (value) {
            if (value.message_id == this.data.message_id) {
                clearTimeout(this._timer);
                this.data.date = value.timestamp;
                this.updateEvent.fire(this.data.message_id);
                delete this.data.undelivered;
                delete this.data.message_id;
                delete this.data.deliverExpired;
            }
        },

        destroy: function () {
            OutgoingMessage.superclass.destroy.apply(this, arguments);
            this.updateEvent.removeAll();
        }
    });

    var History = WA.extend(ConnectionRoster, {

        constructor: function (dialog) {
            History.superclass.constructor.apply(this, arguments);

            this.dialog = dialog;
            this.updateEvent = new U.Event();
            this._instances = {};

            this._fetchHistory();
        },

        _fetchHistory: function () {
            var now = WA.now();
            if (this.dialog.history) {
                U.Array.each(this.dialog.history, function (data) {

                    if (data.undelivered) {
//                        if (now - data.date > UNDELIVERED_TIMEOUT) {
//                            data.deliverExpired = 1;
////                            this.updateEvent.fire(true);
//                            return;
//                        }

                        var message = new OutgoingMessage(data);
                        message.data = data;
                        this._instances[message.message_id] = message;
                        message.updateEvent.on(this._onMessageUpdate, this);
                    }
                }, this);
            }
        },

        append: function (message) {
            this._intersect([message.data]);

            if (message instanceof OutgoingMessage) {
                this._instances[message.data.message_id] = message;
                message.updateEvent.on(this._onMessageUpdate, this);
            }

            if (message instanceof IncomingMessage && message.data.unreaded && !(message instanceof MicblogMessage)) {
                this.dialog.unreaded = 1;
                if (this.dialog.unreadCount < 99) {
                    this.dialog.unreadCount++;
                }
                WA.ContactStates.saveUnread(message.data.from, this.dialog.unreadCount);
                var updateTabs = true;

//                WA.ChangeLog.logAction('unread', {
//                    email: message.data.from,
//                    unread: this.dialog.unreadCount
//                });
            }

            if (message instanceof AuthMessage) {
                this.dialog.auth = true;
                this.dialog.historyLoaded = true;
            }

            this.updateEvent.fire(updateTabs);
        },

        _onMessageUpdate: function (message_id) {
            var message = this._instances[message_id];
            if (message) {
                message.destroy();
                delete this._instances[message_id];
            }
            this._sort();
            this.updateEvent.fire();
        },

        load: function () {
            if (!this.dialog.historyLoaded && !this._requested) {
                this._requested = true;

                var mail = this.dialog.mail;
                SOCK.send('contact/history', {email: mail}, function (success) {
                    if(!success) {
                        WA.setTimeout(WA.createDelegate(function () {
                            SOCK.send('contact/history', {email: mail});
                        }), 1000);
                    }
                }, this);
                /*
                 TODO:
                 , function (resp) {
                 if(!this.__destroyed) {
                 if (resp.timeout || resp.status != 200) {
                 delete this._requested;
                 debugger;
                 }
                 }
                 }, this);
                 */
            }
        },

        _repack: function (arr) {
            var history = [];
            if (arr.history) {
                U.Array.each(arr.history, function (arr) {
                    history.push({
                        from: arr[1].from,
                        text: arr[1].text,
                        date: arr[1].timestamp
                    })
                });
            }
            return history;
        },

        markAsReaded: function () {
            var changed = false;
            if (this.dialog.history) {
                U.Array.each(this.dialog.history, function (data) {
                    if (data.unreaded) {
                        delete data.unreaded;
                        changed = true;
                    }
                }, this);
            }
            return changed;
        },

        makeAuthorized: function () {
            var changed = false;
            if (this.dialog.history) {
                U.Array.each(this.dialog.history, function (data, i) {
                    if (data.auth) {
                        //this.dialog.history.splice(i, 1);
                        delete data.auth;
                        changed = true;
                    }
                }, this);
            }
            return changed;
        },

        _sortCb: function (a, b) {
            if (a.date < b.date) {
                return -1;
            } else if (a.date > b.date) {
                return 1;
            } else {
                return 0;
            }
        },

        _sort: function () {
            if(this.dialog.history && this.dialog.history.sort) {
                this.dialog.history.sort(this._sortCb);
            }
        },

        _intersect: function (history) {
            var changed = false;
            if (!this.dialog.history || this.dialog.history.length == 0) {
                this.dialog.history = history;
                changed = true;
            } else {
                var long_list, keys_hash = {}, short_list;
                if (history.length > this.dialog.history) {
                    long_list = history;
                    short_list = this.dialog.history;
                } else {
                    long_list = this.dialog.history;
                    short_list = history;
                }

                U.Array.each(long_list, function (el) {
                    var key = el.from + el.date + el.text.substr(0, 10);
                    keys_hash[key] = 1;
                });

                U.Array.each(short_list, function (el) {
                    var key = el.from + el.date + el.text.substr(0, 10);
                    if (!keys_hash[key]) {
                        long_list.push(el);
                        changed = true;
                    }
                }, this);

                if (changed) {
                    this.dialog.history = long_list;
                }
            }

            return changed;
        },

        _onConnectionContactHistory: function (value) {
            if (value.email == this.dialog.mail) {
                if(value.disabled) {
                    this.dialog.historyDisabled = '1';
                }
                var history = this._repack(value);
                var changed = this._intersect(history);
                this._sort();
                if (changed || !this.dialog.historyLoaded) {
                    this.dialog.historyLoaded = 1;
                    this.updateEvent.fire();
                }
            }
        },

        destroy: function () {
            History.superclass.destroy.apply(this, arguments);
            U.Object.each(this._instances, function (message) {
                message.destroy();
            }, this);
            this.updateEvent.removeAll();
            this.__destroyed = true;
        }
    });

    var Dialog = WA.extend(Object, {

        constructor: function (mail, data) {
            this.data = data || {
                mail: mail,
                modifDate: +new Date(),
                cast: {},
                unreadCount: 0
            };
            this.data.unreadCount || (this.data.unreadCount = 0);
            this.data.cast[WA.ACTIVE_MAIL] = {
                nick: WA.DialogsAbstraction.nick
            };

            this.history = new History(this.data);
            this.history.updateEvent.on(this._onHistoryUpdate, this);

            this.focusedEvent = new U.Event();
            this.updateEvent = new U.Event();

            this.typingTimeout = new U.DelayedTask({
                interval: 15000,
                fn: this._onTypingTimeout,
                scope: this
            });

            if (this.data.typing) {
                var now = +new Date();
                var timeout = 15000 - (now - this.data.typing);
                this.typingTimeout.start(timeout, this._onTypingTimeout, this);
            }
        },

        updateSelfNick: function (nick) {
            this.data.cast[WA.ACTIVE_MAIL].nick = nick;
            this.data.modifDate = +new Date();
            this.updateEvent.fire();
        },

        _onTypingTimeout: function () {
            if (this.data.typing) {
                delete this.data.typing;
                this.updateEvent.fire(true);
            }
        },

        setTyping: function () {
            this.data.typing = +new Date();
            this.updateEvent.fire(true);
            this.typingTimeout.start();
        },

        stopTyping: function () {
            if (this.data.typing) {
                this.typingTimeout.stop();
                delete this.data.typing;
                this.updateEvent.fire(true);
            }
        },

        setCast: function (nick, status, statusTitle) {
            this.data.cast[this.data.mail] = {
                nick: U.String.htmlEntity(nick),
                status: status,
                statusTitle: statusTitle
            };
            this.updateEvent.fire(true);
        },

        focus: function () {
            this.focusedEvent.fire(this.data.mail, this);
            this.history.load();
        },

        isUnread: function () {
            return this.data.unreaded;
        },

        markAsReaded: function () {
            if (this.history.markAsReaded() || this.data.unreaded) {
                delete this.data.unreaded;
                this.data.unreadCount = 0;
                this.data.modifDate = +new Date();

//                WA.ChangeLog.logAction('unread', {
//                    email: this.data.mail,
//                    unread: 0
//                });

                this.updateEvent.fire(true);
            }
        },

        makeAuthorized: function () {
            if (this.history.makeAuthorized() || this.data.auth) {
                delete this.data.auth;
                delete this.data.historyLoaded;
                this.history.load();
                this.data.modifDate = new Date().getTime();
                this.updateEvent.fire(true);
            }
        },

        getHistory: function () {
            return this.history;
        },

        _onHistoryUpdate: function (updateTabs) {
            this.data.modifDate = +new Date();
            this.updateEvent.fire(updateTabs);
        },

        destroy: function () {
            this.history.destroy();
            this.focusedEvent.removeAll();
            this.updateEvent.removeAll();
            this.typingTimeout.stop();
        }

    });

    var DialogsManager = WA.extend(Object, {

        constructor: function (cache) {
            this._cache = cache;
            this._data = this._cache.data;
            this._instances = {};

            this.MAX_DIALOGS_OPENED = 4;

            this._fetchDialogs();
        },

        _fetchDialogs: function () {
            if (this._data.dialogs && this._data.dialogs.length) {
                U.Array.each(this._data.dialogs, function (dialog) {
                    if (dialog.unreaded || dialog.typing) {
                        this._makeInstance(dialog.mail);
                    }
                }, this);
                var focusedDialog = this.getFocused();
                if (focusedDialog) {
                    focusedDialog.history.load();
                }
            }
        },

        _indexOfDialog: function (mail) {
            return U.Array.indexOfBy(this._data.dialogs, function (dialog) {
                if (dialog.mail == mail) {
                    return true;
                }
            })
        },

//        moveDialogToEnd: function (mail) {
//            var index = this._indexOfDialog(mail);
//            if(index != -1) {
//                var dialog = this._data.dialogs.splice(index, 1)[0];
//                this._data.dialogs.push(dialog);
//                this._data.dialogsModifDate = +new Date();
//            }
//        },

        getIfExist: function (mail) {
            var index = this._indexOfDialog(mail);
            if (index != -1) {
                return this.get(mail);
            }
        },

        get: function (mail) {
            if (mail && !this._instances[mail]) {
                this._makeInstance(mail);
            }
            return this._instances[mail];
        },

        _checkOverflow: function () {
            if (this._data.dialogs.length >= this.MAX_DIALOGS_OPENED) {
                this.close(this._data.dialogs[0].mail === this._data.focused ? this._data.dialogs[1].mail : this._data.dialogs[0].mail);
            }
        },

        _makeInstance: function (mail) {
            var index = this._indexOfDialog(mail);
            if (!this._data.dialogs[index]) {
                this._checkOverflow();
                this._instances[mail] = new Dialog(mail);


                U.Mnt.increaseDialogMax(this.count() + 1);
                this._data.dialogs.push(this._instances[mail].data);
                this._data.dialogsModifDate = +new Date();
            } else {
                this._instances[mail] = new Dialog(mail, this._data.dialogs[index]);
            }

            this._instances[mail].focusedEvent.on(this._onDialogFocused, this);
            this._instances[mail].updateEvent.on(this._onDialogUpdate, this);
        },

        _onDialogFocused: function (mail, dialog) {
            this._data.focused = mail;
            this._cache.commit();
        },

        blur: function () {
            this._data.focused = false;
            this._cache.commit();
        },

        _onDialogUpdate: function (updateTabs) {
            if (updateTabs) {
                this._data.dialogsModifDate = +new Date();
            }
            this._cache.commit();
        },

        count: function () {
            return this._data.dialogs.length;
        },

        getFocused: function () {
            if (this._data.focused) {
                return this.get(this._data.focused);
            } else {
                return false;
            }
        },

        eachDialog: function (fn, scope) {
            U.Array.each(this._data.dialogs, function (dialog, index) {
                var mail = dialog.mail;
                fn.call(scope || this, dialog, mail, index);
            }, this);
        },

        updateSelfNick: function (nick) {
            this._cache.preventCommit();
            U.Object.each(this._instances, function (dialog) {
                dialog.updateSelfNick(nick);
            }, this);
            this._cache.permitCommit();
        },

        closeAll: function () {
            this.destroy();
            this._instances = {};

            this._data.dialogs = [];
            this._data.focused = false;
            this._data.dialogsModifDate = +new Date();

            this._cache.commit();
        },

        close: function (mail) {
            if (this._instances[mail]) {
                this._destroyDialog(this._instances[mail]);
                delete this._instances[mail];
            }

            var index = this._indexOfDialog(mail);
            if (index != -1) {
                this._data.dialogs.splice(index, 1);
                this._data.dialogsModifDate = +new Date();
                var uncommitted = true;

                if (this._data.focused == mail) {
//                    index = this._data.dialogs.length - 1;
//                    if (index != -1) {
//                        uncommitted = false;
//                        this.get(this._data.dialogs[index].mail).focus();
//                    } else {
                        this._data.focused = false;
//                    }
                }

                if (uncommitted) {
                    this._cache.commit();
                }
            }
        },

        _destroyDialog: function (dialog) {
            dialog.destroy();
            delete this._instances[dialog.mail];
        },

        destroy: function () {
            U.Object.each(this._instances, this._destroyDialog, this);
        },

        clearHistory: function () {
            var now = +new Date();
            if (this._data.dialogs && this._data.dialogs.length) {
                U.Array.each(this._data.dialogs, function (dialog) {
                    if (this._instances[dialog.mail]) {
                        this._destroyDialog(this._instances[dialog.mail]);
                    }

                    delete dialog.typing;
                    delete dialog.historyLoaded;
                    delete dialog.history;

                }, this);
                this._cache.commit();
            }
        }

    });

    var DialogsCache = WA.extend(ConnectionRoster, {

        constructor: function (cache) {
            DialogsCache.superclass.constructor.apply(this, arguments);

            this.updateEvent = new U.Event();

            this.data = {
                dialogs: []
            };

            this.dm = null;

            this._sync = WA.Synchronizer.registerComponent('dialogs', ['data'], true, true);
            //this._sync.triggerEvent.on(this._onSync, this);
            FM.focusEvent.on(this._onFocus, this);
        },

        activate: function (cb, scope) {
            this._sync.read(function (data) {
                this._onSync(data);
                this._sync.triggerEvent.on(this._onSync, this);
                cb && cb.call(scope || window);
            }, this);
        },

        deactivate: function () {
            this._sync.triggerEvent.un(this._onSync, this);
            if (this.dm) {
                this.dm.clearHistory();
                this.dm.destroy();
                this.dm = null;
            }

            this.data = {
                dialogs: []
            }
        },

        _onFocus: function () {
            this._sync.read(this._onSyncRead, this);
        },

        _isSyncChanged: function (data) {
            if (data.isChanged('data')) {
                var d = JSON.parse(data.get('data') || 'null');
                if (d && d.modifDate && this.data.modifDate != d.modifDate) {
                    return d;
                }
            }
        },

        _onSyncRead: function (data) {
            data = this._isSyncChanged(data);
            if (data) {
                if (this.dm) {
                    this.dm.destroy();
                    this.dm = null;
                }
                this.data = data;
            }

            if (!this.dm) {
                this.dm = new DialogsManager(this);
            }

            if (data) {
                this.updateEvent.fire(this.data, 1);
            }
        },

        _onSync: function (data) {
            data = this._isSyncChanged(data);
            if (data) {
                if (this.dm) {
                    this.dm.destroy();
                    this.dm = null;
                }
                this.data = data;
                this.updateEvent.fire(this.data, 1);
            }
        },

        _onBeforeConnectionResponse: function () {
            this._preventCommit = 1;
        },

        _onAfterConnectionResponse: function () {
            delete this._preventCommit;
            if (this._uncommitted) {
                this.commit();
            }
        },

        preventCommit: function () {
            this._preventCommit = true;
        },

        permitCommit: function () {
            this._preventCommit = false;
            if (this._uncommitted) {
                this.commit();
            }
        },

        commit: function () {
            if (!this._preventCommit) {
                this.data.modifDate = +new Date();

                delete this._uncommitted;

                this._preventCommit = 1;
                this.updateEvent.fire(this.data);
                if (this._uncommitted) {
                    this.updateEvent.fire(this.data);
                }
                delete this._preventCommit;

                if (!this.data || !this.data.dialogs || !this.data.dialogs.length) {
                    WA.debug.Console.log('WARNING!!!, writing empty dialogs list ', this.data);
                }

                this._sync.write('data', JSON.stringify(this.data));

            } else {
                this._uncommitted = 1;
            }
        }

    });

    WA.DialogsAbstraction = WA.extend(ConnectionRoster, {

        constructor: function () {
            WA.DialogsAbstraction.superclass.constructor.apply(this, arguments);

            this._cache = new DialogsCache();
            this.updateEvent = new U.Event().relay(this._cache.updateEvent);
            this.incomingMessageEvent = new U.Event();
            this.updateSelfNickEvent = new U.Event();


            WA.Storage.whenReady(function () {
                WA.Storage.load(['selfnick'], {
                    success: function (storage) {
                        if (storage['selfnick']) {
                            WA.DialogsAbstraction.nick = storage['selfnick'];
                            this.updateSelfNickEvent.fire(storage['selfnick']);
                        }
                    },
                    scope: this
                })
            }, this);

            U.Mnt.onBeforeFlushEvent.on(function () {
                var count = 0;
                if(this._cache.dm && (count = this._cache.dm.count())) {
                    U.Mnt.count('dialogsCount'+ (count < 6 ? count : 'M'));
                }
            }, this);
        },

        activate: function (cb, scope) {
            this._cache.activate(cb, scope);
        },

        deactivate: function () {
            this._cache.deactivate();
        },

//        moveDialogToEnd: function (mail) {
//            this._cache.dm.moveDialogToEnd(mail);
//        },

        _onConnectionContactStatus: function (value) {
            var dialog = this._cache.dm.getIfExist(value.email);
            if (dialog) {
                dialog.setCast(value.nickname, value.status.type, value.status.title);
            }
        },

        _onConnectionContactMicblogStatus: function (value) {
            var dialog = this._cache.dm.getIfExist(value.from);

            if (dialog) {
                dialog.getHistory().append(new MicblogMessage(value));
            }
        },

        _onConnectionTypingNotify: function (value) {
            var dialog = this._cache.dm.getIfExist(value.from);
            if (dialog) {
                dialog.setTyping();
            }
        },

        _onConnectionAuthRequest: function (value) {
            var email = value.from;
            var message;

            if (WA.MainAgent.isChat(email)) {
                message = new IncomingMessage(value);
                WA.ChangeLog.logAction('add', {
                    email: message.data.from,
                    nickname: message.data.nick || email,
                    status: { type: 'online' }
                });
            } else {
                message = new AuthMessage(value);
            }

            this._onConnectionMessage(message);
        },

        _onConnectionMessage: function (value) {
            var message = null;
            if (value instanceof IncomingMessage) {
                message = value;
            } else {
                message = new IncomingMessage(value);
            }

            var dialog = this._cache.dm.get(message.data.from);
            dialog.setCast(message.data.nick, message.data.status, message.data.statusTitle);
            dialog.stopTyping();
            dialog.getHistory().append(message);

            this._messageArrived = true;
            WA.util.Mnt.activeUserCount();
        },

        _onConnectionWp: function (value) {
            if(value.uniq && value.uniq == this._selfWPUniq && value.result) {
                var index = U.Array.indexOfBy(value.result, function (v) {
                    if(v.email == WA.ACTIVE_MAIL) {
                        return true;
                    }
                }, this);

                if(index != -1) {
                    var nick = WA.MainAgent.WP2Nick(value.result[index]);
                    WA.DialogsAbstraction.nick = nick;

                    WA.Storage.save({
                        'selfnick': nick
                    });

                    this._cache.dm.updateSelfNick(nick);
                    this.updateSelfNickEvent.fire(nick);
                }
            }
        },

        _onConnectionUserInfo: function (value) {
            WA.DialogsAbstraction.nick = value['Mrim.NickName'];

            this._selfWPUniq = Math.round(Math.random()*99999999);
            WA.conn.Socket.send('wp', {
                email: WA.ACTIVE_MAIL,
                uniq: this._selfWPUniq
            });
        },

        _onAfterConnectionResponse: function () {
            if (this._messageArrived) {
                delete this._messageArrived;
                this.incomingMessageEvent.fire();
            }
        },

        postMessage: function (mail, text, resend) {
            var dialog = this._cache.dm.get(mail);
            var message = new OutgoingMessage({text: text, mail: mail, resend: resend});
            dialog.getHistory().append(message);
            message.send();

            S.load(['stat.count'], function (storage) {
                S.save({
                    'stat.count': (+storage['stat.count'] || 0) + 1
                });
            });
            WA.util.Mnt.activeUserCount();
        },

        updateHeight: function () {

        },

        markAsReaded: function (mail) {
            var dialog;
            if(this._cache.dm) {
                if(mail) {
                    dialog = this._cache.dm.getIfExist(mail);
                } else {
                    dialog = this._cache.dm.getFocused();
                }
                if (dialog) {
                    dialog.markAsReaded();
                    WA.ContactStates.saveUnread(dialog.data.mail, 0);
                    WA.ContactStates.removeState(dialog.data.mail, WA.ContactStates.MESSAGE_STATE);
                }
            }
        },

        makeAuthorized: function () {
            var dialog = this._cache.dm.getFocused();
            dialog.makeAuthorized();
        },

        focus: function (mail) {
            var dialog = this._cache.dm.get(mail);
            dialog.focus();
        },

        blur: function () {
            if (this._cache.dm) {
                this._cache.dm.blur();
            }
        },

        closeAll: function () {
            var dm = this._cache.dm;

            var emails = [];
            dm.eachDialog(function (dialog, mail) {
                if (dialog.unreaded) {
                    emails.push(mail);
                }
            }, this);
            WA.ContactStates.removeStates(emails, WA.ContactStates.MESSAGE_STATE);

            dm.closeAll();
        },

        close: function (mail) {

            this._cache.dm.close(mail);
            WA.ContactStates.saveUnread(mail, 0);
            WA.ContactStates.removeState(mail, WA.ContactStates.MESSAGE_STATE);
        },

        open: function (mail, nick, status, statusTitle) {
            var dialog = this._cache.dm.get(mail);
            dialog.setCast(nick, status, statusTitle);
            dialog.focus();
        },

        _extractDataFromDialog: function (dlg) {
            var mail = dlg.data.mail;
            var cast = dlg.data.cast[mail];
            return {
                mail: mail,
                nick: cast.nick,
                status: cast.status,
                statusTitle: cast.statusTitle,
                unreaded: dlg.data.unreaded
            };
        },

        getFocused: function () {
            if(!this._cache.dm) {
                return false;
            }

            var dlg = this._cache.dm.getFocused();
            if(dlg) {
                return this._extractDataFromDialog(dlg);
            } else {
                return false;
            }
        },

        renameContact: function (mail, newNick) {
            var dlg = this._cache.dm.getIfExist(mail);
            if (dlg) {
                var data = this._extractDataFromDialog(dlg);
                dlg.setCast(newNick, data.status, data.statusTitle);
            }
        },

        getDialogCount: function () {
            return this._cache.dm.count();
        }

    });

    WA.DialogsAbstraction.nick = WA.ACTIVE_MAIL;

})();
(function () {

    var WA = WebAgent;
    var U = WA.util;
    var SOCK = WA.conn.Socket;

    var TextArea = WA.extend(WA.ui.Component, {
        constructor: function (config) {
            TextArea.superclass.constructor.apply(this, arguments);

            this.changeEvent = new U.Event();
            this.submitEvent = new U.Event();
            this.expandEvent = new U.Event();

            this._field = null;
            this._hintText = '�������� ���������...';
            this._hintClass = 'nwa_hint_msg';
            this.expanded = false;
            this.expandedWidth = 0;
            this.postMaxLength = 12000;

            this._ctrlDate = 0;

            this._textChecker = new U.DelayedTask({
                interval: 100,
                fn: function () {
                    var domEl = this._field.getEl().dom, parentWidth;
                    if (this._value != this._field.val()) {
                        this._value = this._field.val();

                        if (encodeURIComponent(this._value).length > this.postMaxLength) {
                            this._value = this._value.substr(0, this.postMaxLength);
                            this._field.val(this._value);
                        }

                        this.changeEvent.fire(this._value);

                        var expanded = domEl.scrollHeight > 25;

                        if (TextArea.ghostField) {
                            TextArea.ghostField.getEl().setWidth(this._field.getEl().parent().getWidth());
                            TextArea.ghostField.val(this._value + 'w');
                            expanded = TextArea.ghostField.getEl().dom.scrollHeight > 25;
                        }

                        if (this.expanded != expanded) {
                            this.expanded = expanded;
                            this.expandEvent.fire(expanded);
                        }
                    }
                    if (WA.isFF) { //FF hack to fix overflow:hiddden cursor bug
                        if (this.expandedWidth == 0 && domEl.offsetWidth > domEl.scrollWidth) {
                            this.expandedWidth = domEl.offsetWidth - domEl.scrollWidth;
                        } else if (this.expandedWidth > 0 && domEl.offsetWidth == domEl.scrollWidth) {
                            this.expandedWidth = 0;
                        }
                        parentWidth = this._field.getEl().parent().getWidth();
                        this._field.getEl().setWidth(parentWidth + this.expandedWidth);
                    }

                    this._textChecker.start();
                },
                scope: this
            });
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-chat__input',
                children: {
                    tag: 'div',
                    cls: 'nwa-chat__input-inner',
                    children: {
                        tag: 'div',
                        cls: 'nwa-chat__input-wrap'
                    }
                }
            });

            var submitEl = new WA.ui.Button({
                cls: 'nwa-chat__input-send',
                handler: function (el, ev) {
                    this._submit(ev);
                    U.Mnt.count('submitByButton');
                },
                scope: this,
                tooltip: '���������',
                text: '<span></span>'
            });

            submitEl.render(this.el.first());

            var smilesMenu = new WA.DialogsAgent.Smiles();

            smilesMenu.itemClickEvent.on(function (ignored, item, ev) {
                ev.stopEvent();
                this.addSmile(item.initialConfig);
            }, this);
            var smilesBtn = new WA.ui.Button({
                cls: 'nwa-chat__input-smiles',
                tooltip: '������',
                menu: smilesMenu,
                text: '<span></span>'
            });
            smilesBtn.render(this.el.first());

//            smilesMenu.showEvent.on(function(){
//                smilesBtn.setState(true);
//            }, this);
//
//            smilesMenu.hideEvent.on(function(){
//                smilesBtn.setState(false);
//            }, this);

            this._field = new WA.ui.TextField({
                cls: 'nwa-chat__input-field',
                multiline: true,
                maxLength: this.postMaxLength,
                hintText: this._hintText,
                hintClass: this._hintClass
            });
            this._field.render(this.el.first().first());

            this._field.getEl().on('keydown', function (ev) {
                if (!ev.shiftKey && ev.keyCode == 13) {
                    this._submit(ev);
                    ev.stopEvent();
                    U.Mnt.count('submitByEnter');
                }
            }, this);

            if (!TextArea.ghostField) {
                // service field for checking vscroll
                TextArea.ghostField = new WA.ui.TextField({
                    cls: 'nwa-chat__input-field nwa-input_ghost',
                    multiline: true
                });
                TextArea.ghostField.render(WA.getBody());
            }

            this._textChecker.start();

        },

        _onKeyUp: function (ev) {
            var now = +new Date();
            if (ev.browserEvent.ctrlKey) {
                this._ctrlDate = now;
            }
            if (now - this._ctrlDate < 400 && ev.keyCode == 13) {
                this._submit(ev);
            }
        },

        _submit: function (ev) {
            if (this._field.val().match(/\S/)) {
                if( this.submitEvent.fire(this._field.val()) ) {
                    this._field.val('', true);
                    this._field.focus();
                } else {
                    ev.stopEvent();
                }
                U.Mnt.rbCountAction(); //RB send msg
            }
        },

        set: function (str) {
            this._field.val(str);
            this._value = str;
        },

        _safeSetFocus: function () {
            this._field.focus();
        },

        setFocus: function (deferred) {
            if(!deferred) {
                this._safeSetFocus();
            } else {
                WA.setTimeout(WA.createDelegate(function () {
                    this._safeSetFocus();
                }, this), 10);
            }
            this._textChecker.start();
        },

        addSmile: function (smile) {
            this._field._onFocus(); //hack for IE: force checking hint before actual focus
            this.setFocus();
            var area = this._field.getEl().dom;
            var code = smile.alt;
            var range;
            if (document.selection) {
                range = document.selection.createRange().duplicate();
                range.text = ' ' + code + ' ';
            } else if (typeof area.selectionStart !== 'undefined') {
                range = area.selectionEnd;
                area.value = area.value.substring(0, range) + ' ' + code + ' ' + area.value.substr(range);
            } else {
                area.value += code;
            }
        },

        deactivate: function () {
            this._textChecker.stop();
        }

    });

    var History = WA.extend(WA.ui.Component, {
        constructor: function () {
            History.superclass.constructor.apply(this, arguments);
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-chat__history',
                children: [
                    {
                        tag: 'div',
                        cls: 'nwa-history'
                    }
                ]

            });

            this.contsRootEl = this.el.first();

            this.scrollBar = new WA.ui.ScrollBar({
                source: this.contsRootEl,
                watchResize: WA.isPopup,
                autoRender: this.el
            });
        },

        _groupingKey: '',
        _groupingDate: 0,

        _renderMessage: function (m, cast, showAuthDialog) {

            var nick = cast[m.from] && cast[m.from].nick || m.from;
            var message = m.text || '';
            var tmp;
            var micblog = !!m.micblog;
            if (m.from != WA.ACTIVE_MAIL && m.from.indexOf('@chat.agent') != -1
                    && WA.MainAgent.isChat(m.from)
                    && (tmp = m.text.match(/(.*):\n([\s\S]*)/))) {
                nick = tmp[1];
                message = tmp[2];
            }

            var groupingKey = m.from+ nick;

            if (micblog) {
                if (message == '') {
                    return '';
                }
                this._groupingKey = '';
            } else if(this._groupingKey == groupingKey && m.date-this._groupingDate < 60) {
                var useGroup = true;
                this._groupingDate = m.date;
            } else {
                this._groupingKey = groupingKey;
            }
            this._groupingDate = m.date;

            var messageText = WA.DialogsAgentView.prepareMessageText(message);
            if (micblog) {
                messageText = '<div class="nwa-message__text"><div class="nwa-message__text-wrap">' + messageText + '</div></div>';
            }

            if (m.from != WA.ACTIVE_MAIL) {
                // remember last incoming message
                this._lastMessage = m;
            }

            return U.String.format(
                    '<div{5} class="nwa-message{6}{7}{8}{10}"><div class="nwa-message__date"><span>{0}</span></div><div class="nwa-message__from">{1}{4}</div>{2}{9}</div><div class="nwa-clear-both"></div>',
                    micblog ? '<a href="javascript:;" data-action="reply_micblog" data-post-id="' + m.message_id + '" class="nwa-button_link">��������</a>' : U.String.formatDate(m.date * 1000),
                    nick + (micblog ? ' ������� �������' : ''),
                    messageText + (showAuthDialog ? this._renderAuthMessage(m) : ''),
                    WA.isDebug && 0 ? '' + Math.round(128 + Math.random() * 100).toString(16).substr(-2, 2) + Math.round(128 + Math.random() * 100).toString(16).substr(-2, 2) + Math.round(128 + Math.random() * 100).toString(16).substr(-2, 2) : 'ffffff',
                    '', //m.undelivered ? '<br/><span class="nwa-message__sending"> ������������...</span>' : '',
                    m.undelivered ? ' id="wa-undelivered-message-' + m.message_id + '"' : '',
                    m.unreaded && !micblog ? ' nwa-message-unread' : '',
                    useGroup ? ' nwa-message-group' : (micblog ? ' nwa-message_micblog' : ''),
                    m.from == WA.ACTIVE_MAIL ? ' nwa-message_self' : '',
                    m.deliverExpired ? '<br/><span class="nwa-message__deliver"> ��������� �� ����������. </span>'
                        + '<a href="javascript:;" data-action="resend" data-msg-id="' + m.message_id + '" data-text="' + U.String.htmlEntity(message)
                        + '" class="nwa-button_link">�������������</a>' : '',
                    showAuthDialog ? ' nwa-message_auth' : ''
                );
        },

        _renderPreloader: function () {
            this.contsRootEl.update('<span class="nwa-message__info">�����������...</span>');
        },

        _renderEmpty: function () {
            this.contsRootEl.update('<span class="nwa-message__info">���� ��� ���������</span>');
        },

        _renderAuthMessage: function (msg) {
            var html = '', actions = [
                ['add','������������ � �������� �������', 'nwa-button nwa-button_blue'],
                ['cancel','���������', 'nwa-button_link'],
                ['ignore','������������', 'nwa-button_link'],
                ['spam','��� ����!', 'nwa-button_link']
            ];

            U.Array.each(actions, function (action) {
                html += U.String.format('<button href="javascript:;" data-action="{0}" data-msg-id="{3}" data-text="{4}" class="nwa-message__actions_{0} {2}">{1}</button>', action[0], action[1], action[2], msg.message_id, U.String.htmlEntity(msg.text || ''));
            }, this);

            return '<div class="nwa-message__actions">' + html + '</div>';
        },

        _renderMessages: function (dialog) {
            var html = [], auth = false;

            this._groupingKey = '';
            this._groupingDate = 0;
            if(dialog.historyDisabled) {
                html.push('<span  class="nwa-message__info">��������� �������� ������� ���������, �������� ��������� ����� <a href="//e.mail.ru/cgi-bin/mrimhistory">���</a></span>');
            }

            U.Array.each(dialog.history, function (message) {
                html[html.length] = this._renderMessage(message, dialog.cast, !auth && message.auth);
                if (!auth) auth = message.auth;
            }, this);
            html = html.join('');

            if (!dialog.historyLoaded) {
                html = '<div  class="nwa-message__info">������� ������ ����������...</div>' + html;
            }

            this.contsRootEl.update(html);
        },

        _renderHistory: function (dialog, scrollBottomAnyway) {
            var scrollTop = -1;
            if (!scrollBottomAnyway) {
                scrollTop = this._getScroll();
            }

            this.contsRootEl.hide();

            if (!dialog || !dialog.history) {
                this._renderPreloader();
            } else if (dialog.history.length == 0 && !dialog.historyDisabled) {
                this._renderEmpty();
            } else {
                this._renderMessages(dialog);
            }

            this.contsRootEl.show();

            this._setScroll(scrollTop);
        },

        _setScroll: function (h) {
            if (this.contsRootEl.dom.children.length) {
                if (h == -1) {
                        this.contsRootEl.dom.scrollTop = this.contsRootEl.dom.scrollHeight;
                } else {
                    this.contsRootEl.dom.scrollTop = h;
                }
            }
            this._refreshScroll();
        },

        _refreshScroll: function () {
            this.scrollBar.sync();
            WA.setTimeout( //repeat refresh after animation is finished
                WA.createDelegate(this.scrollBar.sync, this.scrollBar),
                400);
        },

        _getScroll: function () {
            var e = this.contsRootEl.dom;
            if (e.scrollTop + e.clientHeight == e.scrollHeight) {
                return -1;
            } else {
                return e.scrollTop;
            }

        },

        scrollBottom: function () {
            this._setScroll(-1);
        },

        renderHistory: function (dialog, cb, scope) {
            var uniq = dialog.mail + dialog.modifDate;
            if (this._uniq != uniq) {
                this._renderHistory(dialog, false);
                this._uniq = uniq;
            } else {
                this._setScroll(-1);
            }
        },

        getLastMessage: function () {
            return (this._lastMessage || {});
        },

        deactivate: function () {
            delete this._uniq;
        },

        setTopPadding: function (h) {
            this.el.dom.style.top = h + 'px';
        }
    });

    var NameBox = WA.extend(WA.ui.Component, {
        constructor: function () {
            NameBox.superclass.constructor.apply(this, arguments);

            this._editMode = false;
            this.editOk = new U.Event();
        },

        _onRender: function (ct) {

            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-namebox',
                children: [
                    {
                        tag: 'div',
                        cls: 'nwa-namebox__label'
                    },
                    {
                        tag: 'div',
                        cls: 'nwa-namebox__fadeout'
                    }
                ]
            });

            this.textField = this.el.first();

            this.editField = new WA.ui.TextField({
                cls: 'nwa-namebox__input'
            });
            this.editField.render(this.el);
            this.editField.getEl().on('click', function(e){e.stopEvent()})

            this.editField.getEl().on('keydown', function (e) {
                if (e.keyCode == 13) {
                    var val = U.String.trim(this._sanitize(this.editField.val()));
                    if (val != '') {
                        this.editOk.fire(val);
                        this._editOff();
                    } else {
                        this.editField.val(val);
                    }
                    e.preventDefault();
                } else if (e.keyCode == 27) {
                    this._editOff();
                }
            }, this);

            this.editField.blurEvent.on(function(){
                this._editOff();
            }, this);
        },

        _sanitize: function (val) {
            return val.replace(/(\\|\/|<|>|&)/g, '');
        },

        _editOn: function () {
            if (this._editMode) return;
            this._editMode = true;
            this.el.addClass('nwa-namebox_editor');
            this.editField.val(U.String.entityDecode(this.textField.dom.innerHTML));
            this.editField.focus();
            this.editField.select();
        },

        _editOff: function () {
            if (!this._editMode) return;
            this._editMode = false;
            this.editField.blur();
            this.el.removeClass('nwa-namebox_editor');
        },

        setName: function (val) {
            this.textField.update(val);
        },

        edit: function (force) {
            if (force !== false) {
                this._editOn();
            } else {
                this._editOff();
            }
        }
    });

    var DialogView = WA.extend(WA.ui.Component, {

        constructor: function (config, dialog) {
            DialogView.superclass.constructor.call(this, config);

            this.mail = dialog.mail;
            this.nick = U.String.htmlEntity(dialog.cast[dialog.mail].nick);
            this._typingInfo = false;
            this._previewPhoto = null;

            this.messageEvent = new U.Event;
            this.typingEvent = new U.Event;
            this.unreadedEvent = new U.Event();
            this.authActionEvent = new U.Event();

            this._textarea = new TextArea();
            this._textarea.changeEvent.on(this._onMessageChanged, this);
            this._textarea.expandEvent.on(this._onMessageExpand, this);
            this._textarea.submitEvent.on(this._onMessageSubmit, this);

            this._nameBox = new NameBox();
            this._nameBox.editOk.on(function(newName){
                this.renameContactEvent.fire(this.mail, newName);
            }, this);

            this.typingNotifyChecker = new U.DelayedTask({
                interval: 10000,
                fn: this._onTypingNotifyCheck,
                scope: this
            });

            this.tabButton = new WA.ui.TabButton({
                closable: true,
                fader: true,
                defaultIconCls: WA.buildIconClass('default'),
                tabAttributes: {
                    mail: dialog.mail
                }
            });

            this._history = new History();

            this.removeContactEvent = new U.Event();
            this.renameContactEvent = new U.Event();

            this._profile = new WA.Profile(false, {
                fadeIn: true,
                modal: true
            });

        },

        updateTabInfo: function (dialog) {
            var info = this.fetchDialogInfo(dialog);
            this.el.setAttribute('status', info.status);
            this.tabButton.setText(info.nick);
            this.tabButton.getEl().toggleClass('nwa-button-tab_offline', info.status == 'offline');
            this.tabButton.getEl().toggleClass('nwa-button-tab_message', info.status == 'message');
            this.tabButton.getEl().toggleClass('nwa-button-tab_unread', info.unread > 0);
            this.tabButton.setIconCls(WA.buildIconClass(info.status));
            this.tabButton.setTitle(info.statusTitle);
            this.tabButton.setCount(info.unread);
        },

        fetchDialogInfo: function (dialog) {
            var isChat = WA.MainAgent.isChat(dialog.mail);
            var statusTitle = dialog.auth && 'auth' || isChat && 'chat' || dialog.cast[dialog.mail].statusTitle || dialog.cast[dialog.mail].status && dialog.cast[dialog.mail].status.replace(/^icq_/, '') || 'gray';

            return {
                nick: U.String.htmlEntity(dialog.cast[dialog.mail].nick),
                status: dialog.typing && 'typing' || dialog.unreaded && 'message' || dialog.auth && 'auth' || isChat && 'chat' || dialog.cast[dialog.mail].status || 'gray',
                statusTitle: WA.MainAgent.statuses[statusTitle] || statusTitle,
                unread: dialog.unreadCount
            };
        },

        updateWindow: function (dialog) {
            // update nick and status
            var info = this.fetchDialogInfo(dialog);
//            this.statusField.last().update(U.String.htmlEntity(info.statusTitle));
            this.statusField.first().setAttribute('title',U.String.htmlEntity(info.statusTitle));
            if (info.nick != this.nick) {
                this.nick = info.nick;
                //this._nameBox.first().first().update(info.nick);
                this._nameBox.setName(info.nick);
            }
            //todo: implement setStatusClass()
            this.statusField.first().dom.className = WA.buildIconClass(info.status, true);
            // update history
            this._history.renderHistory(dialog);
        },

        _ignoreUser: function () {
            if (!this._ignoreDialog) {
                this._ignoreDialog = new WA.DialogsAgent.RemoveContactDialog({
                    autoRender: this.winEl,
                    okButton: '������������'
                });

                this._ignoreDialog.okEvent.on(function () {
                    this.authActionEvent.fire({
                        mail: this.mail,
                        action: 'ignore'
                    });
                }, this);
            }

            this._ignoreDialog.show('������������ ������������ <span class="nwa-special-nick">' + U.String.htmlEntity(this.nick) + '</span> ?');
        },

        _spamUserConfirm: function () {
            if (!this._spamDialog) {
                this._spamDialog = new WA.DialogsAgent.RemoveContactDialog({
                    autoRender: this.winEl,
                    okButton: '��, ��� ����'
                });

                this._spamDialog.okEvent.on(this._spamUser, this);
            }

            this._spamDialog.show('������������ �� ������������ <span class="nwa-special-nick">' + U.String.htmlEntity(this.nick) + '</span> � ������� �� ���������?');
        },

        _spamUser: function () {
            var message = this._history.getLastMessage();

            this.authActionEvent.fire({
                mail: this.mail,
                action: 'spam',
                msg_id: message.message_id || 0,
                text: message.text || ''
            });
        },

        _openProfile: function () {
            this._profile.show(this.mail);
        },

        _openMoymir: function () {
            var tmp = this.mail.split('@');
            var url = 'http://my.mail.ru/' + tmp[1].split('.')[0] + '/' + tmp[0] + '/';
            window.open(url);
        },

        _openPhotoPage: function () {
            var tmp = this.mail.split('@');
            var url = 'http://photo.mail.ru/' + tmp[1].split('.')[0] + '/' + tmp[0] + '/';
            window.open(url);
        },

        _openPhoto: function (e) {
            e.stopPropagation();

            if (this._previewPhoto == null) {
                this._previewPhoto = new WA.PhotoPreview({
                    mail: this.mail,
                    autoRender: this.winEl,
                    fadeIn: true,
                    modal: true
                });
            }
            this._previewPhoto.show();
        },

        _showLimitAlert: function () {
            if(!this._alert) {
                this._alert = new WA.DialogsAgent.AlertDialog({
                    fadeIn: true,
                    modal: true,
                    autoRender: this.winEl
                });
            }
            this._alert.show();
        },

        _showRemoveDialog: function () {
            if (!this._removeDialog) {
                this._removeDialog = new WA.DialogsAgent.RemoveContactDialog({
                    okButton: '�������',
                    autoRender: this.winEl
                });

                this._removeDialog.okEvent.on(function () {
                    this.removeContactEvent.fire(this.mail);
                }, this);
            }

            this._removeDialog.show('������� <span class="nwa-special-nick">' + U.String.htmlEntity(this.nick) + '</span> �� ������ ���������?');
        },

        _showAuthDialog: function (message) { //obsolete
            var mail = message.from, nick = message.nick || WA.SA.get(mail).nick || mail;
            if (!this._authDialog) {
                this._authDialog = new WA.DialogsAgent.AuthContactDialog({
                    autoRender: this.winEl
                });

                this._authDialog.okEvent.on(function (action) {
                    this.authActionEvent.fire({
                        mail: mail,
                        action: action,
                        nick: nick,
                        msg_id: message.message_id,
                        text: message.text
                    });
                }, this);
            }

            this._authDialog.setMeta({
                mail: mail,
                nick: nick
            });
            this._authDialog.show();
        },

        /**** methods from TabbedDialog ****/

        _onTypingNotifyCheck: function () {
            var now = +new Date();

            if (this._typingInfo && now - this._typingInfo.date < 10000) {
                this.typingEvent.fire(this.mail);
                this.typingNotifyChecker.start();
            } else {
                this.typingNotifyChecker.stop();
            }
        },

        _onMessageExpand: function (expanded) {
            this.expandTextField(expanded);
        },

        _onMessageChanged: function (text) {
            if (text != '') {
                this._typingInfo = {
                    date: +new Date(),
                    mail: this.mail
                };

                if (!this.typingNotifyChecker.isStarted()) {
                    this._onTypingNotifyCheck();
                }
            }
        },

        _onMessageSubmit: function (text) {
            if(encodeURIComponent(text).length < 9000) {
                this.expandTextField(false);
                this.messageEvent.fire(text);
                this.typingNotifyChecker.stop();
            } else {
                this._showLimitAlert();
                return false;
            }
        },

        focus: function (data) {

            this.renderWindow();
            this.el.addClass('nwa-dialog_expanded');

            this._history.scrollBottom();
            this._typingInfo = false;
            this._textarea.setFocus(true);
        },

        addSmile: function (smile) {
            if (this._textarea.rendered) {
                this._textarea.addSmile(smile);
            }
        },

        /**** methods from DialogsAgent ****/

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-dialog',
                id: 'nwaDialog_' + this.mail,
                mail: this.mail
            });

            this.tabButton.render(this.el);
        },

        blur: function () {
            if (this._removeDialog) {
                this._removeDialog.hide();
                this._removeDialog.destroy();
                this._removeDialog = null;
            }
            if (this._alert) {
                this._alert.hide();
                this._alert.destroy();
                this._alert = null;
            }
            this._textarea.deactivate();
            this.el.removeClass('nwa-dialog_expanded');
            if (this._previewPhoto) {
                this._previewPhoto.hide();
            }
            this._profile.hide();
        },

        expandTextField: function (force) {
            this.el.toggleClass('nwa-chat_text_expanded', force !== false);
            WA.setTimeout(WA.createDelegate(this._history.scrollBottom, this._history), 200);
        },

        renderWindow: function () {

            if (this.renderedWin) return;
            this.renderedWin = true;

            var iconUrl, profileItems = [], isIcq = this.mail.indexOf('@uin.icq') != -1;
            if(this.mail.indexOf('@chat') == -1) {
                iconUrl = WA.makeAvatar(this.mail, '_avatar' + (WA.isPopup ? 32 : 22));
            } else {
                //todo: set icon for chats
            }

            var winEl = this.winEl = this.el.createChild({
                tag: 'div',
                cls: 'nwa-chat',
                children: [{
                    tag: 'div',
                    cls: 'nwa-chat__shadow',
                    children: [
                        {
                            tag: 'div',
                            cls: 'nwa-chat__header',
                            children: [
                                {
                                    tag: 'div',
                                    cls: 'nwa-chat__header-avatar-wrap',
                                    children: [
                                        {
                                            tag: 'div',
                                            cls: 'nwa-chat__header-avatar'
                                        }
                                    ]
                                },
                                {
                                    tag: 'div',
                                    cls: 'nwa-chat__close nwa_action_close'
                                }
                            ]
                        }
                    ]
                }]
            }).first();

            var headerEl = winEl.first();

            this.statusField = headerEl.createChild({
                tag: 'div',
                cls: 'nwa-chat__header-status',
                children: [{
                    tag: 'span'
                }]
            });

            if (iconUrl && isIcq) {
                profileItems = [{
                        iconCls: 'nwa-dialog__tools-profile',
                        text: '������',
                        handler: this._openProfile,
                        scope: this
                    },
                    '-'
                ]
            } else if (iconUrl) {
                profileItems = [{
                        iconCls: 'nwa-dialog__tools-profile',
                        text: '������',
                        handler: this._openProfile,
                        scope: this
                    }, {
                        iconCls: 'nwa-dialog__tools-moymir',
                        text: '��� ���',
                        handler: this._openMoymir,
                        scope: this
                    },
                    {
                        iconCls: 'nwa-dialog__tools-photo',
                        text: '����',
                        handler: this._openPhotoPage,
                        scope: this
                    },
                    '-'
                ]
            }

            var toolsMenu = new WA.ui.menu.Menu({
                cls: 'nwa-dialog__tools-menu',
                fadeIn: true,
                items : profileItems.concat([
                    {
                        iconCls: ' ',
                        text: '����',
                        handler: this._spamUserConfirm,
                        scope: this
                    },
                    {
                        iconCls: ' ',
                        text: '������������',
                        handler: this._ignoreUser,
                        scope: this
                    },
                    {
                        iconCls: ' ',
                        text: '�������������',
                        handler: function () {
                            this._nameBox.edit();
                        },
                        scope: this
                    },
                    {
                        iconCls: ' ',
                        text: '�������',
                        handler: this._showRemoveDialog,
                        scope: this
                    }
                ])
            });

            this._tools = new WA.ui.Button({
                cls: 'nwa-chat__header-tools',
                tooltip: '��������',
                scope: this,
                handler: function(obj,e){
                    e.stopEvent();
                },
                menu: toolsMenu
            });

            this._nameBox.render(headerEl);
            this._nameBox.setName(this.nick);
            this._tools.render(headerEl);
            this._history.render(winEl);
            this._textarea.render(winEl);
            this._profile.render(winEl);

            if (iconUrl) {
                var avatar = headerEl.first();
                avatar.setStyle({cursor:'pointer'}).first().setStyle({'background-image':'url('+iconUrl+')'});
                avatar.on('click', this._openPhoto, this);
            }
        },

        _onAfterRender: function () {
            DialogView.superclass._onAfterRender.call(this);
        },

        deactivate: function () {
            this.typingNotifyChecker.stop();
            this._history.deactivate();
            this._textarea.deactivate();
        },

        _animateClosing: function () {
            //insert dummy container
            if (this.el && !WA.isFF) {
                var tmpEl = '<div class="nwa-dialog nwa-dialog_dummy"></div>';
                tmpEl = U.DomHelper.insertHtml('beforeBegin', this.el.dom, tmpEl, true);
                WA.setTimeout(function(){
                    tmpEl.setWidth(0);
                }, 10);
                WA.setTimeout(function(){
                    tmpEl.remove();
                }, 1000);
            }
        },

        destroy: function () {
            this._animateClosing();
            DialogView.superclass.destroy.call(this);
        }

    });


    var DialogTabs = WA.extend(WA.ui.Component, {
        constructor: function (config) {
            DialogTabs.superclass.constructor.apply(this, arguments);

            this.MAX_TABS_VISIBLE = 4;
            this.TAB_WIDTH = 207;// tab width + margin
            this.TAB_ACTIVE_WIDTH = 307; //dialog width + margin
            this.TABS_MAX_SUM_WIDTH = this.TAB_WIDTH * (this.MAX_TABS_VISIBLE - 1) + this.TAB_ACTIVE_WIDTH;
            this.TABS_UPDATE_INTERBAL = 50;

            this.closeTabEvent = new U.Event();
            this.changeFocusEvent = new U.Event();
            this.messageEvent = new U.Event;
            this.renameContactEvent = new U.Event;
            this.removeContactEvent = new U.Event;
            this.typingEvent = new U.Event;
            this.minimizedEvent = new U.Event();
            this.authActionEvent = new U.Event();

            this._openedDialogs = [];
            this._dialogs = {};
            this._focused = false;
            this._modifDate = false;
            this._localData = {};
            this._timer = 0;
            this._hiddenTabsCount = 0;
        },

        _onRender: function (ct) {

            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-root__dialogs',
                children: [
                    {
                        tag: 'div',
                        id: 'nwaDialogs',
                        cls: 'nwa-root__dialogs-inner'
                    }
                ]
            });

            this.el = this.el.first();
            this.el.on('click', this._onClick, this);
            this.el.on('mousedown', this._onMouseDown, this);
            this.el.on('mouseup', this._onMouseUp, this);

            WA.get(window).on('resize',
                function () {
                    if (this._timer == 0) {
                        this.refreshTabBar(false, true);
                    }
                }, this);
        },

        show: function () {
            DialogTabs.superclass.show.apply(this, arguments);
        },

        hide: function () {
            DialogTabs.superclass.hide.apply(this, arguments);
        },

        _onMouseDown: function (ev) {
            var target = ev.getTarget();
            var mail = target.getAttribute('mail');
            if (mail && ev.browserEvent.which == 2) {
                this._wheelClick = mail;
                ev.preventDefault();
            }
        },

        _onMouseUp: function (ev) {
            var target = ev.getTarget();
            var mail = target.getAttribute('mail');
            if (ev.browserEvent.which == 2 && this._wheelClick && this._wheelClick == mail) {
                target.className = 'wa-tab-close';
                this._onClick(ev);
            }
            delete this._wheelClick;
        },

        _onClick: function (ev) {
            var target = ev.getTarget(true);

            var dialogRoot = this._findDialogRootNode(target);
            if (!dialogRoot) return;

            var mail = dialogRoot.getAttribute('mail');

            if (target.hasClass('nwa_action_close') || target.parent().hasClass('nwa_action_close')) {
                this.closeTabEvent.fire(mail);
                ev.stopEvent();
                return;
            }

            // resend undelivered message
            if (target.getAttribute('data-action') === 'resend') {
                var text = U.String.entityDecode(target.getAttribute('data-text'));
                this.messageEvent.fire(mail, text, true);
                return;
            }


            // reply on microblog post
            if (target.getAttribute('data-action') === 'reply_micblog') {
                var postId = target.getAttribute('data-post-id');
                var tmp = mail.split('@');
                var url = 'http://my.mail.ru/' + tmp[1].split('.')[0] + '/' + tmp[0] + '/microposts?postid=' + postId;
                window.open(url);
                return;
            }

            // auth actions
            if (target.up('nwa-message__actions')) {
                var action = target.getAttribute('data-action');
                if (action) {
                    var text = U.String.entityDecode(target.getAttribute('data-text'));
                    var msgId = target.getAttribute('data-msg-id');
                    var nick = this.nick || WA.SA.get(mail).nick || mail;

                    this.authActionEvent.fire({
                        mail: mail,
                        action: action,
                        nick: nick,
                        msg_id: msgId,
                        text: text
                    });
                }

                return;
            }

            // minimize by click on header
            if (target.up('nwa-chat__header')) {
                this.minimizedEvent.fire();
                return;
            }

            if (target.up('nwa-button-tab')) {
                this.changeFocusEvent.fire(mail);
            }
        },

        _findDialogRootNode: function (node) {
            return node.up('nwa-dialog');
        },

        _renderTabs: function (dialogs) {
            var dialogsEl = this.el, openDialogs = [];

            U.Array.each(dialogs, function (dialog, i) {
                openDialogs.push(dialog.mail);

                if (this._openedDialogs.length <= i) {
                    this._openedDialogs.push(dialog.mail);
                }

                // try to take from cache...
                var objDialog = this._dialogs[dialog.mail];

                if (!objDialog) {
                    // ... or create new one
                    objDialog = new DialogView({}, dialog);
                    objDialog.typingEvent.on(function () {this.typingEvent.fire(dialog.mail)}, this);
                    objDialog.messageEvent.on(function (text) {this.messageEvent.fire(dialog.mail, text)}, this);
                    objDialog.renameContactEvent.on(function(mail, nick){this.renameContactEvent.fire(mail, nick);}, this);
                    objDialog.removeContactEvent.on(function(mail){this.removeContactEvent.fire(mail);}, this);
                    objDialog.authActionEvent.on(function(params){this.authActionEvent.fire(params);}, this);
                    objDialog.render(dialogsEl);
                    this._dialogs[dialog.mail] = objDialog;
                }

                //update status
                objDialog.updateTabInfo(dialog);
            }, this);

            U.Array.each(this._openedDialogs, function (dialog, i) {
                if (U.Array.indexOf(openDialogs, dialog) == -1) {
                    this._destroyDialog(dialog);
                }
            }, this);

            this._openedDialogs = openDialogs;

        },

        _destroyDialog: function (mail) {
            if (this._dialogs[mail]) {
                this._dialogs[mail].deactivate();
                this._dialogs[mail].destroy();
                this._dialogs[mail] = null;
            }
            if (this._focused == mail) {
                this._focused = false;
            }
        },

        updateView: function (data) {
            if (this._modifDate != data.dialogsModifDate) {
                this._modifDate = data.dialogsModifDate;
                this._renderTabs(data.dialogs);
            }
            return this.setFocus(data);
        },

        refreshTabBar: function (forceRefresh, forceTimeout) {
            clearTimeout(this._timer);
            this._timer = 0;
            this.refreshTabsVisibility(forceRefresh);
            if (forceTimeout) {
                this._timer = WA.setTimeout(
                    WA.createDelegate( function() {this.refreshTabBar(forceRefresh);}, this),
                    this.TABS_UPDATE_INTERBAL
                );
            }
        },

        refreshTabsVisibility: function (forceRefresh) {
            var tabsCapacity =  this.calcAvailableSpace(this._focused);
            var tabsToHide = this._openedDialogs.length - tabsCapacity;

            if (tabsToHide != this._hiddenTabsCount || forceRefresh) {

                this._hiddenTabsCount = tabsToHide;
                U.Array.each(this._openedDialogs, function (dialog, i) {
                    if (tabsToHide > 0 && dialog != this._focused) {
//                        this._dialogs[dialog].hide();
                        this._dialogs[dialog].getEl().addClass('nwa-dialog_hidden');
                        tabsToHide--;
                    } else {
//                        this._dialogs[dialog].show();
                        this._dialogs[dialog].getEl().removeClass('nwa-dialog_hidden');
                    }
                }, this);
            }
        },

        calcAvailableSpace: function (hasFocused) {
            var actualWidth = this.el.getWidth();
            if (actualWidth > this.TABS_MAX_SUM_WIDTH) {
                return this.MAX_TABS_VISIBLE;
            }

            var tabsCapacity = 0;
            if (hasFocused) {
                tabsCapacity++;
                actualWidth -= this.TAB_ACTIVE_WIDTH;
            }

            U.Array.eachReverse(this._openedDialogs, function (dialog, i) {
                if (dialog == this._focused) return;
                var tabWidth = this.TAB_WIDTH;//this._dialogs[dialog].getEl().getWidth() + 7;
                if (tabWidth >= actualWidth) return false;
                tabsCapacity++;
                actualWidth -= tabWidth;
            }, this);

            return tabsCapacity;
        },

        calcAvailableSpace_old: function (hasFocused) {
            var actualWidth = this.el.getWidth(), diffWidth = 0;
            if (actualWidth > this.TABS_MAX_SUM_WIDTH) {
                return this.MAX_TABS_VISIBLE;
            }
            diffWidth = this.TAB_WIDTH * (this.MAX_TABS_VISIBLE - 1) + (hasFocused ? this.TAB_ACTIVE_WIDTH : this.TAB_WIDTH) - actualWidth;
            if (diffWidth <= 0) {
                return this.MAX_TABS_VISIBLE;
            }
            return this.MAX_TABS_VISIBLE - Math.ceil(diffWidth / this.TAB_WIDTH);
        },

        setFocus: function (data) {
            if (this._focused && this._focused != data.focused) {
                this._dialogs[this._focused].blur();
            }

            if (!data.focused) {
                this._focused = false;
                return false;
            }

            var index = this._indexOf(data.dialogs, data.focused);
            var dialog = data.dialogs[index];
            var focusedDialog = this._dialogs[data.focused];

            if (this._focused != data.focused) {
                this._focused = data.focused;
                focusedDialog.focus(dialog);
            }

            focusedDialog.updateWindow(dialog);

            this.refreshTabBar(true);

            return data.focused;
        },

        _indexOf: function (dialogs, mail) {
            return U.Array.indexOfBy(dialogs, function (dialog) {
                if (dialog.mail == mail) {
                    return true;
                }
            });
        },

        deactivate: function () {
            this._focused = false;
            this._modifDate = false;

            U.Array.each(this._openedDialogs, function (dialog, i) {
                this._destroyDialog(dialog);
            }, this);
            this._openedDialogs = [];
        }
    });

    // expose
    WA.DialogsAgentView = DialogTabs;

//    WA.DialogsAgentView._delimiter = WA.isIE8 && '<span class="nwa-wbr"></span>' || WA.isIE && '<wbr/>' || '&#8203;';

    WA.DialogsAgentView.prepareMessageText = function (text, noLinks) {
        var SMILES = WA.DialogsAgent.Smiles;
        var MAX_LENGTH = 38;

        var ret = text;

        var mark = new Date().getTime();
        var MARK_FORMAT = '{0}' + mark + '_{1} ';

        // link marks
        var links = [];
        ret = ret.replace(/((?:https?|ftp):\/\/([a-z0-9]+[\.-])+[^\s\n\t><"']+[^\s\n\t><"'\.,])/ig, function (ignored, href) {
            links.push(href);
            return U.String.format(MARK_FORMAT, 'L', links.length - 1);
        });

        var obj = SMILES.maskSmiles(ret);
        ret = obj.text;

        // escape html
        ret = U.String.htmlEntity(ret);

        // new lines & tabs
        ret = ret.replace(/(\n|\r\n)/g, '<br />').replace(/(\t)/g, '    ');

        // long words
// wa-321:
//        var delimiter = WA.DialogsAgentView._delimiter;
//        ret = ret.replace(new RegExp('\\S{' + MAX_LENGTH + ',}', 'g'), function (str) {
//            return str.match(new RegExp('(\\S{' + MAX_LENGTH + '}|\\S+)', 'g')).join(delimiter);
//        });

        // parse marks
        ret = ret.replace(new RegExp('(L|S)' + mark + '_(\\d+)', 'g'), function (str, key, index) {
            index = parseInt(index);
            if (WA.isNumber(index) && index >= 0) {
                if ('S' === key) {
                    return smiles.length > index ? smiles[index] : str;
                } else if ('L' === key) {
                    if (links.length > index) {
                        var linkName = links[index];
                        var href = links[index];
// wa-321: do not cut the links
//                        if (linkName.length >= MAX_LENGTH) {
//                            linkName = [
//                                linkName.substr(0, Math.round(MAX_LENGTH / 2)),
//                                '...',
//                                linkName.substr(linkName.length - 5)
//                            ].join('');
//                        }
                        return U.String.format('<a href="{0}" target="_blank">{1}</a>', href, linkName);
                    }
                }
            }
            return str;
        });

        // smiles
        obj.text = ret;
        ret = SMILES.processMessage(obj);

        return ret;

    };
    
})();(function () {

    var WA = WebAgent;
    var U = WA.util;
    var SOCK = WA.conn.Socket;

    WA.DialogsAgent = WA.Activatable.extend(Object, {

        constructor: function (config) {
            WA.DialogsAgent.superclass.constructor.call(this, config);

            this.countChangedEvent = new U.Event();
            this.unreadedEvent = new U.Event();
            this.minimizedEvent = new U.Event();
            this.incomingMessageEvent = new U.Event();
            this.closeTabEventEvent = new U.Event();

            this.visibleDialogChangedEvent = new U.Event();

            this.abstraction = new WA.DialogsAbstraction();
            this.incomingMessageEvent = new U.Event().relay(this.abstraction.incomingMessageEvent);
            this.updateSelfNickEvent = new U.Event().relay(this.abstraction.updateSelfNickEvent);

            this._view = new WA.DialogsAgentView();
            this._view.changeFocusEvent.on(this._onTabsFocusChange, this);
            this.changeFocusEvent = new U.Event().relay(this._view.changeFocusEvent);
            this._view.closeTabEvent.on(this._onTabClose, this);
            this._view.messageEvent.on(this._onMessageSubmit, this);
            this._view.typingEvent.on(this._onMessageTyping, this);
            this._view.minimizedEvent.on(function() {
                if (!WA.isPopup) {
                    this.abstraction.blur();
                    this.minimizedEvent.fire();
                }
            }, this);
            this._view.renameContactEvent.on(function (mail, newNick) {
                this.renameContactEvent.fire(mail, newNick);
                this.abstraction.renameContact(mail, newNick);
            }, this);
            this._view.removeContactEvent.on(function (mail) {
                this.abstraction.close(mail);
                this.removeContactEvent.fire(mail);
            }, this);
            this._view.authActionEvent.on(this._onAuthAction, this);

            this.removeContactEvent = new U.Event();
            this.renameContactEvent = new U.Event();

            WA.UserAttend.attendEvent.on(this._onUserAttend, this);

            this._titleAnimator = new WA.DialogsAgent.TitleAnimator();
        },

        _onMessageSubmit: function (mail, text, resend) {
            this.abstraction.postMessage(mail, text, !!resend)
        },

        _onTabsFocusChange: function (mail, isBottom) {
            this.abstraction.focus(mail);

            this.visibleDialogChangedEvent.fire(mail);
        },

        _onTabClose: function (email) {
            this.abstraction.close(email);
            this.closeTabEventEvent.fire(email);
        },

        _onAuthAction: function (params) {
            var mail = params.mail,
                action = params.action;

            if ('add' === action) {
                WA.MainAgent.sendAddContact(mail, params.nick, function (success) {
                    if (success) {
                        this.abstraction.makeAuthorized();
                    } else {
                        this.abstraction.close(mail);
                    }
                }, this);
            } else if ('cancel' === action) {
                this.abstraction.close(mail);
            } else if ('ignore' === action) {
                WA.conn.Socket.send('contact', {
                    operation: 'ignore',
                    email: mail
                });
                this.abstraction.close(mail);
                WA.ChangeLog.logAction('remove', { email: mail });
            } else if ('spam' === action) {
                if (params.text) {
                    // only send spam if there is a non-empty message
                    WA.conn.Socket.send('message', {
                        spam: 1,
                        to: mail,
                        message_id: params.msg_id,
                        text: params.text
                    });
                }
                this.abstraction.close(mail);
                this.removeContactEvent.fire(mail);
            }
        },

        _onMessageTyping: function (mail) {
            SOCK.send('message', {
                to: mail,
                composing: 1
            });
        },

        _onAbstractionUpdate: function (data) {
            this.countChangedEvent.fire(data.dialogs.length);

            var focused = this._view.updateView(data);

            if (!focused) {
                //return; //wtf?
            }

            if (WA.UserAttend.isAttend) {
                this.abstraction.markAsReaded();
            }

            var duty = this._dutyUnreaded = this._getDutyUnreaded(data);
            this.unreadedEvent.fire(duty.count);

            if (duty.count) {
                this._titleAnimator.start(duty.count, U.String.entityDecode(duty.nick));
            } else {
                this._titleAnimator.stop();
            }
        },

        _getDutyUnreaded: function (data) {
            var res = {
                count: 0,
                dialog: false
            };
            var date = 0;

            U.Array.each(data.dialogs, function (dlg) {
                if (dlg.unreaded) {
                    res.count++;
                    if(date < dlg.modifDate) {
                        date = dlg.modifDate;
                        res.dialog = dlg;
                    }
                }
            }, this);

            if(res.dialog) {
                var mail = res.dialog.mail;
                var c = res.dialog.cast[mail];
                WA.apply(res, {
                    mail: mail,
                    nick: c.nick,
                    status: c.status,
                    statusTitle: c.statusTitle
                });
            }
            return res;
        },

        _onUserAttend: function () {
            this.abstraction.markAsReaded();
        },
        
        markAsReaded: function (mail) {
            this.abstraction.markAsReaded(mail);
        },

        render: function (ct) {
            this._view.render(ct);
        },

        openDialog: function (mail, nick, status, statusTitle) {
            this.abstraction.open(mail, nick, status, statusTitle);
        },

        hasDialogs: function () {
            return this.abstraction.getDialogCount() > 0;
        },

        minimize: function () {
            // minimize all tabs
            this.abstraction.blur();
        },


//        _onShow: function () {
//            var focused = this.abstraction.getFocused();
//            if(focused && !focused.unreaded && this._dutyUnreaded && this._dutyUnreaded.dialog && focused.mail != this._dutyUnreaded.mail) {
//                var d = this._dutyUnreaded;
//                this.openDialog(d.mail, d.nick, d.status, d.statusTitle);
//            }
//        },

        activate: function (cb, scope) {
            return WA.DialogsAgent.superclass.activate.call(this, {
                cb: cb,
                scope: scope
            });
        },

        _onActivate: function (params) {
            this.abstraction.updateEvent.on(this._onAbstractionUpdate, this);
            this.abstraction.activate(params.cb, params.scope);
        },

        _onDeactivate: function () {
            this.abstraction.updateEvent.un(this._onAbstractionUpdate, this);
            this.abstraction.deactivate();
            this._view.deactivate();
            this._titleAnimator.stop();
        },

        actualize: function (cb, scope) {
            cb && cb.call(scope || window);
            // TODO
        },

        enableUserEvents: function () {
            // TODO
        },

        disableUserEvents: function () {
            // TODO
        },

        getVisibleDialog: function () {
            return this.abstraction.getFocused();
        }

    });

})();
(function () {

    var WA = WebAgent;

    WA.DialogsAgent.TitleAnimator = WA.extend(Object, {

        constructor: function () {
            this.originalTitle = document.title;

            this.updater = new WA.util.DelayedTask({
                interval: 500,
                fn: this._doUpdate,
                scope: this
            });

            this._reset();
        },

        _reset: function () {
            this.index = 0;

            this.unreadCount = 0;
            this.subTitle = '';
        },

        _getTitle: function () {
            var index = this.index;

            if (0 === index) {
                return WA.util.String.format('��������� ({0})', this.unreadCount);
            } else if (1 === index) {
                return this.subTitle;
            } else if (2 === index) {
                return '*************';
            } else {
                return this.originalTitle;
            }
        },

        _doUpdate: function () {
            document.title = this._getTitle();

            this.index = ++this.index >= 3 ? 0 : this.index;

            this.updater.start();
        },

        start: function (count, title) {
            if (!this.updater.isStarted()) {
                this.originalTitle = document.title;
            }

            this.unreadCount = count;
            this.subTitle = title;

            this.updater.start();
        },

        stop: function () {
            this.updater.stop();

            this._reset();

            document.title = this.originalTitle;
        }

    });

})();(function () {

    var ICQ = [
        {"code":"428","smile":"*PARDON*"},
        {"code":"314","smile":"*ROFL*"},
        {"code":"039","smile":"*GIVE_HEART*"},
        {"code":"430","smile":"*YAHOO*"},
        {"code":"015","smile":":-D"},
        {"code":"415","smile":"*IN LOVE*"},
        {"code":"402","smile":"*LOL*"},
        {"code":"401","smile":":-!"},
        {"code":"416","smile":"*JOKINGLY*"},
        {"code":"417","smile":"[:-}"},
        {"code":"408","smile":"]:->"},
        {"code":"414","smile":":-{}"},
        {"code":"427","smile":":-$"},
        {"code":"400","smile":"O:-)"},
        {"code":"426","smile":":-)"},
        {"code":"422","smile":":-("},
        {"code":"404","smile":":-["},
        {"code":"424","smile":"=-O"},
        {"code":"429","smile":";-)"},
        {"code":"409","smile":"8-)"},
        {"code":"412","smile":"@}->--"},
        {"code":"403","smile":":-P"},
        {"code":"413","smile":"*THUMBS UP*"},
        {"code":"405","smile":":'-("},
        {"code":"425","smile":":-@"},
        {"code":"410","smile":"*DRINK*"},
        {"code":"415","smile":"*KISSED*"},
        {"code":"419","smile":"%)"},
        {"code":"418","smile":"*NO*"},
        {"code":"406","smile":"*CRAZY*"},
        {"code":"407","smile":"*DANCE*"}
    ];


    // ---------------------------------------------------------------------------------

    var WA = WebAgent;
    var U = WA.util;
    var MENU = WA.ui.menu;

    var Smiles = WA.DialogsAgent.Smiles = WA.extend(WA.ui.Layer, {

        constructor: function (config) {
            var cfg = WA.applyIf({
                cls: 'nwa-dialog__smiles-wrap',
                hideOnClick: true,
                fadeIn: true
            }, config || {});

            this.controlEl = cfg.controlEl || [];
            if (this.controlEl && this.controlEl.length == null) {
                this.controlEl = [this.controlEl];
            }

            this._tabs = new Tabs();
            var len = WebAgent.data.SmileSets.length;
            for (var i = 0; i < len; i++) {
                this._tabs.addTab({cls: 'nwa-smile_tab' + i});
            }
            this._tabs.tabChangeEvent.on(this._onTabChangeEvent, this);

            Smiles.superclass.constructor.call(this, cfg);

            this.itemClickEvent = new WA.util.Event();
        },

        _onAncestorClick: function (el, isSelf, e) {
            if (!isSelf) {
                var c = el;
                var index = null;
                while (this.activeTab && c && !c.equals(this.activeTab.el)) {
                    index = parseInt(c.getAttribute('menu_index'));
                    if (WA.isNumber(index)) {
                        this.itemClickEvent.fire(this, this.activeTab.items[index], e);
                        break;
                    } else {
                        c = c.parent();
                    }
                }
            }
        },

        // set control button element
        setControlElement: function (el) {
            this.controlEl = (el && el.length == null && [el] || el);
        },

        _onClick: function (e) {
            var target = e.getTarget(true);
            // prevent closing menu by mousedown on control button
            for (var i = 0, len = this.controlEl.length; i < len; i++) {
                if (this.controlEl[i].contains(target)) {
                    return false;
                }
            }

            if (this._tabs.el.contains(target)) {
                return true;
            }

            Smiles.superclass._onClick.call(this, e);
        },

        _onAfterRender: function (el) {
            initSmileData();
            loadSmileCssPackage();
            this._tabs.render(this.el);
            this._tabs.setFocus(0);
            this._onTabChangeEvent(0);
        },

        _onTabChangeEvent: function (index) {
            if (this.activeTab) {
                this.activeTab.hide();
                this.activeTab.destroy();
            }

            var setId = WA.data.SmileSets[index], data = [], set, obj;
            if (setId) {
                set = WA.data.SmileData[setId];
                for (var k = 0, len = set.length; k < len; k++) {
                    obj = set[k];
                    data.push({
                        title: obj.tip,
                        alt: getVisualAlt(obj),
                        cls: 'nwa-' + obj.name,
                        html: "<SMILE>id=" + obj.id + " alt='" + obj.alt + "'</SMILE>"
                    });
                }
            }
            this.activeTab = new SmilesTab({items: data, cls: 'nwa-smile_set'+index});
            this.activeTab.render(this.el);
            this.activeTab.show();
        },

        destroy: function () {
            this.itemClickEvent.removeAll();
            this.itemClickEvent = null;

            Smiles.superclass.destroy.call(this);
        }
    });

    var SmilesTab = WA.extend(MENU.Menu, {

        defaultType: MENU.Image,

        constructor: function (config) {
            var cfg = WA.applyIf({
                hideOnClick: false,
                fadeIn: false,
                autoHide: false
            }, config || {});
            if (cfg.cls) cfg.cls += ' nwa-dialog__smiles-menu';

            SmilesTab.superclass.constructor.call(this, cfg);
        }
    });

    var Tabs = WA.extend(WA.ui.Component, {
        constructor: function () {
            Tabs.superclass.constructor.apply(this, arguments);
            this.tabChangeEvent = new U.Event();
            this._list = [];
            this._tabs = [];
            this._index = -1;
        },

        _onRender: function (el) {
            this.el = el.createChild({
                cls: 'nwa-dialog__smiles-tabs'
            });
            this.el.on('click', this._onClickEvent, this);
            this._renderTabs();
        },

        _onClickEvent: function (el) {
            var trg = el.getTarget(true);
            trg = trg.hasClass('nwa-dialog__smiles-tab') && trg || trg.parent();
            var index = trg.getAttribute('data-tab-index');
            if (index != null) {
                if(this.setFocus(index)) {
                    this.tabChangeEvent.fire(index);
                }
            }
        },

        _renderTabs: function () {
            var el;
            U.Array.each(this._list, function (tab, i) {
                el = this.el.createChild({
                    tag: 'div',
                    cls: 'nwa-dialog__smiles-tab ' + (tab.cls || ''),
                    'data-tab-index': i,
                    html: '<span></span>'
                });
                this._tabs.push(el);
            }, this);
        },

        addTab: function (cfg) {
            this._list.push(cfg);
        },

        setFocus: function (index) {
            if (this._index != index) {
                if (this._index != -1) {
                    this._tabs[this._index].removeClass('nwa-dialog__smiles-tab_active');
                }
                this._tabs[index].addClass('nwa-dialog__smiles-tab_active');
                this._index = index;

                return true;
            }
        }

    });

    var strip = (function () {

        var chars = '(){}[]|+:*$';
        var tmp = [];
        var i = 0;
        while (i < chars.length) {
            tmp.push('\\' + chars.charAt(i));
            i++;
        }

        var re = new RegExp('(' + tmp.join('|') + ')', 'g');

        return function (text) {
            return text.replace(re, '\\$1');
        };

    })();

    var allItems, smileCodes = [], smileCodeRE, smileIndexes = [], agentToIcqRE, maskSmilesRE;
    var SMILE_ICQ_RE;
    var agentToIcqHash = {}, icqSmilesHash = {};

    function initSmileData () {
        if (allItems) return;

        allItems = {};
        for (var i = 0, len = WA.data.SmileSets.length; i < len; i++) {
            var setData = WA.data.SmileData[WA.data.SmileSets[i]];
            for (var k = 0, len2 = setData.length; k < len2; k++) {
                var obj = setData[k];
                obj.ind = i;
                allItems[obj.id] = obj;
            }

            smileCodes = smileCodes.concat(U.Array.transform(setData, function (entry) {
                return strip(getVisualAlt(entry));
            }));
            smileIndexes = smileIndexes.concat(U.Array.transform(setData, function (entry) {
                return strip(entry.id);
            }));
        }

        smileCodeRE = new RegExp('(' + smileCodes.join('|') + ')', 'g');

        var agentToIcqList = [];
        var icqSmilesList = [];
        U.Array.each(ICQ, function (t) {
            icqSmilesList.push(strip(t.smile));
            icqSmilesHash[t.smile] = t.code;
            var a = allItems[t.code];
            var textSmile = ':' + a.tip + ':';
            agentToIcqList.push(strip(textSmile));
            agentToIcqHash[textSmile] = t.smile;
        });

        agentToIcqRE = new RegExp('('+ agentToIcqList.join('|')+ ')', 'g');
        SMILE_ICQ_RE = Smiles.SMILE_ICQ_RE = '(' + icqSmilesList.join('|') + ')';
        maskSmilesRE = new RegExp('(' + [SMILE_STR_RE1, SMILE_STR_RE2, SMILE_ICQ_RE].join('|') + ')', 'g');
    }

    function getSmileById (id) {
        initSmileData();
        return allItems[id];
    }

    function getSmileCodes () {
        initSmileData();
        return smileCodes;
    }

    function getSmileCodeRE () {
        initSmileData();
        return smileCodeRE;
    }

    function getSmileByIndex (index) {
        var id = smileIndexes[index];
        return getSmileById(id);
    }

    function getVisualAlt (obj) {
        if (U.Array.indexOf([422,424,426,429], parseInt(obj.id)) > -1) {
            return obj.alt;
        }
        if (obj.alt.indexOf('###') == -1) return obj.alt.substr(0, obj.alt.length - 1) + obj.ind + ':';
        if (obj.id > 9 && obj.id < 32 || obj.id > 33 && obj.id < 39 || obj.id == 40) {
            return obj.tip
        }
        return ':' + obj.tip + ':';
    }

    function buildSmilePackage (ignored, code) {
        var codes = getSmileCodes();
        var i = U.Array.indexOf(codes, strip(code));
        var smile = getSmileByIndex(i);
        if (!smile) return '';
        if (smile.alt.indexOf('###') > -1) return smile.alt;
        return "<SMILE>id="+smile.id+" alt='"+smile.alt+"'</SMILE>";
    }

    Smiles.processTextIcq = (function () {
        var fn = function (nope, smile) {
            return agentToIcqHash[smile];
        };
        var fnS = function (code, smile) {
            return (code.match(getSmileCodeRE()) ? ':' + smile + ':' : code);
        };
        return function (text) {
            initSmileData();
            text = text.replace(/:([^:]{1,40}?)\d:/g, fnS); //remove set number
            text = text.replace(agentToIcqRE, fn);
            return text;
        }
    })();

    Smiles.processText = function (text) {
        return text.replace(getSmileCodeRE(), buildSmilePackage);
    };

    var SMILE_STR_RE1 = Smiles.SMILE_STR_RE1 = "<###\\d+###img(\\d+)>";
    var SMILE_STR_RE2 = Smiles.SMILE_STR_RE2 = "<SMILE>id=(\\d+) alt='([^']+)'</SMILE>";

    var mark = new Date().getTime();
    var MARK_FORMAT = '' + mark + '_{0}_ ';
    var SMILE_MARK_RE = Smiles.SMILE_MARK_RE = '' + mark + '_(\\d+)_';
    var SMILE_FORMAT = '<div class="nwa-smile_inline nwa-{0}" title="{1}" style="min-width: 20px; min-height: 20px;"></div>';

    Smiles.maskSmiles = (function () {
        var list = [];
        var fn = function (nope, nope, old, code, alt, icq) {
            alt = alt || '';
            if(icq) {
                code = icqSmilesHash[icq];
                if(!code) {
                    code = 999;
                    alt = icq;
                }
            } else if(old) {
                code = old;
            }

            list.push({code: code, alt: alt});
            return U.String.format(MARK_FORMAT, list.length-1);
        };
        return function (text) {
            initSmileData();
            list = [];
            return {
                text: text.replace(maskSmilesRE, fn),
                list: list
            };
        }
    })();
    
    Smiles.processMessage = (function () {
        var re = new RegExp('(' + [SMILE_STR_RE1, SMILE_STR_RE2, SMILE_MARK_RE].join('|') + ')', 'g');
        var list = [];
        var fn = function (nope, nope, old, code, alt, index) {
            loadSmileCssPackage();
            alt = alt || '';
            if(list[index]) {
                code = list[index].code;
                alt = list[index].alt;
            } else {
                code = code || old;
            }
            var smile = getSmileById(code);
            var retval = '';
            if(smile) {
                retval = U.String.format(SMILE_FORMAT, smile.name, smile.tip);
            } else if(alt) {
                retval = U.String.htmlEntity(alt);
            }
            return retval;
        };
        return function (text) {
            list = [];
            if(text.list) {
                list = text.list;
                text = text.text;
            }
            return text.replace(re, fn);
        }
    })();

    var smilesLoaded = false;
    function loadSmileCssPackage () {
        if (smilesLoaded) return;
        smilesLoaded = true;
        var head = document.getElementsByTagName('head')[0];
        if (!head) return;
        var el = head.getElementsByTagName('*')[0];
        if (!el) return;
        var style = document.createElement('link');
        style.type = 'text/css';
        style.rel = 'stylesheet';
        style.href = WA.resDomain + WA.resPath + '/smiles.css';
        head.insertBefore(style, el);
    }

    //
    // inline test
    return;
    if (WA.isDebug) {

        var assertEquals = function (testId, expected, actual) {
            if (expected !== actual) {
                alert([
                    '[Smiles.js] ' + testId + ' test failed',
                    'Expected:\n' + expected,
                    'Actual:\n' + actual
                ].join('\n\n'));
            }
        };

        //
        // processText
        (function () {

            var expected = U.Array.transform(DATA, function (entry) {
                return entry.html;
            }).join(' ');

            var actual = U.Array.transform(DATA, function (entry) {
                return ':' + entry.title + ':';
            }).join(' ');

            actual = Smiles.processText(actual);

            assertEquals('processText', expected, actual);

        })();

        //
        // processMessage
        (function () {

            var mySmiles = [DATA[0], DATA[35], DATA[39]];

            var expected = U.Array.transform(mySmiles, function (entry) {
                return U.String.format(IMAGE_FORMAT, entry.path, entry.title);
            }).join(' ');

            var actual = U.Array.transform(mySmiles, function (entry) {
                return entry.html;
            }).join(' ');

            actual = U.String.htmlEntity(actual);
            actual = Smiles.processMessage(actual);

            assertEquals('processMessage', expected, actual);

        })();

    }

})();(function () {

    var WA = WebAgent;
    var U = WA.util;

    var Dlg = WA.DialogsAgent.RemoveContactDialog = WA.extend(WA.ui.Window, {

        closeAction: 'hide',

        constructor: function (config) {
            config = config || {};
            Dlg.superclass.constructor.call(this, {
                cls: 'nwa-dlg-remove',
                fadeIn: true,
                title: '',
                autoRender: config.autoRender || false,
                modal: true,
                bbar: [
                    '->',
                    {
                        cls: 'nwa-button_blue',
                        text: config.okButton || 'OK',
                        handler: function () {
                            this._fireOkEvent();
                        },
                        scope: this
                    },
                    {
                        cls: 'nwa-button_blue',
                        text: '��������',
                        handler: function () {
                            this.close();
                        },
                        scope: this
                    }
                ]
            });

            this.okEvent = new WA.util.Event();
        },

        _onRender: function (ct) {
            Dlg.superclass._onRender.call(this, ct);

            this.body.createChild({
                tag: 'span'
            });
        },

        show: function (msg) {
            Dlg.superclass.show.call(this);
            this.body.first().update(msg);
        },

        _fireOkEvent: function () {
            this.okEvent.fire();
            this.close();
        }
    });

})();(function () {

    var WA = WebAgent;
    var U = WA.util;

    var Dlg = WA.DialogsAgent.AuthContactDialog = WA.extend(WA.ui.Window, {

        closeAction: '_handleClose',

        buttonsConfig: [
            {
                cls: 'nwa-button_blue nwa-dlg-auth__add-contact',
                action: 'add',
                text: '<span class="nwa-dlg-auth__add-contact-label">������������ � ��������</span>'
            },
            {
                cls: 'nwa-button_blue',
                action: 'cancel',
                text: '��������'
            },
            {
                cls: 'nwa-button_link',
                action: 'ignore',
                text: '������������'
            },
            {
                cls: 'nwa-button_link',
                action: 'spam',
                text: '����'
            }
        ],

        constructor: function (config) {
            config = config || {};

            var items = [];

            U.Array.each(this.buttonsConfig, function (button) {
                items.push(new WA.ui.Button({
                    cls: button.cls,
                    handler: function () {
                        this._fireOkEvent(button.action);
                    },
                    scope: this,
                    text: button.text
                }));
            }, this);

            Dlg.superclass.constructor.call(this, {
                cls: 'nwa-dlg-auth',
                fadeIn: true,
                title: '',
                autoRender: config.autoRender || false,
                modal: true,
                items: items
            });

            this.okEvent = new WA.util.Event();
        },

        _onRender: function (ct) {
            Dlg.superclass._onRender.call(this, ct);

            this.body.createChild([{
                    tag: 'div',
                    cls: 'nwa-dlg-auth__title'
                }, {
                    tag: 'div',
                    cls: 'nwa-dlg-auth__avatar'
                }
            ])
        },

        _onShow: function () {
            Dlg.superclass._onShow.call(this);

            this.body.first().update('<span class="nwa-special-nick">' + U.String.htmlEntity(this._meta.nick) + '</span> �������� �����������');
            var previewUrl = WA.makeAvatar(this._meta.mail, '_avatar32');
            this.body.first().next().setStyle('background-image', 'url('+previewUrl+')');
        },

        _fireOkEvent: function (action) {
            this.okEvent.fire(action);
            this.hide();
        },

        _handleClose: function () {
            this._fireOkEvent('cancel');
        },

        setMeta: function (meta) {
            this._meta = meta;
        }
    });

})();(function () {

    var WA = WebAgent;
    var U = WA.util;
    var ConnectionRoster = WA.conn.ConnectionRoster;

    var zodiac = [
        ['ololo', '�����������'],
        ['aries', '����'],
        ['taurus', '�����'],
        ['gemini', '��������'],
        ['cancer', '���'],
        ['leo', '���'],
        ['virgo', '����'],
        ['libra', '����'],
        ['scorpio', '��������'],
        ['sagittarius', '�������'],
        ['capricorn', '�������'],
        ['aquarius', '�������'],
        ['pisces', '����']
    ];

    var months = ['���������', '������', '�������', '�����', '������', '���', '����', '����', '�������', '��������', '�������', '������', '�������'];

    

    var Abstraction = WA.extend(ConnectionRoster, {

        constructor: function () {
            Abstraction.superclass.constructor.apply(this);
            this.updateEvent = new U.Event();
        },

        get: function (mail) {
            if(this._mail != mail) {
                this._mail = mail;
                WA.conn.Socket.send('wp', {
                    email: this._mail,
                    uniq: this._uniq = Math.round(Math.random()*999999)
                });
            }
        },

        _onConnectionWp: function (value) {
            if(value.uniq && value.uniq == this._uniq) {
                if(value.result) {
                    var index = U.Array.indexOfBy(value.result, function (v) {
                        if(v.email == this._mail) {
                            return true;
                        }
                    }, this);
                    if(index != -1) {
                        this.updateEvent.fire(value.result[index]);
                    }
                } else if(value.error) {
                    //todo
                }
            }
        }

    });

    WA.Profile = WA.extend(WA.ui.Component, {

        constructor: function (isAddEnabled, config) {
            WA.Profile.superclass.constructor.call(this, config);
            this.initialConfig.hideOnEscape = true;
            this._isAddEnabled = isAddEnabled;
            
            this._abstraction = new Abstraction();
            this._abstraction.updateEvent.on(this._onUpdate, this);
            this.addEvent = new U.Event();
        },

        show: function (mail, data, isSuggest) {
            this._isSuggest = isSuggest;
            if(this._mail != mail) {
                this._mail = mail;
                this._mailParts = this._mail.match(/([^@]+)@([^\.]+)/);
                this._reset();
                this._data = data;
            }
            WA.Profile.superclass.show.apply(this);
        },

        _onShow: function () {
            WA.Profile.superclass._onShow.apply(this, arguments);
            if(!this._done) {
                if(!this._data) {
                    this._abstraction.get(this._mail);
                } else {
                    this._onUpdate(this._data);
                }
                
                if (!this._userPicLoaded) {
                    this._userPicLoaded = true;
                    this._updateImage();
                }
            }
        },

        _onRender: function (el) {
            this.el = el.createChild({
                cls: 'nwa-wp',
                children: [
                    {
                    },
                    {
                        cls: 'nwa-wp__plate',
                        children: [
                            {
                                cls: 'nwa-wp__white',
                                children: [
                                    {
                                        cls: 'nwa-wp__userpic'
                                    },
                                    {
                                        cls: 'nwa-wp__info'
                                    },
                                    {
                                        cls: 'nwa-wp__line'
                                    },
                                    {
                                        cls: 'nwa-wp__more'
                                    }
                                ]
                            },
                            {
                                tag: 'button',
                                cls: 'nwa-button nwa-button_blue nwa-wp__add-button',
                                children: [
                                    {
                                        tag: 'span',
                                        cls: 'nwa-toolbar__add-contact-label',
                                        children: '�������� � ������ ���������'
                                    }
                                ]
                            },
                            {
                                tag: 'button',
                                cls: 'nwa-button nwa-button_blue nwa-wp__cancel-button',
                                html: '�������'
                            }
                        ]
                    }
                ]
            });

            this.el.first().on('click', this._onCloseButton, this);
            var el = this.el.first().next().first();
            this._userpicEl = el.first();
            this._infoEl = this._userpicEl.next();
            this._moreEl = this._infoEl.next().next();
            this._addButtonEl = el.next().on('click', this._onAddButton, this);
            if(this._isAddEnabled) {
                this._addButtonEl.show();
            }
            el.next().next().on('click', this._onCloseButton, this);

            this._reset();
        },

        _reset: function () {
            this._clearImage();
            this._infoEl.update('<span>��������...</span>');
            this._moreEl.update('<span>��������...</span>');
            this._userPicLoaded = false;
            this._done = false;
        },

        _onAddButton: function () {
            if(this.done) {
                this.addEvent.fire(this._mail, this._name, this._isSuggest);
            }
        },

        _onCloseButton: function () {
            this.hide();
        },

        _clearImage: function () {
            this._userpicEl.setStyle({'background-image':'none'});
        },

        _updateImage: function () {
            this._userpicEl.setStyle({'background-image':'url('+WA.makeAvatar(this._mail, '_avatar')+')'});
        },

        _onUpdate: function (data) {

            var name = [];
            data.firstname && (name.push(data.firstname));
            data.lastname && (name.push(data.lastname));
            name = name.length && name.join(' ') || data.nickname || this._mail;
            this._name = name;
            var isUIN = this._mail.indexOf('@uin.icq') != -1;

            if(data.birthday) {
                var now = new Date();
                var shortYear = now.getFullYear() - 2000;
                var date = data.birthday.split('-');
                if(date[0].length == 2 && date[0] > shortYear ) {
                    date[0] = '19' + date[0];
                } else if(date[0].length == 2 && date[0] <= shortYear ) {
                    date[0] = '20' + date[0];
                }

                var birthday = new Date(date[0], date[1] - 1, date[2]);
                var age = now.getFullYear() - birthday.getFullYear();
                var bday = new Date('' + now.getFullYear() + data.birthday.replace(/^\d+/, ''));
                if(bday > now) {
                    age--;
                }

                var bdayStr = date[2] + ' ' + months[+date[1]] + ' ' + date[0];
            }

            var status = isUIN ? 'icq_online' : 'online';
            var statusTitle = '������';

            var sa = '';
            if(data.sex || age) {
                sa = U.String.format(
                    '<br><span>{0}{2}{1}</span><br>',
                    (['', '�������', '�������'])[data.sex || 0],
                    age ? (age + WebAgent.util.String.pluralize(age, ' ���', ' ���', ' ����')) : '',
                    (data.sex && age ? ' - ' : '')
                )
            }

            var format = '<b>{0}</b><br/>{1}<div class="nwa-wp__status {2}">{3}</div>' +
                (isUIN ? '' : '<a href="http://my.mail.ru/{4}/{5}/" target="_blank">��� ���</a> <a href="http://foto.mail.ru/{4}/{5}/" target="_blank">����</a> <a href="http://maps.mail.ru/?units&origin=webagent" target="_blank">�����</a>');

            this._infoEl.update(U.String.format(
                format,
                U.String.htmlEntity(name),
                sa,
                WA.buildIconClass(status, true),
                statusTitle,
                this._mailParts[2],
                this._mailParts[1]
            ));

            var more = [], horoUrl;
            if(data.birthday) {
                horoUrl = 'http://horo.mail.ru/prediction/' + zodiac[data.zodiac][0] + '/today/';
                more.push(U.String.format(
                    '<span>���� ��������:</span><br>{0}{1}',
                    bdayStr || '',
                    data.zodiac ? ' / <a class="nwa-wp__zodiac nwa-wp__zodiac_' + data.zodiac + '" href="' + horoUrl + '" target="_blank">' + zodiac[data.zodiac][1] + '</a>' : ''
                ));
            }

            var title = '����������� �����:';
            var mail = this._mail;
            if (isUIN) {
                title = 'UIN:';
                mail = mail.replace('@uin.icq', '');
            }
            format = '<span>{1}</span><br/>' + (isUIN ? '{0}' : '<a href="mailto:{0}">{0}</a>');
            more.push(U.String.format(
                format,
                mail,
                title
            ));

            if(data.location) {
                more.push(U.String.format(
                    '<span>����� ����������:</span><br><div class="nwa-wp__location" title="{0}">{0}<div class="nwa-wp__location-fader"></div></div>',
                    U.String.htmlEntity(data.location)
                ))
            }

            this._moreEl.update(more.join('<br><br>'));


            this.done = true;
        }

    });

})();
(function () {
    var WA = WebAgent;

    WA.PhotoPreview = WA.extend(WA.ui.Component, {

        initComponent: function () {
            WA.PhotoPreview.superclass.initComponent.apply(this);
            this.initialConfig.hideOnEscape = true;
            this.mail = this.initialConfig.mail || null;
        },

        _onRender: function (ct) {
            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-ppreview' + (this.initialConfig.fadeIn && !WA.isIE ? ' nwa_fadein' : ''),
                children: [{
                    tag: 'div',
                    cls: 'nwa-ppreview__img ',
                    children: {
                        tag: 'div',
                        cls: 'nwa-ppreview__close'
                    }
                }]
            });
            this.el.on('click', this.hide, this);

            if (this.mail) {
                this._renderPhoto();
            }
        },

        show: function (mail) {
            WA.PhotoPreview.superclass.show.apply(this);
            if (mail && this.mail != mail) {
                this.mail = mail;
                this._renderPhoto();
            }
        },

        _onHide: function (me) {
            WA.PhotoPreview.superclass._onHide.apply(this);
        },

        _renderPhoto: function() {
            var previewUrl = WA.makeAvatar(this.mail, '_avatar180');
            this.el.last().setStyle('background-image', 'url('+previewUrl+')');
        }
    });
})();
(function () {

    var WA = WebAgent;
    var U = WA.util;

    var Dlg = WA.DialogsAgent.AlertDialog = WA.extend(WA.ui.Window, {

        closeAction: 'hide',

        constructor: function (config) {
            config = config || {};
            Dlg.superclass.constructor.call(this, {
                cls: 'nwa-dlg-remove',
                fadeIn: true,
                title: '',
                autoRender: config.autoRender || false,
                modal: true,
                bbar: [
                    '->',
//                    {
//                        cls: 'nwa-button_blue',
//                        text: '��������� ������',
//                        handler: this.close,
//                        scope: this
//                    },
                    {
                        cls: 'nwa-button_blue',
                        text: '��',
                        handler: this.close,
                        scope: this
                    }
                ]
            });

        },

        _onRender: function (ct) {
            Dlg.superclass._onRender.call(this, ct);

            this.body.createChild({
                tag: 'span'
            });
        },

        _onShow: function () {
            Dlg.superclass._onShow.call(this);

            this.body.first().update('������� ������� ���������');
        }
    });

})();(function () {

    var WA = WebAgent;
    var U = WA.util;
    WA.buttons = {};

    // ---------------------------------------------------------------------------------

    var SB = WA.buttons.StatusButton = WA.extend(WA.ui.TabButton, {

        constructor: function (config) {
            config.clickEvent = 'mousedown';
            SB.superclass.constructor.call(this, config);
        },


        setStatus: function (status) {
            if (this._status != status) {
                this.el.first().removeClass(WA.buildIconClass(this._status)).addClass(WA.buildIconClass(status));
                this._status = status;
                this.setInfo('');
                this.el.toggleClass('nwa-button-tab_disabled', status == 'offline' || status == 'gray' || status == 'yellow');
            }
        },

        getStatus: function () {
            return (this._status || null);
//            var matches = this.el.first().getClass().match(/wa-cl-status-([^\s]+)/);
//            return matches ? matches[1] : null;
        },
        
        setInfo: function (text) {
            this.el.setAttribute('title', text);
        },
        
        setPopupMode: function (mode) {
            if (mode) {
                this.setStatus('popup');
            } else {
                this.setStatus(this._status);
            }
        },

        getPopupMode: function () {
            return this._status.indexOf('popup') == 0;
        },

        setCount: function (count) {
            if (count && this._count != count) {
                this._count = count;
                //this.setText('Mail.Ru ����� <span class="nwa-button-tab__count">' + count + '</span>');
            }
        }

    });

})();(function () {

    var WA = WebAgent;
    var Storage = WA.Storage;
    var U = WA.util;
    var US = WA.conn.UserState;
    var SOCK = WA.conn.Socket;
    var FM = WA.FocusManager;
    var StatusButton = WA.buttons.StatusButton;
    var MENU = WA.ui.menu;
//                            'http://webagent.mail.ru/r/webagent/build/popup.html?' + WA.BUILD_NUMBER,
//    var POPUP_URL =  'http://merge-layout-redesign-master.my.rapira11.mail.ru/ru/images/webagent/popup.html?';
    var POPUP_URL = (WA.isLocalhost ? '/popup.html?' : 'http://webagent.mail.ru/r/webagent/popup.html') + (WA.usedBranch != 'master' ? '?branch=' + WA.usedBranch: '');

    var StatusMenu = WA.extend(MENU.Menu, {
        
        render: function (ct) {
            var container = WA.get(ct);
            var el = container.createChild({
                tag: 'div',
                cls: 'nwa-status-menu-root'
            });
            StatusMenu.superclass.render.call(this, el);
        }

    });

    var StatusMenuItem = WA.extend(MENU.Button, {

        constructor: function (config) {
            config.owner = this;
            config.iconCls = WA.buildIconClass(config.status);
            config.text = U.String.htmlEntity(config.text);
            StatusMenuItem.superclass.constructor.call(this, config);
        },

        _onAfterRender: function () {
            this.setText(this._text);
            return StatusMenuItem.superclass._onAfterRender.apply(this, arguments);
        },

        setText: function (text) {
            this.button.setText(U.String.htmlEntity(text));
        },

        getText: function () {
            return U.String.entityDecode(this.button.getText());
        },

        setStatus: function (status) {
            this.initialConfig.status = status;
            this.button.setIconCls(WA.buildIconClass(status));
        },

        getStatus: function () {
            return this.initialConfig.status;
        }

    });


    var StatusEssence = WA.extend(WA.ui.Component, {

        constructor: function () {
            StatusEssence.superclass.constructor.apply(this, arguments);
            if (US.isValidUser()) {
                this._active = undefined;
                this._checkDocumentReady();
            }

            US.permanentShutdownEvent.on(this._onPermanentShutdown, this);
            US.networkErrorEvent.on(this._errorMode, this);
        },

        _checkDocumentReady: function () {
            if (document.body) {
                this._documentReady();
            } else {
                WA.setTimeout(WA.createDelegate(this._documentReady, this), 10);
            }
        },

        _documentReady: function () {
            this._onDocumentReady();
            // dummy comment
            Storage.init();
            Storage.whenReady(this._onStorageInit, this);
        },

        _onStorageInit: function () {
            WA.debug.Console.log('_onStorageInit');
            this.syncStatus = WA.Synchronizer.registerComponent('status', ['status', 'custom'], false);

            this.syncStatus.read(function (data) {
                var status = data.get('status');
                WA.debug.Console.log('STATUS: read, ', status);
                var s = status.split(':');
                this._statusModifDate = s[0];

                WA.Synchronizer.startUp();
                WA.debug.Console.log('Synchronizer.startUp');
            }, this);

            this.syncStatus.triggerEvent.on(function (data) {
                if (data.isChanged('custom')) {
                    this.__statusTitle = data.get('custom');
                }
                if (data.isChanged('status')) {
                    var status = data.get('status');
                    var s = status.split(':');
                    var customTitle = this.__statusTitle;
                    if (this._statusModifDate != s[0]) {
                        this._statusModifDate = s[0];
                        this._onSyncStatus(s[1], customTitle);
                        WA.debug.Console.log('STATUS: sync, ', status);
                    } else {
                        WA.debug.Console.log('STATUS: sync again, ', status);
                    }
                    if (s[1] != 'offline') {
                        U.Mnt.rbCountOpen(); //RB
                    }
                }
            }, this);

            SOCK.requestReadyEvent.on(this._onSocketRequestable, this);
            US.init();
            US.statusChangedEvent.on(this._onUserStateStatusChange, this);
            US.getState(this._setInitialState, this);
        },

        _setInitialState: function (state, param) {
            WA.debug.Console.log('getState ', state);
            if (!state) {
                FM.activate();
            } else if (state == 'error') {
                this._errorMode(param);
            } else if (state == 'online') {
                this._activate(function () {
                    this.syncStatus.read(function (data) {
                        var s = data.get('status').split(':');
                        var customTitle = data.get('custom');
                        this._statusModifDate = s[0];
                        this._onStatusChange(s[1], customTitle);
                        this._listenFM();
                    }, this);
                }, this);
            } else if (state == 'disabled') {
                return;
            } else {
                this._deactivate();
                this._onStatusChange('offline');
                this._listenFM();
            }
        },

        _errorMode: function (attemptDelay) {
            this._onErrorMode();
            if (this._active || this._active === undefined) {
                this._active = false;
                SOCK.discardEvents();
                WA.UserAttend.deactivate();
            }

            this._onStatusChange('yellow');
            this.statusButton.setInfo(U.String.format(WA.TEXT.ERR_INFO_NETWORK, '' + attemptDelay + ' ���'));
            this._requestableCb = WA.createDelegate(this._activate, this);

            FM.activate();
        },

        _onPermanentShutdown: function (event) {
            this._disableUserEvents();
            WA.Synchronizer.stop();
            this._deactivate();

            if (event.isShowInfo) {
                this.statusButton.setStatus('gray');
                this.statusButton.setInfo(event.info);
                this.statusMenu.beforeShowEvent.on(function () {
                    return false;
                });
            } else {
                this.statusButton.hide();
            }
            this.isPermanentShutdown = true;
        },

        _listenFM: function () {
            if (!this._listenFMFlag) {
                this._listenFMFlag = true;
                WA.debug.Console.log('_listenFM');
                FM.focusEvent.on(this._onFocus, this);
                FM.blurEvent.on(this._onBlur, this);
                FM.ifFocused(this._onFocus, null, this);
            }
        },

        _storeStatus: function (status, statusTitle, cb, scope) {
            if (statusTitle) {
                this.syncStatus.write('custom', statusTitle);
            }
            this.syncStatus.write('status', '' + (+new Date()) + ':' + status, cb, scope);
        },

        _onSocketRequestable: function () {
            WA.debug.Console.log('socket requestable');
            if (this._requestableCb) {
                this._requestableCb();
                delete this._requestableCb;
            }
        },

        _onUserStateStatusChange: function (status, statusTitle, readyCb) {
            WA.debug.Console.log('_onUserStateStatusChange ', status, ', isActive:', this._active);
//            var expandCL = (status != 'offline' && !this._active);
            var setStatusCb = WA.createDelegate(function (cb, scope) {
//                if (expandCL) {
//                    this.contactList.show();
//                }

                this._onStatusChange(status, statusTitle);
                this._storeStatus(status, statusTitle, cb, scope);
                this._listenFM();
            }, this);

            if (status != 'offline') {
                if (!this._active) {
                    this._requestableCb = WA.createDelegate(function () {
                        this._activate(setStatusCb);
                    }, this);
                    if (SOCK.requestable) {
                        this._onSocketRequestable();
                    }
                    FM.activate(false, false, true);
                } else {
                    setStatusCb();
                }
            } else {
                this._deactivate();
                setStatusCb(readyCb);
                this.notify.clear();
                return true;
            }
        },

        _onSyncStatus: function (status, customTitle) {
            WA.debug.Console.log('_onSyncStatus ', status);
            var setStatusCb = WA.createDelegate(function () {
                this._onStatusChange(status, customTitle);
                this._listenFM();
            }, this);

            if (status != 'offline') {
                this._activate(setStatusCb, window, true);
            } else {
                this._deactivate();
                setStatusCb();
            }
        },
        // TODO: �� ���������� ������ �������������� ��� ���������
        _activate: function (readyCb, scope, onSync) {
            if (!this._active) {
                this._active = true;
                WA.debug.Console.log('_activate');
                this._onActivate(function () {
                    SOCK.continueEvents();
                    FM.activate(false, false, !onSync);
                    WA.UserAttend.activate();
                    readyCb.call(scope);
                });
            } else {
                readyCb && readyCb.call(scope || window);
            }
        },

        _deactivate: function () {
            delete this._requestableCb;
            if (this._active || this._active === undefined) {
                this._active = false;
                WA.debug.Console.log('_deactivate');

                SOCK.discardEvents();
                FM.deactivate();
                WA.UserAttend.deactivate();
                this._onDeactivate();
            }
        },

        _onErrorMode: function (status) {
            // abstract
        },

        _onStatusChange: function (status) {
            // abstract
        },

        _onActivate: function (onSync, cb, scope) {
            // abstract
        },

        _onDeactivate: function (onSync, cb, scope) {
            // abstract
        },

        _onDocumentReady: function () {
            // abstract
        }
    });

    var DependencyEssence = WA.extend(StatusEssence, {

        _micblogAlertsOff: false,

        constructor: function () {
            this.xMenuItems = U.Array.transform(WA.XStatuses.getCurrents(), function (xs, index) {
                return new StatusMenuItem({
                    itemIndex: index,
                    status: xs.type,
                    text: U.String.ellipsis(xs.text, 20),
                    handler: function (thisButton) {
                        var thisItem = thisButton.initialConfig.owner;
                        this.__changeStatus(thisItem.getStatus(), thisItem.getText());
                    },
                    scope: this
                });
            }, this);

            WA.XStatuses.updateEvent.on(function (newStatuses) {
                U.Array.each(newStatuses, function (xs, index) {
                    var menuItem = this.xMenuItems[index];
                    menuItem.setStatus(xs.type);
                    menuItem.setText(U.String.ellipsis(xs.text, 20));
                }, this);
            }, this);

            DependencyEssence.superclass.constructor.apply(this, arguments);
        },

        __changeStatus: function (status, title) {
            if( this.statusButton.getStatus() == 'offline' && status != 'offline') {
                this.statusButton.setStatus('yellow');
            }
            WA.conn.UserState.setStatus(status, title);
            U.Mnt.rbCountAction();//RB manual status change
        },

        _onCountEvent: function (onlineCount) {
            WA.debug.Console.log('_onCountEvent: ', onlineCount);
            this.statusButton.setCount(onlineCount);
            this.sync.write('contact_count', onlineCount);
        },

        _userEventEnabled: false,

        _enableUserEvents: function () {
            if (!this._userEventEnabled) {
                this._userEventEnabled = true;
                this.contactList.enableUserEvents();
                this.dialogsWindow.enableUserEvents();
                WA.debug.Console.log('_enableUserEvents done');
            }
        },

        _disableUserEvents: function () {
            if (this._userEventEnabled) {
                this._userEventEnabled = false;
                this.contactList.disableUserEvents();
                this.dialogsWindow.disableUserEvents();
                WA.debug.Console.log('_disableUserEvents');
            }

        },

        _toggleStatusMenu: function () {
            if (this.statusMenu && this.statusMenu.rendered) {
                this.statusMenu.toggle();
            }
        },

        _createEssences: function () {
            var statusMenu = this.statusMenu = new StatusMenu({
                fadeIn: true,
                items: [
                    {
                        iconCls: 'nwa-toolbar__status-edit',
                        text: '�������������',
                        handler: function () {
                            WA.XStatuses.openDialog(this.boundEl);
                        },
                        scope: this
                    },
                    '-'
                ].concat(this.xMenuItems).concat([
                    '-',
                    {
                        iconCls: WA.buildIconClass('online'),
                        text: '������',
                        handler: function () {
                            this.__changeStatus('online');
                        },
                        scope: this
                    },
                    {
                        iconCls: WA.buildIconClass('status_chat'),
                        text: '����� ���������',
                        handler: function () {
                            this.__changeStatus('status_chat');
                        },
                        scope: this
                    },
                    {
                        iconCls: WA.buildIconClass('away'),
                        text: '������',
                        handler: function () {
                            this.__changeStatus('away');
                        },
                        scope: this
                    },
                    {
                        iconCls: WA.buildIconClass('dnd'),
                        text: '�� ����������',
                        handler: function () {
                            this.__changeStatus('dnd');
                        },
                        scope: this
                    },
                    {
                        iconCls: WA.buildIconClass('offline'),
                        text: '��������',
                        handler: function () {
                            this.__changeStatus('offline');
                        },
                        scope: this
                    }
                ])
            });

            statusMenu.beforeShowEvent.on(function () {
                statusMenu.getEl().toggleClass('nwa-status-menu_in', !!this._active);

                return !this.statusButton.getPopupMode();
            }, this);

            this.statusButton = new StatusButton({
                cls: 'nwa-button-tab_agent nwa-button-tab_disabled',
                text: 'Mail.Ru �����',
                handler: function (nope, event) {
                    var status = this.statusButton.getStatus();
                    if (this._userEventEnabled) {
                        if (status == 'offline') {
                            this._toggleStatusMenu();
                        } else {
                            this.contactList.toggle();
                            U.Mnt.rbCountAction();//RB open cl
                        }
                    } else if (status == 'offline') {
                        this._toggleStatusMenu();
                    }
                    event.stopEvent();
                },
                scope: this
            });

            // contact list

            this.contactList = new WA.ContactListAgent();

            this.contactList.hideEvent.on(function () {
                this.sync.write('clist', '');
            }, this);

            this.contactList.showEvent.on(function () {
                this.sync.write('clist', '1');
            }, this);

            this.contactList.beforeShowEvent.on(function () {
                if (statusMenu.rendered && statusMenu.isVisible()) return false;
            }, this);

            this.contactList.abstraction.countEvent.on(this._onCountEvent, this);

            this.contactList.contactSelectEvent.on(function (mail, nick, status, statusTitle) {
                this.notify.removeByMail(mail);
                this.openDialog(mail, nick, status, statusTitle);
            }, this);

            this.contactList.playSoundEvent.on(function (canPlay) {
                this._canPlaySound = canPlay;
            }, this);

            this.contactList.micblogAlertsOptionEvent.on(function (val) {
                this._micblogAlertsOff = val;
                this.notify.abstraction.micblogOff = val;
            }, this);

            // dialogs

            this.dialogsWindow = new WA.DialogsAgent();

            this.contactList.addDialogOpenEvent.on(this.dialogsWindow.minimize, this.dialogsWindow);
            this.dialogsWindow.changeFocusEvent.on(this.contactList.addContactDialog.hide, this.contactList.addContactDialog);

//            this.dialogsWindow.countChangedEvent.on(function (count) {}, this);

//            this.dialogsWindow.unreadedEvent.on(function (count) {
//                if (!this.dialogsWindow.isVisible()) {
//                    this.dialogsButton.orange();
//                }

//                if(count > 0) {
//                    this.dialogsButton.blink();
//                } else {
//                    this.dialogsButton.unblink();
//                }
//            }, this);

            this.dialogsWindow.removeContactEvent.on(function (mail) {
                this.contactList.abstraction.removeContact(mail);
            }, this);

            this.dialogsWindow.renameContactEvent.on(function (mail, newNick) {
                this.contactList.abstraction.renameContact(mail, newNick);
            }, this);

            this.dialogsWindow.incomingMessageEvent.on(function () {
                if (this._canPlaySound && this.beep.rendered) {
                    this.beep.play();
                }
            }, this);
            this.dialogsWindow.updateSelfNickEvent.on(this.contactList.setSelfNick, this.contactList);
            
            this.beep = new WA.ui.Beep();

            this.notify = new WA.NotifyAgent();
            this.notify.clickEvent.on(this.openDialog, this);
            this.notify.cancelEvent.on(this.dialogsWindow.markAsReaded, this.dialogsWindow);
            this.notify.onBeforeNotifyEvent.on(function (mail) {
                if(this.dialogsWindow.getVisibleDialog().mail == mail) {
                    return false;
                }
            }, this);

            this.dialogsWindow.visibleDialogChangedEvent.on(function (mail) {
                this.notify.removeByMail(mail);
            }, this);

            this.dialogsWindow.closeTabEventEvent.on(this.notify.removeByMail, this.notify);

            if (!WA.isPopup) {
                WA.getDoc().on('mousedown', function (ev) {
                    var target = ev.getTarget(true);
                    if(!target.isAncestor(this.el)) {
                        this.dialogsWindow.minimize();
                        this.contactList.hide();
                    }
                }, this);
            }

            
        },

        openDialog: function (mail, nick, status, statusTitle) {
            if (this._userEventEnabled) {
                this.dialogsWindow.openDialog(mail, nick, status, statusTitle);
            } else {
                return false;
            }
        }

    });

    var WA_VERSION = 2;

    WA.MainAgent = WA.extend(DependencyEssence, {

        constructor: function () {
            this.popup = WA.MainAgent.Popup.create();
            this.popup.changeStateEvent.on(this._onPopupChangeState, this);

            this.sync = WA.Synchronizer.registerComponent('main_agent', ['contact_count', 'clist', 'dialogs']);
            this.sync.triggerEvent.on(this._onSyncTriggerEvent, this);
            this.sync.showDebugState();


            WA.MainAgent.superclass.constructor.apply(this, arguments);
        },

        _updateAgentVersion: function () {
            WA.Storage.save({ main_agent_version: WA_VERSION }, {
                failure: function () {
                    WA.setTimeout(
                            WA.createDelegate(function () {
                                this._updateAgentVersion();
                            }, this),
                            1000
                            )
                },
                scope: this
            });
        },

        _onPopupChangeState: function (state) {
            if (state) {
                WA.Synchronizer.stop();
                this._disableUserEvents();
                this._setInitialState('popup', null, true);
            } else {
                US.getState(function (userState, param) {
                    this._enableUserEvents();
                    this.statusButton.setPopupMode(false);
                    this.statusButton.el.setAttribute('popup_closing', '');
                    this._setInitialState(userState, param, true);

                    if (!WA.isPopup) {
                        WA.Synchronizer.startUp();
                    }
                }, this);
            }
        },

        _onStatusChange: function (status, customTitle) {
            WA.debug.Console.log('_onStatusChange');

            var title = this._getStatusTitle(status, customTitle);
            this.statusButton.setStatus(status);

            this.contactList.setStatus(status, title);

            this.statusButton.show();

            var isVis = this.isVisible();
            this.show();
            if(!isVis) {
                U.Mnt.end('warmup');
            }

            if (this.popup.getState()) {
                this.statusButton.setPopupMode(true);
                this.statusButton.el.setAttribute('popup_closing', '');
            }
        },

        _getStatusTitle: function (status, customTitle) {
            var statusTitle = WA.MainAgent.statuses[status]
                || customTitle
                || WA.XStatuses.getCurrentText(status)
                || status;
            return statusTitle;
        },

        _onActivate: function (cb, scope) {
            WA.debug.Console.log('_onActivate');

            this.sync.read(function (data) {
                this.statusButton.show();

                WA.ChangeLog.activate();
                WA.ContactStates.activate();

                var count = 0;
                var onActivated = function () {
                    count++;
                    if (count == 2) {
                        cb && cb.call(scope || window);
                    }
                };

                this.dialogsWindow.activate(onActivated);
                this.contactList.activate(onActivated);

                this._onSyncTriggerEvent(data);

            }, this);

            Storage.load(['stat.flushTs', 'stat.count'], {
                success: function (storage) {
                    var FLUSH_INTERVAL = 1000*60*10;
                    var flushFn = function () {
                        Storage.load(['stat.flushTs', 'stat.count'], {
                            success: function (storage) {
                                Storage.save({
                                    'stat.flushTs': +new Date(),
                                    'stat.count': 0
                                }, {
                                    success: function () {
                                        WA.gstat({
                                            param1: 1,
                                            param2: storage['stat.count'] || 0
                                        });
                                        this._statTask.start(FLUSH_INTERVAL);
                                    },
                                    scope: this
                                });
                            },
                            scope: this
                        });
                    };
                    if(!this._statTask) {
                        this._statTask = new U.DelayedTask({
                            fn: flushFn,
                            scope: this
                        });
                    }

                    var now = +new Date();
                    if(!storage['stat.flushTs']) {
                        storage = {
                            'stat.flushTs': now,
                            'stat.count': 0
                        };
                        Storage.save(storage);
                    }
                    var d = now - storage['stat.flushTs'];
                    if(d > FLUSH_INTERVAL) {
                        flushFn.call(this);
                    } else {
                        this._statTask.start(FLUSH_INTERVAL - d);
                    }
                },
                scope: this
            });

        },

        _onErrorMode: function () {
            WA.ChangeLog.deactivate();
            this.contactList.deactivate();
            this.dialogsWindow.deactivate();

            if (this.statusButton.isVisible()) {
                this.contactList.hide();
            }
        },

        _onDeactivate: function () {
            WA.debug.Console.log('_onDeactivate');

            WA.ChangeLog.deactivate();
            WA.ContactStates.deactivate();

            this.dialogsWindow.deactivate();

            this.contactList.deactivate();
            this.contactList.hide();

            this._statTask && this._statTask.stop();
        },

        _createEssences: function () {
            WA.MainAgent.superclass._createEssences.call(this);

            this.contactList.popupChangeVisibleEvent.on(function () {
                if (!WA.isPopup) {
                    window.open(
                            POPUP_URL,
                            'wa_popup',
                            'width=600,height=500,resizable=no,location=no'
                            );
                } else {
                    this.popup.close();
                }
            }, this);
        },

        _onDocumentReady: function () {
            WA.debug.Console.log('_onDocumentReady');
            this._createEssences();
            this.render(WA.getBody());
        },

        _onStorageInit: function () {
            WA.MainAgent.superclass._onStorageInit.call(this);

            this.popup.init();

            this._updateAgentVersion();
        },

        _setInitialState: function (state, param, canSet) {
            if (canSet === true) {
                WA.MainAgent.superclass._setInitialState.call(this, state, param);
            }
        },

        _listenFM: function () {
            if (!this.popup.getState()) {
                WA.MainAgent.superclass._listenFM.call(this);
            }
        },

        _onFocus: function () {
            if (WA.isDebug) {
                document.title = document.title.replace(' [focused]', '') + ' [focused]';
            }

            var count = 0;
            var onReady = function () {
                count++;
                if (count == 3) {
                    this._enableUserEvents();
                }
            };
            this.sync.read(function (data) {
                if (WA.isPopup) {
                    this.contactList.show();
                } else {
                    this._onSyncTriggerEvent(data);
                }
                onReady.call(this);
            }, this);
            this.dialogsWindow.actualize(onReady, this);
            this.contactList.actualize(onReady, this);
        },

        _onBlur: function () {
            if (WA.isDebug) {
                document.title = document.title.replace(' [focused]', '');
            }

            this._disableUserEvents();
        },

        _onSyncTriggerEvent: function (data) {
            var str = [];

            if (data.isChanged('clist')) {
                var clist = data.get('clist');
                str.push('clist=' + clist);
                if (clist) {
                    this.contactList.show();
                } else if (!WA.isPopup) {
                    this.contactList.hide();
                }
            }

            if (data.isChanged('contact_count')) {
                var contact_count = data.get('contact_count');
                str.push('contact_count=' + contact_count);
                if (contact_count) {
                    this.statusButton.setCount(contact_count);
                }
            }

            WA.debug.Console.log('_onSyncTriggerEvent ' + str.join(', '));
        },

        _onRender: function (ct) {
            var rootClass = 'nwa-root'
                + (WA.isPopup ? ' nwa-root_fullscreen' : '')
                + (WA.isFF || WA.isWebKit ? ' nwa-css3-animation' : '')
                + (WA.isIE ? ' nwa-msie' : '');

            U.DomHelper.rapidHtmlInsert({
                tag: 'div',
                id: 'wa-root',
                cls: rootClass,
                style: 'display: none; position: fixed; right: 9500px;',
                children: [
                    {
                        tag: 'div',
                        cls: 'nwa-root__agent'
                    }
                ]
            });
            this.el = WA.get('wa-root');

            this.boundEl = this.el.first();
            this.boundEl.on('scroll', function (ev) {
                ev.stopEvent();
            });

            this.statusButton.render(this.boundEl);

            this.contactList.render(this.boundEl);
            this.contactList.beforeShowEvent.on(function(){
                this.statusButton.hide();
            }, this);
            this.contactList.hideEvent.on(function(){
                this.statusButton.show();
            }, this);
            this.contactList.statusMenuToggleEvent.on(function(){
                this._toggleStatusMenu();
            }, this);


            this.statusMenu.setControlElement(this.contactList.statusToggleBtn.getEl());
            this.statusMenu.render(this.boundEl);

            this.dialogsWindow.render(this.el);

            this.beep.render(this.boundEl);

            this.statusButton.el.on('click', this.__onStatusButtonClick, this);
            Storage.whenReady(function () {
                Storage.load(['oldschool'], {
                    success: function (storage) {
                        if(storage['oldschool']) {
                            this.el.addClass('old-school');
                        }
                    },
                    scope: this
                });
            }, this);
        },

        __onStatusButtonClick: function () {
            var el = this.statusButton.el;
            if (this.popup.getState() && !WA.isPopup && !el.getAttribute('popup_closing')) {
                el.setAttribute('popup_closing', 1);
                this.popup.close();
            }
        }

    });

    WA.MainAgent.sendAddContact = function (email, nickname, cb, scope) {
        WA.conn.Socket.send('contact', {
            operation: 'add',
            email: email,
            nickname: nickname || email
        }, cb || WA.emptyFn, scope || this);
    };

    WA.MainAgent.isChat = function (email) {
        return email.indexOf('@chat.agent') != -1;
    };

    WA.MainAgent.getSettingsUrl = function () {
        return location.hostname.match(/(video|foto|my)\.mail\.ru/)
                ? 'http://my.mail.ru/my/editprops'
                : 'http://e.mail.ru/cgi-bin/editprofile';
    };

    WA.MainAgent.statuses = {
        'online': '������',
        'away': '������',
        'dnd': '�� ����������',
        'offline': '��������',
        'chat': '�����������',
        'gray': '�� �����������',
        'auth': '�� �����������',
        'status_chat': '����� ���������'
    };

    WA.MainAgent.WP2Nick = function (value, defnick) {
        return (value.firstname || value.lastname) && U.String.trim((value.firstname||'') + ' ' + (value.lastname||'')) || value.nickname || defnick || value.email;
    };

})();
(function () {

    var WA = WebAgent;

    // ---------------------------------------------------------------------------------

    var Popup = WA.extend(Object, {

        constructor: function () {
            Popup.superclass.constructor.call(this);

            this.state = 0;
            this.changeStateEvent = new WA.util.Event();

            this.rpc = WA.rpc.Local.createRpc(this._getId());
            this.rpc.remoteEvent.on(this._onRemoteEvent, this);
        },

        _getId: function () {
            WA.abstractError();
        },

        getState: function () {
            return this.state;
        },

        init: function () {
            this.rpc.invoke('activate');
        },

        _onRemoteEvent: function (method, params) {
        },

        _updateState: function (state) {
            this.state = state;
            this.changeStateEvent.fire(state);
            WA.debug.State.set('popup_state', state);
        },

        close: function () {
        }

    });

    // ---------------------------------------------------------------------------------

    var MasterPopup = WA.extend(Popup, {

        _getId: function () {
            return 'MasterPopup';
        },

        _onRemoteEvent: function (method, params) {
            if ('closePopup' === method) {
                this.close();
            }
        },

        init: function () {
            MasterPopup.superclass.init.call(this);

            this._updateState(0);
        },

        close: function () {
            window.close();
        }

    });

    // ---------------------------------------------------------------------------------

    var SlavePopup = WA.MainAgent.Popup = WA.extend(Popup, {

        _getId: function () {
            return 'SlavePopup';
        },

        _onRemoteEvent: function (method, params) {
            if ('changePopupState' === method) {
                this._onChangePopupState(params);
            }
        },

        close: function () {
            this.rpc.invoke('closePopup');
        },

        _onChangePopupState: function (state) {
            this._updateState(state);
            if (!state) {
                WA.debug.State.set('popup_expiredOn', new Date());
            }
        }

    });

    var instance = null;

    WA.MainAgent.Popup = {

        create: function () {
            if (!instance) {
                var clazz = WA.isPopup ? MasterPopup : SlavePopup;
                instance = new clazz();
            }
            return instance;
        }

    };

})();

(function () {

    var WA = WebAgent;
    var U = WA.util;

    var NOTIFY_HEIGHT = 104;
    var SPEED = 0.5;
    var notifyCollection;

    var MINUTE = 60000;
    var HOUR = 60 * MINUTE;
    var DAY = 24 * HOUR;
    var MONTH = 30 * DAY;
    var YEAR = 12 * MONTH;

    var Notify = WA.extend(U.EventDispatcher, {

        constructor: function (img, title, text) {
            Notify.superclass.constructor.apply(this, arguments);
            
            this.img = img;
            this.title = title;
            this.text = text;

            this._opacity = 0;

            this._fadeOutEvent = new U.Event();
        },

        replaceId: '',

        top: 0,

        subtitle: '',
        
        count: 0,

        status: 'online',

        date: 0,

        show: function (noAnimation) {
            if(!this.showed && !this.canceled) {
                this.showed = true;
                notifyCollection.add(this, noAnimation);
                if(this.date) {
                    this._elapsedTimer = new U.DelayedTask({
                        fn: this._onElapsedTimer,
                        scope: this,
                        interval: MINUTE/2
                    });
                    this._elapsedTimer.start();
                }
            }
        },

        _onElapsedTimer: function () {
            var d = new Date() - this.date;
            var n, text;
            if(d > YEAR) {
                n = Math.floor(d/YEAR);
                text = n + WebAgent.util.String.pluralize(n, ' ���', ' ���', ' ����');
            } else if (d > MONTH) {
                n = Math.floor(d/MONTH);
                text = n + WebAgent.util.String.pluralize(n, ' �������', ' �����', ' ������');
            } else if (d > DAY) {
                n = Math.floor(d/DAY);
                text = n + WebAgent.util.String.pluralize(n, ' ����', ' ����', ' ���');
            } else if (d > HOUR) {
                n = Math.floor(d/HOUR);
                text = n + WebAgent.util.String.pluralize(n, ' �����', ' ���', ' ����');
            } else if (d > MINUTE) {
                n = Math.floor(d/MINUTE);
                text = n + WebAgent.util.String.pluralize(n, ' �����', ' ������', ' ������');
            }
            if(n) {
                if (!this.micblog) {
                    this.elapsedEl.update(text+ ' �����');
                }
            }
            this._elapsedTimer.start();
        },

        cancel: function (noAnimation) {
            if(this.showed && !this.canceled) {
                this.showed = false;
                this.canceled = true;
                this.dispatchEvent('close', {
                    target: this
                });

                if(noAnimation) {
                    this.destroy();
                } else {
                    this.setPosition(this._position+1);
                    this._fadeOutEvent.on(this.destroy, this);
                    this.fadeOut();
                }
            }
        },

        fade: function (fadeIn) {
            U.animation.clear(this._fadeAnim);
            var endValue = 0;
            if(fadeIn) {
                endValue = 100;
            }

            this._fadeAnim = U.animation.start(this._opacity, endValue, SPEED, this._onFadeFrame, this);
        },

        fadeIn: function () {
            this.fade(true);
        },

        fadeOut: function () {
            this.fade(false);
        },

        _onFadeFrame: function (value, lastFrame) {
            this.setOpacity(value);

            if(lastFrame && value == 0) {
                this._fadeOutEvent.fire();
            }
        },

        setOpacity: function (value) {
            if(this.el) {
                this.el.setStyle('opacity', value/100);
            }
            this._opacity = value;
        },

        render: function (ct) {
            var captions = ['����� ���������', '���������', '������ �����������'];

            var title = [];
            title.push({
                tag: 'div',
                cls: 'nwa-notify-title-text',
                children: [captions[this.micblog ? 1 : 0]]
            });
            if(this.count) {
                title[0].children.push({
                    tag: 'div',
                    cls: 'nwa-notify-title-count',
                    children: ' ('+ this.count+ ')'
                });
            }

            var message = {
                tag: 'div',
                cls: 'nwa-notify-message',
                children: '<div class="nwa-notify-nick">' + this.title + '</div>' + this.text
            };
            if(this.img) {
                message.style = 'background-image: url('+ this.img+ ');';
            } else {
                message.cls += ' no-image';
            }
            if (this.subtitle) {
                message.children = '<div class="nwa-notify-subtitle">'+ this.subtitle+ '</div>' + message.children;
            }

            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-notify' + (this.micblog ? ' nwa-notify_micblog' : ' nwa-notify_msg'),
                children: [
                    {
                        tag: 'div',
                        cls: 'nwa-notify-status '+ WA.buildIconClass(this.status, true)
                    },
                    {
                        tag: 'div',
                        cls: 'nwa-notify-title',
                        children: title
                    },
                    {
                        tag: 'div',
                        cls: 'nwa-notify-close'
                    },
                    message
                ]
            });
            
            this.elapsedEl = this.el./*last().*/createChild({
                tag: 'div',
                cls: 'nwa-notify-elapsed'
            });

            if (this.micblog) {
                this.elapsedEl.update('<a class="nwa-button_link nwa-notify_reply" data-mail="' + this.replaceId + '" data-post-id="' + this.postId + '" href="javascript:;" >��������</a>');
            }

            this.setOpacity(this._opacity);

            this.notifyEl = this.el.first();

            this.el.on('click', this._onClick, this).
                    on('contextmenu', this._onClick, this).
                    on('mouseover', this._onMouseOver, this);


        },
        
        _onMouseMoveOutside: function (event) {
            var target = event.getTarget();
            if (!this.el.contains(target) && this._mouseover) {
                delete this._mouseover;
                WA.getBody().un('mousemove', this._onMouseMoveOutside, this);
                this.dispatchEvent('mouseout', {
                    target: this
                });
            }
        },

        _onMouseOver: function (event) {
            if(!this._mouseover) {
                this._mouseover = true;
                WA.getBody().on('mousemove', this._onMouseMoveOutside, this);
                this.dispatchEvent('mouseover', {
                    target: this
                });
            }
        },

        _onClick: function (event) {
            var trg = event.getTarget();
            if(trg.className.indexOf('nwa-notify-close')!=-1 || event.button == 2) {
                this.cancel();
                event.stopEvent();
            } else if (trg.className.indexOf('nwa-notify_reply') > -1) {
                var tmp = this.replaceId.split('@');
                var url = 'http://my.mail.ru/' + tmp[1].split('.')[0] + '/' + tmp[0] + '/microposts?postid=' + trg.getAttribute('data-post-id');
                window.open(url);
                this.cancel();
                event.stopPropagation();
            } else {
                this.dispatchEvent('click', {
                    target: this
                });
            }
        },

        setTop: function (top) {
            top = Math.round(top);
            this.top = top;
            this.el.setStyle({
                top: -1 * top + 'px'
            });
            
            if(top >= NOTIFY_HEIGHT && !this._displayed) {
                this._displayed = true;
                this.dispatchEvent('display', {
                    target: this
                });
            }
        },

        setPosition: function (position, noAnimation) {
            this._position = position;
            U.animation.clear(this._moveAnim);
            if(noAnimation) {
                this.setTop(position * NOTIFY_HEIGHT);
            } else {
                this._moveAnim = U.animation.start(this.top, position * NOTIFY_HEIGHT, SPEED, this.setTop, this);
            }
        },

        destroy: function () {
            U.animation.clear(this._fadeAnim);
            U.animation.clear(this._moveAnim);

            WA.getBody().un('mousemove', this._onMouseMoveOutside, this);
            this.el.un('click', this._onClick, this).
                    un('contextmenu', this._onClick, this).
                    un('mouseover', this._onMouseOver, this);
            this.el.remove();

            this._elapsedTimer.stop();

            Notify.superclass.destroy.apply(this, arguments);
        }

    });

    var NotifyCollection = WA.extend(Object, {

        constructor: function () {
            this._collection = [];
        },

        _indexOfNotify: function (notify) {
            var index = -1;
            U.Array.each(this._collection, function (v, i) {
                if(v == notify) {
                    index = i;
                    return false;
                }
            }, this);
            return index;
        },

        _indexOfReplaceId: function (replaceId) {
            var index = -1;
            U.Array.each(this._collection, function (v, i) {
                if(v.replaceId == replaceId) {
                    index = i;
                    return false;
                }
            }, this);
            return index;
        },

        add: function (notify, noAnimation) {
            var top = 0;

            notify.addEventListener('close', WA.createDelegate(this._onNotifyClose, this));

            var index = -1;
            if(notify.replaceId) {
                index = this._indexOfReplaceId(notify.replaceId);
            }

            if(index == -1) {
                if(this._collection.length) {
                    var lastNotify = this._collection[0];
                    var lastBottom = lastNotify.top - NOTIFY_HEIGHT;
                    if(lastBottom < 0) {
                        top = lastBottom;
                    }
                }
                this._collection.unshift(notify);
            } else {
                var oldNotify = this._collection[index];
                top = oldNotify.top;
                oldNotify.cancel(true);
                this._collection.splice(index, 0, notify);
                notify.setOpacity(100);
            }

            notify.render(this.el);
            notify.setTop(top);

            this._reorder(noAnimation);
            if(noAnimation) {
                notify.setOpacity(100);
            } else if(index == -1) {
                notify.fadeIn();
            }
        },

        _reorder: function (noAnimation) {
            U.Array.each(this._collection, function (v, i) {
                v.setPosition((i + 1), noAnimation);
            }, this);
        },

        _onNotifyClose: function (event) {
            var notify = event.target;
            var index = this._indexOfNotify(notify);
            this._collection.splice(index, 1);
            this._reorder();
        },

        render: function (ct) {
            this.el = ct.createChild({
                tag: 'div',
                cls: 'nwa-notify-root'
            });
        }

    });

    var Notifications = WA.Notifications = WA.extend(Object, {

        constructor: function () {
            notifyCollection = new NotifyCollection();
        },

        createNotification: function (img, title, text) {
            return new Notify(img, title, text);
        },

        checkPermission: function () {
            return 0;
        },

        requestPermission: function (cb) {
            cb();
        },

        render: function(ct) {
            notifyCollection.render(ct);
        }

    });

})();
(function () {

    var WA = WebAgent;
    var U = WA.util;
    var SA = WA.SA;
    var ConnectionRoster = WA.conn.ConnectionRoster;
    var FM = WA.FocusManager;
    var TIMEOUT = 5000;
    var JSON = WA.getJSON();

    var FocusBlurWorkaround = WA.extend(Object, {

        constructor: function () {
            this.focusEvent = new U.Event();
            this.blurEvent = new U.Event();

            WA.fly(window).on('blur', this._onIntermediateBlur, this).on('focus', this._onIntermediateFocus, this);
            WA.getBody().on('mousemove', this._onIntermediateMouseMove, this)
                    .on('mousedown', this._onIntermediateMouseMove, this)
                    .on('keydown', this._onIntermediateMouseMove, this);

            this._delayedFocusEvent = new U.DelayedTask({
                interval: 100,
                fn: this._onFocusEvent,
                scope: this
            });

        },

        _onIntermediateMouseMove: function () {
            if(this._delayedFocusEvent.isStarted() && !this._intermediateFocus || !('_intermediateFocus' in this)) {
                this._intermediateFocus = true;
                this._delayedFocusEvent.stop();
                this._onFocusEvent();
            }
        },

        _onIntermediateBlur: function (event) {
            this._intermediateFocus = false;
            this._delayedFocusEvent.start();
        },

        _onIntermediateFocus: function (event) {
            this._intermediateFocus = true;
            this._delayedFocusEvent.start();
        },

        _onFocusEvent: function () {
            this._delayedFocusEvent.stop();
            if(this.focus != this._intermediateFocus) {
                this.focus = this._intermediateFocus;
                
                if(this.focus) {
                    this.focusEvent.fire();
                } else {
                    this.blurEvent.fire();
                }
            }
        }

    });

    var NotifyAbstraction = WA.NotifyAbstraction = WA.extend(ConnectionRoster, {

        constructor: function () {
            NotifyAbstraction.superclass.constructor.apply(this, arguments);

            this.onBeforeNotifyEvent = new U.Event();
            this.createNotifyEvent = new U.Event();
            this.cancelNotifyEvent = new U.Event();
            this.timeoutNotifyEvent = new U.Event();

            FM.focusEvent.on(this._onFMFocus, this);
            FM.blurEvent.on(this._onFMBlur, this);

            this._windowFocus = new FocusBlurWorkaround();
            this._windowFocus.focusEvent.on(this._onNativeFocus, this);
            this._windowFocus.blurEvent.on(this._onNativeBlur, this);

            this._packs = [];
            this._timeouts = {};
            this.micblogOff = false;

            this._sync = WA.Synchronizer.registerComponent('notify', ['packs']);
            this._sync.triggerEvent.on(this._onSync, this);
        },

        _repack: function (value) {
            var mail = value.from;
            var info = SA.get(mail);
            var now = +new Date();

            var pack = {
                notify: {
                    status: info.status,
                    title: info.nick,
                    text: value.text,
                    date: value.timestamp*1000,
                    replaceId: mail
                },
                extra: {
                    status: info.status,
                    status_title: info.status_title,
                    from: mail
                },
                uniq: Math.round(Math.random() * 99999),
                ts: now
            };

            if (WA.MainAgent.isChat(mail)) {
                pack.notify.status = 'chat';
                var tmp = value.text.match(/(.*):\n([\s\S]*)/);
                if(tmp) {
                    pack.notify.subtitle = tmp[1];
                    pack.notify.text = tmp[2];
                }
            } else if(value.auth) {
                pack.notify.status = 'auth';
                pack.notify.text = '������ �����������';
                pack.notify.title = pack.notify.title || mail;
            } else if(value.micblog) {
                pack.notify.micblog = 1;
                pack.notify.postId = value.id;
            }

            // No need to make additional escaping
            //pack.notify.text = U.String.htmlEntity(pack.notify.text);
            pack.notify.title = U.String.htmlEntity(pack.notify.title);
            if(pack.notify.subtitle) {
                pack.notify.subtitle = U.String.htmlEntity(pack.notify.subtitle);
            }
//            pack.notify.text = WA.DialogsAgent.prepareMessageText(pack.notify.text, true);
            pack.notify.text = WA.DialogsAgentView.prepareMessageText(pack.notify.text, true);

            pack.notify.title_plain = pack.notify.title;

            var fromMail = pack.notify.subtitle || mail;
            if(fromMail && fromMail.indexOf('@chat') == -1) {
                pack.notify.img = WA.makeAvatar(fromMail, '_avatar32');
            }

            return pack;
        },

        _indexOfByUniq: function (uniq) {
            var index = -1;
            U.Array.each(this._packs, function (pack, i) {
                if(pack.uniq === uniq) {
                    index = i;
                    return false;
                }
            }, this);
            return index;
        },

        _indexOfByReplaceId: function (replaceId) {
            var index = -1;
            U.Array.each(this._packs, function (pack, i) {
                if(pack.notify.replaceId === replaceId) {
                    index = i;
                    return false;
                }
            }, this);
            return index;
        },

        _saveSync: function () {
            this._sync.write('packs', JSON.stringify(this._packs));
        },

        _onSync: function (data) {
            if(data.isChanged('packs')) {
                var packs = JSON.parse(data.get('packs') || '[]');

                this.clear();

                U.Array.each(packs, function (pack) {
                    this.createNotifyEvent.fire(pack.uniq, pack.notify, pack.extra, true);
                    this._timeoutSet(pack.uniq);
                }, this);
                
                this._packs = packs;
            }
        },

        _timeoutRemove: function (uniq) {
            if(this._timeouts[uniq]) {
                this._timeoutStop(uniq);
                delete this._timeouts[uniq];
            }
        },

        _timeoutSet: function (uniq) {
            if(!this._timeouts[uniq]) {
                this._timeouts[uniq] = new U.DelayedTask({
                    fn: function () {
                        this._remove(uniq);
                        this.timeoutNotifyEvent.fire(uniq);
                    },
                    scope: this,
                    interval: TIMEOUT
                });
            }
        },

        _timeoutStart: function (uniq, interval) {
            //this._timeoutSet(uniq);
            if (this._timeouts[uniq]) {
                this._timeouts[uniq].start(interval);
            }
        },

        _timeoutStop: function (uniq) {
            this._timeoutSet(uniq);
            if(this._timeouts[uniq]) {
                this._timeouts[uniq].stop();
            }
        },

        _remove: function (uniq) {
            this._timeoutRemove(uniq);

            var index = this._indexOfByUniq(uniq);
            if(index != -1) {
                this._packs.splice(index, 1);
            }

            this._saveSync();
        },

        _onAfterConnectionResponse: function () {
            if(this._newMessages) {
                delete this._newMessages;
                var count = 0;
                var _packs = U.Array.clone(this._packs);
                U.Array.eachReverse(_packs, function (pack, i) {
                    if(!pack.showed && count < 3) {
                        pack.showed = true;
                        count++;
                        this.createNotifyEvent.fire(pack.uniq, pack.notify, pack.extra);
                    } else if(count >= 3 && pack.showed) {
                        this.cancelNotifyEvent.fire(pack.uniq);
                    }
                }, this);

                if(this._packs.length > 3) {
                    this._packs.splice(0, this._packs.length -3)
                }

                this._saveSync();
            }
        },

        _onConnectionContactMicblogStatus: function (value) {
            if (this.micblogOff) {
                return;
            }

            if (!value.text) {
                //do not show empty status popup
                return;
            }
            value.micblog = 1;
            this._onConnectionMessage(value);
        },

        _onConnectionMessage: function (value) {
            var pack = this._repack(value);

            if(this.onBeforeNotifyEvent.fire(pack.extra.from) !== false) {
                var index = this._indexOfByReplaceId(pack.notify.replaceId);
                if(index != -1) {
                    var oldPack = this._packs[index];

                    this._timeouts[oldPack.uniq].stop();
                    delete this._timeouts[oldPack.uniq];

                    pack.notify.count = (oldPack.notify.count || 1) + 1;
                    pack.notify.title_plain += ' (' + pack.notify.count + ')';
                    this._packs[index] = pack;
                } else {
                    this._packs.push(pack);
                }

                this._timeoutSet(pack.uniq);
                if(this._windowFocus.focus) {
                    this._timeoutStart(pack.uniq);
                }

                this._newMessages = true;
            }
        },

        _onConnectionAuthRequest: function (value) {
            value.auth = 1;
            this._onConnectionMessage(value);
        },

        stopTimeout: function () {
            var changed = false;
            var now = +new Date();
            U.Object.each(this._packs, function (pack, replaceId) {
                this._timeoutStop(pack.uniq);
                pack.ts = now;
                changed = true;
            }, this);

            if(changed) {
                this._saveSync();
            }
        },

        restartTimeout: function () {
            var changed = false;
            var now = +new Date();
            U.Object.each(this._packs, function (pack, replaceId) {
                this._timeoutStart(pack.uniq);
                pack.ts = now;
                changed = true;
            }, this);

            if(changed) {
                this._saveSync();
            }
        },

        restoreTimeout: function () {
            var now = +new Date();
            U.Object.each(this._packs, function (pack, replaceId) {
                this._timeoutStart(pack.uniq, TIMEOUT - (now - pack.ts));
            }, this);
        },

        _onFMFocus: function () {
            this.restoreTimeout();
        },

        _onFMBlur: function () {
            this.stopTimeout();
        },

        close: function (uniq) {
            this._remove(uniq);
        },

        clear: function () {
            var packs = U.Array.clone(this._packs);
            U.Array.each(packs, function (pack) {
                this.cancelNotifyEvent.fire(pack.uniq, true);
                this._timeoutRemove(pack.uniq);
            }, this);
        },

        _onNativeFocus: function () {
            FM.ifFocused(this.restoreTimeout, false, this);
        },

        _onNativeBlur: function () {
            FM.ifFocused(this.stopTimeout, false, this);
        },

        removeByReplaceId: function (replaceId) {
            var packs = U.Array.clone(this._packs);
            U.Array.each(packs, function (pack) {
                if(pack.notify.replaceId == replaceId) {
                    this.cancelNotifyEvent.fire(pack.uniq, true);
                    this._timeoutRemove(pack.uniq);
                }
            }, this);
        }

    });
})();

(function () {

    var WA = WebAgent;
    var U = WA.util;
    var notifications;

    var NotifyAgent = WA.NotifyAgent = WA.extend(Object, {

        constructor: function () {

            this._notifies = {};

            this.clickEvent = new U.Event();
            this.cancelEvent = new U.Event();

            this.abstraction = new WA.NotifyAbstraction();
            this.abstraction.createNotifyEvent.on(this._onCreateNotify, this);
            this.abstraction.cancelNotifyEvent.on(this._onCancelNotify, this);

            this._timeouted = {};
            this.abstraction.timeoutNotifyEvent.on(this._onTimeoutNotify, this);

            this._onCloseScope = WA.createDelegate(this._onClose, this);
            this._onMouseOverScope = WA.createDelegate(this._onMouseOver, this);
            this._onMouseOutScope = WA.createDelegate(this._onMouseOut, this);
//            this._onDisplayEventScope = WA.createDelegate(this._onDisplayEvent, this);
            this._onClickScope = WA.createDelegate(this._onClick, this);

            this.onBeforeNotifyEvent = new U.Event().relay(this.abstraction.onBeforeNotifyEvent);
        },

        _makeNotify: function (info, noAnimation) {
            if(!notifications) {
                notifications = window.notifications;
            }

            var oldNotifyData = this._getByReplaceId(info.replaceId);
            if(oldNotifyData) {
                oldNotifyData.notify.removeEventListener('close', this._onCloseScope);
                this._removeNotify(oldNotifyData);
            }
            
            var notify = notifications.createNotification(info.img, info.title_plain, info.text);
            WA.apply(notify, info);

            notify.addEventListener('close', this._onCloseScope);
            notify.addEventListener('mouseover', this._onMouseOverScope);
            notify.addEventListener('mouseout', this._onMouseOutScope);
//            notify.addEventListener('display', this._onDisplayEventScope);
            notify.addEventListener('click', this._onClickScope);

            notify.show(noAnimation);
            
            return notify;
        },

        _onCreateNotify: function (uniq, info, extra, noAnimation) {
            this._notifies[uniq] = {
                info: info,
                extra: extra,
                uniq: uniq,
                notify: this._makeNotify(info, noAnimation)
            };
        },

        _onTimeoutNotify: function (uniq, noAnimation) {
            this._timeouted[uniq] = 1;
            this._onCancelNotify(uniq, noAnimation);
        },

        _onCancelNotify: function (uniq, noAnimation) {
            var data = this._notifies[uniq];
            if(data) {
                data.notify.cancel(noAnimation);
            }
        },

        _onMouseOver: function () {
            this.abstraction.stopTimeout();
        },

        _onMouseOut: function () {
            this.abstraction.restartTimeout();
        },

        _onClick: function (event) {
            var notify = event.target;
            var data = this._getByNotify(notify);
            this.clickEvent.fire(data.extra.from, data.notify.title, data.extra.status, data.extra.status_title);
            notify.cancel();
        },

        _removeNotify: function (data) {
            this.abstraction.close(data.uniq);
            delete this._notifies[data.uniq];
        },

        _onClose: function (event) {
            var notify = event.target;
            var data = this._getByNotify(notify);
            this._removeNotify(data);
            
            if(!this._timeouted[data.uniq]) {
                this.cancelEvent.fire(data.extra.from);
            } else {
                delete this._timeouted[data.uniq];
            }
        },

        _getByNotify: function (notify) {
            var retval = false;
            U.Object.each(this._notifies, function (data, k) {
                if(data.notify === notify) {
                    retval = data;
                    return false;
                }
            }, this);
            return retval;
        },

        _getByReplaceId: function (replaceId) {
            var retval = false;
            U.Object.each(this._notifies, function (data, k) {
                if(data.info.replaceId === replaceId) {
                    retval = data;
                    return false;
                }
            }, this);
            return retval;
        },

        clear: function () {
            this.abstraction.clear();
        },

        removeByMail: function (mail) {
            this.abstraction.removeByReplaceId(mail);
        }

    });

})();/*
when(window, 'WebAgent', function () {
    WebAgent.API.error.on(function (err) {
        switch(err.type) {
            case 'OFFLINE':
                if( confirm('go online ?') ) {
                    WebAgent.API.online();
                }
                break;
            case 'OPEN_DIALOG_FAIL':
                document.location = 'magent:email@mail.ru';
                break;
        }
    })
});

<a href="magent:support@corp.mail.ru" onclick="return when(window, 'WebAgent', function () { WebAgent.API.openDialog('ID') })">support</a>
 */

(function () {
    var WA = WebAgent,
        U = WA.util,
        US = WA.conn.UserState,
        SOCK = WA.conn.Socket,
        S = WA.Storage,
        ConnectionRoster = WA.conn.ConnectionRoster;

    var API = WA.extend(ConnectionRoster, {

        constructor: function () {
            API.superclass.constructor.apply(this, arguments);
            this.errorEvent = new U.Event();


            SOCK.requestReadyEvent.on(function () {
                this._openQueue();
            }, this);

            US.permanentShutdownEvent.on(function (event) {
                if (event.reason == 'noAuth') {
                    this._status = 'offline';
                    this._noauth = true;
                }
            }, this);
            US.statusChangedEvent.on(function (status) {
                this._status = status;
            }, this);

            US.readyEvent.on(function () {
                if(WA.ma.isPermanentShutdown) {
                    this._disabled = true;
                    this._cancelOpening('disabledByUser');
                }
                US.getActualStatus(function (status) {
                    this._status = status;
                }, this);
            }, this);

            S.whenReady(function () {
                WA.ma.popup.changeStateEvent.on(function (isOpened) {
                    if(!isOpened) {
                        this._openQueue();
                    }
                }, this);
                
                US.getActualStatus(function (status) {

                    if (!this._confirmEventsListened) {
                        this._confirmEventsListened = true;
                        WA.ma.contactList.onlineConfirmSuccessEvent.on(function () {
                            WA.ma.statusButton.setStatus('yellow');
                            WA.conn.UserState.setStatus('online');
                        }, this);
                        WA.ma.contactList.onlineConfirmDeclineEvent.on(function () {
                            this._cancelOpening('onlineDecline');
                        }, this);
                    }

                    if(status == 'disabled') {
                        this._disabled = true;
                        this._cancelOpening('disabledByUser');
                    } else {
                        this._status = status;
                    }
                }, this);
            }, this);
        },

        _cancelOpening: function (message) {
            if (!message) {
                debugger;
            }
            if(this._queue.length) {
                this.errorEvent.fire({
                    message: message,
                    queue: this._queue
                });

                this._queue = [];
            }
        },

        _queue: [],

        _idlist: {},

        _addAction: function (type, args) {
            var id = args.id,
                mail = args.mail;

            if (WA.ma.popup.getState()) {
                WA.ma.popup.close();
            }

            var index = this._indexOfQueue(id, mail);
            if(index == -1) {
                if (!mail && this._idlist[id]) {
                    mail = this._idlist[id];
                }
                args = WA.apply(args, {
                    type: type
                });
                this._queue.push(args);
            }

            if(this._disabled) {
                this._cancelOpening('disabledByUser');
                return true;
            } else if(this._noauth) {
                this._cancelOpening('invalidUser');
                return true;
            } else if (WA.ma.popup.getState()) {
                WA.ma.popup.close();
            } else {
                this._openQueue();
            }

            return false;
        },

        /**
         * Open dialog
         * @param id    (opt)
         * @param mail  (opt)
         */
        openDialog: function (id, mail) {
            return this._addAction('openDialog', {
                id: id,
                mail: mail
            });
        },

        /**
         * Get profile
         * @param id    (opt)
         * @param mail  (opt)
         */
        getProfile: function (id, mail, cb, scope) {
            return this._addAction('getProfile', {
                id: id,
                mail: mail,
                cb: cb,
                scope: scope
            });
        },

        _indexOfQueue: function (id, mail) {
            return U.Array.indexOfBy(this._queue, function (e) {
                if(e.id && e.id == id || e.mail && e.mail == mail) {
                    return true;
                }
            }, this);
        },

        _onConnectionContactMail: function (value) {
            var index = this._indexOfQueue(value.id);
            if(index != -1) {
                this._queue[index].mail = value.mail;
                this._openKnown();
            }
        },

        _openQueue: function () {
            if(this._status && this._queue.length) {
                if(this._status != 'offline') {
                    if(this._queue.length) {
                        var idlist = [];

                        U.Array.each(this._queue, function (c) {
                            if(!c.mail && !c.requested) {
                                idlist.push(c.id);
                                c.requested = true;
                            }
                        }, this);

                        if (idlist.length) {
                            WA.conn.Connection.getMailById(idlist[0]);
                        } else {
                            this._openKnown();
                        }
                    }
                } else {
                    WA.conn.UserState.getUserStatus(function (status) {
                        if (status.type == 'offline') {
                            WA.ma.contactList.onlineConfirm('���������� �����������?');
                        } else {
                            WA.ma.contactList.onlineConfirm('���������� �����������? ������ ������� ����� ���������');
                        }
                    }, function (errno, errmsg) {
                        this._cancelOpening(errmsg);
                    }, this);

                }
            }
        },
        
        _onConnectionMaillist: function (value) {
            U.Array.each(value, function (contact) {
                this._idlist[contact.id] = contact.mail;
                
                var index = this._indexOfQueue(contact.id);
                this._queue[index].mail = contact.mail;
            }, this);
            this._openKnown();
        },

        _openKnown: function () {
            var maillist = [], mailhash = {};
            var queue = [];
            U.Array.each(this._queue, function (e, index) {
                if(e.mail) {
                    if (e.type == 'openDialog') {
                        maillist.push(e.mail);
                        mailhash[e.mail] = index;
                    } else if (e.type == 'getProfile') {
                        WebAgent.ma.contactList.addContactDialog._abstraction.get(e.mail, e.cb, e.scope);
                    }
                } else {
                    queue.push(e);
                }
            }, this);
            this._queue = queue;
            WA.ma.contactList.abstraction.getContactsInfo(maillist, function (contacts) {
                U.Array.each(contacts, function (contact) {
                    delete mailhash[contact[0]];
                    WA.ma.dialogsWindow.openDialog.apply(WA.ma.dialogsWindow, contact);
                }, this);
                
                var lastindex = -1, mail;
                U.Object.each(mailhash, function (index, m) {
                    if(index > lastindex) {
                        lastindex = index;
                        mail = m;
                    }
                }, this);
                if(mail) {
                    WA.ma.contactList.searchContact(mail, true);
                }
            }, this);

        }

    });

    WA.API = new API();
})();WebAgent.util.Mnt.wrapTryCatch(function () {
    WebAgent.util.Mnt.begin('warmup');
    var renderNotify = function () {
        window.notifications = new WebAgent.Notifications();
        notifications.render(ma.boundEl);
    };
    
    var ma = WebAgent.ma = new WebAgent.MainAgent(WebAgent.isPopup);
    if(ma.rendered) {
        renderNotify();
    } else {
        ma.renderEvent.on(renderNotify);
    }

});