/*
    http://www.JSON.org/json2.js
    2011-10-19

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

window._JSON = (function () {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    function thisDateToJSON (key) {
        return isFinite(this.valueOf())
            ? this.getUTCFullYear()     + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate())      + 'T' +
                f(this.getUTCHours())     + ':' +
                f(this.getUTCMinutes())   + ':' +
                f(this.getUTCSeconds())   + 'Z'
            : null;
    }

    function thisValueOf (key) {
        return this.valueOf();
    }

    /* for better isolation
    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = thisDateToJSON;

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = thisValueOf;
    }
    */

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        /* for better isolation
        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }
        */

        if (value && typeof value === 'object') {
            if (value instanceof Date) {
                value = thisDateToJSON.call(value);
            } else if (value instanceof String || value instanceof Number || value instanceof Boolean) {
                value = thisValueOf.call(value);
            }
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0
                    ? '[]'
                    : gap
                    ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                    : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0
                ? '{}'
                : gap
                ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    /* for better isolation
    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {
    */
    // define public methods
    return {
        stringify: function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        },


// If the JSON object does not yet have a parse method, give it one.
    /* for better isolation
    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {
    */
        parse: function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function'
                    ? walk({'': j}, '')
                    : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        }
    }
}());

/**
 * easyXDM
 * http://easyxdm.net/
 * Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
(function (window, document, location, setTimeout, decodeURIComponent, encodeURIComponent) {
	// double inclusion safeguard
	if ("easyXDM" in window) {
		return;
	}/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, JSON, XMLHttpRequest, window, escape, unescape, ActiveXObject */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

var global = this;
var channelId = 0;
var emptyFn = Function.prototype;
var reURI = /^(http.?:\/\/([^\/\s]+))/; // returns groups for origin (1) and domain (2)
var reParent = /[\-\w]+\/\.\.\//; // matches a foo/../ expression 
var reDoubleSlash = /([^:])\/\//g; // matches // anywhere but in the protocol
var IFRAME_PREFIX = "easyXDM_";
var HAS_NAME_PROPERTY_BUG;


// http://peter.michaux.ca/articles/feature-detection-state-of-the-art-browser-scripting
function isHostMethod(object, property){
    var t = typeof object[property];
    return t == 'function' ||
    (!!(t == 'object' && object[property])) ||
    t == 'unknown';
}

function isHostObject(object, property){
    return !!(typeof(object[property]) == 'object' && object[property]);
}

// end

// http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/
function isArray(o){
    return Object.prototype.toString.call(o) === '[object Array]';
}

// end

/*
 * Cross Browser implementation for adding and removing event listeners.
 */
var on, un;
if (isHostMethod(window, "addEventListener")) {
    on = function(target, type, listener){
        target.addEventListener(type, listener, false);
    };
    un = function(target, type, listener){
        target.removeEventListener(type, listener, false);
    };
}
else if (isHostMethod(window, "attachEvent")) {
    on = function(object, sEvent, fpNotify){
        object.attachEvent("on" + sEvent, fpNotify);
    };
    un = function(object, sEvent, fpNotify){
        object.detachEvent("on" + sEvent, fpNotify);
    };
}
else {
    throw new Error("Browser not supported");
}

/*
 * Cross Browser implementation of DOMContentLoaded.
 */
var isReady = false, domReadyQueue = [];
if ("readyState" in document) {
    isReady = document.readyState == "complete";
}
else {
    // If readyState is not supported in the browser, then in order to be able to fire whenReady functions apropriately
    // when added dynamically _after_ DOM load, we have to deduce wether the DOM is ready or not.
    if (document.body) {
        // document.body is not available prior to the body being built
        // This does mean that we might fire it prematurely, but we only need the body element to be available for appending.
        isReady = true;
    }
}

function dom_onReady(){
    dom_onReady = emptyFn;
    isReady = true;
    for (var i = 0; i < domReadyQueue.length; i++) {
        domReadyQueue[i]();
    }
    domReadyQueue.length = 0;
}


if (!isReady) {
    if (isHostMethod(window, "addEventListener")) {
        on(document, "DOMContentLoaded", dom_onReady);
    }
    else {
        on(document, "readystatechange", function(){
            if (document.readyState == "complete") {
                dom_onReady();
            }
        });
        if (document.documentElement.doScroll && window === top) {
            (function doScrollCheck(){
                if (isReady) {
                    return;
                }
                // http://javascript.nwbox.com/IEContentLoaded/
                try {
                    document.documentElement.doScroll("left");
                } 
                catch (e) {
                    setTimeout(doScrollCheck, 1);
                    return;
                }
                dom_onReady();
            }());
        }
    }
    
    // A fallback to window.onload, that will always work
    on(window, "load", dom_onReady);
}
/**
 * This will add a function to the queue of functions to be run once the DOM reaches a ready state.
 * If functions are added after this event then they will be executed immediately.
 * @param {function} fn The function to add
 * @param {Object} scope An optional scope for the function to be called with.
 */
function whenReady(fn, scope){
    if (isReady) {
        fn.call(scope);
        return;
    }
    domReadyQueue.push(function(){
        fn.call(scope);
    });
}

/*
 * Methods for working with URLs
 */
/**
 * Get the domain name from a url.
 * @param {String} url The url to extract the domain from.
 * @returns The domain part of the url.
 * @type {String}
 */
function getDomainName(url){
    return url.match(reURI)[2];
}

/**
 * Returns  a string containing the schema, domain and if present the port
 * @param {String} url The url to extract the location from
 * @return {String} The location part of the url
 */
function getLocation(url){
    return url.match(reURI)[1];
}

/**
 * Resolves a relative url into an absolute one.
 * @param {String} url The path to resolve.
 * @return {String} The resolved url.
 */
function resolveUrl(url){
    
    // replace all // except the one in proto with /
    url = url.replace(reDoubleSlash, "$1/");
    
    // If the url is a valid url we do nothing
    if (!url.match(/^(http||https):\/\//)) {
        // If this is a relative path
        var path = (url.substring(0, 1) === "/") ? "" : location.pathname;
        if (path.substring(path.length - 1) !== "/") {
            path = path.substring(0, path.lastIndexOf("/") + 1);
        }
        
        url = location.protocol + "//" + location.host + path + url;
    }
    
    // reduce all 'xyz/../' to just '' 
    while (reParent.test(url)) {
        url = url.replace(reParent, "");
    }
    
    return url;
}

/**
 * Appends the parameters to the given url.<br/>
 * The base url can contain existing query parameters.
 * @param {String} url The base url.
 * @param {Object} parameters The parameters to add.
 * @return {String} A new valid url with the parameters appended.
 */
function appendQueryParameters(url, parameters){
    
    var hash = "", indexOf = url.indexOf("#");
    if (indexOf !== -1) {
        hash = url.substring(indexOf);
        url = url.substring(0, indexOf);
    }
    var q = [];
    for (var key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            q.push(key + "=" + encodeURIComponent(parameters[key]));
        }
    }
    return url + ((url.indexOf("?") === -1) ? "?" : "&") + q.join("&") + hash;
}

var query = (function(){
    var query = {}, pair, search = location.search.substring(1).split("&"), i = search.length;
    while (i--) {
        pair = search[i].split("=");
        try{
            query[pair[0]] = decodeURIComponent(pair[1]);
        }catch(e){}
    }
    return query;
}());

/*
 * Helper methods
 */
/**
 * Helper for checking if a variable/property is undefined
 * @param {Object} v The variable to test
 * @return {Boolean} True if the passed variable is undefined
 */
function undef(v){
    return typeof v === "undefined";
}

/**
 * A safe implementation of HTML5 JSON. Feature testing is used to make sure the implementation works.
 * @return {JSON} A valid JSON conforming object, or null if not found.
 */
function getJSON(){
    if (window.JSON && window.JSON.stringify(['a']) === '["a"]') {
        return window.JSON;
    } else {
        return window._JSON;
    }

    
    var cached = {};
    var obj = {
        a: [1, 2, 3]
    }, json = "{\"a\":[1,2,3]}";


    if (JSON && typeof JSON.stringify === "function" && JSON.stringify(obj).replace((/\s/g), "") === json) {
        // this is a working JSON instance
        return JSON;
    }
    if (Object.toJSON) {
        if (Object.toJSON(obj).replace((/\s/g), "") === json) {
            // this is a working stringify method
            cached.stringify = Object.toJSON;
        }
    }
    
    if (typeof String.prototype.evalJSON === "function") {
        obj = json.evalJSON();
        if (obj.a && obj.a.length === 3 && obj.a[2] === 3) {
            // this is a working parse method           
            cached.parse = function(str){
                return str.evalJSON();
            };
        }
    }
    
    if (cached.stringify && cached.parse) {
        // Only memoize the result if we have valid instance
        getJSON = function(){
            return cached;
        };
        return cached;
    }
    return null;
}

/**
 * Applies properties from the source object to the target object.<br/>
 * @param {Object} target The target of the properties.
 * @param {Object} source The source of the properties.
 * @param {Boolean} noOverwrite Set to True to only set non-existing properties.
 */
function apply(destination, source, noOverwrite){
    var member;
    for (var prop in source) {
        if (source.hasOwnProperty(prop)) {
            if (prop in destination) {
                member = source[prop];
                if (typeof member === "object") {
                    apply(destination[prop], member, noOverwrite);
                }
                else if (!noOverwrite) {
                    destination[prop] = source[prop];
                }
            }
            else {
                destination[prop] = source[prop];
            }
        }
    }
    return destination;
}

// This tests for the bug in IE where setting the [name] property using javascript causes the value to be redirected into [submitName].
function testForNamePropertyBug(){
    var el = document.createElement("iframe");
    el.name = "easyXDM_TEST";
    apply(el.style, {
        position: "absolute",
        left: "-2000px",
        top: "0px"
    });
    document.body.appendChild(el);
    HAS_NAME_PROPERTY_BUG = !(el.contentWindow === window.frames[el.name]);
    document.body.removeChild(el);
}

/**
 * Creates a frame and appends it to the DOM.
 * @param config {object} This object can have the following properties
 * <ul>
 * <li> {object} prop The properties that should be set on the frame. This should include the 'src' property.</li>
 * <li> {object} attr The attributes that should be set on the frame.</li>
 * <li> {DOMElement} container Its parent element (Optional).</li>
 * <li> {function} onLoad A method that should be called with the frames contentWindow as argument when the frame is fully loaded. (Optional)</li>
 * </ul>
 * @return The frames DOMElement
 * @type DOMElement
 */
function createFrame(config){
    if (undef(HAS_NAME_PROPERTY_BUG)) {
        testForNamePropertyBug();
    }
    var frame;
    // This is to work around the problems in IE6/7 with setting the name property. 
    // Internally this is set as 'submitName' instead when using 'iframe.name = ...'
    // This is not required by easyXDM itself, but is to facilitate other use cases 
    if (HAS_NAME_PROPERTY_BUG) {
        frame = document.createElement("<iframe name=\"" + config.props.name + "\"/>");
    }
    else {
        frame = document.createElement("IFRAME");
        frame.name = config.props.name;
    }
    
    frame.id = frame.name = config.props.name;
    delete config.props.name;
    
    if (config.onLoad) {
        on(frame, "load", config.onLoad);
    }
    
    if (typeof config.container == "string") {
        config.container = document.getElementById(config.container);
    }
    
    if (!config.container) {
        // This needs to be hidden like this, simply setting display:none and the like will cause failures in some browsers.
        frame.style.position = "absolute";
        frame.style.left = "-2000px";
        frame.style.top = "0px";
        config.container = document.body;
    }
    
    frame.border = frame.frameBorder = 0;
    config.container.insertBefore(frame, config.container.firstChild);
    
    // transfer properties to the frame
    apply(frame, config.props);
    return frame;
}

/**
 * Check whether a domain is allowed using an Access Control List.
 * The ACL can contain * and ? as wildcards, or can be regular expressions.
 * If regular expressions they need to begin with ^ and end with $.
 * @param {Array/String} acl The list of allowed domains
 * @param {String} domain The domain to test.
 * @return {Boolean} True if the domain is allowed, false if not.
 */
function checkAcl(acl, domain){
    // normalize into an array
    if (typeof acl == "string") {
        acl = [acl];
    }
    var re, i = acl.length;
    while (i--) {
        re = acl[i];
        re = new RegExp(re.substr(0, 1) == "^" ? re : ("^" + re.replace(/(\*)/g, ".$1").replace(/\?/g, ".") + "$"));
        if (re.test(domain)) {
            return true;
        }
    }
    return false;
}

/*
 * Functions related to stacks
 */
/**
 * Prepares an array of stack-elements suitable for the current configuration
 * @param {Object} config The Transports configuration. See easyXDM.Socket for more.
 * @return {Array} An array of stack-elements with the TransportElement at index 0.
 */
function prepareTransportStack(config){
    var protocol = config.protocol, stackEls;
    config.isHost = config.isHost || undef(query.xdm_p);
    
    if (!config.props) {
        config.props = {};
    }
    if (!config.isHost) {
        config.channel = query.xdm_c;
        config.secret = query.xdm_s;
        config.remote = query.xdm_e;
        protocol = query.xdm_p;
        if (config.acl && !checkAcl(config.acl, config.remote)) {
            throw new Error("Access denied for " + config.remote);
        }
    }
    else {
        config.remote = resolveUrl(config.remote);
        config.channel = config.channel || "default" + channelId++;
        config.secret = Math.random().toString(16).substring(2);
        if (undef(protocol)) {
            if (getLocation(location.href) == getLocation(config.remote)) {
                /*
                 * Both documents has the same origin, lets use direct access.
                 */
                protocol = "4";
            }
            else if (isHostMethod(window, "postMessage") || isHostMethod(document, "postMessage")) {
                /*
                 * This is supported in IE8+, Firefox 3+, Opera 9+, Chrome 2+ and Safari 4+
                 */
                protocol = "1";
            }
            else if (isHostMethod(window, "ActiveXObject") && isHostMethod(window, "execScript")) {
                /*
                 * This is supported in IE6 and IE7
                 */
                protocol = "3";
            }
            else if (navigator.product === "Gecko" && "frameElement" in window && navigator.userAgent.indexOf('WebKit') == -1) {
                /*
                 * This is supported in Gecko (Firefox 1+)
                 */
                protocol = "5";
            }
            else if (config.remoteHelper) {
                /*
                 * This is supported in all browsers that retains the value of window.name when
                 * navigating from one domain to another, and where parent.frames[foo] can be used
                 * to get access to a frame from the same domain
                 */
                config.remoteHelper = resolveUrl(config.remoteHelper);
                protocol = "2";
            }
            else {
                /*
                 * This is supported in all browsers where [window].location is writable for all
                 * The resize event will be used if resize is supported and the iframe is not put
                 * into a container, else polling will be used.
                 */
                protocol = "0";
            }
        }
    }
    
    switch (protocol) {
        case "0":// 0 = HashTransport
            apply(config, {
                interval: 100,
                delay: 2000,
                useResize: true,
                useParent: false,
                usePolling: false
            }, true);
            if (config.isHost) {
                if (!config.local) {
                    // If no local is set then we need to find an image hosted on the current domain
                    var domain = location.protocol + "//" + location.host, images = document.body.getElementsByTagName("img"), image;
                    var i = images.length;
                    while (i--) {
                        image = images[i];
                        if (image.src.substring(0, domain.length) === domain) {
                            config.local = image.src;
                            break;
                        }
                    }
                    if (!config.local) {
                        // If no local was set, and we are unable to find a suitable file, then we resort to using the current window 
                        config.local = window;
                    }
                }
                
                var parameters = {
                    xdm_c: config.channel,
                    xdm_p: 0
                };
                
                if (config.local === window) {
                    // We are using the current window to listen to
                    config.usePolling = true;
                    config.useParent = true;
                    config.local = location.protocol + "//" + location.host + location.pathname + location.search;
                    parameters.xdm_e = config.local;
                    parameters.xdm_pa = 1; // use parent
                }
                else {
                    parameters.xdm_e = resolveUrl(config.local);
                }
                
                if (config.container) {
                    config.useResize = false;
                    parameters.xdm_po = 1; // use polling
                }
                config.remote = appendQueryParameters(config.remote, parameters);
            }
            else {
                apply(config, {
                    channel: query.xdm_c,
                    remote: query.xdm_e,
                    useParent: !undef(query.xdm_pa),
                    usePolling: !undef(query.xdm_po),
                    useResize: config.useParent ? false : config.useResize
                });
            }
            stackEls = [new easyXDM.stack.HashTransport(config), new easyXDM.stack.ReliableBehavior({}), new easyXDM.stack.QueueBehavior({
                encode: true,
                maxLength: 4000 - config.remote.length
            }), new easyXDM.stack.VerifyBehavior({
                initiate: config.isHost
            })];
            break;
        case "1":
            stackEls = [new easyXDM.stack.PostMessageTransport(config)];
            break;
        case "2":
            stackEls = [new easyXDM.stack.NameTransport(config), new easyXDM.stack.QueueBehavior(), new easyXDM.stack.VerifyBehavior({
                initiate: config.isHost
            })];
            break;
        case "3":
            stackEls = [new easyXDM.stack.NixTransport(config)];
            break;
        case "4":
            stackEls = [new easyXDM.stack.SameOriginTransport(config)];
            break;
        case "5":
            stackEls = [new easyXDM.stack.FrameElementTransport(config)];
            break;
    }
    // this behavior is responsible for buffering outgoing messages, and for performing lazy initialization
    stackEls.push(new easyXDM.stack.QueueBehavior({
        lazy: config.lazy,
        remove: true
    }));
    return stackEls;
}

/**
 * Chains all the separate stack elements into a single usable stack.<br/>
 * If an element is missing a necessary method then it will have a pass-through method applied.
 * @param {Array} stackElements An array of stack elements to be linked.
 * @return {easyXDM.stack.StackElement} The last element in the chain.
 */
function chainStack(stackElements){
    var stackEl, defaults = {
        incoming: function(message, origin){
            this.up.incoming(message, origin);
        },
        outgoing: function(message, recipient){
            this.down.outgoing(message, recipient);
        },
        callback: function(success){
            this.up.callback(success);
        },
        init: function(){
            this.down.init();
        },
        destroy: function(){
            this.down.destroy();
        }
    };
    for (var i = 0, len = stackElements.length; i < len; i++) {
        stackEl = stackElements[i];
        apply(stackEl, defaults, true);
        if (i !== 0) {
            stackEl.down = stackElements[i - 1];
        }
        if (i !== len - 1) {
            stackEl.up = stackElements[i + 1];
        }
    }
    return stackEl;
}

/**
 * This will remove a stackelement from its stack while leaving the stack functional.
 * @param {Object} element The elment to remove from the stack.
 */
function removeFromStack(element){
    element.up.down = element.down;
    element.down.up = element.up;
    element.up = element.down = null;
}

/*
 * Export the main object and any other methods applicable
 */
/** 
 * @class easyXDM
 * A javascript library providing cross-browser, cross-domain messaging/RPC.
 * @version 2.4.9.102
 * @singleton
 */
global.easyXDM = {
    /**
     * The version of the library
     * @type {string}
     */
    version: "2.4.9.102",
    /**
     * This is a map containing all the query parameters passed to the document.
     * All the values has been decoded using decodeURIComponent.
     * @type {object}
     */
    query: query,
    /**
     * @private
     */
    stack: {},
    /**
     * Applies properties from the source object to the target object.<br/>
     * @param {object} target The target of the properties.
     * @param {object} source The source of the properties.
     * @param {boolean} noOverwrite Set to True to only set non-existing properties.
     */
    apply: apply,
    
    /**
     * A safe implementation of HTML5 JSON. Feature testing is used to make sure the implementation works.
     * @return {JSON} A valid JSON conforming object, or null if not found.
     */
    getJSONObject: getJSON,
    /**
     * This will add a function to the queue of functions to be run once the DOM reaches a ready state.
     * If functions are added after this event then they will be executed immediately.
     * @param {function} fn The function to add
     * @param {object} scope An optional scope for the function to be called with.
     */
    whenReady: whenReady
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global console, _FirebugCommandLine,  easyXDM, window, escape, unescape, isHostObject, undef, _trace, isReady, emptyFn */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, isHostObject, isHostMethod, un, on, createFrame, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/** 
 * @class easyXDM.DomHelper
 * Contains methods for dealing with the DOM
 * @singleton
 */
easyXDM.DomHelper = {
    /**
     * Provides a consistent interface for adding eventhandlers
     * @param {Object} target The target to add the event to
     * @param {String} type The name of the event
     * @param {Function} listener The listener
     */
    on: on,
    /**
     * Provides a consistent interface for removing eventhandlers
     * @param {Object} target The target to remove the event from
     * @param {String} type The name of the event
     * @param {Function} listener The listener
     */
    un: un,
    /**
     * Checks for the presence of the JSON object.
     * If it is not present it will use the supplied path to load the JSON2 library.
     * This should be called in the documents head right after the easyXDM script tag.
     * http://json.org/json2.js
     * @param {String} path A valid path to json2.js
     */
    requiresJSON: function(path){
        if (!isHostObject(window, "JSON")) {
            document.write('<script type="text/javascript" src="' + path + '"></script>');
        }
    }
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

(function(){
    // The map containing the stored functions
    var _map = {};
    
    /**
     * @class easyXDM.Fn
     * This contains methods related to function handling, such as storing callbacks.
     * @singleton
     * @namespace easyXDM
     */
    easyXDM.Fn = {
        /**
         * Stores a function using the given name for reference
         * @param {String} name The name that the function should be referred by
         * @param {Function} fn The function to store
         * @namespace easyXDM.fn
         */
        set: function(name, fn){
            _map[name] = fn;
        },
        /**
         * Retrieves the function referred to by the given name
         * @param {String} name The name of the function to retrieve
         * @param {Boolean} del If the function should be deleted after retrieval
         * @return {Function} The stored function
         * @namespace easyXDM.fn
         */
        get: function(name, del){
            var fn = _map[name];
            
            if (del) {
                delete _map[name];
            }
            return fn;
        }
    };
    
}());
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, chainStack, prepareTransportStack, getLocation, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.Socket
 * This class creates a transport channel between two domains that is usable for sending and receiving string-based messages.<br/>
 * The channel is reliable, supports queueing, and ensures that the message originates from the expected domain.<br/>
 * Internally different stacks will be used depending on the browsers features and the available parameters.
 * <h2>How to set up</h2>
 * Setting up the provider:
 * <pre><code>
 * var socket = new easyXDM.Socket({
 * &nbsp; local: "name.html",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
 * &nbsp; &nbsp; socket.postMessage("foo-message");
 * &nbsp; },
 * &nbsp; onMessage: function(message, origin) {
 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
 * &nbsp; }
 * });
 * </code></pre>
 * Setting up the consumer:
 * <pre><code>
 * var socket = new easyXDM.Socket({
 * &nbsp; remote: "http:&#47;&#47;remotedomain/page.html",
 * &nbsp; remoteHelper: "http:&#47;&#47;remotedomain/name.html",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
 * &nbsp; &nbsp; socket.postMessage("foo-message");
 * &nbsp; },
 * &nbsp; onMessage: function(message, origin) {
 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
 * &nbsp; }
 * });
 * </code></pre>
 * If you are unable to upload the <code>name.html</code> file to the consumers domain then remove the <code>remoteHelper</code> property
 * and easyXDM will fall back to using the HashTransport instead of the NameTransport when not able to use any of the primary transports.
 * @namespace easyXDM
 * @constructor
 * @cfg {String/Window} local The url to the local name.html document, a local static file, or a reference to the local window.
 * @cfg {Boolean} lazy (Consumer only) Set this to true if you want easyXDM to defer creating the transport until really needed. 
 * @cfg {String} remote (Consumer only) The url to the providers document.
 * @cfg {String} remoteHelper (Consumer only) The url to the remote name.html file. This is to support NameTransport as a fallback. Optional.
 * @cfg {Number} delay The number of milliseconds easyXDM should try to get a reference to the local window.  Optional, defaults to 2000.
 * @cfg {Number} interval The interval used when polling for messages. Optional, defaults to 300.
 * @cfg {String} channel (Consumer only) The name of the channel to use. Can be used to set consistent iframe names. Must be unique. Optional.
 * @cfg {Function} onMessage The method that should handle incoming messages.<br/> This method should accept two arguments, the message as a string, and the origin as a string. Optional.
 * @cfg {Function} onReady A method that should be called when the transport is ready. Optional.
 * @cfg {DOMElement|String} container (Consumer only) The element, or the id of the element that the primary iframe should be inserted into. If not set then the iframe will be positioned off-screen. Optional.
 * @cfg {Array/String} acl (Provider only) Here you can specify which '[protocol]://[domain]' patterns that should be allowed to act as the consumer towards this provider.<br/>
 * This can contain the wildcards ? and *.  Examples are 'http://example.com', '*.foo.com' and '*dom?.com'. If you want to use reqular expressions then you pattern needs to start with ^ and end with $.
 * If none of the patterns match an Error will be thrown.  
 * @cfg {Object} props (Consumer only) Additional properties that should be applied to the iframe. This can also contain nested objects e.g: <code>{style:{width:"100px", height:"100px"}}</code>. 
 * Properties such as 'name' and 'src' will be overrided. Optional.
 */
easyXDM.Socket = function(config){
    
    // create the stack
    var stack = chainStack(prepareTransportStack(config).concat([{
        incoming: function(message, origin){
            config.onMessage(message, origin);
        },
        callback: function(success){
            if (config.onReady) {
                config.onReady(success);
            }
        }
    }])), recipient = getLocation(config.remote);
    
    // set the origin
    this.origin = getLocation(config.remote);
	
    /**
     * Initiates the destruction of the stack.
     */
    this.destroy = function(){
        stack.destroy();
    };
    
    /**
     * Posts a message to the remote end of the channel
     * @param {String} message The message to send
     */
    this.postMessage = function(message){
        stack.outgoing(message, recipient);
    };
    
    stack.init();
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef,, chainStack, prepareTransportStack, debug, getLocation */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/** 
 * @class easyXDM.Rpc
 * Creates a proxy object that can be used to call methods implemented on the remote end of the channel, and also to provide the implementation
 * of methods to be called from the remote end.<br/>
 * The instantiated object will have methods matching those specified in <code>config.remote</code>.<br/>
 * This requires the JSON object present in the document, either natively, using json.org's json2 or as a wrapper around library spesific methods.
 * <h2>How to set up</h2>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; &#47;&#47; this configuration is equal to that used by the Socket.
 * &nbsp; remote: "http:&#47;&#47;remotedomain/...",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the proxy
 * &nbsp; &nbsp; rpc.foo(...
 * &nbsp; }
 * },{
 * &nbsp; local: {..},
 * &nbsp; remote: {..}
 * });
 * </code></pre>
 * 
 * <h2>Exposing functions (procedures)</h2>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {
 * &nbsp; &nbsp; nameOfMethod: {
 * &nbsp; &nbsp; &nbsp; method: function(arg1, arg2, success, error){
 * &nbsp; &nbsp; &nbsp; &nbsp; ...
 * &nbsp; &nbsp; &nbsp; }
 * &nbsp; &nbsp; },
 * &nbsp; &nbsp; &#47;&#47; with shorthand notation 
 * &nbsp; &nbsp; nameOfAnotherMethod:  function(arg1, arg2, success, error){
 * &nbsp; &nbsp; }
 * &nbsp; },
 * &nbsp; remote: {...}
 * });
 * </code></pre>

 * The function referenced by  [method] will receive the passed arguments followed by the callback functions <code>success</code> and <code>error</code>.<br/>
 * To send a successfull result back you can use
 *     <pre><code>
 *     return foo;
 *     </pre></code>
 * or
 *     <pre><code>
 *     success(foo);
 *     </pre></code>
 *  To return an error you can use
 *     <pre><code>
 *     throw new Error("foo error");
 *     </code></pre>
 * or
 *     <pre><code>
 *     error("foo error");
 *     </code></pre>
 *
 * <h2>Defining remotely exposed methods (procedures/notifications)</h2>
 * The definition of the remote end is quite similar:
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {...},
 * &nbsp; remote: {
 * &nbsp; &nbsp; nameOfMethod: {}
 * &nbsp; }
 * });
 * </code></pre>
 * To call a remote method use
 * <pre><code>
 * rpc.nameOfMethod("arg1", "arg2", function(value) {
 * &nbsp; alert("success: " + value);
 * }, function(message) {
 * &nbsp; alert("error: " + message + );
 * });
 * </code></pre>
 * Both the <code>success</code> and <code>errror</code> callbacks are optional.<br/>
 * When called with no callback a JSON-RPC 2.0 notification will be executed.
 * Be aware that you will not be notified of any errors with this method.
 * <br/>
 * <h2>Specifying a custom serializer</h2>
 * If you do not want to use the JSON2 library for non-native JSON support, but instead capabilities provided by some other library
 * then you can specify a custom serializer using <code>serializer: foo</code>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {...},
 * &nbsp; remote: {...},
 * &nbsp; serializer : {
 * &nbsp; &nbsp; parse: function(string){ ... },
 * &nbsp; &nbsp; stringify: function(object) {...}
 * &nbsp; }
 * });
 * </code></pre>
 * If <code>serializer</code> is set then the class will not attempt to use the native implementation.
 * @namespace easyXDM
 * @constructor
 * @param {Object} config The underlying transports configuration. See easyXDM.Socket for available parameters.
 * @param {Object} jsonRpcConfig The description of the interface to implement.
 */
easyXDM.Rpc = function(config, jsonRpcConfig){
    
    // expand shorthand notation
    if (jsonRpcConfig.local) {
        for (var method in jsonRpcConfig.local) {
            if (jsonRpcConfig.local.hasOwnProperty(method)) {
                var member = jsonRpcConfig.local[method];
                if (typeof member === "function") {
                    jsonRpcConfig.local[method] = {
                        method: member
                    };
                }
            }
        }
    }
	
    // create the stack
    var stack = chainStack(prepareTransportStack(config).concat([new easyXDM.stack.RpcBehavior(this, jsonRpcConfig), {
        callback: function(success){
            if (config.onReady) {
                config.onReady(success);
            }
        }
    }]));
	
    // set the origin 
    this.origin = getLocation(config.remote);
	
    
    /**
     * Initiates the destruction of the stack.
     */
    this.destroy = function(){
        stack.destroy();
    };
    
    stack.init();
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.SameOriginTransport
 * SameOriginTransport is a transport class that can be used when both domains have the same origin.<br/>
 * This can be useful for testing and for when the main application supports both internal and external sources.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote document to communicate with.
 */
easyXDM.stack.SameOriginTransport = function(config){
    var pub, frame, send, targetOrigin;
    
    return (pub = {
        outgoing: function(message, domain, fn){
            send(message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            
            if (config.isHost) {
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname,
                        xdm_c: config.channel,
                        xdm_p: 4 // 4 = SameOriginTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                easyXDM.Fn.set(config.channel, function(sendFn){
                    send = sendFn;
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                    return function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    };
                });
            }
            else {
                send = parent.easyXDM.Fn.get(config.channel, true)(function(msg){
                    pub.up.incoming(msg, targetOrigin);
                });
                setTimeout(function(){
                    pub.up.callback(true);
                }, 0);
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.PostMessageTransport
 * PostMessageTransport is a transport class that uses HTML5 postMessage for communication.<br/>
 * <a href="http://msdn.microsoft.com/en-us/library/ms644944(VS.85).aspx">http://msdn.microsoft.com/en-us/library/ms644944(VS.85).aspx</a><br/>
 * <a href="https://developer.mozilla.org/en/DOM/window.postMessage">https://developer.mozilla.org/en/DOM/window.postMessage</a>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote domain to communicate with.
 */
easyXDM.stack.PostMessageTransport = function(config){
    var pub, // the public interface
 frame, // the remote frame, if any
 callerWindow, // the window that we will call with
 targetOrigin; // the domain to communicate with
    /**
     * Resolves the origin from the event object
     * @private
     * @param {Object} event The messageevent
     * @return {String} The scheme, host and port of the origin
     */
    function _getOrigin(event){
        if (event.origin) {
            // This is the HTML5 property
            return event.origin;
        }
        if (event.uri) {
            // From earlier implementations 
            return getLocation(event.uri);
        }
        if (event.domain) {
            // This is the last option and will fail if the 
            // origin is not using the same schema as we are
            return location.protocol + "//" + event.domain;
        }
        throw "Unable to retrieve the origin of the event";
    }
    
    /**
     * This is the main implementation for the onMessage event.<br/>
     * It checks the validity of the origin and passes the message on if appropriate.
     * @private
     * @param {Object} event The messageevent
     */
    function _window_onMessage(event){
        var origin = _getOrigin(event);
        if (origin == targetOrigin && event.data.substring(0, config.channel.length + 1) == config.channel + " ") {
            var cb = function () {
                pub.up.incoming(event.data.substring(config.channel.length + 1), origin);
            };
            window.WebAgent && WebAgent.util.Mnt.wrapTryCatch(cb) || cb();
        }
    }
    
    return (pub = {
        outgoing: function(message, domain, fn){
            callerWindow.postMessage(config.channel + " " + message, domain || targetOrigin);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            un(window, "message", _window_onMessage);
            if (frame) {
                callerWindow = null;
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            if (config.isHost) {
                // add the event handler for listening
                on(window, "message", function waitForReady(event){
                    if (event.data == config.channel + "-ready") {
                        // replace the eventlistener
                        callerWindow = ("postMessage" in frame.contentWindow) ? frame.contentWindow : frame.contentWindow.document;
                        un(window, "message", waitForReady);
                        on(window, "message", _window_onMessage);
                        setTimeout(function(){
                            pub.up.callback(true);
                        }, 0);
                    }
                });
                
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host,
                        xdm_c: config.channel,
                        xdm_p: 1 // 1 = PostMessage
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
            }
            else {
                // add the event handler for listening
                on(window, "message", _window_onMessage);
                callerWindow = ("postMessage" in window.parent) ? window.parent : window.parent.document;
                callerWindow.postMessage(config.channel + "-ready", targetOrigin);
                
                setTimeout(function(){
                    pub.up.callback(true);
                }, 0);
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, apply, query, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.FrameElementTransport
 * FrameElementTransport is a transport class that can be used with Gecko-browser as these allow passing variables using the frameElement property.<br/>
 * Security is maintained as Gecho uses Lexical Authorization to determine under which scope a function is running.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote document to communicate with.
 */
easyXDM.stack.FrameElementTransport = function(config){
    var pub, frame, send, targetOrigin;
    
    return (pub = {
        outgoing: function(message, domain, fn){
            send.call(this, message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            
            if (config.isHost) {
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname + location.search,
                        xdm_c: config.channel,
                        xdm_p: 5 // 5 = FrameElementTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                frame.fn = function(sendFn){
                    delete frame.fn;
                    send = sendFn;
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                    // remove the function so that it cannot be used to overwrite the send function later on
                    return function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    };
                };
            }
            else {
                if (document.referrer && document.referrer != query.xdm_e) {
                    window.parent.location = query.xdm_e;
                }
                else {
                    if (document.referrer != query.xdm_e) {
                        // This is to mitigate origin-spoofing
                        window.parent.location = query.xdm_e;
                    }
                    send = window.frameElement.fn(function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    });
                    pub.up.callback(true);
                }
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global global, getNixProxy, easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, isHostMethod, apply, query, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.NixTransport
 * NixTransport is a transport class that uses the strange fact that in IE <8, the window.opener property can be written to and read from all windows.<br/>
 * This is used to pass methods that are able to relay messages back and forth. To avoid context-leakage a VBScript (COM) object is used to relay all the strings.<br/>
 * This transport is loosely based on the work done by <a href="https://issues.apache.org/jira/browse/SHINDIG-416">Shindig</a>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote domain to communicate with.
 * @cfg {String} secret the pre-shared secret used to secure the communication.
 */
easyXDM.stack.NixTransport = function(config){
    var pub, // the public interface
 frame, send, targetOrigin, proxy;
    
    return (pub = {
        outgoing: function(message, domain, fn){
            send(message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            proxy = null;
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            if (config.isHost) {
                try {
                    if (!isHostMethod(window, "getNixProxy")) {
                        window.execScript('Class NixProxy\n' +
                        '    Private m_parent, m_child, m_Auth\n' +
                        '\n' +
                        '    Public Sub SetParent(obj, auth)\n' +
                        '        If isEmpty(m_Auth) Then m_Auth = auth\n' +
                        '        SET m_parent = obj\n' +
                        '    End Sub\n' +
                        '    Public Sub SetChild(obj)\n' +
                        '        SET m_child = obj\n' +
                        '        m_parent.ready()\n' +
                        '    End Sub\n' +
                        '\n' +
                        // The auth string, which is a pre-shared key between the parent and the child, 
                        // and that can only be set once by the parent, secures the communication, and also serves to provide
                        // 'proof' of the origin of the messages.
                        // Before passing the message on to the recipent we convert the message into a primitive, 
                        // this mitigates modifying .toString as an attack vector.
                        '    Public Sub SendToParent(data, auth)\n' +
                        '        If m_Auth = auth Then m_parent.send(CStr(data))\n' +
                        '    End Sub\n' +
                        '    Public Sub SendToChild(data, auth)\n' +
                        '        If m_Auth = auth Then m_child.send(CStr(data))\n' +
                        '    End Sub\n' +
                        'End Class\n' +
                        'Function getNixProxy()\n' +
                        '    Set GetNixProxy = New NixProxy\n' +
                        'End Function\n', 'vbscript');
                    }
                    proxy = getNixProxy();
                    proxy.SetParent({
                        send: function(msg){
                            pub.up.incoming(msg, targetOrigin);
                        },
                        ready: function(){
                            setTimeout(function(){
                                pub.up.callback(true);
                            }, 0);
                        }
                    }, config.secret);
                    send = function(msg){
                        proxy.SendToChild(msg, config.secret);
                    };
                } 
                catch (e1) {
                    throw new Error("Could not set up VBScript NixProxy:" + e1.message);
                }
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname + location.search,
                        xdm_c: config.channel,
                        xdm_s: config.secret,
                        xdm_p: 3 // 3 = NixTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                frame.contentWindow.opener = proxy;
            }
            else {
                if (document.referrer && document.referrer != query.xdm_e) {
                    window.parent.location = query.xdm_e;
                }
                else {
                    if (document.referrer != query.xdm_e) {
                        // This is to mitigate origin-spoofing
                        window.parent.location = query.xdm_e;
                    }
                    try {
                        // by storing this in a variable we negate replacement attacks
                        proxy = window.opener;
                    } 
                    catch (e2) {
                        throw new Error("Cannot access window.opener");
                    }
                    proxy.SetChild({
                        send: function(msg){
                            // the timeout is necessary to have execution continue in the correct context
                            global.setTimeout(function(){
                                pub.up.incoming(msg, targetOrigin);
                            }, 0);
                        }
                    });
                    
                    send = function(msg){
                        proxy.SendToParent(msg, config.secret);
                    };
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                }
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, getLocation, appendQueryParameters, resolveUrl, createFrame, debug, un, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.NameTransport
 * NameTransport uses the window.name property to relay data.
 * The <code>local</code> parameter needs to be set on both the consumer and provider,<br/>
 * and the <code>remoteHelper</code> parameter needs to be set on the consumer.
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remoteHelper The url to the remote instance of hash.html - this is only needed for the host.
 * @namespace easyXDM.stack
 */
easyXDM.stack.NameTransport = function(config){
    
    var pub; // the public interface
    var isHost, callerWindow, remoteWindow, readyCount, callback, remoteOrigin, remoteUrl;
    
    function _sendMessage(message){
        var url = config.remoteHelper + (isHost ? "#_3" : "#_2") + config.channel;
        callerWindow.contentWindow.sendMessage(message, url);
    }
    
    function _onReady(){
        if (isHost) {
            if (++readyCount === 2 || !isHost) {
                pub.up.callback(true);
            }
        }
        else {
            _sendMessage("ready");
            pub.up.callback(true);
        }
    }
    
    function _onMessage(message){
        pub.up.incoming(message, remoteOrigin);
    }
    
    function _onLoad(){
        if (callback) {
            setTimeout(function(){
                callback(true);
            }, 0);
        }
    }
    
    return (pub = {
        outgoing: function(message, domain, fn){
            callback = fn;
            _sendMessage(message);
        },
        destroy: function(){
            callerWindow.parentNode.removeChild(callerWindow);
            callerWindow = null;
            if (isHost) {
                remoteWindow.parentNode.removeChild(remoteWindow);
                remoteWindow = null;
            }
        },
        onDOMReady: function(){
            isHost = config.isHost;
            readyCount = 0;
            remoteOrigin = getLocation(config.remote);
            config.local = resolveUrl(config.local);
            
            if (isHost) {
                // Register the callback
                easyXDM.Fn.set(config.channel, function(message){
                    if (isHost && message === "ready") {
                        // Replace the handler
                        easyXDM.Fn.set(config.channel, _onMessage);
                        _onReady();
                    }
                });
                
                // Set up the frame that points to the remote instance
                remoteUrl = appendQueryParameters(config.remote, {
                    xdm_e: config.local,
                    xdm_c: config.channel,
                    xdm_p: 2
                });
                apply(config.props, {
                    src: remoteUrl + '#' + config.channel,
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                remoteWindow = createFrame(config);
            }
            else {
                config.remoteHelper = config.remote;
                easyXDM.Fn.set(config.channel, _onMessage);
            }
            // Set up the iframe that will be used for the transport
            
            callerWindow = createFrame({
                props: {
                    src: config.local + "#_4" + config.channel
                },
                onLoad: function onLoad(){
                    // Remove the handler
                    un(callerWindow, "load", onLoad);
                    easyXDM.Fn.set(config.channel + "_load", _onLoad);
                    (function test(){
                        if (typeof callerWindow.contentWindow.sendMessage == "function") {
                            _onReady();
                        }
                        else {
                            setTimeout(test, 50);
                        }
                    }());
                }
            });
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.HashTransport
 * HashTransport is a transport class that uses the IFrame URL Technique for communication.<br/>
 * <a href="http://msdn.microsoft.com/en-us/library/bb735305.aspx">http://msdn.microsoft.com/en-us/library/bb735305.aspx</a><br/>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String/Window} local The url to the local file used for proxying messages, or the local window.
 * @cfg {Number} delay The number of milliseconds easyXDM should try to get a reference to the local window.
 * @cfg {Number} interval The interval used when polling for messages.
 */
easyXDM.stack.HashTransport = function(config){
    var pub;
    var me = this, isHost, _timer, pollInterval, _lastMsg, _msgNr, _listenerWindow, _callerWindow;
    var useParent, _remoteOrigin;
    
    function _sendMessage(message){
        if (!_callerWindow) {
            return;
        }
        var url = config.remote + "#" + (_msgNr++) + "_" + message;
        ((isHost || !useParent) ? _callerWindow.contentWindow : _callerWindow).location = url;
    }
    
    function _handleHash(hash){
        _lastMsg = hash;
        pub.up.incoming(_lastMsg.substring(_lastMsg.indexOf("_") + 1), _remoteOrigin);
    }
    
    /**
     * Checks location.hash for a new message and relays this to the receiver.
     * @private
     */
    function _pollHash(){
        if (!_listenerWindow) {
            return;
        }
        var href = _listenerWindow.location.href, hash = "", indexOf = href.indexOf("#");
        if (indexOf != -1) {
            hash = href.substring(indexOf);
        }
        if (hash && hash != _lastMsg) {
            _handleHash(hash);
        }
    }
    
    function _attachListeners(){
        _timer = setInterval(_pollHash, pollInterval);
    }
    
    return (pub = {
        outgoing: function(message, domain){
            _sendMessage(message);
        },
        destroy: function(){
            window.clearInterval(_timer);
            if (isHost || !useParent) {
                _callerWindow.parentNode.removeChild(_callerWindow);
            }
            _callerWindow = null;
        },
        onDOMReady: function(){
            isHost = config.isHost;
            pollInterval = config.interval;
            _lastMsg = "#" + config.channel;
            _msgNr = 0;
            useParent = config.useParent;
            _remoteOrigin = getLocation(config.remote);
            if (isHost) {
                config.props = {
                    src: config.remote,
                    name: IFRAME_PREFIX + config.channel + "_provider"
                };
                if (useParent) {
                    config.onLoad = function(){
                        _listenerWindow = window;
                        _attachListeners();
                        pub.up.callback(true);
                    };
                }
                else {
                    var tries = 0, max = config.delay / 50;
                    (function getRef(){
                        if (++tries > max) {
                            throw new Error("Unable to reference listenerwindow");
                        }
                        try {
                            _listenerWindow = _callerWindow.contentWindow.frames[IFRAME_PREFIX + config.channel + "_consumer"];
                        } 
                        catch (ex) {
                        }
                        if (_listenerWindow) {
                            _attachListeners();
                            pub.up.callback(true);
                        }
                        else {
                            setTimeout(getRef, 50);
                        }
                    }());
                }
                _callerWindow = createFrame(config);
            }
            else {
                _listenerWindow = window;
                _attachListeners();
                if (useParent) {
                    _callerWindow = parent;
                    pub.up.callback(true);
                }
                else {
                    apply(config, {
                        props: {
                            src: config.remote + "#" + config.channel + new Date(),
                            name: IFRAME_PREFIX + config.channel + "_consumer"
                        },
                        onLoad: function(){
                            pub.up.callback(true);
                        }
                    });
                    _callerWindow = createFrame(config);
                }
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.ReliableBehavior
 * This is a behavior that tries to make the underlying transport reliable by using acknowledgements.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration.
 */
easyXDM.stack.ReliableBehavior = function(config){
    var pub, // the public interface
 callback; // the callback to execute when we have a confirmed success/failure
    var idOut = 0, idIn = 0, currentMessage = "";
    
    return (pub = {
        incoming: function(message, origin){
            var indexOf = message.indexOf("_"), ack = message.substring(0, indexOf).split(",");
            message = message.substring(indexOf + 1);
            
            if (ack[0] == idOut) {
                currentMessage = "";
                if (callback) {
                    callback(true);
                }
            }
            if (message.length > 0) {
                pub.down.outgoing(ack[1] + "," + idOut + "_" + currentMessage, origin);
                if (idIn != ack[1]) {
                    idIn = ack[1];
                    pub.up.incoming(message, origin);
                }
            }
            
        },
        outgoing: function(message, origin, fn){
            currentMessage = message;
            callback = fn;
            pub.down.outgoing(idIn + "," + (++idOut) + "_" + message, origin);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug, undef, removeFromStack*/
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.QueueBehavior
 * This is a behavior that enables queueing of messages. <br/>
 * It will buffer incoming messages and dispach these as fast as the underlying transport allows.
 * This will also fragment/defragment messages so that the outgoing message is never bigger than the
 * set length.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration. Optional.
 * @cfg {Number} maxLength The maximum length of each outgoing message. Set this to enable fragmentation.
 */
easyXDM.stack.QueueBehavior = function(config){
    var pub, queue = [], waiting = true, incoming = "", destroying, maxLength = 0, lazy = false, doFragment = false;
    
    function dispatch(){
        if (config.remove && queue.length === 0) {
            removeFromStack(pub);
            return;
        }
        if (waiting || queue.length === 0 || destroying) {
            return;
        }
        waiting = true;
        var message = queue.shift();
        
        pub.down.outgoing(message.data, message.origin, function(success){
            waiting = false;
            if (message.callback) {
                setTimeout(function(){
                    message.callback(success);
                }, 0);
            }
            dispatch();
        });
    }
    return (pub = {
        init: function(){
            if (undef(config)) {
                config = {};
            }
            if (config.maxLength) {
                maxLength = config.maxLength;
                doFragment = true;
            }
            if (config.lazy) {
                lazy = true;
            }
            else {
                pub.down.init();
            }
        },
        callback: function(success){
            waiting = false;
            var up = pub.up; // in case dispatch calls removeFromStack
            dispatch();
            up.callback(success);
        },
        incoming: function(message, origin){
            if (doFragment) {
                var indexOf = message.indexOf("_"), seq = parseInt(message.substring(0, indexOf), 10);
                incoming += message.substring(indexOf + 1);
                if (seq === 0) {
                    if (config.encode) {
                        incoming = decodeURIComponent(incoming);
                    }
                    pub.up.incoming(incoming, origin);
                    incoming = "";
                }
            }
            else {
                pub.up.incoming(message, origin);
            }
        },
        outgoing: function(message, origin, fn){
            if (config.encode) {
                message = encodeURIComponent(message);
            }
            var fragments = [], fragment;
            if (doFragment) {
                // fragment into chunks
                while (message.length !== 0) {
                    fragment = message.substring(0, maxLength);
                    message = message.substring(fragment.length);
                    fragments.push(fragment);
                }
                // enqueue the chunks
                while ((fragment = fragments.shift())) {
                    queue.push({
                        data: fragments.length + "_" + fragment,
                        origin: origin,
                        callback: fragments.length === 0 ? fn : null
                    });
                }
            }
            else {
                queue.push({
                    data: message,
                    origin: origin,
                    callback: fn
                });
            }
            if (lazy) {
                pub.down.init();
            }
            else {
                dispatch();
            }
        },
        destroy: function(){
            destroying = true;
            pub.down.destroy();
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, debug */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.VerifyBehavior
 * This behavior will verify that communication with the remote end is possible, and will also sign all outgoing,
 * and verify all incoming messages. This removes the risk of someone hijacking the iframe to send malicious messages.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration.
 * @cfg {Boolean} initiate If the verification should be initiated from this end.
 */
easyXDM.stack.VerifyBehavior = function(config){
    var pub, mySecret, theirSecret, verified = false;
    
    function startVerification(){
        mySecret = Math.random().toString(16).substring(2);
        pub.down.outgoing(mySecret);
    }
    
    return (pub = {
        incoming: function(message, origin){
            var indexOf = message.indexOf("_");
            if (indexOf === -1) {
                if (message === mySecret) {
                    pub.up.callback(true);
                }
                else if (!theirSecret) {
                    theirSecret = message;
                    if (!config.initiate) {
                        startVerification();
                    }
                    pub.down.outgoing(message);
                }
            }
            else {
                if (message.substring(0, indexOf) === theirSecret) {
                    pub.up.incoming(message.substring(indexOf + 1), origin);
                }
            }
        },
        outgoing: function(message, origin, fn){
            pub.down.outgoing(mySecret + "_" + message, origin, fn);
        },
        callback: function(success){
            if (config.initiate) {
                startVerification();
            }
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, getJSON, debug, emptyFn, isArray */
//
// easyXDM
// http://easyxdm.net/
// Copyright(c) 2009, �?yvind Sean Kinsey, oyvind@kinsey.no.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

/**
 * @class easyXDM.stack.RpcBehavior
 * This uses JSON-RPC 2.0 to expose local methods and to invoke remote methods and have responses returned over the the string based transport stack.<br/>
 * Exposed methods can return values synchronous, asyncronous, or bet set up to not return anything.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} proxy The object to apply the methods to.
 * @param {Object} config The definition of the local and remote interface to implement.
 * @cfg {Object} local The local interface to expose.
 * @cfg {Object} remote The remote methods to expose through the proxy.
 * @cfg {Object} serializer The serializer to use for serializing and deserializing the JSON. Should be compatible with the HTML5 JSON object. Optional, will default to JSON.
 */
easyXDM.stack.RpcBehavior = function(proxy, config){
    var pub, serializer = config.serializer || getJSON();
    var _callbackCounter = 0, _callbacks = {};
    
    /**
     * Serializes and sends the message
     * @private
     * @param {Object} data The JSON-RPC message to be sent. The jsonrpc property will be added.
     */
    function _send(data){
        data.jsonrpc = "2.0";
        pub.down.outgoing(serializer.stringify(data));
    }
    
    /**
     * Creates a method that implements the given definition
     * @private
     * @param {Object} The method configuration
     * @param {String} method The name of the method
     * @return {Function} A stub capable of proxying the requested method call
     */
    function _createMethod(definition, method){
        var slice = Array.prototype.slice;
        
        return function(){
            var l = arguments.length, callback, message = {
                method: method
            };
            
            if (l > 0 && typeof arguments[l - 1] === "function") {
                //with callback, procedure
                if (l > 1 && typeof arguments[l - 2] === "function") {
                    // two callbacks, success and error
                    callback = {
                        success: arguments[l - 2],
                        error: arguments[l - 1]
                    };
                    message.params = slice.call(arguments, 0, l - 2);
                }
                else {
                    // single callback, success
                    callback = {
                        success: arguments[l - 1]
                    };
                    message.params = slice.call(arguments, 0, l - 1);
                }
                _callbacks["" + (++_callbackCounter)] = callback;
                message.id = _callbackCounter;
            }
            else {
                // no callbacks, a notification
                message.params = slice.call(arguments, 0);
            }
            if (definition.namedParams && message.params.length === 1) {
                message.params = message.params[0];
            }
            // Send the method request
            _send(message);
        };
    }
    
    /**
     * Executes the exposed method
     * @private
     * @param {String} method The name of the method
     * @param {Number} id The callback id to use
     * @param {Function} method The exposed implementation
     * @param {Array} params The parameters supplied by the remote end
     */
    function _executeMethod(method, id, fn, params){
        if (!fn) {
            if (id) {
                _send({
                    id: id,
                    error: {
                        code: -32601,
                        message: "Procedure not found."
                    }
                });
            }
            return;
        }
        
        var success, error;
        if (id) {
            success = function(result){
                success = emptyFn;
                _send({
                    id: id,
                    result: result
                });
            };
            error = function(message, data){
                error = emptyFn;
                var msg = {
                    id: id,
                    error: {
                        code: -32099,
                        message: message
                    }
                };
                if (data) {
                    msg.error.data = data;
                }
                _send(msg);
            };
        }
        else {
            success = error = emptyFn;
        }
        // Call local method
        if (!isArray(params)) {
            params = [params];
        }
        try {
            var result = fn.method.apply(fn.scope, params.concat([success, error]));
            if (!undef(result)) {
                success(result);
            }
        } 
        catch (ex1) {
            error(ex1.message);
        }
    }
    
    return (pub = {
        incoming: function(message, origin){
            var data = serializer.parse(message);
            if (data.method) {
                // A method call from the remote end
                if (config.handle) {
                    config.handle(data, _send);
                }
                else {
                    _executeMethod(data.method, data.id, config.local[data.method], data.params);
                }
            }
            else {
                // A method response from the other end
                var callback = _callbacks[data.id] || {};
                if (data.error) {
                    if (callback.error) {
                        callback.error(data.error);
                    }
                }
                else if (callback.success) {
                    callback.success(data.result);
                }
                delete _callbacks[data.id];
            }
        },
        init: function(){
            if (config.remote) {
                // Implement the remote sides exposed methods
                for (var method in config.remote) {
                    if (config.remote.hasOwnProperty(method)) {
                        proxy[method] = _createMethod(config.remote[method], method);
                    }
                }
            }
            pub.down.init();
        },
        destroy: function(){
            for (var method in config.remote) {
                if (config.remote.hasOwnProperty(method) && proxy.hasOwnProperty(method)) {
                    delete proxy[method];
                }
            }
            pub.down.destroy();
        }
    });
};
})(window, document, location, window.nativeSetTimeout || window.setTimeout, decodeURIComponent, encodeURIComponent);
(function () {
    // empty comment
    var WA = window.WebAgent = window.WebAgent || {};

    var getUserLogin = function () {
        return (/Mpop=.*?:([^@:]+@[^:]+)/.exec(document.cookie.toString()) || [0, false])[1];
    };

    var ACTIVE_MAIL = getUserLogin();

    if (ACTIVE_MAIL) {
        var SAFE_ACTIVE_MAIL = ACTIVE_MAIL.replace(/[^a-z0-9]+/ig, '').toLowerCase();
    }

    var isDebug = location.href.indexOf('localhost') != -1 || location.href.indexOf('wa_debug') != -1;

    var docMode = document.documentMode;
    var ua = navigator.userAgent.toLowerCase();
    var checkUA = function (regexp) {
        return regexp.test(ua);
    };
    var isOpera = checkUA(/opera/);
    var isWebKit = checkUA(/webkit/);
    var isIE = !isOpera && checkUA(/msie/);
    var isIE8 = isIE && (checkUA(/msie 8/) && docMode != 7);
    var isGecko = !isWebKit && checkUA(/gecko/);
    var isGecko3 = isGecko && checkUA(/rv:1\.9/);

    var UID = 1;

    var apply = function (to, from, defaults) {
        if (defaults) {
            apply(to, defaults);
        }
        if (from) {
            for (var key in from) {
                to[key] = from[key];
            }
        }
        return to;
    };

    apply(WebAgent, {

        isDebug: isDebug,

        isIE: isIE,

        isIE8: isIE8,

        isFF36: isGecko3,

        isFF: isGecko,

        isWebKit: isWebKit,

        isOpera: isOpera,

        isSecure: (''+document.location).split(':')[0] == 'https',

        isLoadReduce: isIE,

        ACTIVE_MAIL: ACTIVE_MAIL,

        SAFE_ACTIVE_MAIL: SAFE_ACTIVE_MAIL,

        TEXT: {
            ERR_INFO_NETWORK: '������ �������� ����������. ��������� ������� ����������� ���������� ����� {0}. ��������, ����� ����������� ������������ ������.',
            ERR_INFO_TOO_MANY_CONNECTIONS: '������� ����� ����������� �� �������� ������������.',
            ERR_INFO_INVALID_USER: '��� ����������� ������ ���������� �������� ��������.'
        },

        resizeableLayout: true,

        apply: apply,

        applyIf: function (to, from) {
            if (from) {
                for (var key in from) {
                    if (typeof to[key] === 'undefined') {
                        to[key] = from[key];
                    }
                }
            }
            return to;
        },

        isPopup: window.__WebAgent__isPopup || false,

        namespace: function (path) {
            var arr = path.split('.');
            var o = window[arr[0]] = window[arr[0]] || {};
            WebAgent.each(arr.slice(1), function (el) {
                o = o[el] = o[el] || {};
            });
            return o;
        },

        getJSON: function () {
            // workaround for Prototype, see: https://prototype.lighthouseapp.com/projects/8886/tickets/730-prototypejs-breaks-firefox-35-native-json
            if (window.JSON && window.JSON.stringify(['a']) == '["a"]') {
                return window.JSON;
            } else {
                return window._JSON;
            }
        },

        getUserLogin: getUserLogin,

        isNumber: function (v) {
            return typeof v === 'number' && isFinite(v);
        },

        isString: function (v) {
            return typeof v === 'string';
        },

        isFunction: function (v) {
            return typeof v === 'function';
        },

        isObject: function (v) {
            return typeof v === 'object';
        },

        isBoolean: function(value) {
            return typeof value === 'boolean';
        },

        isDate: function (v) {
            return Object.prototype.toString.apply(v) === '[object Date]';
        },

        isArray: function (v) {
            // http://ajaxian.com/archives/isarray-why-is-it-so-bloody-hard-to-get-right
            return Object.prototype.toString.apply(v) === '[object Array]';
        },

        toArray: function (v) {
            if (WebAgent.isArray(v)) {
                return v;
            } else {
                return Array.prototype.slice.call(v, 0);
            }
        },

        each: function (arr, fn, scope) {
            var len = arr.length;
            for (var i = 0; i < len; ++i) {
                if (fn.call(scope || window, arr[i], i, arr) === false) {
                    return;
                }
            }
        },

        createDelegate: function (fn, scope, args, appendArgs) {
            return function () {
                var callArgs = WebAgent.toArray(arguments);
                if (appendArgs === true) {
                    callArgs = callArgs.concat(args || []);
                } else if (WebAgent.isNumber(appendArgs)) {
                    callArgs = callArgs.slice(0, appendArgs).concat(args || []);
                }
                return fn.apply(scope || window, callArgs);
            };
        },

        buildId: function (suffix) {
            return 'mailru-webagent-' + suffix;
        },

        buildIconClass: function(status, addDefaultIcon) {
            if (!status || /[<>\"\']/.test(status)) {
                status = 'online';
            }
            return (addDefaultIcon === true ? 'wa-cl-status-default ' : '') + 'wa-cl-status-' + status;
        },

        generateId: function () {
            return WebAgent.buildId('gen-' + UID++);
        },

        emptyFn: function () {
        },

        now: function () {
            return Math.floor(new Date() / 1000);
        },

        setTimeout: function (code, interval, scope) {
            var cb = function () {
                WA.util.Mnt.wrapTryCatch(code, scope);
            };
            if (window.nativeSetTimeout) {
                return window.nativeSetTimeout(cb, interval);
            } else {
                return window.setTimeout(cb, interval);
            }
        },

        setInterval: function (code, interval) {
            var cb = function () {
                WA.util.Mnt.wrapTryCatch(code);
            };
            if (window.nativeSetInterval) {
                return window.nativeSetInterval(cb, interval);
            } else {
                return window.setInterval(cb, interval);
            }
        },

        makeGet: function (hash) {
            var get = [];
            WebAgent.util.Object.each(hash, function (v, k) {
                get[get.length] = k + '=' + encodeURIComponent(v);
            });
            return get.join('&');
        },

        error: function (e) {
            if (isDebug) {
                debugger;
            }
            if(WA.Mnt) {
                WA.Mnt.log(e);
            }
            throw e;
        },

        abstractError: function () {
            WebAgent.error('Abstract method');
        },

        gstat: function (params) {
            if(typeof params == 'string') {
                var name = params;
                params = {};
                params[name] = 1;
            }
            var url = [];
            WebAgent.util.Object.each(params, function (q, n) {
                url.push('webagent.' + n + '=' + q);
            });
            new Image().src='//mail.ru/gstat?' + url.join('&') + '&rnd=' + Math.random();
        },

        makeAvatar: function (mail, type, domain) {
            domain = domain || 'avt.imgsmail.ru';
            var t = mail.match(/([^@]+)@([^\.]+)/i);
            var url = domain + '/' + t[2] + '/' + t[1] + '/' + type;
//            if(WA.isSecure) {
//                return 'https://proxy.imgsmail.ru/avt/?url717=' + url;
//            } else {
                return '//' + url;
//            }
        }

    });

})();
(function () {

    function override(source, overrides) {
        var p = source.prototype;
        WebAgent.apply(p, overrides);
        if (WebAgent.isIE && overrides.hasOwnProperty('toString')) {
            p.toString = overrides.toString;
        }
    }

    WebAgent.apply(WebAgent, {

        extend: function (superclass, overrides) {
            var oc = Object.prototype.constructor;

            var sub;
            if (overrides.constructor != oc) {
                sub = overrides.constructor;
            } else {
                sub = function () {
                    superclass.apply(this, arguments);
                };
            }

            var F = function() {
            };

            var subP;
            var superP = superclass.prototype;

            F.prototype = superP;

            subP = sub.prototype = new F();
            subP.constructor = sub;
            sub.superclass = superP;

            if (superP.constructor == oc) {
                superP.constructor = superclass;
            }

            subP.superclass = function() {
                return superP;
            };

            override(sub, overrides);

            return sub;
        }

    });

})();
(function () {

    var WA = WebAgent;

    // ---------------------------------------------------------------------------------

    var addDomListener = function (dom, eventName, fn, useCapture) {
        if (dom.addEventListener) {
            dom.addEventListener(eventName, fn, !!useCapture);
        } else if (dom.attachEvent) {
            dom.attachEvent('on' + eventName, fn);
        }
    };

    var removeDomListener = function (dom, eventName, fn, useCapture) {
        if (dom.removeEventListener) {
            dom.removeEventListener(eventName, fn, !!useCapture);
        } else if (dom.detachEvent) {
            dom.detachEvent('on' + eventName, fn);
        }
    };

    // ---------------------------------------------------------------------------------


    var EVENT_BUTTON_MAP = WA.isIE ? { b1: 0 , b4: 1, b2: 2 } : { b0: 0, b1: 1, b2: 2 };

    var DomEvent = WA.extend(Object, {

        constructor: function (e) {
            var ev = this.browserEvent = e || {}, doc;

            this.button = ev.button ? EVENT_BUTTON_MAP['b' + ev.button] : (ev.which ? ev.which - 1 : -1);
            if (/(dbl)?click/.test(ev.type) && this.button == -1) {
                this.button = 0;
            }

            this.type = ev.type;
            this.keyCode = ev.keyCode;

            this.shiftKey = ev.shiftKey;
            this.ctrlKey = ev.ctrlKey || ev.metaKey || false;
            this.altKey = ev.altKey;
            this.pageX = ev.pageX;
            this.pageY = ev.pageY;

            if (ev.pageX == null && ev.clientX != null) {
                doc = document.documentElement;
                this.pageX = ev.clientX + (doc && doc.scrollLeft || 0);
                this.pageY = ev.clientY + (doc && doc.scrollTop || 0);
            }
        },

        getKeyCode: function () {
            return this.browserEvent.keyCode;
        },

        getCharCode: function () {
            return this.browserEvent.charCode;
        },

        getTarget: function (returnElement) {
            var node = this.browserEvent.target || this.browserEvent.srcElement;
            if (!node) {
                return null;
            } else {
                var dom = node.nodeType == 3 ? node.parentNode : node;
                return returnElement ? WA.get(dom) : dom;
            }
        },

        stopPropagation: function () {
            var event = this.browserEvent;
            if (event) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else {
                    event.cancelBubble = true;
                }
            }
        },

        preventDefault: function () {
            var event = this.browserEvent;
            if (event) {
                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }
            }
        },

        stopEvent: function () {
            this.stopPropagation();
            this.preventDefault();
        }

    });

    // ---------------------------------------------------------------------------------

    var Container = {

        items: {},

        register: function (el) {
            var id = el.identify();
            if (!this.items[id]) {
                this.items[id] = new ContainerItem(el);
            }
        },

        getElement: function (id) {
            var item = this.items[id];
            if (item && item.isValid()) {
                return item.el;
            } else {
                return null;
            }
        },

        each: function (fn, scope) {
            WA.util.Object.each(this.items, fn, scope || this);
        },

        addListener: function (el, eventName, fn, scope, options) {
            this.register(el);
            var item = this.items[el.getId()];
            item.addListener(eventName, fn, scope, options);
        },

        removeListener: function (el, eventName, fn, scope) {
            var item = this.items[el.getId()];
            if (item) {
                item.removeListener(eventName, fn, scope);
            }
        },

        removeListeners: function (el, recursively, eventName) {
            var item = this.items[el.getId()];
            if (item) {
                item.removeListeners(eventName);
                if (recursively && el.dom.childNodes) {
                    el.each(function (childEl) {
                        this.removeListeners(childEl, recursively, eventName);
                    }, this);
                }
            }
        },

        remove: function (el) {
            var id = el.getId();
            var item = this.items[id];
            if (item) {
                item.destroy();
                delete this.items[id];
            }
        }

    };

    // ---------------------------------------------------------------------------------

    var ContainerItem = WA.extend(Object, {

        constructor: function (el) {
            this.el = el;
            this.events = {};
            this.domHandlers = {};
        },

        isValid: function () {
            var el = this.el;
            var dom = el.dom;
            var isInvalid = !dom || !dom.parentNode || (!dom.offsetParent && !document.getElementById(el.getId()));
            return !isInvalid || dom === document || dom === document.body || dom === window;
        },

        addListener: function (eventName, fn, scope, options) {
            var event = this.events[eventName];
            if (!event) {
                event = this.events[eventName] = new WA.util.Event();
                this.domHandlers[eventName] = WA.createDelegate(
                        function (e, ename) {
                            WA.util.Mnt.wrapTryCatch(function () {
                                this.events[ename].fire(new DomEvent(e), this.el);
                            }, this);
                        },
                        this,
                        [eventName],
                        1
                        );
                if (eventName == 'afterresize') {
                    addDomListener(this.el.dom, 'resize',
                        WA.createDelegate(function (e) {
                            if (this._timer > 0) {
                                clearTimeout(this._timer);
                            }
                            this._timer = WA.setTimeout(
                                WA.createDelegate(function() {
                                    this.domHandlers[eventName].call(window, e);
                                }, this),
                                150
                            );
                        }, this)
                    );
                } else {
                    addDomListener(this.el.dom, eventName, this.domHandlers[eventName], !!(options||{}).useCapture);
                }
            }
            event.on(fn, scope || this.el, options);
        },

        removeListener: function (eventName, fn, scope) {
            var event = this.events[eventName];
            if (event) {
                event.un(fn, scope || this.el);
            }
        },

        removeListeners: function (eventName) {
            if (eventName) {
                var event = this.events[eventName];
                if (event) {
                    event.removeAll();
                }
            } else {
                WA.util.Object.each(this.events, function (ignored, ename) {
                    this.removeListeners(ename);
                }, this);
            }
        },

        destroy: function () {
            var eventNames = WA.util.Object.getKeys(this.events);
            WA.util.Array.each(eventNames, function (eventName) {
                this.removeListeners(eventName);
                removeDomListener(this.el.dom, eventName, this.domHandlers[eventName]);
                delete this.events[eventName];
                delete this.domHandlers[eventName];
            }, this);

            this.events = null;
            this.domHandlers = null;
            this.el = null;
        }

    });

    // ---------------------------------------------------------------------------------

    var garbageCollect = function () {
        Container.each(function (item) {
            if (!item.isValid()) {
                this.remove(item.el);
            }
        });
    };

    WA.setInterval(garbageCollect, 30000);

    // ---------------------------------------------------------------------------------

    var propCache = {},
        camelRe = /(-[a-z])/gi,
        propFloat = WA.isIE ? 'styleFloat' : 'cssFloat';


    function camelFn(mchkCache, a) {
        return a.charAt(1).toUpperCase();
    }

    function chkCache(prop) {
        return propCache[prop] || (propCache[prop] = prop == 'float' ? propFloat : prop.replace(camelRe, camelFn));

    }

    var Element = WA.extend(Object, {

        constructor: function (dom) {
            this.dom = dom;
        },

        getId: function () {
            return this.dom.id;
        },

        identify: function () {
            if (!this.dom.id) {
                return this.dom.id = WA.generateId();
            } else {
                return this.dom.id;
            }
        },

        getWidth: function () {
            return Math.max(this.dom.offsetWidth, this.isVisible() ? 0 : this.dom.clientWidth) || 0;
        },

        getHeight: function () {
            return Math.max(this.dom.offsetHeight, this.isVisible() ? 0 : this.dom.clientHeight) || 0;
        },

        getSize: function () {
            return {
                width: this.getWidth(),
                height: this.getHeight()
            };
        },

        setWidth: function (width) {
            this.dom.style.width = width + 'px';
            return this;
        },

        setHeight: function (height) {
            this.dom.style.height = height + 'px';
            return this;
        },

        setSize: function (width, height) {
            if (WA.isObject(width)) {
                return this.setSize(width.width, width.height);
            } else {
                this.setWidth(width);
                this.setHeight(height);
                return this;
            }
        },

        addListener: function (eventName, fn, scope, optons) {
            Container.addListener(this, eventName, fn, scope, optons);
            return this;
        },

        on: function (eventName, fn, scope, options) {
            return this.addListener(eventName, fn, scope, options);
        },

        removeListener: function (eventName, fn, scope) {
            Container.removeListener(this, eventName, fn, scope);
            return this;
        },

        un: function (eventName, fn, scope) {
            return this.removeListener(eventName, fn, scope);
        },

        removeListeners: function (recursively, eventName) {
            Container.removeListeners(this, recursively, eventName);
            return this;
        },

        setAttribute: function (name, value) {
            this.dom.setAttribute(name, value);
        },

        getAttribute: function (name) {
            return this.dom.getAttribute(name);
        },

        removeClass: function (className) {
            var clsArray = this.dom.className.split(' ');
            this.dom.className = WA.util.Array.remove(clsArray, className).join(' ');
            return this;
        },

        addClass: function (className) {
            if (!this.hasClass(className)) {
                this.dom.className += ' ' + className;
            }
            return this;
        },

        hasClass: function (className) {
            return (' ' + this.dom.className + ' ').indexOf(' ' + className + ' ') != -1;
        },

        toggleClass: function (className, state) {
            var isBool = typeof state === "boolean";
            state = isBool ? state : !this.hasClass(className);
            this[state ? 'addClass' : 'removeClass'](className);
            return this;
        },

        getClass: function () {
            return this.dom.className;
        },

        update: function (html) {
            this.dom.innerHTML = html;
            return this;
        },

        insertFirst: function (el) {
            var newNode = WA.get(el);
            if (this.first()) {
                this.insertBefore(newNode, this.first());
                return newNode;
            }
            return newNode.appendTo(this);
        },

        appendChild: function (el) {
            return WA.get(el).appendTo(this);
        },

        appendTo: function (el) {
            WA.getDom(el).appendChild(this.dom);
            return this;
        },

        insertBefore: function (el, before) {
            WA.getDom(el).insertBefore(this.dom, before);
            return this;
        },

        createChild: function (config, atTheBeginning) {
            var el = WA.util.DomHelper.insertHtml(atTheBeginning === true ? 'afterBegin' : 'beforeEnd', this.dom, config);
            return WA.fly(el);
        },

        setVisible: function (visible) {
            this.dom.style.display = visible ? 'block' : 'none';
            return this;
        },

        isVisible: function () {
            return this.dom.style.display == 'block';
        },

        show: function () {
            return this.setVisible(true);
        },

        hide: function () {
            return this.setVisible(false);
        },

        toggle: function () {
            return this.setVisible(!this.isVisible());
        },

        each: function (fn, scope) {
            var nodes = this.dom.childNodes;
            var len = nodes.length;
            for (var i = 0; i < len; ++i) {
                var node = WA.isFunction(nodes) ? nodes(i) : nodes[i];
                if (node.nodeType == 1) {
                    var el = node.id ? WA.get(node) : WA.fly(node);
                    if (fn.call(scope || this, el, i) === false) {
                        return;
                    }
                }
            }
        },

        matchNode: function (dir, start) {
            var node = this.dom[start];
            while (node) {
                if (node.nodeType == 1) {
                    return WA.get(node);
                }
                node = node[dir];
            }
            return null;
        },

        first: function () {
            return this.matchNode('nextSibling', 'firstChild');
        },

        last: function () {
            return this.matchNode('previousSibling', 'lastChild');
        },

        prev: function () {
            return this.matchNode('previousSibling', 'previousSibling');
        },

        next: function () {
            return this.matchNode('nextSibling', 'nextSibling');
        },

        parent: function () {
            return this.matchNode('parentNode', 'parentNode');
        },

        up: function (cls) {
            var node = this;
            while (node) {
                if (node.hasClass(cls)) return node;
                node = node.parent();
            }
        },

        /* don't rely on this in old browsers, use feature detection */
        offset: function () {
            var box = {top: 0, left: 0};

            if ('getBoundingClientRect' in document.documentElement ) {
                try {
                    box = this.dom.getBoundingClientRect();
                } catch(ex) {}
            } else { /* this method doesn't support old browsers */ }

            var doc = this.dom.ownerDocument,
                docElem = doc.documentElement,
                top = box.top + (window.pageYOffset || docElem.scrollTop),
                left = box.left + (window.pageXOffset || docElem.scrollLeft);

            return {top: top, left: left};
        },

        __removeDom: (function () {
            var d = null;
            return function (node) {
                if (node.tagName != 'BODY') {
                    if (WA.isIE && !WA.isIE8) {
                        d = d || document.createElement('div');
                        d.appendChild(node);
                        d.innerHTML = '';
                    } else {
                        node.parentNode.removeChild(node);
                    }
                }
            };
        })(),

        remove: function () {
            if (this._maskEl) {
                this._maskEl.remove();
                this._maskEl = null;
            }

            Container.remove(this);
            this.__removeDom(this.dom);
            this.dom = null;
        },

        isMasked: function () {
            return this.hasClass('nwa-overlay');
        },

        mask: function () {
            return this.addClass('nwa-overlay');
        },

        unmask: function () {
            return this.removeClass('nwa-overlay');
        },

        isAncestor: function (el) {
            var parent = WA.getDom(el);
            if (parent.contains) {
                return parent.contains(this.dom);
            } else {
                var c = this.dom;
                while (c = c.parentNode) {
                    if (parent === c) {
                        return true;
                    }
                }
                return false;
            }
        },

        contains: function (el) {
            return WA.fly(el).isAncestor(this);
        },

        equals: function (el) {
            return this.dom === el.dom;
        },

        setStyle : function(prop, value){
            var tmp,
                style;
            if (!WA.isObject(prop)) {
                tmp = {};
                tmp[prop] = value;
                prop = tmp;
            }
            for (style in prop) {
                value = prop[style];
                style == 'opacity' ?
                    this.setOpacity(value) :
                    this.dom.style[chkCache(style)] = value;
            }
            return this;
        },

         setOpacity : function(opacity, animate){
            var me = this,
                s = me.dom.style;
            if (WA.isIE) {
                s.zoom = 1;
                s.filter = (s.filter || '').replace(/alpha\([^\)]*\)/gi,"") +
                           (opacity == 1 ? "" : " alpha(opacity=" + opacity * 100 + ")");
            } else {
                s.opacity = opacity;
            }
            return me;
        }

    });

    WA.apply(WA, {

        getDom: function (el) {
            if (el instanceof Element) {
                return el.dom;
            } else if (typeof el === 'string') {
                return document.getElementById(el);
            } else {
                return el;
            }
        },

        fly: function (el) {
            if (el instanceof Element) {
                return el;
            } else {
                return new Element(el);
            }
        },

        get: function (el) {
            if (el instanceof Element) {
                return el;
            } else {
                var dom = WA.getDom(el);
                if (dom) {
                    var ret = Container.getElement(dom.id);
                    if (!ret) {
                        ret = new Element(dom);
                        Container.register(ret);
                    }
                    return ret;
                } else {
                    return null;
                }
            }
        },

        getBody: function () {
            return new Element(document.body);
        },

        getDoc: function () {
            return new Element(document);
        }

    });

})();
WebAgent.namespace('WebAgent');

(function () {

    var WA = WebAgent;

    var OVERRIDES = {

        isActive: function () {
            return this.__isActive === true;
        },

        activate: function (params) {
            if (!this.isActive()) {
                this.__isActive = true;
                return this._onActivate(params) !== false;
            } else {
                return false;
            }
        },

        _onActivate: function (params) {
        },

        deactivate: function (params) {
            if (this.isActive()) {
                this.__isActive = false;
                return this._onDeactivate(params) !== false;
            } else {
                return false;
            }
        },

        _onDeactivate: function (params) {
        }

    };

    WA.Activatable = WA.extend(Object, OVERRIDES);

    WA.Activatable.extend = function (superclass, overrides) {
        var sp = WA.extend(superclass, OVERRIDES);
        return WA.extend(sp, overrides);
    };

})();
WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    var Animation = U.Animation = WA.extend(Object, {

        constructor: function () {
            this._onFrameScope = WA.createDelegate(this._onFrame, this);
            this._list = {};
            this._index = -1;
        },

        _requestFrame: function () {
            this.requestAnimationFrame.call(window, this._onFrameScope);
        },

        start: function (from, to, pxPerMs, cb, scope) {
            if(!this._started) {
                this._started = true;
                this._requestFrame();
            }

            this._index++;
            this._list[this._index] = {
                from: from,
                to: to,
                pxPerMs: pxPerMs,
                cb: cb,
                scope: scope,
                date: +new Date(),
                first: true
            };
            return this._index;
        },

        clear: function (index) {
            delete this._list[index];
        },

        _onFrame: function () {
            var count = 0;
            U.Object.each(this._list, function (v, index) {
                count++;
                var value;
                var d = (+new Date() - v.date) * v.pxPerMs;
                var lastFrame = false;
                if(v.from < v.to) {
                    value = v.from + d;
                    if(value > v.to) {
                        lastFrame = true;
                        value = v.to;
                    }
                } else {
                    value = v.from - d;
                    if(value < v.to) {
                        lastFrame = true;
                        value = v.to;
                    }
                }

                v.cb.call(v.scope || window, value, lastFrame, v.first);
                
                v.first = false;
                if(lastFrame) {
                    this.clear(index);
                }
            }, this);
            
            if(count) {
                this._requestFrame();
            } else {
                this._started = false;
            }
        },

        requestAnimationFrame: (function(){
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function(callback, element){
                    WA.setTimeout(callback, 1000 / 60);
                };
        })()

    });

    U.animation = new U.Animation();

})();WebAgent.namespace('WebAgent.util');

(function () {

    var A = WebAgent.util.Array = {

        each: function (arr, fn, scope) {
            WebAgent.each(arr, fn, scope);
        },
        
        eachReverse: function (arr, fn, scope) {
            var j = 0;
            for (var i = arr.length; i--;) {
                if (fn.call(scope || window, arr[i], j, arr) === false) {
                    return;
                }
                j++;
            }            
        },

        transform: function (arr, fn, scope) {
            var ret = [];
            A.each(arr, function () {
                var el = fn.apply(this, arguments);
                ret.push(el);
            }, scope);
            return ret;
        },

        filter: function (arr, fn, scope) {
            var ret = [];
            A.each(arr, function (el, index, allItems) {
                if (fn.call(this, el, index, allItems)) {
                    ret.push(el);
                }
            }, scope);
            return ret;
        },

        indexOf: function (arr, el) {
            var fn = null;
            if (el instanceof RegExp) {
                fn = function (entry) {
                    return el.test(entry);
                };
            } else {
                fn = function (entry) {
                    return el === entry;
                };
            }

            return A.indexOfBy(arr, fn);
        },

        indexOfBy: function (arr, fn, scope) {
            var ret = -1;
            A.each(arr, function (el, index) {
                if (fn.call(scope || window, el, index) === true) {
                    ret = index;
                    return false;
                }
            });
            return ret;
        },

        findBy: function (arr, fn, scope) {
            var index = A.indexOfBy(arr, fn, scope);
            if (index != -1) {
                return arr[index];
            } else {
                return null;
            }
        },

        removeAt: function (arr, index) {
            arr.splice(index, 1);
            return arr;
        },

        remove: function (arr, el) {
            var index = A.indexOf(arr, el);
            if (index != -1) {
                return A.removeAt(arr, index);
            } else {
                return arr;
            }
        },

        removeBy: function (arr, fn, scope) {
            var index = A.indexOfBy(arr, fn, scope);
            if (index != -1) {
                return A.removeAt(arr, index);
            } else {
                return null;
            }
        },

        clone: function (arr) {
            return Array.prototype.slice.call(arr);
        }

    };

})();
WebAgent.namespace('WebAgent.util');

(function () {

    WebAgent.util.Date = {

        getElapsed: function (date1, date2) {
            date2 = date2 || new Date();
            return date2.getTime() - date1.getTime();
        }

    };

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;

    WA.util.DelayedTask = WA.extend(Object, {

        constructor: function (config) {
            WA.apply(this, config);

            this.id = null;

            if (this.fn) {
                this._initFn(this.fn, this.scope);
            }
        },

        _initFn: function (fn, scope) {
            this.scope = scope || this.scope || this;
            this.fn = WA.createDelegate(fn, this.scope);
        },

        isStarted: function () {
            return this.id != null;
        },

        start: function (interval, fn, scope) {
            if (this.isStarted()) {
                this.stop();
            }

            if (interval > 0) {
                this.interval = interval;
            }

            if (fn) {
                this._initFn(fn, scope);
            }

            this.id = WA.setTimeout(this.fn, this.interval);
        },

        stop: function () {
            if (this.isStarted()) {
                clearTimeout(this.id);
                this.id = null;
            }
        }

    });

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    var tableRe = /^(?:table|tbody|tr|td)$/i;
    var tableElRe = /^(?:td|tr|tbody)$/i;
    var emptyTags = /^(?:br|frame|hr|img|input|link|meta|range|spacer|wbr|area|param|col)/i;

    var tempTableEl = null;
    var afterbegin = 'afterbegin',
            afterend = 'afterend',
            beforebegin = 'beforebegin',
            beforeend = 'beforeend',
            ts = '<table>',
            te = '</table>',
            tbs = ts + '<tbody>',
            tbe = '</tbody>' + te,
            trs = tbs + '<tr>',
            tre = '</tr>' + tbe;

    function ieTable(depth, s, h, e) {
        tempTableEl.innerHTML = [s, h, e].join('');

        var i = -1;
        var el = tempTableEl;
        var ns;

        while (++i < depth) {
            el = el.firstChild;
        }

        if (ns = el.nextSibling) {
            var df = document.createDocumentFragment();
            while (el) {
                ns = el.nextSibling;
                df.appendChild(el);
                el = ns;
            }
            el = df;
        }

        return el;
    }

    function insertIntoTable(tag, where, el, html) {
        var node, before;

        tempTableEl = tempTableEl || document.createElement('div');

        if (tag == 'td' && (where == afterbegin || where == beforeend) ||
                !tableRe.test(tag) && (where == beforebegin || where == afterend)) {
            return null;
        }
        before = where == beforebegin ? el :
                where == afterend ? el.nextSibling :
                        where == afterbegin ? el.firstChild : null;

        if (where == beforebegin || where == afterend) {
            el = el.parentNode;
        }

        if (tag == 'td' || (tag == 'tr' && (where == beforeend || where == afterbegin))) {
            node = ieTable(4, trs, html, tre);
        } else if ((tag == 'tbody' && (where == beforeend || where == afterbegin)) ||
                (tag == 'tr' && (where == beforebegin || where == afterend))) {
            node = ieTable(3, tbs, html, tbe);
        } else {
            node = ieTable(2, ts, html, te);
        }
        el.insertBefore(node, before);
        return node;
    }

    function createHtml(o) {
        var html = '';
        if (WA.isString(o)) {
            html = o;
        } else if (WA.isArray(o)) {
            U.Array.each(o, function (entry) {
                html += createHtml(entry);
            });
        } else {
            var tag = o.tag || 'div';
            html = '<' + tag;
            var attrs = [];
            U.Object.each(o, function (val, attr) {
                if (U.Array.indexOf(['tag', 'children', 'html'], attr) == -1) {
                    attrs.push(('cls' === attr ? 'class' : attr) + '="' + val + '"');
                }
            });
            if (attrs.length > 0) {
                html += ' ' + attrs.join(' ');
            }

            if (emptyTags.test(tag)) {
                html += '/>';
            } else {
                html += '>';
                if (o.children) {
                    html += createHtml(o.children);
                } else if (o.html) {
                    html += o.html;
                }
                html += '</' + tag + '>';
            }
        }
        return html;
    }

    var DH = U.DomHelper = {

        append: function (el, html, returnElement) {
            return DH.insertHtml('beforeEnd', el, html, returnElement);
        },

        insertHtml: function (where, el, html, returnElement) {
            var hash = {},
                    hashVal,
                    setStart,
                    range,
                    frag,
                    rangeEl,
                    rs;

            where = where.toLowerCase();
            html = createHtml(html);

            hash[beforebegin] = ['BeforeBegin', 'previousSibling'];
            hash[afterend] = ['AfterEnd', 'nextSibling'];

            if (el.insertAdjacentHTML) {
                if (tableRe.test(el.tagName) && (rs = insertIntoTable(el.tagName.toLowerCase(), where, el, html))) {
                    return returnElement ? WA.get(rs) : rs;
                }

                hash[afterbegin] = ['AfterBegin', 'firstChild'];
                hash[beforeend] = ['BeforeEnd', 'lastChild'];
                if ((hashVal = hash[where])) {
                    el.insertAdjacentHTML(hashVal[0], html);
                    rs = el[hashVal[1]];
                    return returnElement ? WA.get(rs) : rs;
                }
            } else {
                range = el.ownerDocument.createRange();
                setStart = 'setStart' + (/end/i.test(where) ? 'After' : 'Before');
                if (hash[where]) {
                    range[setStart](el);
                    frag = range.createContextualFragment(html);
                    el.parentNode.insertBefore(frag, where == beforebegin ? el : el.nextSibling);
                    rs = el[(where == beforebegin ? 'previous' : 'next') + 'Sibling'];
                    return returnElement ? WA.get(rs) : rs;
                } else {
                    rangeEl = (where == afterbegin ? 'first' : 'last') + 'Child';
                    if (el.firstChild) {
                        range[setStart](el[rangeEl]);
                        frag = range.createContextualFragment(html);
                        if (where == afterbegin) {
                            el.insertBefore(frag, el.firstChild);
                        } else {
                            el.appendChild(frag);
                        }
                    } else {
                        el.innerHTML = html;
                    }
                    rs = el[rangeEl];
                    return returnElement ? WA.get(rs) : rs;
                }
            }
            WA.error('Illegal insertion point -> "' + where + '"');
        },

        whenReady: function (fn, scope) {
            easyXDM.whenReady(fn, scope);
        },

        rapidHtmlInsert: function (html) {
            var before = document.body.getElementsByTagName('*')[0];
            if (document.body && before) {
                document.body.insertBefore(document.createElement('div'), before).innerHTML = createHtml(html);
            } else {
                WA.setTimeout(function () {
                    DH.rapidHtmlInsert(html);
                }, 10);
            }
        },

        htmlEntities: function (str) {
            return (str || '').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }

    };

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    // -----------------------------------------------------------------------------------------------

    var BaseListener = WA.extend(Object, {

        constructor: function (fn, scope) {
            this.fn = fn;
            this.scope = scope;
        },

        equals: function (fn, scope) {
            if (this.fn === fn) {
                if (scope) {
                    if (this.scope === scope) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        },

        invoke: function (args) {
            if (this.fn) {
                return this.fn.apply(this.scope || window, args);
            }
        },

        destroy: function () {
            delete this.fn;
            delete this.scope;
        }

    });

    // -----------------------------------------------------------------------------------------------

    var SingleListener = WA.extend(BaseListener, {

        constructor: function (fn, scope, owner) {
            SingleListener.superclass.constructor.call(this, fn, scope);
            this.owner = owner;
        },

        invoke: function (args) {
            var ret = SingleListener.superclass.invoke.call(this, args);
            this.owner.removeListener(this.fn, this.scope);
            
            return ret;
        },

        destroy: function () {
            SingleListener.superclass.destroy.call(this);
            delete this.owner;
        }

    });

    // -----------------------------------------------------------------------------------------------

    U.Event = WA.extend(Object, {

        constructor: function () {
            this.suspended = false;
            this.listeners = [];
        },

        hasListeners: function () {
            return this.listeners.length > 0;
        },

        addListener: function (fn, scope, options) {
            options = options || {};

            var listener = null;

            if (options.single === true) {
                listener = new SingleListener(fn, scope, this);
            } else {
                listener = new BaseListener(fn, scope);
            }

            this.listeners.push(listener);

            return this;
        },

        on: function (fn, scope, options) {
            return this.addListener(fn, scope, options);
        },

        findListener: function (fn, scope) {
            return U.Array.indexOfBy(this.listeners, function (listener) {
                return listener.equals(fn, scope);
            });
        },

        removeListener: function (fn, scope) {
            var index = this.findListener(fn, scope);
            if (index != -1) {
                var listener = this.listeners[index];
                listener.destroy();
                if (this.firing) {
                    // TODO: lolwut?
                    this.__listeners = this.listeners.slice(0);
                }
                this.listeners.splice(index, 1);
            }
            return this;
        },

        un: function (fn, scope) {
            return this.removeListener(fn, scope);
        },

        fire: function () {
            var ret = true;
            if (!this.suspended) {
                var args = arguments;
                this.firing = true;
                var listeners = this.listeners.slice(0);
                U.Array.each(listeners, function (listener, i) {
                    if (listener.invoke(args) === false) {
                        return ret = false;
                    }
                }, this);
                this.firing = false;
                if (this._needRemoveAll) {
                    delete this._needRemoveAll;
                    this.removeAll();
                }
            }
            return ret;
        },

        removeAll: function () {
            if (!this.firing) {
                while (this.listeners.length > 0) {
                    var listener = this.listeners.shift();
                    listener.destroy();
                }
            } else {
                this._needRemoveAll = true;
            }
        },

        relay: function (event) {
            event.on(function () {
                return this.fire.apply(this, arguments);
            }, this);
            return this;
        },

        suspend: function () {
            this.suspended = true;
        },

        resume: function () {
            this.suspended = false;
        }

    });

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    U.EventConfirm = WA.extend(U.Event, {
        fire: function () {
            var ret = true;
            if (!this.suspended) {
                this.firing = true;
                
                var args = Array.prototype.slice.call(arguments, 0);
                var readyCb = args.shift();
                if(readyCb && readyCb.success) {
                    readyCb = WA.createDelegate(readyCb.success, readyCb.scope || window);
                }

                var confirmCount = 0;
                var checkReady = function () {
                    if(confirmCount == 0) {
                        readyCb && readyCb();
                    }
                };

                args.push(function () {
                    confirmCount--;
                    checkReady();
                });

                var listeners = this.listeners.slice(0);
                U.Array.each(listeners, function (listener, i) {
                    var r = listener.invoke(args);
                    if (r === false) {
                        return ret = false;
                    } else  if(r === true) {
                        confirmCount++;
                    }
                }, this);
                checkReady();
                
                this.firing = false;
            }
            return ret;
        }
    });

})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;
    var U = WA.util;

    U.EventDispatcher = WA.extend(Object, {

        constructor: function () {
            this._listeners = {};
        },

        addEventListener: function (type, cb) {
            if(!this._listeners[type]) {
                this._listeners[type] = [];
            }

            this._listeners[type].push(cb);
        },

        dispatchEvent: function (type, event) {
            if(this['on'+ type]) {
                this['on'+ type](event);
            }

            var listeners = this._listeners[type];
            if(listeners) {
                for(var i = 0; i<listeners.length; i++) {
                    listeners[i].call(window, event);
                }
            }
        },

        removeEventListener: function (type, cb) {
            var listeners = this._listeners[type];
            if(listeners) {
                var res = [];
                for(var i = 0; i<listeners.length; i++) {
                    if(listeners[i] !== cb) {
                        res.push(listeners[i])
                    }
                }
                this._listeners[type] = res;
            }
            
        },

        destroy: function () {
            // not implemented yet
        }

    });


})();WebAgent.namespace('WebAgent.util');

(function () {

    var WA = WebAgent;

    var srcChar = "qwertyuiopasdfghjklzxcvbnm[{]};:',<.> ��������������������������������";
    var mapChar = "������������������������������������� qwertyuiop[]asdfghjkl;'zxcvbnm,.";
    var delimPos = 37;

    WA.util.KeyMapping = {

        translate: function (word) {
            var alterWord = '',
                len = 0,
                lang = null,
                pos = -1,
                char = '',
                i = 0;

            word = (word || '').toLowerCase();
            len = word.length;

            while (i < len) {
                char = word[i++];
                pos = srcChar.indexOf(char);
                if (!lang) {
                    // detect base language
                    if (pos > -1 && pos < delimPos) {
                        lang = 'en';
                    } else if (pos > delimPos) {
                        lang = 'ru';
                    }
                }

                if (lang == 'ru' && pos > delimPos || lang == 'en' && pos < delimPos && pos > -1) {
                    char = mapChar[pos];
                }

                alterWord += char;
            }

            return alterWord;
        }
    };

})();WebAgent.namespace('WebAgent.util');

(function () {
    var WA = WebAgent;
    var U = WA.util;
    var JSON = WA.getJSON();
    var S;

    var browser = window.opera && 'op' ||
        window.chrome && 'ch' ||
        window.WebKitAnimationEvent && 'sf' ||
        document.attachEvent && 'ie' ||
        'mozInnerScreenX' in window && 'ff' ||
        'na';

    var DAY = 1000 * 60 * 60 * 24;

    var usedBranch = WA.usedBranch;
    if(!usedBranch) {
        usedBranch = (document.location.search.match(/&usedBranch=([^=&]+)/) || []) [1];
    }
    var shortDomain = document.location.host.replace(/(?:^|\.)(\w)[^\.]+/g, '$1');
    var FLUSH_DELAY = 10 * 60000;
    var FLUSH_DIALOGS_MAX_INTERVAL = DAY;
    //var PREFIX = ('UA-' + browser + '_D-' + shortDomain + '_B-' + WA.usedBranch).substr(0, 20);
    var PREFIX = ('B-' + usedBranch).substr(0, 20);
    //var GRAPHITE_PREFIX = 'webagent.' + shortDomain + '.' + WA.usedBranch + '.' + browser + '.';
    var GRAPHITE_PREFIX = 'webagent.' + usedBranch + '.';

    var M = WA.extend(Object, {

        constructor: function () {
            var now = this._timeStart = +new Date(),
                elapsed = now - (localStorage.waMonitoringFlushDate || 0),
                timeleft = FLUSH_DELAY - elapsed;

            this.onBeforeFlushEvent = new U.Event();

            if (elapsed > FLUSH_DELAY - 5000) {
                localStorage.waMonitoringFlushDate = now;
                timeleft = 5000;
            }

            this._flushTask = new U.DelayedTask({
                fn: this._flush,
                scope: this
            });
            this._flushTask.start(timeleft);
            
            this._storeTask = new U.DelayedTask({
                interval: 0,
                fn: this._store,
                scope: this
            });

            this._flushDialogsMaxTask = new U.DelayedTask({
                fn: this._flushDialogsMaxFn,
                scope: this
            });
            new U.DelayedTask().start(10, this._flushDialogsMaxInit, this);

            WA.fly(window).un('beforeunload', this._flush, this);
            WA.fly(window).un('unload', this._flush, this);
        },

        activeUserCount: function () {
            if(!this._userCounted && WA.Storage) {
                this._userCounted = true;
                S || (S = WA.Storage);

                S.load(['activeUserCountDate'], {
                    success: function (storage) {
                        var now = +new Date();
                        var date = +storage['activeUserCountDate'];
                        if(!date || now - date > 1000*60*60*24) {
                            this.count('activeUser');
                            S.save({
                                activeUserCountDate: now
                            });
                        }
                    },
                    scope: this
                })
            }
        },

        _flushDialogsMaxInit: function () {
            if(!WA.Storage) { // in rpc.html do nothing
                return;
            }

            WA.Storage.whenReady(function () {
                S = WA.Storage;
                S.load(['dialogsStat'], {
                    success: function (storage) {
                        var now = +new Date();
                        var stat = JSON.parse(storage['dialogsStat']||0) || {d: now, c: 0};
                        var d = FLUSH_DIALOGS_MAX_INTERVAL - (now - stat.d);
                        this._flushDialogsMaxTask.start(d<0 ? 0 : d);
                    },
                    scope: this
                })
            }, this);
        },

        _flushDialogsMaxFn: function () {
            S.load(['dialogsStat'], {
                success: function (storage) {
                    var now = +new Date();
                    var stat = JSON.parse(storage['dialogsStat']||0);
                    if(stat && stat.c) {
                        this.count('dialogsMaxUniq' + (stat.c > 6 ? 'M' : stat.c));
                    }
                    S.save({
                        'dialogsStat': JSON.stringify({
                            d: now,
                            c: 0
                        })
                    });
                },
                scope: this
            });
        },

        increaseDialogMax: function (count) {
            S || (S = WA.Storage);

            S.load(['dialogsStat'], {
                success: function (storage) {
                    var stat = JSON.parse(storage['dialogsStat']||0) || {c: 0};

                    if(count > stat.c) {
                        S.save({
                            'dialogsStat': JSON.stringify({
                                d: +new Date(),
                                c: count
                            })
                        });
                        this._flushDialogsMaxTask.start(FLUSH_DIALOGS_MAX_INTERVAL);
                    }
                },
                scope: this
            }, this);
        },

        _ts: {},
        _intervals: [],
        _counters: {},
        _timeSum: 0,
        _timeStart: 0,

        log: function (params) {
            try {
                if(typeof params == 'object') {
                    params =  WA.makeGet(params)
                }
            } catch(e) {}
            
            new Image().src='//mrilog.mail.ru/empty.gif?' + params + '&WALOG&resPath=' + WebAgent.resPath + '&location=' + document.location;
        },

        wrapTryCatch: function (cb, scope) {
            var t1 = +new Date();
            
            if(WA.isProduction) {
                try {
                    cb.call(scope || window);
                } catch (e) {
                    U.Mnt.count('errorsJS');
                    
                    e = e || {};
                    if (typeof e == 'string') {
                        e = {
                            message: e
                        };
                    }
                    try {
                        var err = {
                            login: WA.ACTIVE_MAIL,
                            type: e.name,
                            message: e.message,
                            line: e.line || e.lineNumber || e.number,
                            file: e.fileName,
                            stack: e.stack
                        };
                        this.log(WA.makeGet(err));
                    } catch (e) {
                        this.log('error on stringify error: ' + e);
                    }
                }
            } else {
                cb.call(scope || window);
            }

            var t2 = +new Date();
            this._timeSum += t2 - t1;
        },

        count: function (param, q) {
            if(!this._counters[param]) {
                this._counters[param] = 0;
            }
            this._counters[param] += (q === undefined ? 1 : q);
            this._storeTask.start();

            if(param == 'submitByButton' || param == 'submitByEnter') {
                var id = ({
                    'otvet': '790314',
                    'webagent': '726182',
                    'e': '726182',
                    'my': '726184',
                    'foto': '726184',
                    'video': '726184',
                    'news': '726185',
                    'maps': '726193',
                    'pogoda': '827834',
                    'health': '827835'
                })[location.host.split('.')[0]];
                if (id) {
                    new Image().src = '//rs.mail.ru/d' + id + '.gif?' + Math.random();
                }
            }
        },

        begin: function (name) {
            this._ts[name] = +new Date();

        },

        end: function (name) {
            if(this._ts[name]) {
                this._intervals.push(name.substr(0, 20) + ':' + (+new Date() - this._ts[name]));
                this._storeTask.start();
            }
        },

        _store: function () {
            if( this._intervals.length ) {
                localStorage['waMonitoringI_' + (+new Date())] = this._intervals.join(',');
                this._intervals = [];
            }

            var isCounterExist = false;
            U.Object.each(this._counters, function () {
                isCounterExist = true;
                return false;
            });
            if( isCounterExist ) {
                localStorage['waMonitoringC_' + (+new Date())] = JSON.stringify(this._counters);
                this._counters = {};
            }
        },

        setStartOccupancy: function () {
            this._isFirstFlush = true;
        },

        _flush: function () {
            if(!usedBranch) {
                return;
            }

            if( this._intervals.length ) {
                this._flushTask.start(5000);
                return;
            }
            
            this.onBeforeFlushEvent.fire();

            var now = +new Date();
            var occupancy = this._timeSum / (now - this._timeStart) * 100;
            this._timeSum = 0;
            this._timeStart = now;


            var parts = [(this._isFirstFlush ? 'occupancyStart:' : 'occupancy:') + occupancy.toFixed(2)];
            this._isFirstFlush = false;
            var intervalsNotEmpty = false;
            
            var deleteList = [];
            var counters = {};
            var dump = [];
            for(var i=0, len = localStorage.length; i < len; i++) {
                var key = localStorage.key(i);
                if(!key) {
                    continue;
                }
                var value = localStorage[key];
                
                if(key.indexOf('waMonitoringI_') == 0) {
                    intervalsNotEmpty = true;
                    var s = parts[parts.length-1] + ',' + value;
                    if(s.length > 1000) {
                        parts.push(value);
                    } else {
                        parts[parts.length-1] = s;
                    }

                    deleteList.push(key);
                } else if(key.indexOf('waMonitoringC_') == 0) {
                    U.Object.each(JSON.parse(value), function (v, k) {
                        if( !counters[k] ) {
                            counters[k] = 0;
                        }
                        counters[k] += v;
                    }, this);

                    deleteList.push(key);
                } else {
                    dump.push([key, value]);
                }
            }

            if(dump.length < deleteList.length) {
                localStorage.clear();
                U.Array.each(dump, function (v) {
                    localStorage[v[0]] = v[1];
                });
            } else {
                U.Array.each(deleteList, function (key) {
                    localStorage.removeItem(key);
                });
            }

            var reqcount = 0, graphiteCount = 0, radarCount = 0;
            if(intervalsNotEmpty) {
                U.Array.each(parts, function (part) {
                    new Image().src = '//webagent.radar.imgsmail.ru/update?p=webagent&t=' + PREFIX + '&v=0&i=' + part + '&rnd=' + Math.random();
                    reqcount++;
                    radarCount++;
                });
            }

//            var buff = GRAPHITE_PREFIX + 'sum=1';
            var buff = '';
            U.Object.each(counters, function (v, k) {
                buff += (buff != '' ? '&' : '') + GRAPHITE_PREFIX + k + '=' + v;
                if(buff.length > 1000) {
                    new Image().src = '//mail.ru/gstat?' + buff + '&rnd=' + Math.random();
                    buff = '';
                    reqcount++;
                    graphiteCount++;
                }
            }, this);
            if(buff != '') {
                new Image().src = '//mail.ru/gstat?' + buff + '&rnd=' + Math.random();
                reqcount++;
                graphiteCount++;
            }

            if(reqcount) {
                this.count('monitoringReqCount', reqcount);
                this.count('monitoringReqCountGraphite', graphiteCount);
                this.count('monitoringReqCountRadar', radarCount);
            }

            this._flushTask.start(FLUSH_DELAY);
        },

        rbCountOpen: function () {
            this._countRB(706711);
        },

        rbCountAction: function () {
            this._countRB(706784);
        },

        _countRB: function (id) {
            var url = '//rs.mail.ru/d' + id + '.gif';
            WA.setTimeout(function(){
                new Image().src = url + '?rnd=' + Math.random();
            }, 0);
        }

    });

    U.Mnt = new M();

})();

WebAgent.namespace('WebAgent.util');

(function () {

    var O = WebAgent.util.Object = {

        each: function (obj, fn, scope) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (fn.call(scope || window, obj[key], key) === false) {
                        return;
                    }
                }
            }
        },

        pair: function (key, value) {
            var o = {};
            o[key] = value;
            return o;
        },

        getKeys: function (obj) {
            var keys = [];
            O.each(obj, function (ignored, key) {
                keys.push(key);
            });
            return keys;
        }

    };

})();WebAgent.namespace('WebAgent.util');

(function () {

    var S = WebAgent.util.String = {

        format: function () {
            var args = WebAgent.toArray(arguments);
            var format = args.shift();
            return format.replace(/\{(\d+)\}/g, function (ignored, i) {
                return args[i];
            });
        },

        capitalize: function (value) {
            return value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
        },

        parseTemplate: function (template, data) {
            return template.replace(/\{([^\}]+)\}/g, function (ignored, p) {
                return data[p];
            });
        },

        pluralize: function (num, zero, one, two) {
            return (num % 10 == 1 && num % 100 !== 11 ? one : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? two : zero));
        },
        
        htmlEntity: function (text) {
            return (text || '').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        },

        entityDecode: function (text) {
            return (text || '').replace(/&lt;/ig, '<').replace(/&gt;/ig, '>').replace(/&quot;/ig, '"');
        },

        ellipsis: function (str, len) {
            if (str.length > len) {
                return str.substr(0, len - 3) + '...';
            } else {
                return str;
            }
        },

        formatDate: function (ts) {
            
            // TODO: COPYPASTA!!!
            
            var tso = new Date(ts-0);
            var msg_date = tso.toLocaleTimeString().replace(/:\d\d(\sgmt.*)?$/i, '');
            if( ts + 86400000 < (new Date()).valueOf() )	{	// 86400000 = 1000*60*60*24
                var splitter = '.';
                var den = tso.getDate();
                var mes = tso.getMonth()+1;
                mes+='';
                if (mes.length==1)
                    mes='0'+mes;
                var god = tso.getYear()+'';
                god = god.slice(1,3);
                msg_date =  den + splitter + mes + splitter + god +' '+ msg_date;
            }
            return msg_date

        },

        quote: (function () {

            var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
                escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
                gap,
                indent,
                meta = {    // table of character substitutions
                    '\b': '\\b',
                    '\t': '\\t',
                    '\n': '\\n',
                    '\f': '\\f',
                    '\r': '\\r',
                    '"' : '\\"',
                    '\\': '\\\\'
                },
                rep;


            return function (string) {
                escapable.lastIndex = 0;
                return escapable.test(string) ?
                    '"' + string.replace(escapable, function (a) {
                        var c = meta[a];
                        return typeof c === 'string' ? c :
                            '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                    }) + '"' :
                    '"' + string + '"';
            }
        })(),

        trim: function (str) {
            return str.replace(/^\s+|\s+$/g, '');
        }

    };

})();(function () {

    var WA = WebAgent;
    var extend = WA.extend;

    // -----------------------------------------------------------------------------------------------

    var AbstractStorage = extend(Object, {

        _prefix: '_STRG_',

        load: function (key) {
        },

        save: function (key, value) {
        },

        remove: function (key) {
        },

        clear: function () {
        }

    });

    // -----------------------------------------------------------------------------------------------

    var StorageProxy = extend(AbstractStorage, {

        constructor: function (storage) {
            this.storage = storage;
        },

        _processLoadSuccess: function (successFn, data, loadOptions) {
            if (WA.isFunction(successFn)) {
                successFn(data, loadOptions);
            }

            return {
                success: true,
                result: data
            };
        },

        _processLoadError: function (errorFn, e) {
            if (WA.isFunction(errorFn)) {
                errorFn(e);
            }

            return {
                success: false,
                error: e
            };
        },

        load: function (key, successFn, errorFn) {
            var data = null;

            var options = {
                params: key
            };

            if (WA.isArray(key)) {
                try {
                    data = {};
                    WA.each(key, function (k) {
                        data[k] = this.storage.load(k);
                    }, this);
                } catch (e) {
                    return this._processLoadError(errorFn, e);
                }
                return this._processLoadSuccess(successFn, data, options);
            } else {
                try {
                    data = this.storage.load(key);
                } catch (e) {
                    return this._processLoadError(errorFn, e);
                }
                return this._processLoadSuccess(successFn, data, options);
            }
        },

        save: function (data, successFn, errorFn) {
            try {
                WA.util.Object.each(data, function (value, key) {
                    this.storage.save(key, value);
                }, this);
            } catch (e) {
                if (WA.isFunction(errorFn)) {
                    errorFn(e);
                }
                return false;
            }
            if (WA.isFunction(successFn)) {
                successFn();
            }
            return true;
        },

        remove: function (key, successFn, errorFn) {
            if (WA.isArray(key)) {
                try {
                    WA.each(key, function (k) {
                        this.storage.remove(k);
                    }, this);
                } catch (e) {
                    errorFn(e);
                    return;
                }
                successFn();
            } else {
                try {
                    this.storage.remove(key);
                } catch (e) {
                    errorFn(e);
                    return;
                }
                successFn();
            }
        },

        clear: function (successFn, errorFn) {
            try {
                this.storage.clear();
            } catch (e) {
                errorFn(e);
                return;
            }
            successFn();
        }

    });

    // -----------------------------------------------------------------------------------------------

    var LocalStorage = extend(AbstractStorage, {

        constructor: function () {
            this._preventiveCleanup = 3;
        },

        load: function (key) {
            return localStorage[this._prefix + key] || '';
        },

        save: function (key, value) {
            key = this._prefix + key;
            var backup = localStorage[key];

            try {
                if (!this._preventiveCleanup) {
                    localStorage.removeItem(key);
                }
                localStorage[key] = value;
            } catch (e) {
                try {
                    localStorage.removeItem(key);
                    localStorage[key] = value;

                    if (this._preventiveCleanup) {
                        this._preventiveCleanup--;
                    }
                } catch (e) {
                    localStorage[key] = backup;
                }
                throw 'Writing failed';
            }
        },

        remove: function (key, cb) {
            key = this._prefix + key;
            try {
                localStorage.removeItem(key);
            } catch (e) {
                localStorage[key] = '';
                throw e;
            }
        },

        clear: function (cb) {
            localStorage.clear();
            cb();
        }

    });

    // -----------------------------------------------------------------------------------------------

    var UserData = extend(AbstractStorage, {

        _storageName: 'storageUserData',

        constructor: function () {
            WA.util.DomHelper.rapidHtmlInsert('<div id="ud" style="position:absolute; top:0; left:-1000px;"></div>');
            this._el = WebAgent.getDom('ud');
            this._el.addBehavior("#default#userData");
        },

        load: function (key) {
            key = (this._prefix + key).replace(/\$/g, '-');
            this._el.load(this._storageName);
            return decodeURIComponent(this._el.getAttribute(key) || '');
        },

        save: function (key, value) {
            key = (this._prefix + key).replace(/\$/g, '-');
            this._el.setAttribute(key, encodeURIComponent(value));
            this._el.save(this._storageName);
        },

        remove: function (key) {
            key = (this._prefix + key).replace(/\$/g, '-');
            this._el.removeAttribute(key);
            this._el.save(this._storageName);
        },

        clear: function () {
            // TODO
        }

    });

    // -----------------------------------------------------------------------------------------------

    var Cookie = extend(AbstractStorage, {

        _getKey: function (key) {
            return encodeURIComponent(this._prefix + key);
        },

        _setCookie: function (name, value, expires, path, domain, secure) {
            document.cookie = name + '=' + encodeURIComponent(value) +
                ((expires) ? '; expires=' + new Date(expires).toUTCString() : '') +
                ((path) ? '; path=' + path : '') +
                ((domain) ? '; domain=' + domain : '') +
                ((secure) ? '; secure' : '');
        },

        load: function (key) {
            return decodeURIComponent(((''+document.cookie).match(new RegExp(this._getKey(key) + '=([^;]+)')) || [0,''])[1]);
        },

        save: function (key, value) {
            this._setCookie(this._getKey(key), value, 0);
        },

        remove: function (key) {
            this._setCookie(this._getKey(key), '', +new Date() -31536000);
        },

        each: function (cb, scope) {
            (''+document.cookie).replace(new RegExp(this._prefix + '([^=]+)=([^;]*)', 'g'), function (nope, key, value) {
                cb.call(scope || window, decodeURIComponent(value), key);
            });
        },

        clear: function (key) {
            this.each(function (value, key) {
                this.remove(key);
            }, this);
        }

    });

    // -----------------------------------------------------------------------------------------------

    var instances = {};
    var engineList = {
        'localStorage': LocalStorage,
        'cookie': Cookie
    };

    WA.StorageProvider = {

        getInstance: function (engine) {
            if(!engine || !engineList[engine]) {
                engine = 'cookie';
//                if (!window.localStorage) {
//                    engine = 'cookie';
//                } else {
//                    engine = 'localStorage';
//                }
            }

            if(!instances[engine]) {
                var storage = new (engineList[engine])();
                instances[engine] = new StorageProxy(storage);
            }

            return instances[engine];
        }

    };

})();WebAgent.namespace('WebAgent.rpc');

(function () {

    var WA = WebAgent;
    var invokers = {};
    var cache = {};

    var dispatch = function (id, rpc) {
        var obj = cache[id];
        if (!obj) {
            var clazz = invokers[id];
            if (clazz) {
                obj = cache[id] = new clazz(id, rpc);
            }
        }
        return obj;
    };

    var Remote = WA.rpc.Remote = {

        invoke: function (rpc, options, successFn, errorFn) {
            var id = options.id;
            var obj = dispatch(id, rpc);
            if (obj && options.method) {
                return obj.invoke(options.method, options.params || {}, successFn, errorFn);
            } else {
                try {
                    WA.error('Invoker not found: ' + id);
                } catch (e) {
                    errorFn(e);
                }
            }
            return null;
        },

        register: function (id, clazz) {
            if (!invokers[id]) {
                invokers[id] = clazz;
            }
        }

    };

    Remote.Invoker = WA.extend(WA.Activatable, {

        constructor: function (id, rpc) {
            this.id = id;
            this.rpc = rpc;
            this.storage = WA.StorageProvider.getInstance(WA.isIE ? 'cookie' : 'localStorage');
            this.hugeStorage = WA.StorageProvider.getInstance('localStorage');

            this.initInvoker();
        },

        initInvoker: function () {
        },

        invoke: function (method, params, successFn, errorFn) {
            if ('activate' === method || 'deactivate' === method) {
                var ret = this[method](params);
                successFn();
                return ret;
            } else {
                return this._invokeMethod(method, params, successFn, errorFn);
            }
        },

        _invokeMethod: function (method, params, successFn, errorFn) {
            WA.abstractError();
        },

        _invokeRemoteMethod: function (method, params) {
            this.rpc.invoke({
                id: this.id,
                method: method,
                params: params
            });
        }

    });

})();
(function () {

    var WA = WebAgent;
    var Remote = WA.rpc.Remote;

    var START_TIME = +new Date();
    var PREFIX = 'fm_';
    var CURRENT = PREFIX + 'current';

    var MINIMAL_CHECK_INTERVAL = 300;
    if(window.opera) {
        var NORMAL_CHECK_INTERVAL = MINIMAL_CHECK_INTERVAL;
        var TIMEOUT = 4000;
    } else {
        NORMAL_CHECK_INTERVAL = 15000;
        TIMEOUT = NORMAL_CHECK_INTERVAL * 2;
    }

    var FMI = WA.extend(Remote.Invoker, {

        initInvoker: function () {
            FMI.superclass.initInvoker.call(this);

            WA.fly(window).on('unload', this._onWindowUnload, this);
            WA.fly(window).on('beforeunload', this._onWindowUnload, this);

            this.focused = false;

            this.uid = PREFIX + '_uid_' + new Date().getTime();

            this.checker = new WA.util.DelayedTask({
                interval: MINIMAL_CHECK_INTERVAL,
                fn: this._doFocusCheck,
                scope: this
            });
        },

        _onWindowUnload: function () {
            this._writeCurrent(true);

            WA.fly(window).un('unload', this._onWindowUnload, this);
            WA.fly(window).un('beforeunload', this._onWindowUnload, this);
        },

        _onWindowStorage: function (ev) {
            var event = ev.browserEvent;
            if('key' in event && 'newValue' in event) {
                if(event.key === '_STRG_' + CURRENT) {
                    this._doFocusCheck();
                }
            } else if (WA.isFF36) {
                this.checker.start(1);
            } else {
                this._doFocusCheck();
            }
        },

        _isExpired: function (time) {
            return new Date().getTime() - time > TIMEOUT;
        },

        _invokeMethod: function (method, params, successFn, errorFn) {
            if ('tryToFocus' === method) {
                return this._tryToFocus(true, successFn, errorFn);
            } else if ('ifFocused' === method) {
                return this._ifFocused(successFn, errorFn);
            }
        },

        _writeCurrent: function (onUnload, onAfterUnload) {
            var data = WA.util.Object.pair(CURRENT, this.uid + ';' + new Date().getTime() + ';' + START_TIME + ';' + (+!!onUnload) + ';' + (+!!onAfterUnload));
            return this.storage.save(data);
        },

        _readCurrent: function () {
            var o = this.storage.load(CURRENT);
            if (o.success) {
                var data = o.result;
                if (data) {
                    var tmp = data.split(';');
                    return {
                        uid: tmp[0],
                        time: parseInt(tmp[1]),
                        start: parseInt(tmp[2]),
                        unloaded: parseInt(tmp[3]),
                        afterunload: parseInt(tmp[4])
                    };

                }
            }
            return {
                uid: null,
                time: 0,
                start: 0,
                unloaded: 0,
                afterunload: 0
            };
        },

        _onActivate: function (forceFocus) {
            WA.fly(WA.isIE8 ? document : window).on('storage', this._onWindowStorage, this);
            var o = this._readCurrent();
            if (!o.uid || o.uid === this.uid || this._isExpired(o.time) || forceFocus) {
                this._tryToFocus();
            }

            if (!this.checker.isStarted()) {
                this.checker.start(MINIMAL_CHECK_INTERVAL);
            }
        },

        _onDeactivate: function () {
            WA.fly(WA.isIE8 ? document : window).un('storage', this._onWindowStorage, this);
            this.checker.stop();
            this._blur();
        },

        _tryToFocus: function (byUserAction, successFn, errorFn) {
            this._waitingFocusingDone = !!byUserAction;
            var retval = this._writeCurrent();
            this._doFocusCheck();
            return retval;
        },

        _ifFocused: function (successFn, errorFn) {
            var current = this._readCurrent();
            var ret = current.uid === this.uid;
            successFn(ret);
            return ret;
        },

        _focus: function (isExpired) {
            if (!this.focused) {
                this.focused = true;
                this._invokeRemoteMethod('focus', isExpired);
            }
        },

        _blur: function () {
            if (this.focused) {
                this.focused = false;
                this._invokeRemoteMethod('blur');
            }
        },

        _doFocusCheck: function () {
            var current = this._readCurrent();
            
            var isExpired = this._isExpired(current.time);
            if (current.afterunload) {
                if(START_TIME <= current.start) {
                    this._writeCurrent();
                    this._focus(isExpired);
                }
            } else if ((current.uid === this.uid ) || isExpired || current.unloaded) {
                this._writeCurrent(false, current.unloaded);
                this._focus(isExpired);
            } else {
                this._blur();
            }
            
            if(this._waitingFocusingDone) {
                this._invokeRemoteMethod('focusingDone');
                delete this._waitingFocusingDone;
            }
            
            this.checker.start(isExpired ? MINIMAL_CHECK_INTERVAL : NORMAL_CHECK_INTERVAL);
        }

    });

    Remote.register('FocusManager', FMI);

})();(function () {

    var WA = WebAgent;
    var Remote = WA.rpc.Remote;

    var HugeStorage = WA.extend(Remote.Invoker, {

        _invokeMethod: function (method, params, successFn, errorFn) {
            try {
                return this.hugeStorage[method](params, successFn, errorFn);
            } catch (e) {
                if (WA.isFunction(errorFn)) {
                    errorFn(e);
                }
            }
        }

    });

    Remote.register('HugeStorage', HugeStorage);

})();(function () {

    var WA = WebAgent;
    var Remote = WA.rpc.Remote;

    var MPI = WA.extend(Remote.Invoker, {

        initInvoker: function () {
            MPI.superclass.initInvoker.call(this);

            this.updater = new WA.util.DelayedTask({
                    interval: 500,
                    fn: function () {
                        this._doPopupUpdate();
                    },
                    scope: this
                });
        },

        _onActivate: function () {
            this.updater.start();
        },

        _doPopupUpdate: function () {
            var o = this.storage.load('popup_state');
            if (o.success) {
                var state = o.result;
                if ('CLOSE' === state) {
                    this.storage.save({ popup_state: '0' });
                    this._invokeRemoteMethod('closePopup', '');
                } else {
                    this.storage.save({ popup_state: new Date().getTime() });
                }
            }
            this.updater.start();
        }

    });

    Remote.register('MasterPopup', MPI);

})();(function () {

    var WA = WebAgent;
    var Remote = WA.rpc.Remote;

    var SPI = WA.extend(Remote.Invoker, {

        initInvoker: function () {
            SPI.superclass.initInvoker.call(this);

            this.state = null;

            this.checker = new WA.util.DelayedTask({
                interval: 1000,
                fn: function () {
                    this._doPopupCheck();
                },
                scope: this
            });
        },

        _onActivate: function () {
            this.checker.start();
        },

        _invokeMethod: function (method, params, successFn, errorFn) {
            if ('closePopup' === method) {
                return this.storage.save({ popup_state: 'CLOSE' }, successFn, errorFn);
            }
        },

        _doChangePopupState: function (newState) {
            if (this.state !== newState) {
                this.state = newState;
                this._invokeRemoteMethod('changePopupState', newState);
            }
        },

        _checkPopupState: function (newState) {
            newState = parseInt(newState);

            var time = new Date().getTime();
            if (WA.isNumber(newState)) {
                if (time - newState > 2500) {
                    this._doChangePopupState(0);
                } else {
                    this._doChangePopupState(1);
                }
            } else {
                this._doChangePopupState(0);
            }
        },

        _doPopupCheck: function () {
            var o = this.storage.load('popup_state');
            if (o.success) {
                this._checkPopupState(o.result);
            }
            this.checker.start();
        }

    });

    Remote.register('SlavePopup', SPI);

})();(function () {

    var WA = WebAgent;
    var Remote = WA.rpc.Remote;

    var Storage = WA.extend(Remote.Invoker, {

        _invokeMethod: function (method, params, successFn, errorFn) {
            try {
                return this.storage[method](params, successFn, errorFn);
            } catch (e) {
                if (WA.isFunction(errorFn)) {
                    errorFn(e);
                }
            }
        }

    });

    Remote.register('Storage', Storage);

})();