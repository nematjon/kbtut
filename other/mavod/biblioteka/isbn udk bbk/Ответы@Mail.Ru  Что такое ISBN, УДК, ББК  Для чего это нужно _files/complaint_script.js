// $Id: complaint_script.js,v 1.15 2011/10/06 13:30:16 brednikov Exp $

function absPosition(obj) { // ���������� top - left ���������� ����� obj
    var ox = 0;
    var oy = 0;
    while( obj ) {
        ox += obj.offsetLeft;
        oy += obj.offsetTop;
        //  � ��������� ������� ������-�� ��� �������� �� �������� ����������� �����
        if ( obj.offsetParent == null ) {
           obj = obj.nodeName == 'BODY' ?  null : obj.parentNode;
        }

        if ( obj!=null ) obj = obj.offsetParent;
    }
    return {x:ox,y:oy};
}

function showComplaintReason(_link) {
  shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\"></a></div>\
  <div class=\"mtm10\">�������:</div>\
    <form action=\"" + _link.href.replace('action=abuse', 'action=abuselist') + "\" onsubmit='if(/[a-zA-Za-��-�]{3,}/.test(document.getElementById(\"spamtextArea\").value)) {return sendComplaint(\"" + _link.id + "\");} else {alert(\"������� ������� ������!\");return false;}'>\
    <textarea name=report rows=4 style='width:210px; height:80px; margin: 7px 0; font-size: 12px; font-family: Arial;' id='spamtextArea'></textarea>\
    <input type=submit id=butForSpam value='�������� ����������' style='font-size: 95%; font-family: tahoma;';>\
  </form>\
    ", _link);
    gebi("spamtextArea").focus();
    return false;
}

function sendComplaint(id) { 
    _link = document.getElementById(id);
    var url = "/doit";
    var params = cutParams(_link.href) + '&report=' + document.getElementById("spamtextArea").value +  '&ajax=1';
    var method = "POST";
    var onload = reportResult;
    var contentType = "application/x-www-form-urlencoded; charset=windows-1251";
    shawDivDialog("���� ��������� �������", _link);
    return setAjaxRequest(method, url, params, onload, false, contentType, false, _link);
}

function reportResult() {
    var obj, doc, docRes, errorCode, error;
    obj = eval( "(" + this.req.responseText + ")");
    switch ( obj.error ){
	case 'errabusealready':
	    error = '�� ��� ���������� �� ����� ������';
	    error_code = 2;
	    break;
	case 'errabusenotfound':
	    error = '������ �� �������. ���������� ��� ���';
	    error_code = 1;
	    break;
	case 'errabuselimit':
	    error = '�� �������� �� ������ ��������� ������. ���������� ������� ��� ����� ������.';
	    error_code = 2;
	    break;
	case 'erruserbanned':
	    error = '��� ������ ������ �� ������';
	    error_code = 2;
	    break;
	case 'okabuse':
	    error = '���� ������ ����� ����������� � ��������� �����';
	    error_code = 0;
	    break;
	case 'errusrnotfound':
	    error = '������������ �� ������';
	    error_code = 2;
	    break;
	case 'errusrwasbanned':
	    error = '������������ ������� � ��� ��������� ��� ����������� �������������';
	    error_code = 2;
	    break;
	case 'errmoddenied':
	    error = '<b>������ ��������</b>. <br>� ��� ��� ���� �� ���������� ����� ��������';
	    error_code = 2;
	    break;
	case 'errmodlimit':
	    error = '�� �������� �� ������ ��������� ������� � ������������� �������';
	    error_code = 2;
	    break;
	default:
	    error = '��� ��������� ������� ��������� ������<br>���������� ��������� ��� ���';
//      document.write( '<textarea>' + this.req.responseText + '</textarea>' );
	    error_code = 1;
	    break;
    }

    if ( error_code > 0 ) {
		shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div><div class=\"text_body\"><span class=\"red\">" + error + "</span></div>");
    }else {
		shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div><div class=\"text_body\"><span class=\"green\">" + error + "</span></div>");
    }
    //  ���� ������ ��������� ��� ������ �������, ������ ������
    if ( error_code==0 || error_code> 1 ) {
    	requestsHash[this.hashKey + 1].style.display = "none";
    }    
}



// ========== BEGIN: reiting ===========================
function sendQReiting(_link) {
    var url = "/doit";
    var params = cutParams(_link.href) + '&ajax=1';
    var method = "POST";
    var onload = reportReitResult;
    var onerror = errorReit;
    _link = _link.parentNode; 
    shawDivDialog('���� ��������� �������', _link);
    return setAjaxRequest(method, url, params, onload, onerror, false, false, _link);
}

function errorReit() {
    var error = "��������� ������. ���������� ��� ��� ��� <a target=\"blank\" href=\"http://otvet.mail.ru/askauth/\">���������� � �������������</a>";
    shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div><div class=\"text_body\"><span class=\"red\">" + error + "</span></div>");
}

function reportReitResult() {
    var error, error_code, reit, minus, plus, qual, error, html;
    var xml  = this.req.responseXML ? this.req.responseXML : 0;
    
    if ( xml ){
        //  �������� ������ �� XML
  minus = xml.getElementsByTagName('minus').item(0).firstChild.nodeValue;
        plus  = xml.getElementsByTagName('plus').item(0).firstChild.nodeValue;
        qual  = xml.getElementsByTagName('qual').item(0).firstChild.nodeValue;
  error   = xml.getElementsByTagName('error').item(0).firstChild.nodeValue;
        reit  = plus - minus;
    
  html = '<a href="?intr=1#interes" onclick="return showIntrVote(this, <tmpl_var qid>);" class="serv95 low">'+ reit + '&nbsp;' + getplural(reit, '����', '�����', '������') + ':&nbsp;'+ qual + '</a>';
        document.getElementById('qstintdiv').innerHTML = html;
    document.getElementById('interes_span').style.display = 'inline';
    }

	switch (error) {
		case 'okintqst' :
			error = '<b>��� ����� ������</b>.<br>�� ���������� 1 ����';
			error_code = 0;
		break;
		case 'okintqstx2' :
			error = '<b>��� ����� ������</b>.<br>�� ���������� 2 �����';
			error_code = 0;
		break;
		case 'erruserbanned' :
			error = '��� ������ ������ �� ������';
			error_code = 2;
		break;
		case 'errintnotfound':
			error = '�� �� ������ ���������� �� ���� ������';
			error_code = 2;
		break;
		case 'errintalready':
			error = '�� ��� ���������� �� ���� ������';
			error_code = 2;
		break;
		default :
			error = '��� ��������� ������� ��������� ������<br>���������� ��������� �������';
			error_code = 2;
		break;
	}

	if (error_code > 0) {
		shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div><div class=\"text_body\"><span class=\"red\">" + error + "</span></div>");
	}else {
		shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div><div class=\"text_body\"><span class=\"green\">" + error + "</span></div>");
	}
	//  ���� ������ ��������� ��� ����� ������, ������ ������
	if ( error_code > 1) {
		document.getElementById("qstintdiv").style.display = "none";
	}
}
                                                                            // ========== BEGIN: reiting ===========================



function cutParams(allUrl) {
  var params = allUrl.substring(allUrl.indexOf("?") + 1, allUrl.length);
  params = params.replace('action=abuse', 'action=authabuse');
  return params;
}
function shawDivDialog(html, basis, _x, _y) {
  var div = gebi("spam_dial");
  div.innerHTML = html;
  if (basis) {
    var _top = absPosition(basis).y + (_y ? _y : 0);
    var _left = absPosition(basis).x + (_x ? _x : 0);
  }
  if (typeof _top != 'undefined') div.style.top = _top + "px";
  if (typeof _left != 'undefined') div.style.left = _left + "px";
  div.style.display = "";
}
function hideDivDialog() {
  gebi("spam_dial").style.display = "none";
}


function showThanksDialog(_link, aid, nick) {
    shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div>\
    ���� �� ������ ������������� ������������ <b>" + nick + "</b> �� �������� �����, ��������� SMS � ����� 22+" + aid + 
    " �� �������� ����� 7099.\
    ������������ ������� ���� &laquo;�������&raquo; � 50 ������ �� ����.\
    <br>��������� ������:<br>\
    ��� ������ &mdash; $0.99 ��� ���;<br>\
    ��� ������� &mdash; 6 ���. � ���.\
    <br><br>\
    <a href='/thanks/" + aid + "/'>������ ������</a>\
    ", _link);
    return false;
}

function showIntrVote(_link, qid) {
    var url = "/doit";
    var params = 'action=xml_getintrvote&qid=' + qid;
    var method = "POST";
    var onload = reportIntrVoteResult;
    var onerror = errorReit;
    shawDivDialog('���� ��������� �������', _link);
    _link.style.display = "none";
    return setAjaxRequest(method, url, params, onload, onerror, false, false, _link);
}


function reportIntrVoteResult() {
    var html = "";
    var email = "";
    var xml  = this.req.responseXML ? this.req.responseXML : 0;
    if ( xml ){
        var length = xml.getElementsByTagName('usr').length;
        if ( length ){
            for ( i=0; i<length; i++){
                email   =  xml.getElementsByTagName('email').item(i).firstChild.nodeValue; 
                html    += "<div class=\"mb5\"><img src=\"/img/"
                        + ( xml.getElementsByTagName('vote').item(i).firstChild.nodeValue==1 ? 'ico_pos' : 'ico_neg' )
                        + ".gif\" class=\"mr3\"  /> <a href='http://www.mail.ru/agent?message&to="
                        + email + "'><img width=13 height=13 src='http://status.mail.ru/?"
                        + email + "'></a> <a href='/"+ xml.getElementsByTagName('domain').item(i).firstChild.nodeValue   
                        + "/" +  xml.getElementsByTagName('name').item(i).firstChild.nodeValue  + "/'>"
                        + xml.getElementsByTagName('nick').item(i).firstChild.nodeValue + "</a></div>";
            }
            document.getElementById('interes_who').innerHTML = html;
            document.getElementById('interes_div').style.display = 'block';
        }
    }
    hideDivDialog();
}

function getplural(num, one, two, five){
    num = Math.abs(num);
    num %= 100;
    if ( num>19 ) { num %= 10; }
    if ( num==0 || num>4 ) { return five; }
    else if ( num==1 ) { return one; }
    else { return two; }
}

function sendQstAnsMark(_link){
    var href, params, url, method, onload, onerror;
    href = _link.href;
    params = href.substring(href.indexOf("?") + 1, href.length) + '&ajax=1';
    url = '/doit';
    method = 'POST';
    onload = reportMark;
    onerror = errorMark;
    _link = _link.parentNode.parentNode;
    _link.style.display = 'none';
    do _link = _link.nextSibling;
    while ( _link.nodeName != 'DIV' );

    _link.innerHTML = '<span class="orange bold">������ ��������������...</span>';
    return setAjaxRequest(method, url, params, onload, onerror, false, false, _link);
}

function setClassName(elem, classes){
    if ( elem.getAttribute("className") ){
        elem.setAttribute("className", classes);
    }
    else{
        elem.setAttribute("class", classes);
    }
}

function errorMark(){
    var error = "��������� ������. ���������� ��� ��� ��� <a target=\"blank\" href=\"http://otvet.mail.ru/askauth/\">���������� � �������������</a>";
    shawDivDialog("<div class=\"right mt5\"><a onclick=\"hideDivDialog(); return false;\" href=\"#\"><img src=\"/img/close_help.gif\" width=\"7\" height=\"7\" alt=\"�������\" /></a></div><div class=\"text_body\"><span class=\"red\">" + error + "</span></div>");
}

function reportMark(){
    var obj, doc, docRes, errorCode, error;
    obj = eval( "(" + this.req.responseText + ")");
    if ( obj.aid ){
        doc = document.getElementById('ansMark:' + obj.aid);
        docRes = document.getElementById('ansMarkRes:' + obj.aid);
    }
    else{
        doc = document.getElementById('qstMark:' + obj.qid);
        docRes = document.getElementById('qstMarkRes:' + obj.qid);
    }

    switch ( obj.error ){
        case 'okmarkadd' :
                error = '<b>���� ������ �������</b>';
                errorCode = 0;
                break;
        case 'errcantmark' :
                error = '�� �������� �� ������ ��������� ������� � ������. <br /> ��������, �� ��������� ������ �� �� ������ �� ������ ������� � �� ���� �� ������ ������. <br />��� ����������� ��������� ������������� �������������� �������.';
                errorCode = 2;
                break;
        case 'errnobestansselected' :
                error = '����� ��������� ������ �� ���� ������, ���������� ������� ������ ����� ��� ������������� ������� �����';
                errorCode = 2;
                break;
        case 'errmarkalready' :
                error = '�� ��� ��������� ���� �����';
                errorCode = 0;
                break;
        case 'errmarknotfound' :
                error = '';
                errorCode = 2;
                break;
	case 'errflood' :
		error = '������ �������� ������ ��������� ������� �����';
		errorCode = 2;
		break;
        default :
                error = '��� ��������� ������� ��������� ������<br>���������� ��������� �������';
                errorCode = 2;
                break;
    }

    if ( errorCode > 0 ){
        docRes.innerHTML = '<span class="red bold">' + error + '</span>';
    }
    else{
        docRes.style.display = 'none';
        setClassName(doc, "QualityR RInA");
	if ( obj.aid ){
	    setClassName(doc, "QualityR RInA");
	    doc.innerHTML = '<span>������: <b>' + obj.avg + '</b></span><span>�������: <b>' + obj.totalSum + '</b></span>' + ( obj.totalMarks > 0 ? '(<a class="votes" href="/ansmarks/' + obj.aid + '/">' + obj.totalMarks + '&nbsp;' + getplural(obj.totalMarks, '�����', '������' ,'�������') +'</a>)' : '');
	}
	else{
	    setClassName(doc, "QualityR");
	    doc.innerHTML = '<span>������: <b>' + obj.avg + '</b></span><span><a href="/questtop/">�������</a>: <b>' + obj.totalSum + '</b></span>' + ( obj.totalMarks > 0 ? '(<a class="votes" href="/qstmarks/' + obj.qid + '/">' + obj.totalMarks + '&nbsp;' + getplural(obj.totalMarks, '�����', '������' ,'�������') +'</a>)' : '');
	}
    }
    doc.style.display = 'block';
}


var rivalsLoaded = false;
var currUsr 	= '';                  
var cabUsr	= '';

function getRivals(cabEMail, userEMail){
    if ( !rivalsLoaded) {  
	currUsr	= userEMail;
	cabUsr	= cabEMail;
        var href, params, url, method, onload, onerror;
        href 	= 'http://otvet.mail.ru/';
	params 	= 'action=rivals&email=' + cabEMail + '&ajax=1';
        url	= '/doit';
	method	= 'POST';
        onload	= reportRivals;
	onerror	= errorRivals;
	document.getElementById('actStat').style.display = 'block';
        return setAjaxRequest(method, url, params, onload, onerror, false, false, document.getElementById("rivals"));
    }
    document.getElementById('actStat').style.display = 'block';
}

function reportRivals(){
    var objm, length, i, html, user, bold;
    obj = eval( "(" + this.req.responseText + ")");
    if ( !obj.undefined ){
        length = obj.rivals.length;
	html = '';
	for (i=0; i<length; i++){
		bold = cabUsr == obj.rivals[i].EMAIL ? 1 : 0;
	    if ( currUsr == obj.rivals[i].EMAIL ) {
		user = '<span class="mr5">' + obj.rivals[i].PLACE + ' ����� � ���</span> ';
	    } else {
		user = obj.rivals[i].PLACE + ' <a href="http://www.mail.ru/agent?message&amp;to=' + obj.rivals[i].EMAIL + '"><img alt="������, ����� ���������� � ���� ��������� � Mail.Ru ������" src="http://status.mail.ru/?' + obj.rivals[i].EMAIL + '&amp;9x9"/></a> <a class="mr5" href="/' + obj.rivals[i].EMAILDOMAIN + '/' + obj.rivals[i].EMAILBOX + '/">' + obj.rivals[i].NICK + '</a>';
	    }
	    html += ( bold ? '<b>' : '' ) + '<div class="mb3">' + user + ' (' + obj.rivals[i].POINTS + ' ' +
		    getplural(obj.rivals[i].POINTS, '����', '�����', '������') + ')</div>' + ( bold ? '</b>' : '' );
        }
	document.getElementById("rivals").innerHTML = html;
	rivalsLoaded = true;
    }
}

function errorRivals(){
	document.getElementById("rivals").innerHTML = '��������� ������!<br />���������� ��������� �������.';
}
