function wordCount(){
	var str = " " + qtext;
	pattern = str.match( / [a-zA-Z�-��-߸�]{2}[a-zA-Z�-��-߸�0-9-]+/g, str );
	return (pattern ? pattern.length : 0);
}

function getWords(){
	var str, retval="";
	str = " " + qtext;
	pattern = str.match( / [a-zA-Z�-��-߸�0-9-]+/g, str );
	if ( pattern ) {
		for ( i=0; i<pattern.length; i++){
			retval += pattern[i];
		}
	}
	return retval;
}


var wordStart   = 0;  //  ����, ��� ���� ����� �����
var reqWord 	= 0;  //  ����� ����, ��� ������� ��������� ��� ������� ������
var minWordCont = 4;  //  ����������� ����� ����, ��� ������� ������ ������, ���� ����� ��������� ������������
var minWordStop = 3;  //  ����������� ����� ����, ��� ������� ������ ������, ���� ���� ��������� �������� �����
var curReqCount = 0;  //  ����� �������� � ������� � ������� �������
var maxReqCount = 5;  //  ������������ ����� �������� � �������
var lastWord 	= ""; //  ��������� ������
var resCount    = 3;  //  ����� ��������� ��������
var qtext 		= ""; //  ����� �������
var qid			= 0;  //  ������������� �������

function keypress(e) {
	var key, chr, count;
	key = getkey(e);

	if ( key ){
		chr = String.fromCharCode( key );
		if ( chr.match(/[A-Z-]/)  ){
			if ( ! wordStart ) wordStart = 1;
		}else{
			if ( wordStart ) {
				if ( !chr.match(/[0-9]/) ){
					wordStart = 0;
					count = wordCount();
					if ( count != reqWord ){
						//  ������ ������
						reqWord = count;
						loadHint( count, minWordCont );
					}
				}
			}
		}
	}
}


function getkey(e){
	if (!e) e = window.event;
	return e.keyCode ? e.keyCode : e.which ?  e.which : 0;
}

function loadHint(count, mincount){ 
	count = parseInt(count);
	if ( isNaN(count) ) {
		count     = wordCount();
		mincount  = minWordStop;
	}
	//  �������� ��������� ����������
	if ( curReqCount < maxReqCount &&  count >= mincount ){
		var word = getWords();
		if ( word != lastWord ){
			document.getElementById('temp').innerHTML = '';
			lastWord = word;
			curReqCount++;
			fastSearch();
		}
	}
}

function init_search() {
	if ( !qtext ){
		var doc = document.getElementById("temp");
        qtext = doc.innerHTML;
        qid = doc.getAttribute("relid");
        doc.innerHTML = "";
	}
	if ( wordCount() ){
    	loadHint();
	}
}

function fastSearch(){
//	var url = "/search/similar";
	var url = "http://otvet.mail.ru/search/similar";
	var params = "n="+ resCount +"&q=" +  getWords() + "&zvstate=3";  //document.getElementById("qst").value;
	var method = "POST";
	var onload  = fastSearchParser;
	var onerror = fastSearchError;
	var contentType = "application/x-www-form-urlencoded; charset=windows-1251";

	return setAjaxRequest(method, url, params, onload, onerror, contentType, false, false);
}






function fastSearchParser(){
	var obj, hint, qst, id, snippet, text, len, nick, email, parsedEmail;

	len = this.req.responseXML.getElementsByTagName('id').length;

	if ( len>0 ) {
		hint    = '<h2 class="mb10">��� ����� ���� � ������� ��������</h2>'
				+ '<div class="AskedL">';

		for ( i=0; i<len; i++){
			id = this.req.responseXML.getElementsByTagName('id').item(i).firstChild.nodeValue;

			if ( id != qid ){
				nick    = this.req.responseXML.getElementsByTagName('nick').item(i).firstChild.nodeValue;
				email   = this.req.responseXML.getElementsByTagName('email').item(i).firstChild.nodeValue;
				text    = this.req.responseXML.getElementsByTagName('text').item(i).firstChild.nodeValue;
				text    = text.replace(/^[\r\n ]+/, "");
				parsedEmail = parseEMail(email);

				hint  += "<div class=\"I" + (i == len - 1 ? " last" : "" ) +"\">"
						+"<div><img width=\"16\" height=\"16\" class=\"middle mr5\" title=\"��������� �� ������\" alt=\"\" src=\"/img/ico_status_A.gif\"/>"
						+"<a class=\"subj\" href=\"/question/" + id + "/\">" + text + "</a></div>"
						+"<div class=\"add\">"
						+"<a href=\"http://www.mail.ru/agent?message&amp;to=" + email + "\"><img width=\"9\" height=\"9\" alt=\"\" src=\"http://status.mail.ru/?" + email + "&9x9\"/></a>"
						+"<a href=\"/" + parsedEmail.domain + "/" + parsedEmail.box + "/\">" + nick + "</a>"
						+"&nbsp;<a href=\"#url?to=" + email + "\" target=\"_blank\" onclick=\"return dropDownUserMenu(this, event, 0, 1, 0);\"><img title=\"���� ������������\" alt=\"���� ������������\" src=\"/img/ico_flips.gif\" class=\"ml2 middle\" height=\"9\" width=\"9\" /></a>"
						+"</div></div>";						
			}
		}
		hint += '</div>';
		document.getElementById('qst_open').innerHTML = hint;
		showOurDiv();
	} else {
		showOtherDiv();
	}
}


function fastSearchError(){
	return false;
}

function showOtherDiv(){
}

function showOurDiv(){
	var leftDiv, rightDiv, otherDiv;
	leftDiv 	= document.getElementById('newcomers_left');
	rightDiv 	= document.getElementById('newcomers_right');
	otherDiv	= document.getElementById('qst_open');
	setClassName(leftDiv, 'w50');
	setClassName(rightDiv, 'w50');
	if ( leftDiv.getAttribute("className") ){
		otherDiv.setAttribute("className", '');
	} else {
		otherDiv.setAttribute("class", '');
	}
}

