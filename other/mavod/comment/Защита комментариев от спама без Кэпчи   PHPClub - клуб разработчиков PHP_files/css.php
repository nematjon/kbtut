@CHARSET "UTF-8";

/* --- xenforo.css --- */

/*
 * YUI reset-fonts.css
 *
Copyright (c) 2009, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.7.0
*/
html{color:#000;background:#FFF;}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,cite,code,dfn,em,strong,th,var,optgroup{font-style:inherit;font-weight:inherit;}del,ins{text-decoration:none;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}q:before,q:after{content:'';}abbr,acronym{border:0;font-variant:normal;}sup{vertical-align:baseline;}sub{vertical-align:baseline;}legend{color:#000;}input,button,textarea,select,optgroup,option{font-family:inherit;font-size:inherit;font-style:inherit;font-weight:inherit;}input,button,textarea,select{*font-size:100%;}body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}table{font-size:inherit;font:100%;}pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}

/*
 * XenForo Core CSS
 *
 */

html
{
	background-color: rgb(240,240,240);

	overflow-y: scroll !important;
}

body
{
	font-family: Arial, Helvetica, sans-serif;
color: rgb(20,20,20);
word-wrap: break-word;
line-height: 1.27;

}

/* counteract the word-wrap setting in 'body' */
pre, textarea
{
	word-wrap: normal;
}

a:link,
a:visited
{
	color: #176093;
text-decoration: none;

}

	a[href]:hover
	{
		text-decoration: underline;

	}
	
	a:hover
	{
		_text-decoration: underline;
	}
	
	a.noOutline
	{
		outline: 0 none;
	}
	
	.emCtrl,
	.messageContent a
	{
		border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
	}
	
		.emCtrl:hover,
		.emCtrl:focus,
		.ugc a:hover,
		.ugc a:focus
		{
			/*position: relative;
			top: -1px;*/
			text-decoration: none;
			box-shadow: 5px 5px 7px #CCCCCC; -webkit-box-shadow:5px 5px 7px #CCCCCC; -moz-box-shadow:5px 5px 7px #CCCCCC; -khtml-box-shadow:5px 5px 7px #CCCCCC;
			outline: 0 none;
		}
		
			.emCtrl:active,
			.ugc a:active
			{
				position: relative;
				top: 1px;
				box-shadow: 2px 2px 7px #CCCCCC; -webkit-box-shadow:2px 2px 7px #CCCCCC; -moz-box-shadow:2px 2px 7px #CCCCCC; -khtml-box-shadow:2px 2px 7px #CCCCCC;
				outline: 0 none;
			}

	.ugc a:link,
	.ugc a:visited
	{
		padding: 0 3px;
margin: 0 -3px;
border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;

	}
	
		.ugc a:hover,
		.ugc a:focus
		{
			color: #6d3f03;
background: #fff4e5 url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;

		}
		
img.smilie
{
	vertical-align: text-bottom;
	margin: 0 1px;
}
		
/** title bar **/

.titleBar
{
	margin-bottom: 10px;
}

/* clearfix */ .titleBar { zoom: 1; } .titleBar:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.titleBar h1
{
	font-size: 18pt;
overflow: hidden;
zoom: 1;

}

	.titleBar h1 em
	{
		color: rgb(100,100,100);
	}
		
	.titleBar h1 .Popup
	{
		float: left;
	}

#pageDescription
{
	font-size: 11px;
color: rgb(150,150,150);
margin-top: 2px;

}

.topCtrl
{
	float: right;
}
	
	.topCtrl h2
	{
		font-size: 12pt;
	}
		
/** images **/

img
{
	-ms-interpolation-mode: bicubic;
}

a.avatar 
{ 
	*cursor: pointer; /* IE7 refuses to do this */ 
} 

.avatar img,
.avatar .img,
.avatarCropper
{
	background-color: rgb(252, 252, 255);
padding: 2px;
border: 1px solid #a5cae4;
border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;

}

.avatar.plainImage img,
.avatar.plainImage .img
{
	border: none;
	border-radius: 0; -webkit-border-radius: 0; -moz-border-radius: 0; -khtml-border-radius: 0;
	padding: 0;
	background-position: left top;
}

	.avatar .img
	{
		display: block;
		background-repeat: no-repeat;
		background-position: 2px 2px;
		text-indent: 1000px;
		overflow: hidden;
		white-space: nowrap;
		word-wrap: normal;
	}

	.avatar .img.s { width: 48px;  height: 48px;  }
	.avatar .img.m { width: 96px;  height: 96px;  }
	.avatar .img.l { width: 192px; height: 192px; }

.avatarCropper
{
	width: 192px;
	height: 192px;
}

.avatarCropper a,
.avatarCropper span,
.avatarCropper label
{
	overflow: hidden;
	position: relative;
	display: block;
	width: 192px;
	height: 192px;
}

.avatarCropper img
{
	padding: 0;
	border: none;
	border-radius: 0; -webkit-border-radius: 0; -moz-border-radius: 0; -khtml-border-radius: 0;

	position: relative;
	display: block;
}

.avatarScaler img
{
	max-width: 192px;
	_width: 192px;
}

/* ***************************** */

body .dimmed, body a.dimmed, body .dimmed a { color: rgb(100,100,100); }
body .muted, body a.muted, body .muted a { color: rgb(150,150,150); }
body .faint, body a.faint, body .faint a { color: rgb(200,200,200); }

.highlight { font-weight: bold; }

.concealed,
.concealed a,
.cloaked,
.cloaked a
{
	text-decoration: inherit !important;
	color: inherit !important;
	*clear:expression( style.color = parentNode.currentStyle.color, style.clear = "none", 0);
}

a.concealed:hover,
.concealed a:hover
{
	text-decoration: underline !important;
}

/* ***************************** */

.xenTooltip
{
	font-size: 11px;
color: rgb(255, 255, 255);
background: url(rgba.php?r=0&g=0&b=0&a=153); background: rgba(0,0,0, 0.6); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);
padding: 5px 10px;
border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
display: none;
z-index: 15000;
cursor: default;

}

.xenTooltip a,
.xenTooltip a:hover
{
	color: rgb(255, 255, 255);
	text-decoration: underline;
}

	.xenTooltip .arrow
	{
		border-top:  6px solid rgb(0,0,0); border-top:  6px solid rgba(0,0,0, 0.6); _border-top:  6px solid rgb(0,0,0);
border-right: 6px solid transparent;
border-bottom: 1px none black;
border-left: 6px solid transparent;
position: absolute;
bottom: -6px;
left: 9px;
line-height: 0px;
width: 0px;
height: 0px;

		
		/* Hide from IE6 */
		_display: none;
	}

.xenTooltip.statusTip
{
	/* Generated by XenForo.StatusTooltip JavaScript */
	padding: 5px 10px;
line-height: 17px;
width: 250px;
height: auto;

}

	.xenTooltip.statusTip .arrow
	{
		border: 6px solid transparent;
border-right-color:  rgb(0,0,0); border-right-color:  rgba(0,0,0, 0.6); _border-right-color:  rgb(0,0,0);
border-left: 1px none black;
top: 6px;
left: -6px;
bottom: auto;
right: auto;

	}

/* ***************************** */

#PreviewTooltip
{
	display: none;
}

.xenPreviewTooltip
{
	border: 10px solid #032A46;
border-radius: 10px; -webkit-border-radius: 10px; -moz-border-radius: 10px; -khtml-border-radius: 10px;
position: relative;
box-shadow: 0px 12px 25px rgba(0,0,0, 0.5); -webkit-box-shadow:0px 12px 25px rgba(0,0,0, 0.5); -moz-box-shadow:0px 12px 25px rgba(0,0,0, 0.5); -khtml-box-shadow:0px 12px 25px rgba(0,0,0, 0.5);
width: 400px;

	
	display: none;	
	z-index: 15000;
	cursor: default;
	
	border-color:  rgb(3,42,70); border-color:  rgba(3,42,70, 0.5); _border-color:  rgb(3,42,70);
}

	.xenPreviewTooltip .arrow
	{
		border-top:  15px solid rgb(3,42,70); border-top:  15px solid rgba(3,42,70, 0.25); _border-top:  15px solid rgb(3,42,70);
border-right: 15px solid transparent;
border-bottom: 1px none black;
border-left: 15px solid transparent;
position: absolute;
bottom: -15px;
left: 22px;

		
		_display: none;
	}
	
		.xenPreviewTooltip .arrow span
		{
			border-top: 15px solid rgb(252, 252, 255);
border-right: 15px solid transparent;
border-bottom: 1px none black;
border-left: 15px solid transparent;
position: absolute;
top: -17px;
left: -15px;

		}

	.xenPreviewTooltip .section,
	.xenPreviewTooltip .sectionMain,
	.xenPreviewTooltip .primaryContent,
	.xenPreviewTooltip .secondaryContent
	{
		margin: 0;
	}
	
		.xenPreviewTooltip .previewContent
		{
			overflow: hidden; zoom: 1;
			min-height: 1em;
		}

/* ***************************** */

.importantMessage
{
	margin: 10px 0;
	color: #6d3f03;
	background-color: #fff4e5;
	text-align: center;
	padding: 5px;
	border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
	border: solid 1px #f9bc6d;
}

.importantMessage a
{
	font-weight: bold;
	color: #6d3f03;
}

/* ***************************** */

.section
{
	background-color: rgb(252, 252, 255);
margin: 5px auto;

}

.sectionMain
{
	background-color: rgb(252, 252, 255);
padding: 10px;
margin: 10px auto;
border: 1px solid #a5cae4;
border-radius: 10px; -webkit-border-radius: 10px; -moz-border-radius: 10px; -khtml-border-radius: 10px;

}

.heading,
.xenForm .formHeader
{
	font-weight: bold;
font-size: 11pt;
color: rgb(249, 249, 250);
background-color: #65a5d1;
padding: 5px 10px;
margin-bottom: 3px;
border-bottom: 1px solid #176093;
border-top-left-radius: 5px; -webkit-border-top-left-radius: 5px; -moz-border-radius-topleft: 5px; -khtml-border-top-left-radius: 5px;
border-top-right-radius: 5px; -webkit-border-top-right-radius: 5px; -moz-border-radius-topright: 5px; -khtml-border-top-right-radius: 5px;

}

	.heading { color: rgb(249, 249, 250); }

.subHeading
{
	font-size: 11px;
color: #6d3f03;
background: #f9d9b0 url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
padding: 5px 10px;
margin: 3px auto 0;
border-top: 1px solid #f9d9b0;
border-bottom: 1px solid #f9bc6d;

}

	.subHeading a { color: #6d3f03; }

.textHeading,
.xenForm .sectionHeader
{
	font-weight: bold;
color: black;
padding: 0px 5px 2px;
margin: 10px auto 0;
border-bottom: 1px solid #d7edfc;

}

.xenForm .sectionHeader,
.xenForm .formHeader
{
	margin: 10px 0;
}

.primaryContent > .textHeading:first-child,
.secondaryContent > .textHeading:first-child
{
	margin-top: 0;
}


.primaryContent
{
	background-color: rgb(252, 252, 255);
padding: 10px;
border-bottom: 1px solid #d7edfc;

}

	.primaryContent a
	{
		color: #176093;

	}

.secondaryContent
{
	background: rgb(249, 249, 250) url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
padding: 10px;
border-bottom: 1px solid #d7edfc;

}

	.secondaryContent a
	{
		color: #176093;

	}

.sectionFooter
{
	font-size: 11px;
color: #65a5d1;
background: #d7edfc url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
padding: 4px 10px;
border-bottom: 1px solid #a5cae4;
line-height: 16px;

}

	.sectionFooter a { color: #65a5d1; }

/* used for section footers with central buttons, esp. in report viewing */

.actionList
{
	text-align: center;
}

/* left-right aligned options */

.opposedOptions
{
	overflow: hidden; zoom: 1;
}
	
	.opposedOptions .left
	{
		float: left;
	}
	
	.opposedOptions .right
	{
		float: right;
	}

.columns
{
	overflow: hidden; zoom: 1;
}

	.columns .columnContainer
	{
		float: left;
	}
	
		.columns .columnContainer .column
		{
			margin-left: 3px;
		}
		
		.columns .columnContainer:first-child .column
		{
			margin-left: 0;
		}

.c50_50 .c1,
.c50_50 .c2 { width: 49.99%; }

.c70_30 .c1 { width: 70%; }
.c70_30 .c2 { width: 29.99%; }

.c60_40 .c1 { width: 60%; }
.c60_40 .c2 { width: 39.99%; }

.c40_30_30 .c1 { width: 40%; }
.c40_30_30 .c2,
.c40_30_30 .c3 { width: 29.99%; }

.c50_25_25 .c1 { width: 50%; }
.c50_25_25 .c2,
.c50_25_25 .c3 { width: 25%; }

/* ***************************** */
/* Basic Tabs */

.tabs
{
	font-size: 11px;
border-bottom: 1px solid #a5cae4;
white-space: nowrap;
word-wrap: normal;
height: 19px;

}

.tabs li
{
	float: left;
}

.tabs li a,
.tabs.noLinks li
{
	color: rgb(20,20,20);
text-decoration: none;
background-color: rgb(249, 249, 250);
padding: 0 5px;
margin-right: -1px;
margin-bottom: -1px;
border: 1px solid #a5cae4;
border-top-left-radius: 5px; -webkit-border-top-left-radius: 5px; -moz-border-radius-topleft: 5px; -khtml-border-top-left-radius: 5px;
border-top-right-radius: 5px; -webkit-border-top-right-radius: 5px; -moz-border-radius-topright: 5px; -khtml-border-top-right-radius: 5px;
display: inline-block;
line-height: 18px;
cursor: pointer;
outline: 0 none;
height: 18px;

}

.tabs li:hover a,
.tabs.noLinks li:hover
{
	text-decoration: none;
background-color: #d7edfc;
		
}

.tabs li.active a,
.tabs.noLinks li.active
{
	background-color: rgb(252, 252, 255);
padding-bottom: 1px;
border-bottom: 1px none black;

}

/* Tabs inside forms */

.xenForm .tabs
{
	padding: 0 30px;
}

/* ***************************** */
/* Popup Menus */

.Popup
{
	position: relative;
}

	.Popup.inline
	{
		display: inline;
	}
	
/** Popup menu trigger **/

.Popup .arrowWidget
{
	background: transparent url('styles/default/xenforo/widgets/circle-arrow-down.png') no-repeat right center;
margin-top: -2px;
display: inline-block;
*margin-top: 0;
vertical-align: middle;
width: 16px;
height: 14px;

}

.PopupOpen .arrowWidget
{
	background-image: url('styles/default/xenforo/widgets/circle-arrow-up.png');

}

.Popup .PopupControl,
.Popup.PopupContainerControl
{
	display: inline-block;
	cursor: pointer;
}

	.Popup .PopupControl:hover,
	.Popup.PopupContainerControl:hover
	{
		color: #176093;
text-decoration: none;
background-color: #d7edfc;

	}

	.Popup .PopupControl:focus,
	.Popup .PopupControl:active,
	.Popup.PopupContainerControl:focus,
	.Popup.PopupContainerControl:active
	{
		outline: 0;
	}
	
	.Popup .PopupControl.PopupOpen,
	.Popup.PopupContainerControl.PopupOpen
	{
		color: #2b485c;
background: #6cb2e4 url('styles/default/xenforo/gradients/tab-unselected-25px-light.png') repeat-x top;
border-top-left-radius: 3px; -webkit-border-top-left-radius: 3px; -moz-border-radius-topleft: 3px; -khtml-border-top-left-radius: 3px;
border-top-right-radius: 3px; -webkit-border-top-right-radius: 3px; -moz-border-radius-topright: 3px; -khtml-border-top-right-radius: 3px;
border-bottom-right-radius: 0px; -webkit-border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px; -khtml-border-bottom-right-radius: 0px;
border-bottom-left-radius: 0px; -webkit-border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; -khtml-border-bottom-left-radius: 0px;
text-shadow: 1px 1px 2px white;
box-shadow: 0px 5px 5px rgba(0,0,0, 0.5); -webkit-box-shadow:0px 5px 5px rgba(0,0,0, 0.5); -moz-box-shadow:0px 5px 5px rgba(0,0,0, 0.5); -khtml-box-shadow:0px 5px 5px rgba(0,0,0, 0.5);

	}
	
	.Popup .PopupControl.BottomControl.PopupOpen,
	.Popup.PopupContainerControl.BottomControl.PopupOpen
	{
		border-top-left-radius: 0px; -webkit-border-top-left-radius: 0px; -moz-border-radius-topleft: 0px; -khtml-border-top-left-radius: 0px;
		border-top-right-radius: 0px; -webkit-border-top-right-radius: 0px; -moz-border-radius-topright: 0px; -khtml-border-top-right-radius: 0px;
		border-bottom-left-radius: 3px; -webkit-border-bottom-left-radius: 3px; -moz-border-radius-bottomleft: 3px; -khtml-border-bottom-left-radius: 3px;
		border-bottom-right-radius: 3px; -webkit-border-bottom-right-radius: 3px; -moz-border-radius-bottomright: 3px; -khtml-border-bottom-right-radius: 3px;
	}
		
		.Popup .PopupControl.PopupOpen:hover,
		.Popup.PopupContainerControl.PopupOpen:hover
		{
			text-decoration: none;
		}
		
/** Menu body **/

.Menu
{
	font-size: 11px;
background-color: white;
border: 1px solid #6cb2e4;
border-top: 5px solid #6cb2e4;
overflow: hidden;
box-shadow: 0px 5px 5px rgba(0,0,0, 0.5); -webkit-box-shadow:0px 5px 5px rgba(0,0,0, 0.5); -moz-box-shadow:0px 5px 5px rgba(0,0,0, 0.5); -khtml-box-shadow:0px 5px 5px rgba(0,0,0, 0.5);

	
	min-height: @menuMinheight;
	min-width: 200px;
	*width: 200px;
	
	/* makes menus actually work... */
	position: absolute;
	z-index: 7500;
	display: none;
}

/* allow menus to operate when JS is disabled */
.Popup:hover .Menu
{
	display: block;
}

.Popup:hover .Menu.JsOnly
{
	display: none;
}

.Menu.BottomControl
{
	border-top-width: 1px;
	border-bottom-width: 3px;
	box-shadow: 0px 0px 0px transparent; -webkit-box-shadow:0px 0px 0px transparent; -moz-box-shadow:0px 0px 0px transparent; -khtml-box-shadow:0px 0px 0px transparent;
}

	.Menu > li > a,
	.Menu .menuRow
	{
		display: block;
	}
		
/* Menu header */

.Menu .menuHeader
{
	overflow: hidden; zoom: 1;
}

.Menu .menuHeader h3
{
	font-size: 15pt;

}

.Menu .menuHeader .muted
{
	font-size: 11px;

}

/* Links lists */

.Menu .blockLinksList
{	
	max-height: 400px;
	overflow: auto;
}

/* form popups */

.formPopup
{
	width: 250px;
}

	.formPopup form,
	.formPopup .ctrlUnit
	{
		margin: 5px auto;
	}
	
		.formPopup .ctrlUnit
		{
		}
		
	.formPopup .textCtrl,
	.formPopup .button
	{
		width: 232px;
	}
		
	.formPopup .ctrlUnit dt label
	{
		display: block;
		margin-bottom: 2px;
	}
		
	.formPopup .submitUnit dd
	{
		text-align: center;
	}
	
	.formPopup .primaryControls
	{
		zoom: 1;
		white-space: nowrap;
		word-wrap: normal;
		padding: 0 5px;
	}
	
		.formPopup .primaryControls input.textCtrl
		{
			margin-bottom: 0;
		}
	
	.formPopup .secondaryControls
	{
		padding: 0 5px;
	}
	
		.formPopup .controlsWrapper
		{
			background: #d7edfc url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
			border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
			padding: 5px;
			margin: 5px 0;
			font-size: 11px;
		}

			.formPopup .controlsWrapper .textCtrl
			{
				width: 222px;
			}
	
	.formPopup .advSearchLink
	{
		display: block;
		text-align: center;
		padding: 5px;
		font-size: 11px;
		border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
		border: 1px solid #d7edfc;
		background: rgb(249, 249, 250) url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
	}
	
		.formPopup .advSearchLink:hover
		{
			background-color: #d7edfc;
			text-decoration: none;
		}

/* All overlays must have this */
.xenOverlay
{
	display: none;
	z-index: 10000;
	width: 690px; /*calc: 90=overlay padding+borders*/
}

	.xenOverlay .overlayScroll
	{
		max-height: 400px;
		overflow: auto;
	}

.overlayOnly /* needs a bit more specificity over regular buttons */
{
	display: none !important;
}

	.xenOverlay .overlayOnly
	{
		display: block !important;
	}
	
	.xenOverlay input.overlayOnly,
	.xenOverlay button.overlayOnly,
	.xenOverlay a.overlayOnly
	{
		display: inline !important;
	}
	
	.xenOverlay a.close 
	{
		background-image: url('styles/default/xenforo/overlay/close.png');
position: absolute;
right: 4px;
top: 4px;
cursor: pointer;
width: 35px;
height: 35px;

	}

/* Generic form overlays */

.xenOverlay .formOverlay
{
	color: #eee;
background: url(rgba.php?r=0&g=0&b=0&a=191); background: rgba(0,0,0, 0.75); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#BF000000,endColorstr=#BF000000);
padding: 15px 25px;
border:  20px solid rgb(0,0,0); border:  20px solid rgba(0,0,0, 0.25); _border:  20px solid rgb(0,0,0);
border-radius: 20px; -webkit-border-radius: 20px; -moz-border-radius: 20px; -khtml-border-radius: 20px;
box-shadow: 0px 25px 50px rgba(0,0,0, 0.5); -webkit-box-shadow:0px 25px 50px rgba(0,0,0, 0.5); -moz-box-shadow:0px 25px 50px rgba(0,0,0, 0.5); -khtml-box-shadow:0px 25px 50px rgba(0,0,0, 0.5);
_zoom: 1;

}

	.xenOverlay .formOverlay a.muted,
	.xenOverlay .formOverlay .muted a
	{
		color: rgb(150,150,150);
	}

	.xenOverlay .formOverlay .heading
	{
		font-weight: bold;
font-size: 12pt;
color: rgb(249, 249, 250);
background-color: #176093;
padding: 5px 10px;
margin-bottom: 10px;
border: 1px solid #2b485c;
border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;

	}

	.xenOverlay .formOverlay .subHeading
	{
		font-weight: bold;
font-size: 11px;
color: rgb(249, 249, 250);
background-color: #2b485c;
padding: 5px 10px;
margin-bottom: 10px;
border: 1px solid #176093;
border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -khtml-border-radius: 3px;

	}
	
	.xenOverlay .formOverlay .textHeading
	{
		color: rgb(249, 249, 250);

	}

	.xenOverlay .formOverlay .textCtrl
	{
		color: rgb(249, 249, 250);
background-color: black;
border-color: #a5cae4;

	}

	.xenOverlay .formOverlay .textCtrl:focus
	{
		background: #032A46 none;

	}

	.xenOverlay .formOverlay .textCtrl.disabled
	{
		background: url(rgba.php?r=0&g=0&b=0&a=63); background: rgba(0,0,0, 0.25); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#3F000000,endColorstr=#3F000000);

	}

	.xenOverlay .formOverlay .textCtrl.prompt
	{
		color: rgb(160,160,160);

	}

	.xenOverlay .formOverlay .ctrlUnit dt dfn,
	.xenOverlay .formOverlay .ctrlUnit dd li .hint,
	.xenOverlay .formOverlay .ctrlUnit dd .explain
	{
		color: #bbb;

	}

	.xenOverlay .formOverlay a
	{
		color: #fff;

	}

		.xenOverlay .formOverlay a.button
		{
			color: black;

		}

	.xenOverlay .formOverlay .avatar img,
	.xenOverlay .formOverlay .avatar .img,
	.xenOverlay .formOverlay .avatarCropper
	{
		background-color: transparent;
	}
	
	/* tabs in form overlay */
	
	.xenOverlay .formOverlay .tabs /* the actual tabs */
	{
		border-color: #a5cae4;

	}

		.xenOverlay .formOverlay .tabs a
		{
			background-color: transparent;
border-color: #a5cae4;

		}
		
			.xenOverlay .formOverlay .tabs a:hover
			{
				background: url(rgba.php?r=255&g=255&b=255&a=63); background: rgba(255,255,255, 0.25); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#3FFFFFFF,endColorstr=#3FFFFFFF);

			}
			
			.xenOverlay .formOverlay .tabs .active a
			{
				background-color: black;

			}
			
	.xenOverlay .formOverlay .tabPanel /* panels switched with the tab controls */
	{
		background: transparent url('styles/default/xenforo/color-picker/panel.png') repeat-x top;
border: 1px solid #a5cae4;

	}


/* Generic overlays */

.xenOverlay .section,
.xenOverlay .sectionMain
{
	padding: 0px;
border: 20px solid #032A46;
border-radius: 20px; -webkit-border-radius: 20px; -moz-border-radius: 20px; -khtml-border-radius: 20px;
box-shadow: 0px 25px 50px rgba(0,0,0, 0.5); -webkit-box-shadow:0px 25px 50px rgba(0,0,0, 0.5); -moz-box-shadow:0px 25px 50px rgba(0,0,0, 0.5); -khtml-box-shadow:0px 25px 50px rgba(0,0,0, 0.5);

	
	border-color:  rgb(3,42,70); border-color:  rgba(3,42,70, 0.65); _border-color:  rgb(3,42,70);
}

.xenOverlay > .section,
.xenOverlay > .sectionMain
{
	background: none;
	margin: 0;
}

	.xenOverlay .section .heading,
	.xenOverlay .sectionMain .heading
	{
		border-radius: 0; -webkit-border-radius: 0; -moz-border-radius: 0; -khtml-border-radius: 0;
		margin-bottom: 0;
	}

	.xenOverlay .section .subHeading,
	.xenOverlay .sectionMain .subHeading
	{
		margin-top: 0;
	}

	.xenOverlay .section .sectionFooter,
	.xenOverlay .sectionMain .sectionFooter
	{
		overflow: hidden; zoom: 1;
	}
	
		.xenOverlay .sectionFooter .button,
		.xenOverlay .sectionFooter .buttonContainer
		{
			min-width: 75px;
			*min-width: 0;
			float: right;
		}
		
			.xenOverlay .sectionFooter .buttonContainer .button
			{
				float: none;
			}

/* The AJAX progress indicator overlay */

#AjaxProgress.xenOverlay
{
	width: 100%;
	overflow: hidden; zoom: 1;
}

	#AjaxProgress.xenOverlay .content
	{
		background: rgb(0, 0, 0) url('styles/default/xenforo/widgets/ajaxload.info_FFFFFF_facebook.gif') no-repeat center center; background: rgba(0,0,0, 0.5) url('styles/default/xenforo/widgets/ajaxload.info_FFFFFF_facebook.gif') no-repeat center center;
border-bottom-left-radius: 10px; -webkit-border-bottom-left-radius: 10px; -moz-border-radius-bottomleft: 10px; -khtml-border-bottom-left-radius: 10px;
float: right;
width: 85px;
height: 30px;

	}

/* Timed message for redirects */

.xenOverlay.timedMessage
{
	color: black;
background: transparent url('styles/default/xenforo/overlay/timed-message.png') repeat-x;
border-bottom: 1px solid black;
width: 100%;

}

	.xenOverlay.timedMessage .content
	{
		font-size: 18pt;
padding: 30px;
text-align: center;

	}
	
/* Inline Editor */

.xenOverlay .section .secondaryContent.messageContainer
{
	padding: 0;
}

.xenOverlay .section .messageContainer .mceLayout
{
	border: none;	
}

.xenOverlay .section .messageContainer tr.mceFirst td.mceFirst
{
	border-top: none;
}

.xenOverlay .section .messageContainer tr.mceLast td.mceLast,
.xenOverlay .section .messageContaner tr.mceLast td.mceIframeContainer
{
	border-bottom: none;
}

.xenOverlay .section .textCtrl.MessageEditor,
.xenOverlay .section .mceLayout,
.xenOverlay .section .bbCodeEditorContainer textarea
{
	width: 100% !important;
	min-height: 260px;
	_height: 260px;
	box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing: border-box;
}

.alerts .alertGroup
{
	margin-bottom: 20px;
}

.alerts .primaryContent
{
	overflow: hidden; zoom: 1;
	padding: 5px;
}

.alerts .avatar
{
	float: left;
}

.alerts .avatar img
{
	width: 32px;
	height: 32px;
}

.alerts .alertText
{
	margin-left: 32px;
	padding: 0 5px;
}

.alerts h3
{
	display: inline;
}

.alerts h3 .subject
{
	font-weight: bold;
}

.alerts .timeRow
{
	font-size: 11px;
	margin-top: 5px;
}

	.alerts .new .time,
	.alertsPopup .new .time
	{
		padding-right: 13px;
		background: transparent url('styles/default/xenforo/widgets/new-11.png') no-repeat right center;
	}

/** Data tables **/

table.dataTable
{
	width: 100%;
	_width: 99.5%;
	margin: 10px 0;
}

.dataTable tr.dataRow td
{
	border-bottom: 1px solid #d7edfc;
	padding: 5px 10px;
}

.dataTable tr.dataRow td.secondary
{
	background: rgb(249, 249, 250) url("styles/default/xenforo/gradients/category-23px-light.png") repeat-x top;
}

.dataTable tr.dataRow th
{
	background: #f9d9b0 url("styles/default/xenforo/gradients/category-23px-light.png") repeat-x top;
	border-bottom: 1px solid #f9bc6d;
	border-top: 1px solid #f9d9b0;
	color: #6d3f03;
	font-size: 11px;
	padding: 5px 10px;
}

.dataTable .dataRow .dataOptions
{
	text-align: right;
	white-space: nowrap;
	word-wrap: normal;
	padding: 0;
}

.dataTable .dataRow .dataOptions a.secondaryContent
{
	display: inline-block;
	border-left: 1px solid #d7edfc;
	border-bottom: none;
	padding: 7px 10px 6px;
	font-size: 11px;
}

	.dataTable .dataRow .dataOptions a.secondaryContent:hover
	{
		background-color: #d7edfc;
		text-decoration: none;
	}

	.dataTable .dataRow .delete
	{
		padding: 0px;
		width: 26px;
		border-left: 1px solid #d7edfc;
		background: rgb(249, 249, 250) url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
	}	
				
		.dataTable .dataRow .delete a
		{
			display: block;
			background: transparent url('styles/default/xenforo/permissions/deny.png') no-repeat center center;
			cursor: pointer;
		
			padding: 5px;
			width: 16px;
			height: 16px;
			
			overflow: hidden;
			white-space: nowrap;
			text-indent: -1000px;
		}

.memberListItem
{
	overflow: hidden;
zoom: 1;

}

	.memberListItem .avatar,
	.memberListItem .icon
	{
		float: left;

	}
	
	/* ----------------------- */
	
	.memberListItem .extra
	{
		font-size: 11px;
float: right;

	}

		.memberListItem .extra .DateTime
		{
			display: block;
		}
	
	.memberListItem .member
	{
		margin-left: 65px;

	}
	
	/* ----------------------- */
		
		.memberListItem h3.username
		{
			font-weight: bold;
font-size: 13pt;
margin-bottom: 3px;

		}
			
		.memberListItem .username.guest
		{
			font-style: italic;
font-weight: normal;

		}
	
	/* ----------------------- */
		
		.memberListItem .userInfo
		{
			font-size: 11px;
margin-bottom: 3px;

		}
		
			.memberListItem .userBlurb
			{
			}
		
				.memberListItem .userBlurb .userTitle
				{
					font-weight: bold;

				}
				
	
	/* ----------------------- */
		
		.memberListItem .member .contentInfo
		{
			margin-top: 5px;

		}
	
	/* ----------------------- */
	
	
/* extended member list items have a fixed 200px right column */

.memberListItem.extended .extra
{
	width: 200px;
}

.memberListItem.extended .member
{
	margin-right: 210px;
}

/** Facebook **/

.fbUnit
{
	padding-top: 5px;
}

a.fbLogin,
#loginBar a.fbLogin
{
	background: #29447e url(http://static.ak.fbcdn.net/images/connect_sprite.png);
	background-repeat: no-repeat;
	cursor: pointer;
	display: inline-block;
	padding: 0px 0px 0px 1px;
	text-decoration: none;
	outline: none;
	
	background-position: left -188px;
	font-size: 11px;
	line-height: 14px;
}

a.fbLogin:active,
#loginBar a.fbLogin:active
{
	background-position: left -210px;
}

a.fbLogin:hover,
#loginBar a.fbLogin:hover
{
	text-decoration: none;
}

	a.fbLogin span
	{
		background: #5f78ab url(http://static.ak.fbcdn.net/images/connect_sprite.png);
		border-top: solid 1px #879ac0;
		border-bottom: solid 1px #1a356e;
		color: white;
		display: block;
		font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
		font-weight: bold;
		padding: 2px 6px 3px 6px;
		margin: 1px 1px 0px 21px;
		text-shadow: none;
	}

	a.fbLogin:active span
	{
		border-bottom: solid 1px #29447e;
		border-top: solid 1px #45619d;
		background: #4f6aa3;
		text-shadow: none;
	}

/* ***************************** */
/* un-reset, mostly from YUI */

.baseHtml h1 { font-size:138.5%; } 
.baseHtml h2 { font-size:123.1%; }
.baseHtml h3 { font-size:108%; } 
.baseHtml h1, .baseHtml h2, .baseHtml h3 {  margin:1em 0; } 
.baseHtml h1, .baseHtml h2, .baseHtml h3, .baseHtml h4, .baseHtml h5, .baseHtml h6, .baseHtml strong { font-weight:bold; } 
.baseHtml abbr, .baseHtml acronym { border-bottom:1px dotted #000; cursor:help; }  
.baseHtml em {  font-style:italic; } 
.baseHtml blockquote, .baseHtml ul, .baseHtml ol, .baseHtml dl { margin:1em; } 
.baseHtml ol, .baseHtml ul, .baseHtml dl { margin-left:2em; } 
.baseHtml ul ul, .baseHtml ul ol, .baseHtml ul dl, .baseHtml ol ul, .baseHtml ol ol, .baseHtml ol dl, .baseHtml dl ul, .baseHtml dl ol, .baseHtml dl dl { margin-top:0; margin-bottom:0; }
.baseHtml ol li { list-style: decimal outside; } 
.baseHtml ul li { list-style: disc outside; } 
.baseHtml ol ul li, .baseHtml ul ul li { list-style-type: circle; }
.baseHtml ol ol ul li, .baseHtml ol ul ul li, .baseHtml ul ol ul li, .baseHtml ul ul ul li { list-style-type: square; }
.baseHtml ul ol li, .baseHtml ul ol ol li, .baseHtml ol ul ol li { list-style: decimal outside; }
.baseHtml dl dd { margin-left:1em; } 
.baseHtml th, .baseHtml td { border:1px solid #000; padding:.5em; } 
.baseHtml th { font-weight:bold; text-align:center; } 
.baseHtml caption { margin-bottom:.5em; text-align:center; } 
.baseHtml p, .baseHtml pre, .baseHtml fieldset, .baseHtml table { margin-bottom:1em; }

.PageNav
{
	font-size: 11px;
padding: 2px 0;
overflow: hidden;
zoom: 1;
line-height: 16px;
word-wrap: normal;

}
	
	.PageNav .pageNavHeader,
	.PageNav a,
	.PageNav .scrollable
	{
		display: block;
		float: left;
		margin-right: 3px;
	}
	
	.PageNav .pageNavHeader
	{
		padding: 1px 0;
	}

	.PageNav a
	{
		text-decoration: none;
border: 1px solid transparent;
border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -khtml-border-radius: 3px;
text-align: center;

		
		min-width: 19px;
	}
	
		.PageNav a[rel=start]
		{
			min-width: 19px !important;
		}

		.PageNav a.text
		{
			width: auto !important;
			padding: 0 4px;
		}
	
		.PageNav a
		{
			color: #8f6c3f;
background-color: #fff4e5;
border-color: #f9d9b0;
/* url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top*/

		}
		
		.PageNav a.currentPage
		{
			color: #2b485c;
background-color: rgb(249, 249, 250);
border-color: #6cb2e4;
position: relative;

		}

		a.PageNavPrev,
		a.PageNavNext
		{
			color: rgb(20,20,20);
background-color: transparent;
padding: 1px;
border: 1px none black;
cursor: pointer;

			
			width: 19px !important;
		}
		
		.PageNav a:hover,
		.PageNav a:focus
		{
			color: #8f6c3f;
text-decoration: none;
background-color: #f9d9b0;
border-color: #f9bc6d;

		}
		
	.PageNav a.distinct
	{
		margin-left: 3px;
	}
			
	.PageNav .scrollable
	{
		position: relative;
		overflow: hidden;
		width: 117px; /* width of 5 page numbers plus their margin & border */
		height: 18px; /* only needs to be approximate */
	}
	
		.PageNav .scrollable .items
		{
			display: block;
			width: 20000em; /* contains scrolling items, should be huge */
			position: absolute;
			display: block;
		}
		
/** Edge cases - large numbers of digits **/

.PageNav .gt999 
{
	font-size: 9px;
	letter-spacing: -0.05em; 
}

.PageNav.pn5 a { width: 29px; } .PageNav.pn5 .scrollable { width: 167px; }
.PageNav.pn6 a { width: 33px; } .PageNav.pn6 .scrollable { width: 187px; }
.PageNav.pn7 a { width: 37px; } .PageNav.pn7 .scrollable { width: 207px; }

/* ***************************** */
/* DL Name-Value Pairs */

.pairs dt,
.pairsInline dt,
.pairsRows dt,
.pairsColumns dt,
.pairsJustified dt
{
	color: rgb(150,150,150);
}

.pairsInline dl,
.pairsInline dt,
.pairsInline dd
{
	display: inline;
}

.pairsRows dt,
.pairsRows dd
{
	display: inline-block;
	vertical-align: top;

	*display: inline;
	*margin-right: 1ex;
	*zoom: 1;
}

dl.pairsColumns,
dl.pairsJustified,
.pairsColumns dl,
.pairsJustified dl
{
	overflow: hidden; zoom: 1;
}

.pairsColumns dt,
.pairsColumns dd
{
	float: left;
	width: 48%;
}

.pairsJustified dt
{
	float: left;
}
.pairsJustified dd
{
	float: right;
	text-align: right;
}


/* ***************************** */
/* Lists that put all elements on a single line */

.listInline ul,
.listInline ol,
.listInline li,
.listInline dl,
.listInline dt,
.listInline dd
{
	display: inline;
}

/* intended for use with .listInline, produces 'a, b, c, d' / 'a * b * c * d' lists */

.commaImplode li
{
	margin-right: 0.2em;
}

.commaImplode li:after,
.commaElements > *:after
{
	content: ',';
}

.commaImplode li:last-child:after,
.commaElements > *:last-child:after
{
	content: '';
}

.bulletImplode li:before
{
	content: '\2022\a0';
}

.bulletImplode li:first-child:before
{
	content: '';
}

/* Three column list display */

.threeColumnList
{
	overflow: hidden; zoom: 1;
}

.threeColumnList li
{
	float: left;
	width: 32%;
	margin: 2px 1% 2px 0;
}

/* ***************************** */
/* Preview tooltips (threads etc.) */

.previewTooltip
{
}
		
	.previewTooltip .avatar
	{
		float: left;
	}
	
	.previewTooltip .text
	{
		margin-left: 64px;
	}
	
		.previewTooltip blockquote
		{
			font-size: 11pt;
font-family: Arial, Helvetica, sans-serif;
text-decoration: none;
line-height: 1.4;

			
			font-size: 10pt;
			max-height: 150px;
			overflow: hidden;
		}
	
		.previewTooltip .posterDate
		{
			font-size: 11px;
			padding-top: 5px;
			border-top: 1px solid #d7edfc;
			margin-top: 5px;
		}

/* ***************************** */
/* List of block links */

.blockLinksList
{
	font-size: 11px;
padding: 2px;

}
		
	.blockLinksList a,
	.blockLinksList label
	{
		color: #176093;
padding: 5px 10px;
border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
display: block;
outline: 0 none;

	}
	
		.blockLinksList a:hover,
		.blockLinksList a:focus,
		.blockLinksList label:hover,
		.blockLinksList label:focus
		{
			text-decoration: none;
background-color: #d7edfc;

		}
		
		.blockLinksList a:active,
		.blockLinksList a.selected,
		.blockLinksList label:active,
		.blockLinksList label.selected
		{
			color: #032A46;
background-color: #a5cae4;

		}
		
		.blockLinksList a.selected,
		.blockLinksList label.selected
		{
			font-weight: bold;
display: block;

		}
		
		.blockLinksList span.depthPad
		{
			display: block;
		}

/* ***************************** */
/* Normally-indented nested lists */

.indentList ul,
.indentList ol
{
	margin-left: 2em;
}

/* ***************************** */
/* AJAX progress image */

.InProgress
{
	background: transparent url('styles/default/xenforo/widgets/ajaxload.info_B4B4DC_facebook.gif') no-repeat right center;
}

/* ***************************** */
/* Hidden inline upload iframe */

.hiddenIframe
{
	display: block;
	width: 500px;
	height: 300px;
}

/* ***************************** */
/* Exception display */

.traceHtml { font-size:11px; font-family:calibri, verdana, arial, sans-serif; }
.traceHtml .function { color:rgb(180,80,80); font-weight:normal; }
.traceHtml .file { font-weight:normal; }
.traceHtml .shade { color:rgb(128,128,128); }
.traceHtml .link { font-weight:bold; }

/* ***************************** */
/* Indenting for options */

._depth0 { padding-left:  0em; }
._depth1 { padding-left:  2em; }
._depth2 { padding-left:  4em; }
._depth3 { padding-left:  6em; }
._depth4 { padding-left:  8em; }
._depth5 { padding-left: 10em; }
._depth6 { padding-left: 12em; }
._depth7 { padding-left: 14em; }
._depth8 { padding-left: 16em; }
._depth9 { padding-left: 18em; }

.xenOverlay .errorOverlay
{
	color: white;
	padding: 25px;
	border-radius: 20px; -webkit-border-radius: 20px; -moz-border-radius: 20px; -khtml-border-radius: 20px;	
	border:  20px solid rgb(0,0,0); border:  20px solid rgba(0,0,0, 0.25); _border:  20px solid rgb(0,0,0);
	
	background: url(rgba.php?r=0&g=0&b=0&a=191); background: rgba(0,0,0, 0.75); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#BF000000,endColorstr=#BF000000);
}

	.xenOverlay .errorOverlay .heading
	{
		padding: 5px 10px;
		font-weight: bold;
		font-size: 12pt;
		background: rgb(180,0,0);
		color: white;
		margin-bottom: 10px;
		border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
		border: 1px solid rgb(100,0,0);
	}

	.xenOverlay .errorOverlay li
	{
		line-height: 2;
	}

/*** inline errors ***/

.formValidationInlineError
{
	display: none;
	position: absolute;
	z-index: 5000;
	background-color: white;
	border: 1px solid rgb(180,0,0);
	color: rgb(180,0,0);
	box-shadow: 2px 2px 10px #999; -webkit-box-shadow:2px 2px 10px #999; -moz-box-shadow:2px 2px 10px #999; -khtml-box-shadow:2px 2px 10px #999;
	border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -khtml-border-radius: 3px;
	padding: 2px 5px;
	font-size: 11px;
	width: 175px;
	min-height: 2.5em;
	_height: 2.5em;
}

/** Block errors **/

.errorPanel
{
	margin: 10px 0 20px;
	color: rgb(180,0,0);
	background: rgb(255, 235, 235);
	border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
	border: 1px solid rgb(180,0,0);
}

	.errorPanel .errorHeading
	{
		margin: .75em;
		font-weight: bold;
		font-size: 12pt;
	}
	
	.errorPanel .errors
	{
		margin: .75em 2em;
		display: block;
		line-height: 1.5;
	}

/* Undo some nasties */

input[type=search]
{
	-webkit-appearance: textfield;
	-webkit-box-sizing: content-box;
}

/* Basic, common, non-templated BB codes */

.bbCodeImage
{
	max-width: 100%;
}

.bbCodeImageFullSize
{
	position: absolute;
	z-index: 50000;
}

img.smilie
{
	vertical-align: text-bottom;
	margin: 0 1px;
}

.bbCodeStrike
{
	text-decoration: line-through;
}

/* --- form.css --- */

/** Forms **/

.xenForm
{
	margin: 10px auto;
	width: 750px;
}

	.xenOverlay .xenForm
	{
		width: 600px;
	}

.xenForm .ctrlUnit dd
{
	width: 500px;
}

	.xenOverlay .xenForm .ctrlUnit dd
	{
		width: 350px;
	}

.xenForm .ctrlUnit dd .textCtrl
{
	width: 462px;
}

	.xenOverlay .xenForm .ctrlUnit dd .textCtrl
	{
		width: 362px;
	}

.xenForm .ctrlUnit dd li > ul .textCtrl
{
	width: 442px;
}

	.xenOverlay .xenForm .ctrlUnit dd li > ul .textCtrl
	{
		width: 342px;
	}

.xenForm .ctrlUnit.fullWidth dd .textCtrl,
.xenForm .ctrlUnit.fullWidth dd .explain,
.xenForm .ctrlUnit.fullWidth dd .mceLayout
{
	margin-left: 30px;
}

.xenForm .ctrlUnit.fullWidth dd .textCtrl,
.xenForm .ctrlUnit.fullWidth dd .mceLayout
{
	width: 682px;
}

	.xenOverlay .xenForm .ctrlUnit.fullWidth dd .textCtrl,
	.xenOverlay .xenForm .ctrlUnit.fullWidth dd .mceLayout
	{
		width: 532px;
	}

	.xenForm .ctrlUnit dd > select.textCtrl
	{
		min-width: 150px;
	}

	.xenForm .ctrlUnit dd .textCtrl[size],
	.xenForm .ctrlUnit dd .textCtrl.autoSize,
	.xenForm .ctrlUnit.fullWidth dd .textCtrl[size],
	.xenForm .ctrlUnit.fullWidth dd .textCtrl.autoSize,
	.xenOverlay .xenForm .ctrlUnit dd .textCtrl[size],
	.xenOverlay .xenForm .ctrlUnit dd .textCtrl.autoSize,
	.xenOverlay .xenForm .ctrlUnit.fullWidth dd .textCtrl[size],
	.xenOverlay .xenForm .ctrlUnit.fullWidth dd .textCtrl.autoSize
	{
		width: auto !important;
		min-width: 0;
	}

	.xenForm .ctrlUnit dd .textCtrl.number
	{
		width: 150px;
	}


.xenForm .sectionHeader:first-child
{
	margin-top: 0;
}

/** Sections **/

.xenForm fieldset
{
	border-top: 1px solid #d7edfc;
	margin: 20px auto;
}

.xenForm > fieldset:first-child
{
	border-top: none;
	margin: auto;
}

.xenForm .PreviewContainer + fieldset
{
	border-top: none;
}

.xenForm fieldset + .ctrlUnit,
.xenForm .submitUnit
{
	border-top: 1px solid #d7edfc;
}

.xenForm fieldset + .ctrlUnit
{
	padding-top: 10px;
}

.xenForm .primaryContent + .submitUnit,
.xenForm .secondaryContent + .submitUnit
{
	margin-top: 0;
	border-top: none;
}

.xenForm .ctrlUnit.submitUnit dd
{
	/*url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;*/
	/*background: rgb(249, 249, 250);
	
	border: 1px solid #d7edfc;
	border-bottom-color: #a5cae4;
	border-top: none;
	padding-left: 10px;
	
	width: 458px;
	
	border-bottom-left-radius: 5px; -webkit-border-bottom-left-radius: 5px; -moz-border-radius-bottomleft: 5px; -khtml-border-bottom-left-radius: 5px;
	border-bottom-right-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-radius-bottomright: 5px; -khtml-border-bottom-right-radius: 5px;*/
	
	padding-top: 5px;
	padding-bottom: 5px;
}

/* now undo that */

.xenOverlay .ctrlUnit.submitUnit dd,
.Menu .ctrlUnit.submitUnit dd,
#QuickSearch .ctrlUnit.submitUnit dd
{
	border: none;
	background: none;
}

.xenForm .ctrlUnit
{
	
}

	/** Sections Immediately Following Headers **/

	.xenForm .sectionHeader + fieldset,
	.xenForm .heading + fieldset,
	.xenForm .subHeading + fieldset
	{
		border-top: none;
		margin-top: 0;
	}


/** *********************** **/
/** TEXT INPUTS             **/
/** *********************** **/

.textCtrl
{
	font-size: 13px;
font-family: Arial, Helvetica, sans-serif;
color: #000000;
background-color: #ffffff;
padding: 3px;
margin-bottom: 2px;
border: 1px solid silver;
border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;
outline: 0;

}

textarea.textCtrl
{
	word-wrap: break-word;
}

	.textCtrl:focus
	{
		background: rgb(255,255,240) url('styles/default/xenforo/gradients/form-element-focus-25.png') repeat-x;
border-top-color: rgb(150,150,150);
border-bottom-color: rgb(230,230,230);

	}	

	textarea.textCtrl:focus
	{
		background-image: url('styles/default/xenforo/gradients/form-element-focus-100.png');

	}

	input.textCtrl.disabled,
	textarea.textCtrl.disabled,
	.disabled .textCtrl
	{
		font-style: italic;
color: rgb(100,100,100);
background-color: rgb(245,245,245);

	}

	.textCtrl.prompt
	{
		font-style: italic;
color: rgb(160,160,160);

	}

	.textCtrl::-webkit-input-placeholder
	{
		font-style: italic;
color: rgb(160,160,160);

	}
	
	.textCtrl.autoSize
	{
		width: auto !important;
	}

	.textCtrl.number
	{
		text-align: right;
		width: 150px;
	}

	.textCtrl.code
	{
		font-family: Consolas, "Courier New", Courier, monospace;
white-space: pre;
word-wrap: normal;
direction: ltr;

	}
	
	input.textCtrl[type="password"]
	{
		font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
	}

	input[type="email"],
	input[type="url"]
	{
		direction: ltr;
	}

textarea.textCtrl.Elastic
{
	/* use for jQuery.elastic */
	max-height: 300px;
}











/** *********************** **/
/** BUTTONS                 **/
/** *********************** **/

.button
{
	font-size: 12px;
font-family: Arial, Helvetica, sans-serif;
color: rgb(0, 0, 0);
background: rgb(220,220,235) url('styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
padding: 0px 6px;
border: 1px solid rgb(221, 221, 235);
border-top-color: rgb(255, 255, 255);
border-bottom-color: rgb(179, 179, 189);
border-radius: 7px; -webkit-border-radius: 7px; -moz-border-radius: 7px; -khtml-border-radius: 7px;
text-align: center;
box-shadow: 0px 1px 4px 0px rgb(200,200,210); -webkit-box-shadow:0px 1px 4px 0px rgb(200,200,210); -moz-box-shadow:0px 1px 4px 0px rgb(200,200,210); -khtml-box-shadow:0px 1px 4px 0px rgb(200,200,210);
text-shadow: 0px -1px 2px white;
outline: none;
line-height: 23px;
display: inline-block;
cursor: pointer;
box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing: border-box;
height: 23px;

}

.button.smallButton
{
	font-size: 11px;
	padding: 0px 4px;
	line-height: 21px;
	height: 21px;
	border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
}

.button.primary
{
	background-color: #a5cae4;
}

input.button.disabled,
a.button.disabled,
input.button.primary.disabled,
a.button.primary.disabled
{
	color: #999;
	background-color: #EEE;
	border-color: #CCC;
	box-shadow: 0 0 0 transparent; -webkit-box-shadow:0 0 0 transparent; -moz-box-shadow:0 0 0 transparent; -khtml-box-shadow:0 0 0 transparent;
}

	.button::-moz-focus-inner
	{
		border: none;
	}

	a.button
	{
		display: inline-block;
		color: black;
	}

	.button:hover,
	.button[href]:hover,
	.buttonProxy:hover .button
	{
		color: black;
text-decoration: none;
background-color: rgb(255,255,200);
border-color: rgb(255,255,200);
border-top-color: white;
border-bottom-color: rgb(190,190,170);

	}

	.button:focus
	{
		border-color: #6cb2e4;

	}

	.button:active,
	.button.ToggleButton.checked,
	.buttonProxy:active .button
	{
		background-color: rgb(153, 153, 163);
border-color: rgb(200,200,215);
border-top-color: #b3b3bd;
border-bottom-color: white;
box-shadow: 0px 0px 0px 0px transparent; -webkit-box-shadow:0px 0px 0px 0px transparent; -moz-box-shadow:0px 0px 0px 0px transparent; -khtml-box-shadow:0px 0px 0px 0px transparent;
outline: 0;

	}

	.button.ToggleButton
	{
		cursor: default;
		width: auto;
	}
	
	.button.ToggleButton.checked
	{
		background-color: rgb(255,150,50);
	}

	.button.inputSupplementary
	{
		width: 25px;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.button.inputSupplementary.add
	{
		color: green;
	}

	.button.inputSupplementary.delete
	{
		color: red;
	}

	.submitUnit .button
	{
		min-width: 100px;
		*min-width: 0;
	}

















/** Control Units **/

.xenForm .ctrlUnit
{
	position: relative;
	margin: 10px auto;
}

/* clearfix */ .xenForm .ctrlUnit { zoom: 1; } .xenForm .ctrlUnit:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.xenForm .ctrlUnit.fullWidth
{
	overflow: visible;
}

/** Control Unit Labels **/

.xenForm .ctrlUnit dt
{
	padding-top: 4px;
padding-right: 15px;
text-align: right;
vertical-align: top;

	width: 235px;
	float: left;
}

	.xenOverlay .xenForm .ctrlUnit dt
	{
		width: 185px;
	}

.xenForm .ctrlUnit.fullWidth dt,
.xenForm .ctrlUnit.submitUnit.fullWidth dt
{
	float: none;
	width: auto;
	text-align: left;
	height: auto;
}

	.xenForm .ctrlUnit dt label
	{
		margin-left: 30px;
	}

	/** Hidden Labels **/

	.xenForm .ctrlUnit.surplusLabel dt label
	{
		display: none;
	}

	/** Section Links **/

	.ctrlUnit.sectionLink dt
	{
		text-align: left;
		font-size: 11px;
		padding-top: none;
	}

		.ctrlUnit.sectionLink dt a
		{
			margin-left: 11px; /*TODO: sectionHeader padding + border*/
		}		

	/** Hints **/

	.ctrlUnit dt dfn
	{
		font-style: italic;
font-size: 10px;
color: rgb(150,150,150);
margin-left: 30px;
display: block;

	}

	/** Inline Errors **/

	.ctrlUnit dt .error
	{
		font-size: 10px;
color: red;
display: block;

	}
	
	.ctrlUnit dt dfn,
	.ctrlUnit dt .error,
	.ctrlUnit dt a
	{
		font-weight: normal;
	}

.xenForm .ctrlUnit.submitUnit dt
{
	height: 19px;
	display: block;
}

	.ctrlUnit.submitUnit dt.InProgress
	{
		background: transparent url('styles/default/xenforo/widgets/ajaxload.info_B4B4DC_facebook.gif') no-repeat center center;
	}

/** Control Holders **/

.xenForm .ctrlUnit dd
{
	/*todo: kill property */
	
	float: left;
}

.xenForm .ctrlUnit.fullWidth dd
{
	float: none;
	width: auto;
	padding-left: 0;
}

/** Explanatory paragraph **/

.ctrlUnit dd .explain
{
	font-size: 11px;
color: rgb(150,150,150);
margin-top: 2px;
margin-right: 30px;

	/*TODO:max-width: auto;*/
}

.ctrlUnit.fullWidth dd .explain
{
	/*TODO:max-width: auto;*/
}

/** List items inside controls **/

.ctrlUnit dd li
{
	margin: 4px 0;
}

/** Hints underneath checkbox / radio controls **/

.ctrlUnit dd li .hint
{
	font-size: 11px;
	color: rgb(150,150,150);
	margin-left: 20px;
}

/** DISABLERS **/

.ctrlUnit dd li > ul
{
	margin-left: 20px;
}
	
/** Other stuff... **/

.ctrlUnit dd .helpLink
{
	font-size: 10px;
}

.ctrlUnit.textValue dt
{
	padding-top: 0px;
}

/*.xenForm .ctrlUnit dt { background-color: #ffffc6; }
.xenForm .ctrlUnit dd { background-color: rgb(235,235,255); }
.xenForm fieldset { background-color: rgb(255,230,180); }*/

.button.spinBoxButton
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11pt;
}

#calroot
{
	margin-top: -1px;
	width: 198px;
	padding: 2px;
	background-color: rgb(252, 252, 255);
	font-size: 11px;
	border: 1px solid #65a5d1;
	border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
	box-shadow: 0 0 15px #666; -webkit-box-shadow:0 0 15px #666; -moz-box-shadow:0 0 15px #666; -khtml-box-shadow:0 0 15px #666;
	z-index: 7500;
}

#calhead
{	
	padding: 2px 0;
	height: 22px;
} 

	#caltitle {
		font-size: 11pt;
		color: #65a5d1;
		float: left;
		text-align: center;
		width: 155px;
		line-height: 20px;
	}
	
	#calnext, #calprev {
		display: block;
		width: 20px;
		height: 20px;
		font-size: 11pt;
		line-height: 20px;
		text-align: center;
		float: left;
		cursor: pointer;
	}

	#calnext {
		float: right;
	}

	#calprev.caldisabled, #calnext.caldisabled {
		visibility: hidden;	
	}

#caldays {
	height: 14px;
	border-bottom: 1px solid #65a5d1;
}

	#caldays span {
		display: block;
		float: left;
		width: 28px;
		text-align: center;
		color: #65a5d1;
	}

#calweeks {
	margin-top: 4px;
}

.calweek {
	clear: left;
	height: 22px;
}

	.calweek a {
		display: block;
		float: left;
		width: 27px;
		height: 20px;
		text-decoration: none;
		font-size: 11px;
		margin-left: 1px;
		text-align: center;
		line-height: 20px;
		border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -khtml-border-radius: 3px;
	} 
	
		.calweek a:hover, .calfocus {
			background-color: rgb(249, 249, 250);
		}

a.caloff {
	color: rgb(150,150,150);		
}

a.caloff:hover {
	background-color: rgb(249, 249, 250);		
}

a.caldisabled {
	background-color: #efefef !important;
	color: #ccc	!important;
	cursor: default;
}

#caltoday {
	font-weight: bold;
}

#calcurrent {
	background-color: #65a5d1;
	color: rgb(249, 249, 250);
}
ul.autoCompleteList
{
	background: rgb(249, 249, 250) url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
	
	border: 1px solid #6cb2e4;
	padding: 2px;
	
	font-size: 11px;
	
	min-width: 180px;
	_width: 180px;
	
	z-index: 1000;
}

ul.autoCompleteList li
{
	padding: 5px 10px;
}

ul.autoCompleteList li:hover,
ul.autoCompleteList li.selected
{
	background-color: #d7edfc;
	border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
}

/** status editor **/

.statusEditorCounter
{
	color: green;
}

.statusEditorCounter.warning
{
	color: orange;
	font-weight: bold;
}

.statusEditorCounter.error
{
	color: red;
	font-weight: bold;
}

.explain .statusHeader
{
	display: inline;
}

.explain .CurrentStatus
{
	color: rgb(20,20,20);
	font-style: italic;
	padding-left: 5px;
}

/* BB code-based editor styling */

.xenForm .ctrlUnit.fullWidth dd .bbCodeEditorContainer
{
	margin-left: 30px;
}

.xenForm .ctrlUnit.fullWidth dd .bbCodeEditorContainer textarea
{
	margin-left: 0;
	min-height: 200px;
}

.bbCodeEditorContainer a
{
	font-size: 11px;
}


/* phpclubBBCodeEditor buttons */
div.bbCodeEditorButton
{
	display: inline; 
	cursor: pointer; 
	font-size: 120%; 
	padding: 2px 6px; 
	margin: 2px 2px 2px 0pt;
}

div.bbCodeBold
{
	font-weight: bold;
}
div.bbCodeItalic
{
	font-style:italic;
}
div.bbCodeUnderLine
{
	text-decoration: underline;
}

/* --- public.css --- */

#header
{
	background: #176093 url('styles/default/russian/topbg.gif');

}

/* clearfix */ #header .pageWidth .pageContent { zoom: 1; } #header .pageWidth .pageContent:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

	#logo
	{
		display: block;
		float: left;
		line-height: 65px;
		height: 65px;
		vertical-align: middle;
	}

		/* IE6/7 vertical align fix */
		#logo span
		{
			*display: inline-block;
			*height: 100%;
		}

		#logo a:hover
		{
			text-decoration: none;
		}

		#logo img
		{
			vertical-align: middle;
		}

	#visitorInfo
	{
		float: right;
		min-width: 250px;
		_width: 250px;
		overflow: hidden; zoom: 1;
		background: #a5cae4;
		padding: 5px;
		border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
		margin: 10px 0;
		border: 1px solid #032A46;
		color: #032A46;
	}

		#visitorInfo .avatar
		{
			float: left;
			display: block;
		}

			#visitorInfo .avatar .img
			{
				border-color: #65a5d1;
			}

		#visitorInfo .username
		{
			font-size: 18px;
			text-shadow: 1px 1px 10px white;
			color: #032A46;
			white-space: nowrap;
			word-wrap: normal;
		}

		#alerts
		{
			zoom: 1;
		}

		#alerts #alertMessages
		{
			padding-left: 5px;
		}

		#alerts li.alertItem
		{
			font-size: 11px;
		}

			#alerts .label
			{
				color: #032A46;
			}

.footer .pageContent
{
	font-size: 11px;
color: #a5cae4;
background-color: #176093;
border-bottom-right-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-radius-bottomright: 5px; -khtml-border-bottom-right-radius: 5px;
border-bottom-left-radius: 5px; -webkit-border-bottom-left-radius: 5px; -moz-border-radius-bottomleft: 5px; -khtml-border-bottom-left-radius: 5px;
overflow: hidden;
zoom: 1;

}
	
	.footer a,
	.footer a:visited
	{
		color: #a5cae4;
padding: 5px;
display: block;

	}
	
		.footer a:hover,
		.footer a:active
		{
			color: #d7edfc;

		}

	.footer .choosers
	{
		padding-left: 5px;
float: left;
overflow: hidden;
zoom: 1;

	}
	
		.footer .choosers dt
		{
			display: none;
		}
		
		.footer .choosers dd
		{
			float: left;
		}
		
	.footerLinks
	{
		padding-right: 5px;
float: right;
overflow: hidden;
zoom: 1;

	}
	
		.footerLinks li
		{
			float: left;
		}

.footerLegal .pageContent
{
	font-size: 11px;
	overflow: hidden; zoom: 1;
	padding: 5px 0 15px;
	text-align: center;
}
	
	#copyright
	{
		color: rgb(100,100,100);
		float: left;
	}
	
	#legal
	{
		float: right;
	}
	
		#legal li
		{
			float: left;
			margin-left: 10px;
		}

.breadBoxTop,
.breadBoxBottom
{
	padding: 10px 5px;
margin: 0 -5px;
overflow: hidden;
zoom: 1;
clear: both;

}

.breadBoxTop
{
}

.breadBoxTop .topCtrl
{
	margin-left: 5px;
float: right;
line-height: 24px;

}

.breadcrumb
{
	font-size: 11px;
background: rgb(249, 249, 250) url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
border: 1px solid #a5cae4;
border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
overflow: hidden;
zoom: 1;

}

.breadcrumb .boardTitle
{
	display: none;

}

.breadcrumb .crust
{
	display: block;
float: left;
position: relative;
zoom: 1;

}

.breadcrumb .crust a.crumb
{
	text-decoration: none;
background-color: rgb(249, 249, 250);
padding: 0 10px 0 18px;
margin-bottom: -1px;
border-bottom: 1px solid #a5cae4;
outline: 0 none;
-moz-outline-style: 0 none;
display: block;
line-height: 24px;
_border-bottom: none;

}

	.breadcrumb .crust:first-child a.crumb
	{
		padding-left: 10px;
border-top-left-radius: 4px; -webkit-border-top-left-radius: 4px; -moz-border-radius-topleft: 4px; -khtml-border-top-left-radius: 4px;
border-bottom-left-radius: 4px; -webkit-border-bottom-left-radius: 4px; -moz-border-radius-bottomleft: 4px; -khtml-border-bottom-left-radius: 4px;

	}
	
	.breadcrumb .crust:last-child a.crumb
	{
		font-weight: bold;

	}

.breadcrumb .crust .arrow
{
	border: 12px solid transparent;
border-right: 1px none black;
border-left-color: #a5cae4;
display: block;
position: absolute;
right: -12px;
top: 0px;
z-index: 50;
width: 0px;
height: 0px;

}

.breadcrumb .crust .arrow span
{
	border: 12px solid transparent;
border-right: 1px none black;
border-left-color: rgb(249, 249, 250);
display: block;
position: absolute;
left: -13px;
top: -12px;
z-index: 51;
white-space: nowrap;
overflow: hidden;
text-indent: 9999px;
width: 0px;
height: 0px;

}

.breadcrumb .crust:hover a.crumb
{
	background-color: #d7edfc;

}

.breadcrumb .crust:hover .arrow span
{
	border-left-color: #d7edfc;
}

	.breadcrumb .crust .arrow
	{
		/* hide from IE6 */
		_display: none;
	}

.breadcrumb .jumpMenuTrigger
{
	background: transparent url('styles/default/xenforo/widgets/quicknav.png') no-repeat;
margin: 5px;
display: block;
float: right;
white-space: nowrap;
text-indent: 9999px;
overflow: hidden;
width: 13px;
height: 13px;

}

#moderatorBar
{
	background-color: #032A46;
	border-bottom: 1px solid #65a5d1;
	font-size: 11px;
}

/* clearfix */ #moderatorBar { zoom: 1; } #moderatorBar:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

#moderatorBar .pageContent
{
	padding: 2px 0;
}

#moderatorBar a
{
	display: inline-block;
	padding: 2px 10px;
	border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -khtml-border-radius: 3px;
}

#moderatorBar a,
#moderatorBar .itemCount
{
	color: #a5cae4;
}

	#moderatorBar a:hover
	{
		text-decoration: none;
		background-color: #2b485c;
		color: #d7edfc;
	}

/* TODO: maybe sort out the vertical alignment of the counters so they they are properly centered */

#moderatorBar .itemLabel,
#moderatorBar .itemCount
{
	display: inline-block;
	height: 16px;
	line-height: 16px;
}

#moderatorBar .itemCount
{	
	background: #2b485c;
	padding-left: 6px;
	padding-right: 6px;
	
	text-align: center;
	
	font-weight: bold;
	
	border-radius: 2px; -webkit-border-radius: 2px; -moz-border-radius: 2px; -khtml-border-radius: 2px;
	text-shadow: none;
}

	#moderatorBar .itemCount.alert
	{
		background: #e03030;
		color: white;
		box-shadow: 2px 2px 5px rgba(0,0,0, 0.25); -webkit-box-shadow:2px 2px 5px rgba(0,0,0, 0.25); -moz-box-shadow:2px 2px 5px rgba(0,0,0, 0.25); -khtml-box-shadow:2px 2px 5px rgba(0,0,0, 0.25);
	}
	
#moderatorBar .adminLink
{
	float: right;
}

#moderatorBar .permissionTest,
#moderatorBar .permissionTest:hover
{
	background: #e03030;
	color: white;
	box-shadow: 2px 2px 5px rgba(0,0,0, 0.25); -webkit-box-shadow:2px 2px 5px rgba(0,0,0, 0.25); -moz-box-shadow:2px 2px 5px rgba(0,0,0, 0.25); -khtml-box-shadow:2px 2px 5px rgba(0,0,0, 0.25);
	font-weight: bold;
}

#navigation .pageContent
{
	height: 52px;
	position: relative;
}

	.navTabs
	{
		font-size: 11px;
background-color: #2b485c;
padding: 0 25px;
border: 1px solid #65a5d1;
border-bottom: 1px solid #032A46;
border-top-left-radius: 10px; -webkit-border-top-left-radius: 10px; -moz-border-radius-topleft: 10px; -khtml-border-top-left-radius: 10px;
border-top-right-radius: 10px; -webkit-border-top-right-radius: 10px; -moz-border-radius-topright: 10px; -khtml-border-top-right-radius: 10px;

		
		height: 25px;
	}
	
		.navTabs .publicTabs
		{
			float: left;
		}
		
		.navTabs .visitorTabs
		{
			float: right;
		}
	
			.navTabs .navTab
			{
				float: left;
				white-space: nowrap;
				word-wrap: normal;
			}


/* ---------------------------------------- */
/* Links Inside Tabs */

.navTabs .navLink,
.navTabs .SplitCtrl
{
	display: block;
float: left;
vertical-align: text-bottom;
text-align: center;
outline: 0 none;

	
	height: 25px;
	line-height: 25px;
}

	.navTabs .publicTabs .navLink
	{
		padding: 0 15px;
	}
	
	.navTabs .visitorTabs .navLink
	{
		padding: 0 10px;
	}
	
	.navTabs .navLink:hover
	{
		text-decoration: none;
	}
	
	/* ---------------------------------------- */
	/* unselected tab, popup closed */
	
	.navTabs .navTab.PopupClosed .navLink
	{
		color: #a5cae4;
	}
	
		.navTabs .navTab.PopupClosed:hover
		{
			background-color: #176093;
		}
		
			.navTabs .navTab.PopupClosed .navLink:hover
			{
				color: #ffffff;
			}
		
	.navTabs .navTab.PopupClosed .arrowWidget
	{
		background-image: url('styles/default/xenforo/widgets/circle-arrow-down-light.png');
	}
	
	.navTabs .navTab.PopupClosed .SplitCtrl
	{
		margin-left: -14px;
		width: 14px;
	}
		
		.navTabs .navTab.PopupClosed:hover .SplitCtrl
		{
			background: transparent url('styles/default/xenforo/widgets/nav_menu_gadget.png') no-repeat center right;
		}
	
	/* ---------------------------------------- */
	/* selected tab */

	.navTabs .navTab.selected .navLink
	{
		font-weight: bold;
color: #032A46;
background: #65a5d1 url('styles/default/xenforo/gradients/navigation-tab.png') repeat-x top;
padding-top: 2px;
margin-top: -2px;
border: 1px solid #032A46;
border-bottom: 1px none black;
border-top-left-radius: 2px; -webkit-border-top-left-radius: 2px; -moz-border-radius-topleft: 2px; -khtml-border-top-left-radius: 2px;
border-top-right-radius: 2px; -webkit-border-top-right-radius: 2px; -moz-border-radius-topright: 2px; -khtml-border-top-right-radius: 2px;
text-shadow: 0px 0px 3px rgb(249, 249, 250);

	}
	
	.navTabs .navTab.selected .SplitCtrl
	{
		display: none;
	}
	
	.navTabs .navTab.selected .arrowWidget
	{
		background-image: url('styles/default/xenforo/widgets/circle-arrow-down.png');
	}
	
	/* ---------------------------------------- */
	/* unselected tab, popup open */
	
	.navTabs .navTab.PopupOpen .navLink
	{
	}
	
	
	/* ---------------------------------------- */
	/* selected tab, popup open (account) */
	
	.navTabs .navTab.selected.PopupOpen .navLink
	{
		color: #2b485c;
background: #6cb2e4 url('styles/default/xenforo/gradients/tab-unselected-25px-light.png') repeat-x top;
border-top-left-radius: 3px; -webkit-border-top-left-radius: 3px; -moz-border-radius-topleft: 3px; -khtml-border-top-left-radius: 3px;
border-top-right-radius: 3px; -webkit-border-top-right-radius: 3px; -moz-border-radius-topright: 3px; -khtml-border-top-right-radius: 3px;
border-bottom-right-radius: 0px; -webkit-border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px; -khtml-border-bottom-right-radius: 0px;
border-bottom-left-radius: 0px; -webkit-border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; -khtml-border-bottom-left-radius: 0px;
text-shadow: 1px 1px 2px white;
box-shadow: 0px 5px 5px rgba(0,0,0, 0.5); -webkit-box-shadow:0px 5px 5px rgba(0,0,0, 0.5); -moz-box-shadow:0px 5px 5px rgba(0,0,0, 0.5); -khtml-box-shadow:0px 5px 5px rgba(0,0,0, 0.5);

	}
	
/* ---------------------------------------- */
/* Second Row */

.navTabs .navTab.selected .tabLinks
{
	background: #65a5d1 url('styles/default/xenforo/gradients/navigation-tab.png') repeat-x top;
	
	width: 100%;	
	padding: 0;
	border: none;
	overflow: hidden; zoom: 1;	
	position: absolute;
	left: 0px;	
	top: 27px;
	height: 25px;
	background-position: 0px -25px;
	*clear:expression(style.width = document.getElementById('navigation').offsetWidth + 'px', style.clear = "none", 0);
}

	.navTabs .navTab.selected .blockLinksList
	{
		background: none;
		padding: 0;
		border: none;
	}

	.navTabs .navTab.selected .tabLinks .menuHeader
	{
		display: none;
	}
	
	.navTabs .navTab.selected .tabLinks li
	{
		float: left;
		padding: 2px 0;
	}

		.navTabs .navTab.selected .tabLinks li:first-child
		{
			margin-left: 8px;
		}
	
		.navTabs .navTab.selected .tabLinks a
		{
			font-size: 11px;
color: #ffffff;
padding: 1px 10px;
display: block;

			
			line-height: 19px;
		}
		
			.navTabs .navTab.selected .tabLinks a:hover,
			.navTabs .navTab.selected .tabLinks a:focus
			{
				color: #2b485c;
text-decoration: none;
background-color: #d7edfc;
padding: 0 9px;
border: 1px solid #6cb2e4;
border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
text-shadow: 1px 1px 0px rgb(249, 249, 250);
outline: 0;

				
			}
	
/* ---------------------------------------- */
/* Alert Balloons */
	
.navTabs .navLink .itemCount
{
	font-weight: bold;
font-size: 9px;
color: white;
background-color: #e03030;
padding: 0 2px;
border-radius: 2px; -webkit-border-radius: 2px; -moz-border-radius: 2px; -khtml-border-radius: 2px;
position: absolute;
right: 2px;
top: -12px;
line-height: 16px;
min-width: 12px;
_width: 12px;
text-align: center;
text-shadow: none;
white-space: nowrap;
word-wrap: normal;
box-shadow: 2px 2px 5px rgba(0,0,0, 0.25); -webkit-box-shadow:2px 2px 5px rgba(0,0,0, 0.25); -moz-box-shadow:2px 2px 5px rgba(0,0,0, 0.25); -khtml-box-shadow:2px 2px 5px rgba(0,0,0, 0.25);
height: 16px;

}

	.navTabs .navLink .itemCount .arrow
	{
		border: 3px solid transparent;
border-top-color: #e03030;
border-bottom: 1px none black;
position: absolute;
bottom: -3px;
right: 4px;
line-height: 0px;
text-shadow: none;
_display: none;
/* Hide from IE6 */
width: 0px;
height: 0px;

	}
	
/* ---------------------------------------- */
/* Account Popup Menu */

.navTabs .navTab.account .navLink
{
	font-weight: bold;
}

#AccountMenu
{
	width: 274px;
}

#AccountMenu .menuHeader
{
	position: relative;
}

	#AccountMenu .menuHeader .avatar
	{
		float: left;
		margin-right: 10px;
	}

	#AccountMenu .menuHeader .visibilityForm
	{
		margin-top: 10px;
		color: #176093;
	}
	
	#AccountMenu .menuHeader .links .fl
	{
		position: absolute;
		bottom: 10px;
		left: 116px;
	}

	#AccountMenu .menuHeader .links .fr
	{
		position: absolute;
		bottom: 10px;
		right: 10px;
	}
	
#AccountMenu .menuColumns
{
	overflow: hidden; zoom: 1;
	padding: 2px;
}

	#AccountMenu .menuColumns ul
	{
		float: left;
		padding: 0;
		max-height: none;
		overflow: hidden;
	}

		#AccountMenu .menuColumns a,
		#AccountMenu .menuColumns label
		{
			width: 115px;
		}

#AccountMenu .statusPoster textarea
{
	width: 245px;
	margin: 0;
	resize: vertical;
}

#AccountMenu .statusPoster .submitUnit
{
	margin-top: 5px;
	text-align: right;
}

	#AccountMenu .statusPoster .submitUnit .statusEditorCounter
	{
		float: left;
		line-height: 23px;
		height: 23px;
	}
	
/* ---------------------------------------- */
/* Inbox, Alerts Popups */

.navPopup
{
	width: 260px;
}

.navPopup a:hover,
.navPopup .listItemText a:hover
{
	background: none;
	text-decoration: underline;
}

	.navPopup .menuHeader .InProgress
	{
		float: right;
		display: block;
		width: 20px;
		height: 20px;
	}

.navPopup .listPlaceholder
{
	max-height: 350px;
	overflow: auto;
}

	.navPopup .listPlaceholder ol.secondaryContent
	{
		padding: 0 10px;
	}

		.navPopup .listPlaceholder ol.secondaryContent.Unread
		{
			background-color: rgb(255, 255, 200);
		}

.navPopup .listItem
{
	overflow: hidden; zoom: 1;
	padding: 5px 0;
	border-bottom: 1px solid #d7edfc;
}

.navPopup .listItem:last-child
{
	border-bottom: none;
}

.navPopup .PopupItemLinkActive:hover
{
	margin: 0 -8px;
	padding: 5px 8px;
	border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
	background-color: #d7edfc;
	cursor: pointer;
}

.navPopup .avatar
{
	float: left;
}

	.navPopup .avatar img
	{
		width: 32px;
		height: 32px;
	}

.navPopup .listItemText
{
	margin-left: 37px;
}

	.navPopup .listItemText .muted
	{
		font-size: 9px;
	}

	.navPopup .unread .listItemText .title,
	.navPopup .listItemText .subject
	{
		font-weight: bold;
	}

.navPopup .sectionFooter .floatLink
{
	float: right;
}

#searchBar
{
	position: relative;
	zoom: 1;
	z-index: 1;
}

	#QuickSearch
	{
		display: block;
		
		position: absolute;
		right: 20px;
		top: -18px;
		
		margin: 0;
		
		background-color: rgb(240,240,240);
		border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px;
		padding-top: 5px;
		_padding-top: 3px;
		z-index: 7500;
	}
			
		#QuickSearch .secondaryControls
		{
			display: none;
		}
	
		#QuickSearch.active
		{
			box-shadow: 5px 5px 25px rgba(0,0,0, 0.5); -webkit-box-shadow:5px 5px 25px rgba(0,0,0, 0.5); -moz-box-shadow:5px 5px 25px rgba(0,0,0, 0.5); -khtml-box-shadow:5px 5px 25px rgba(0,0,0, 0.5);
			padding-bottom: 5px;
		}

/** move the header to the top again **/

#headerMover
{
	position: relative;
	zoom: 1;
	/*padding-top: 117px;*/ /* +2 borders */
}

	#headerMover #headerProxy
	{
		background: #176093 url('styles/default/russian/topbg.gif');
		height: 117px;*/ /* +2 borders */
	}

	#headerMover #header
	{
		width: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
	}


/** Generic page containers **/

.pageWidth
{
	margin: 0 10px;
min-width: 853px;
_width: 976px;
_margin: 0 auto;
width: auto;

}

#content .pageWidth
{
	background-color: rgb(252, 252, 255);
	/*border-left: 1px solid #d7edfc;
	border-right: 1px solid #d7edfc;*/
}

#content .pageContent
{
	background-color: rgb(240,240,240);
padding: 2px 5px;

}

/* clearfix */ #content .pageContent { zoom: 1; } #content .pageContent:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

/* sidebar structural elements */

.mainContainer
{
	 float: left;
	 margin-right: -260px;
	 width: 100%;
}

	.mainContent
	{
		margin-right: 260px;
	}

.sidebar
{
	font-size: 11px;
float: right;
width: 250px;

}















/* visitor panel */

.sidebar .visitorPanel
{
	overflow: hidden; zoom: 1;
}

	.sidebar .visitorPanel h2 .muted
	{
		display: none;
	}

	.sidebar .visitorPanel .avatar
	{
		margin-right: 5px;
float: left;
width: 96px;
height: 96px;

		
		width: auto;
		height: auto;
	}
	
		.sidebar .visitorPanel .avatar img
		{
			width: 96px;
			height: 96px;
		}
	
	.sidebar .visitorPanel .username
	{
		font-weight: bold;
font-size: 11pt;

	}
	
	.sidebar .visitorPanel .stats
	{
		margin-top: 2px;

	}













	
/* generic sidebar blocks */
		
.sidebar .section .primaryContent   h3,
.sidebar .section .secondaryContent h3
{
	font-size: 12pt;
color: #6cb2e4;
padding-bottom: 2px;
margin-bottom: 5px;
border-bottom: 1px solid #d7edfc;

}

.sidebar .section .primaryContent   h3 a,
.sidebar .section .secondaryContent h3 a
{
	font-size: 12pt;
color: #6cb2e4;
}

.sidebar .section .secondaryContent .footnote,
.sidebar .section .secondaryContent .minorHeading
{
	color: #6cb2e4;
margin-top: 5px;

}

	.sidebar .section .secondaryContent .minorHeading a
	{
		color: #6cb2e4;
	}












/* list of users with 32px avatars, username and user title */

.sidebar .avatarList li
{
	margin: 5px 0;
overflow: hidden;
zoom: 1;

}

	.sidebar .avatarList .avatar
	{
		margin-right: 5px;
float: left;
width: 32px;
height: 32px;

		
		width: auto;
		height: auto;
	}
		
	.sidebar .avatarList .avatar img
	{
		width: 32px;
		height: 32px;
	}
	
	.sidebar .avatarList .username
	{
		font-size: 11pt;
margin-top: 2px;
display: block;

	}
	
	.sidebar .avatarList .userTitle
	{
		color: rgb(150,150,150);

	}









/* list of users */

.sidebar .userList
{
}

	.sidebar .userList .username
	{
		font-size: 11px;

	}

	.sidebar .userList .username.invisible
	{
		color: #65a5d1;

	}
	
	.sidebar .userList .username.followed
	{
		
	}

	.sidebar .userList .moreLink
	{
		display: block;
	}
	
	
	
	
/* people you follow online now */

.followedOnline
{
	margin-top: 3px;
margin-bottom: -5px;
overflow: hidden;
zoom: 1;

}

.followedOnline li
{
	margin-right: 5px;
margin-bottom: 5px;
float: left;

}

	.followedOnline .avatar
	{
		width: 32px;
height: 32px;

		
		width: auto;
		height: auto;
	}
	
		.followedOnline .avatar img
		{
			width: 32px;
			height: 32px;
		}
	
	
	

	
	
/* call to action */

#SignupButton
{
	background-color: white;
padding: 3px;
margin: 10px 30px;
border: 1px solid #f9bc6d;
border-radius: 8px; -webkit-border-radius: 8px; -moz-border-radius: 8px; -khtml-border-radius: 8px;
text-align: center;
line-height: 30px;
box-shadow: 0px 2px 5px rgba(0,0,0, 0.2); -webkit-box-shadow:0px 2px 5px rgba(0,0,0, 0.2); -moz-box-shadow:0px 2px 5px rgba(0,0,0, 0.2); -khtml-box-shadow:0px 2px 5px rgba(0,0,0, 0.2);
display: block;
cursor: pointer;
height: 30px;

}

	#SignupButton .inner
	{
		font-weight: bold;
font-size: 12pt;
font-family: Arial, Helvetica, sans-serif;
color: #ffffff;
background: #e68c17 url('styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x center -7px;
border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;
display: block;
text-shadow: 0px 0px 3px rgba(0,0,0, 0.5);

	}
	
	#SignupButton:hover .inner
	{
		text-decoration: none;
background-color: #f9bc6d;

	}
	
	#SignupButton:active
	{
		box-shadow: 0 0 3px rgba(0,0,0, 0.2); -webkit-box-shadow:0 0 3px rgba(0,0,0, 0.2); -moz-box-shadow:0 0 3px rgba(0,0,0, 0.2); -khtml-box-shadow:0 0 3px rgba(0,0,0, 0.2);
position: relative;
top: 2px;

	}

/** Text used in message bodies **/

.messageText
{
	font-size: 11pt;
font-family: Arial, Helvetica, sans-serif;
text-decoration: none;
line-height: 1.4;

}

/** Link groups and pagenav container **/

.pageNavLinkGroup
{
	overflow: hidden; zoom: 1;
	margin: 10px auto 0;
	padding-bottom: 10px;
}

.pageNavLinkGroup + *
{
	margin-top: 0;
}

	.pageNavLinkGroup .linkGroup
	{
		float: right;
	}

.linkGroup
{
	font-size: 11px;
	line-height: 16px;
}

	.linkGroup a
	{
		display: block;
		float: left;
		margin-left: 10px;
		padding: 3px 0;
	}
	
	.linkGroup .Popup
	{
		float: left;
		display: block;
		margin-left: 10px;
	}
		.linkGroup .Popup a
		{
			margin-left: 0px;
			*margin-left: 10px;
		}

/** Call to action buttons **/

a.callToAction
{
	background: #a5cae4 url('styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x center top;
padding: 2px;
border: 1px solid #a5cae4;
border-radius: 6px; -webkit-border-radius: 6px; -moz-border-radius: 6px; -khtml-border-radius: 6px;
display: inline-block;
line-height: 20px;
box-shadow: 1px 1px 5px rgba(0,0,0, 0.15); -webkit-box-shadow:1px 1px 5px rgba(0,0,0, 0.15); -moz-box-shadow:1px 1px 5px rgba(0,0,0, 0.15); -khtml-box-shadow:1px 1px 5px rgba(0,0,0, 0.15);
outline: 0 none;
height: 20px;

	
}

	a.callToAction span
	{
		font-weight: bold;
font-size: 11px;
font-family: Arial, Helvetica, sans-serif;
color: #ffffff;
background: #65a5d1 url('styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x center -8px;
padding: 0 15px;
border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -khtml-border-radius: 3px;
display: block;
/*text-shadow: 0px 0px 3px #176093;*/

	}
	
	a.callToAction:hover
	{
		text-decoration: none;
	}

		a.callToAction:hover span
		{
			background-color: #6cb2e4;

		}
	
	a.callToAction:active
	{
		background-color: #176093;

	}

/* note: ol.miniMemberList must be contained -within- a clearfix */

.miniMemberList
{
	margin-left: -5px;
}

	.miniMemberList li
	{
		float: left;
	}

		.miniMemberList li a
		{
			display: block;
			padding: 3px 7px;
		}

			.miniMemberList li a:hover,
			.miniMemberList li a:focus
			{
				background: #d7edfc url('styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
				border: 1px solid #a5cae4;
				padding: 2px 6px;
			}

/*********/

/* User name classes */


.visitorNotice
{
	margin: 10px auto;
	padding: 10px;
	border: 1px solid #f9bc6d;
	border-radius: 10px; -webkit-border-radius: 10px; -moz-border-radius: 10px; -khtml-border-radius: 10px;
}

	.visitorNotice .content
	{
		padding: 10px;
		background-color: #f9d9b0;
		color: #6d3f03;
	}

		.visitorNotice .signUp
		{
			text-align: right;
			margin-top: 10px;
		}

			.visitorNotice .signUp .button
			{
				background-color: #f9d9b0;
				color: #6d3f03;
				font-size: 11pt;
				font-weight: bold;
			}

.avatarHeap
{
	overflow: hidden; zoom: 1;
}

	.avatarHeap ol
	{
		margin-right: -4px;
		margin-top: -4px;
	}
	
		.avatarHeap li
		{
			float: left;
			margin-right: 4px;
			margin-top: 4px;
		}
