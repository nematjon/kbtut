jQuery(window).load(function() {
	
	//SET SIDEBAR HEIGHT
	
	var window = jQuery(document).height();
	var sidebar = jQuery('#sidebar').height();
	var main = jQuery('#main').height();
	
	if(sidebar < main){
		jQuery('#sidebar').height(window - 40);
	}
	
});

jQuery(document).ready(function() {

	//SET SIDEBAR HEIGHT
	
	var window = jQuery(document).height();
	var sidebar = jQuery('#sidebar').height();
	var main = jQuery('#main').height();
	
	if(sidebar < main){
		jQuery('#sidebar').height(window - 40);
	}
	
	//SOCIAL PROFILES WIDGET
	
	jQuery('#social li:odd').css('background-color','#4f8fbf');
	
	//CHAT POST LI STYLING
	
	
	jQuery('.post.chat .media ul li:odd').css('background-color','#d9d7bd').css('text-align','right');
	jQuery('.post.chat .media ul li:odd .name').css('padding','0 0 0 10px');
	jQuery('.post.chat .media ul li:even .name').css('padding','0 10px 0 0');
	
	//COMMENTFORM
	
	var author = jQuery('#commentform #author').val();
	var email = jQuery('#commentform #email').val();
	var url = jQuery('#commentform #url').val();
	
	if (author == '') { jQuery('#commentform #author').val('Your Name') };
	if (email == '') { jQuery('#commentform #email').val('Your Email') };
	if (url == '') { jQuery('#commentform #url').val('Your URL') };
	
	jQuery('#commentform #author').focus(function() {
	
		var val = jQuery(this).val();
	
		if(val == 'Your Name'){	jQuery(this).val(''); }
	
	});
	
	jQuery('#commentform #author').blur(function() {
	
		var val = jQuery(this).val();
	
		if(val == ''){	jQuery(this).val('Your Name'); }
	
	});
	
	jQuery('#commentform #email').focus(function() {
	
		var val = jQuery(this).val();	
		if(val == 'Your Email'){ jQuery(this).val(''); }
	
	});
	
	jQuery('#commentform #email').blur(function() {
	
		var val = jQuery(this).val();	
		if(val == ''){ jQuery(this).val('Your Email'); }
	
	});
	
	jQuery('#commentform #url').focus(function() {
	
		var val = jQuery(this).val();	
		if(val == 'Your URL'){ jQuery(this).val(''); }
	
	});
	
	jQuery('#commentform #url').blur(function() {
	
		var val = jQuery(this).val();	
		if(val == ''){ jQuery(this).val('Your URL'); }
	
	});


});