﻿<?php

$lang["toj"]["1"]="Тоҷикӣ";
$lang["rus"]["1"]="Русӣ";
$lang["eng"]["1"]="Ангилисӣ";

$lang["toj"]["2"]="Таджикский";
$lang["rus"]["2"]="Русский";
$lang["eng"]["2"]="Английский";

$lang["toj"]["3"]="Tajik";
$lang["rus"]["3"]="Russian";
$lang["eng"]["3"]="English";

	$lang["site_title"]["1"]="Сайт biblioteka.kbtut.tj";
	$lang["site_title"]["2"]="Сайт biblioteka.kbtut.tj";
	$lang["site_title"]["3"]="Сайт biblioteka.kbtut.tj";

//menu
	//menu1
	$lang["mavodhoi_talimi"]["1"]="Маводҳои таълимӣ";
	$lang["mavodhoi_talimi"]["2"]="Учебное пособие";
	$lang["mavodhoi_talimi"]["3"]="The manual";

	$lang["next"]["1"]="Давом";
	$lang["next"]["2"]="Далее";
	$lang["next"]["3"]="Next";
	//menu1
	
	//menu2
	$lang["fehrist"]["1"]="Феҳрист";
	$lang["fehrist"]["2"]="Каталог";
	$lang["fehrist"]["3"]="The catalogue";
	
	$lang["alifboi"]["1"]="Алифбоӣ";
	$lang["alifboi"]["2"]="Алфавитное";
	$lang["alifboi"]["3"]="The alphabetic";
	
	$lang["elektroni"]["1"]="Электронӣ";
	$lang["elektroni"]["2"]="Электронное";
	$lang["elektroni"]["3"]="The electronic";
	
	$lang["az_rui_fan"]["1"]="Аз рӯи фан";
	$lang["az_rui_fan"]["2"]="По предмету";
	$lang["az_rui_fan"]["3"]=" In a subject";
	
	$lang["justujui_sistemavi"]["1"]="Ҷустуҷӯи системавӣ";
	$lang["justujui_sistemavi"]["2"]="Системный поиск";
	$lang["justujui_sistemavi"]["3"]="System search";
	//menu 2
//menu


$lang["nomi_kitob"]["1"]="Номи китоб";
$lang["nomi_kitob"]["2"]="Наименование книги";
$lang["nomi_kitob"]["3"]="The book name";

$lang["muallif"]["1"]="Муаллиф";
$lang["muallif"]["2"]="Автор";
$lang["muallif"]["3"]="The author";

$lang["shumorai_sahifa"]["1"]="Шумораи саҳифа";
$lang["shumorai_sahifa"]["2"]="Количество страниц";
$lang["shumorai_sahifa"]["3"]="Amount of pages";

$lang["soli_nashr"]["1"]="Соли нашр";
$lang["soli_nashr"]["2"]="Год издательство";
$lang["soli_nashr"]["3"]="Year publishing house";

$lang["hamai_raqamho_va_simvolho"]["1"]="Ҳамаи рақамҳо ва символҳо";
$lang["hamai_raqamho_va_simvolho"]["2"]="Все числы и символы";
$lang["hamai_raqamho_va_simvolho"]["3"]="All number and symbols";

$lang["joi_nashr"]["1"]="Ҷои нашр";
$lang["joi_nashr"]["2"]="Место издательство";
$lang["joi_nashr"]["3"]="Place publishing house";

$lang["shumora"]["1"]="Шумора";
$lang["shumora"]["2"]="Количество";
$lang["shumora"]["3"]="Amount";

$lang["zabon"]["1"]="Забон";
$lang["zabon"]["2"]="Язык";
$lang["zabon"]["3"]="Language";

$lang["malumot_oidi_kitob"]["1"]="Маълумот оиди китоб";
$lang["malumot_oidi_kitob"]["2"]="Информация о книге";
$lang["malumot_oidi_kitob"]["3"]="The information on the book";

$lang["id_nomgu"]["1"]="ID-номгӯ";
$lang["id_nomgu"]["2"]="ID-наименование";
$lang["id_nomgu"]["3"]="ID-name";

$lang["isbn"]["1"]="ISBN";
$lang["isbn"]["2"]="ISBN";
$lang["isbn"]["3"]="ISBN";

$lang["bbk"]["1"]="BBK";
$lang["bbk"]["2"]="BBK";
$lang["bbk"]["3"]="BBK";

$lang["udk"]["1"]="UDK";
$lang["udk"]["2"]="UDK";
$lang["udk"]["3"]="UDK";

$lang["format"]["1"]="Формат";
$lang["format"]["2"]="Формат";
$lang["format"]["3"]="Format";

$lang["kategoriya"]["1"]="Категория";
$lang["kategoriya"]["2"]="Категория";
$lang["kategoriya"]["3"]="Category";

$lang["namudi_mukova"]["1"]="Намуди мукова";
$lang["namudi_mukova"]["2"]="Вид обложки";
$lang["namudi_mukova"]["3"]="Cover kind";

$lang["mikdori_kitob"]["1"]="Микдори китоб";
$lang["mikdori_kitob"]["2"]="Количество книг";
$lang["mikdori_kitob"]["3"]="Amount of books";

$lang["rasm_eft_nashud"]["1"]="Расм ёфт нашуд.";
$lang["rasm_eft_nashud"]["2"]="Рисунок не найден.";
$lang["rasm_eft_nashud"]["3"]="Drawing is not found.";

$lang["kitobhona"]["1"]="Китобхона";
$lang["kitobhona"]["2"]="Библиотека";
$lang["kitobhona"]["3"]="Library";

$lang["justuju_dar_sayt"]["1"]="Ҷустуҷӯ дар сомона";
$lang["justuju_dar_sayt"]["2"]="Поиск по сайту";
$lang["justuju_dar_sayt"]["3"]="Search in site";

$lang["sahifai_avval"]["1"]="Cаҳифаи аввал";
$lang["sahifai_avval"]["2"]="Главная станица"; 
$lang["sahifai_avval"]["3"]="The main page"; 

$lang["dar_borai_mo"]["1"]="Дар бораи мо";  
$lang["dar_borai_mo"]["2"]="О нас";  
$lang["dar_borai_mo"]["3"]="About us";  

$lang["donishgoh"]["1"]="Донишгоҳ";
$lang["donishgoh"]["2"]="Университет";
$lang["donishgoh"]["3"]="University";

$lang["akhbor"]["1"]="Ахбор";
$lang["akhbor"]["2"]="Новости";
$lang["akhbor"]["3"]="News";

$lang["maqola"]["1"]="Мақола";
$lang["maqola"]["2"]="Статьи";
$lang["maqola"]["3"]="Article";

$lang["ba_qaytgiri"]["1"]="Ба қайдгири";
$lang["ba_qaytgiri"]["2"]="Регистрация";
$lang["ba_qaytgiri"]["3"]="Registration";

$lang["parol"]["1"]="Парол";
$lang["parol"]["2"]="Пароль";
$lang["parol"]["3"]="Password";

$lang["login"]["1"]="Логин";
$lang["login"]["2"]="Логин";
$lang["login"]["3"]="User";


$lang["daromad"]["1"]="Ворид шудан";
$lang["daromad"]["2"]="Вход";
$lang["daromad"]["3"]="Enter";

$lang["baromad"]["1"]="Баромад";
$lang["baromad"]["2"]="Выход";
$lang["baromad"]["3"]="Exit";


$lang["natija"]["1"]="Натиҷа";
$lang["natija"]["2"]="Результат";
$lang["natija"]["3"]="Result";


$lang["sahifa"]["1"]="Саҳифа";
$lang["sahifa"]["2"]="Станица";
$lang["sahifa"]["3"]="Page";

$lang["justuju"]["1"]="Ҷустуҷӯ";
$lang["justuju"]["2"]="Поиск";
$lang["justuju"]["3"]="Search";

$lang["farq_nadorad"]["1"]="Фарк надорад";
$lang["farq_nadorad"]["2"]="nest";
$lang["farq_nadorad"]["3"]="nest";

$lang["justujui_vase"]["1"]="Ҷустуҷӯи васеъ";
$lang["justujui_vase"]["2"]="Расширенный поиск";
$lang["justujui_vase"]["3"]="The expanded search";

$lang["natija_taqriban"]["1"]="Натиҷа: тақрибан";
$lang["natija_taqriban"]["2"]="Результат: примерно";
$lang["natija_taqriban"]["3"]="nest";



$lang["id_kitob"]["1"]="ID - китоб";
$lang["id_kitob"]["2"]="ID - книг";
$lang["id_kitob"]["3"]="ID - book";

$lang["mamuriyat"]["1"]="Маъмурият";
$lang["mamuriyat"]["2"]="Администратор";
$lang["mamuriyat"]["3"]="Administrator";

$lang["autor_site"]["1"]="Неъматҷон Неъматов ва  Аҳмедова Дилафрӯз";
$lang["autor_site"]["2"]="Неъматджон Неъматов и  Ахмедова Дилафруз";
$lang["autor_site"]["3"]="Nematjon Nematov and Akhmedova Dilafruz";

$lang["menyui_sait"]["1"]="Менюи асосӣ";
$lang["menyui_sait"]["2"]="Главный меню";
$lang["menyui_sait"]["3"]="The main menu";

$lang["ba_avval"]["1"]="Ба аввал";
$lang["ba_avval"]["2"]="К началу";
$lang["ba_avval"]["3"]="To the beginning";

$lang["ba_okhir"]["1"]="Ба охир";
$lang["ba_okhir"]["2"]="К концу";
$lang["ba_okhir"]["3"]="By the end";

$lang["guzashta"]["1"]="Саҳифаи гузашта";
$lang["guzashta"]["2"]="Предыдущая";
$lang["guzashta"]["3"]="The previous";

$lang["oyanda"]["1"]="Оянда";
$lang["oyanda"]["2"]="Следующая";
$lang["oyanda"]["3"]="The following";

$lang["parol_yo_login_khato_ast"]["1"]="Парол ё логин хато аст.";
$lang["parol_yo_login_khato_ast"]["2"]="Парол или логин не правилно";
$lang["parol_yo_login_khato_ast"]["3"]="The password or login not correctly";

$lang["login_vorid_karda_nashud"]["1"]="Логин ворид карда нашуд.";
$lang["login_vorid_karda_nashud"]["2"]="Логин  не ведено";
$lang["login_vorid_karda_nashud"]["3"]="Login not entered";

$lang["parol_vorid_karda_nashud"]["1"]="Парол ворид карда нашуд.";
$lang["parol_vorid_karda_nashud"]["2"]="Пароль не ведено";
$lang["parol_vorid_karda_nashud"]["3"]="The password not entered";

$lang["login_va_parol_vorid_karda_nashud."]["1"]="Логин ва парол ворид карда нашуд.";
$lang["login_va_parol_vorid_karda_nashud"]["2"]="Логин и парол не ведено";
$lang["login_va_parol_vorid_karda_nashud"]["3"]="login va parol vorid karda nashud";

$lang["delete_ok"]["1"]="Маъдумот бо муваффақият хориҷ карда шуд";
$lang["delete_ok"]["2"]="Информация успешна была удалена";
$lang["delete_ok"]["3"]="The information was successful is removed";

$lang["not_add_aptly"]["1"]="Мутаассифона маълумот илова карда нашуд";
$lang["not_add_aptly"]["2"]="К содалению информация не была добавлена";
$lang["not_add_aptly"]["3"]="Unfortunately the information has not been added";

$lang["not_delete_aptly"]["1"]="Мутаассифона маъдумот хориҷ  карда нашуд";
$lang["not_delete_aptly"]["2"]="К сожалению информация не была удалена";
$lang["not_delete_aptly"]["3"]="Unfortunately the information has not been removed";

$lang["not_update_aptly"]["1"]="Мутаассифона маълумот тағйир  дода нашуд";
$lang["not_update_aptly"]["2"]="К сожалению информация не была изменена";
$lang["not_update_aptly"]["3"]="Unfortunately the information has not been changed";

$lang["news_exists"]["1"]="Ахбор бо чунин сарлавҳа мавҷуд аст";
$lang["news_exists"]["2"]="Новость с таким загаловкам не сушествует";
$lang["news_exists"]["3"]="Ахбор бо чунин сарлавҳа мавҷуд аст";

$lang["diplomp_exists"]["1"]="Лоиҳаи дипломи бо чунин мавзӯъ мавҷуд аст";
$lang["diplomp_exists"]["2"]="Имеется дипломный проект на эту тему";
$lang["diplomp_exists"]["3"]="There is a degree project on this theme";

$lang["ba_qafo"]["1"]="Ба қафо";
$lang["ba_qafo"]["2"]="Назад";
$lang["ba_qafo"]["3"]="Back";

$lang["hama_malumotro_pura_namoed"]["1"]="Ҳама маълумотро пура намоед";
$lang["hama_malumotro_pura_namoed"]["2"]="Необходимо заполнить все поля";
$lang["hama_malumotro_pura_namoed"]["3"]="It is necessary to fill all fields";

$lang["sanai_ilova_shuda"]["1"]="Санаи иловашуда";
$lang["sanai_ilova_shuda"]["2"]="Дата ввода";
$lang["sanai_ilova_shuda"]["3"]="Input date";

$lang["shumora_az_nazar_guzaronida_shuda"]["1"]="Шумораи аз назар гузаронида шуда";
$lang["shumora_az_nazar_guzaronida_shuda"]["2"]="Количество проверенных";
$lang["shumora_az_nazar_guzaronida_shuda"]["3"]="Quantity checked up";

$lang["shumorai_khorij_shuda"]["1"]="Шумораи хориҷ шуда";
$lang["shumorai_khorij_shuda"]["2"]="Количество удаленных";
$lang["shumorai_khorij_shuda"]["3"]="Quantity of the removed";

$lang["khorij_kardan"]["1"]="Хориҷ кардан";
$lang["khorij_kardan"]["2"]="Удалить";
$lang["khorij_kardan"]["3"]="To remove";


$lang["kitobkhona"]["1"]="Китобхона";
$lang["kitobkhona"]["2"]="Библиотека";
$lang["kitobkhona"]["3"]="Library";

$lang["nom"]["1"]="Ном";
$lang["nom"]["2"]="Имя";
$lang["nom"]["3"]="Name";

$lang["nasab"]["1"]="Насаб";
$lang["nasab"]["2"]="Фамилия";
$lang["nasab"]["3"]="Surname";

$lang["nomi_padar"]["1"]="Номи падар";
$lang["nomi_padar"]["2"]="Отчество";
$lang["nomi_padar"]["3"]="Patronymic";

$lang["guruh"]["1"]="Гурӯҳ";
$lang["guruh"]["2"]="Группа";
$lang["guruh"]["3"]="Group";

$lang["sol"]["1"]="Сол";
$lang["sol"]["2"]="Год";
$lang["sol"]["3"]="Year";

$lang["mavzu"]["1"]="Мавзӯъ";
$lang["mavzu"]["2"]="Тема";
$lang["mavzu"]["3"]="Theme";

$lang["namudi_failho"]["1"]="Намуди файлҳо";
$lang["namudi_failho"]["2"]="Виды файлов";
$lang["namudi_failho"]["3"]="Kinds of files";

$lang["boyad_boshad"]["1"]="бояд бошад";
$lang["boyad_boshad"]["2"]="необходимо должно бить";
$lang["boyad_boshad"]["3"]="It should is necessary to beat";

$lang["hachmi_fail_az"]["1"]="Ҳаҷми файл аз";
$lang["hachmi_fail_az"]["2"]="Размер файла от";
$lang["hachmi_fail_az"]["3"]="The size of a file from";

$lang["mb_kalon_yo_ba"]["1"]="Мб калон ё ба";
$lang["mb_kalon_yo_ba"]["2"]="Мб калон ё ба";
$lang["mb_kalon_yo_ba"]["3"]="Мб калон ё ба";

$lang["mb_barobar_naboshad"]["1"]="МБ баробар набошад";
$lang["mb_barobar_naboshad"]["2"]="МБ баробар набошад";
$lang["mb_barobar_naboshad"]["3"]="МБ баробар набошад";

$lang["copy_ok"]["1"]="Файл бо муваффақият нусха гирифта шуд";
$lang["copy_ok"]["2"]="Копирование файла успешна завершилась";
$lang["copy_ok"]["3"]="File copying it is successful has come to the end";

$lang["sol_hato_dohil_karda_shud"]["1"]="Сол хато дохил карда шуд";
$lang["sol_hato_dohil_karda_shud"]["2"]="Не правильно ведена информация";
$lang["sol_hato_dohil_karda_shud"]["3"]="Not correctly enter the information";

$lang["file"]["1"]="Файл";
$lang["file"]["2"]="Файл";
$lang["file"]["3"]="File";

$lang["marhamat_namoed"]["1"]="Марҳамат намоед";
$lang["marhamat_namoed"]["2"]="Добро пажаловать";
$lang["marhamat_namoed"]["3"]="Welcome";


$lang["mavodho"]["1"]="Маводҳо";
$lang["mavodho"]["2"]="Маводҳо";
$lang["mavodho"]["3"]="Маводҳо";

$lang["loihai_diplomi"]["1"]="Лоиҳаи дипломӣ";
$lang["loihai_diplomi"]["2"]="Дипломный проект";
$lang["loihai_diplomi"]["3"]="The degree project";

$lang["ba_fanhoi_munosib_ilova_namudan"]["1"]="Ба фанҳои муносиб  илова намудан";
$lang["ba_fanhoi_munosib_ilova_namudan"]["2"]="Добавить в соответствующую дисциплину";
$lang["ba_fanhoi_munosib_ilova_namudan"]["3"]="To add in corresponding discipline";

$lang["kitobhoi_nav"]["1"]="Китобҳои нав";
$lang["kitobhoi_nav"]["2"]="Новые книги";
$lang["kitobhoi_nav"]["3"]="New books";

$lang["kitob_az_bazai_malumot_yoft_nashud"]["1"]="Китоб аз базаи маълумот ёфт нашуд";
$lang["kitob_az_bazai_malumot_yoft_nashud"]["2"]="Книга не найдена в базе данных";
$lang["kitob_az_bazai_malumot_yoft_nashud"]["3"]="The book is not found in a database";

$lang["nomi_fan"]["1"]="Номи фан";
$lang["nomi_fan"]["2"]="Наименование дисциплины"; 
$lang["nomi_fan"]["3"]="The discipline name"; 

$lang["az_rui_ikhtisos"]["1"]="Аз рӯи ихтисос";
$lang["az_rui_ikhtisos"]["2"]="По специальности";
$lang["az_rui_ikhtisos"]["3"]="On a speciality";

$lang["not_page"]["1"]="Саҳифа ёфт нашуд";
$lang["not_page"]["2"]="Страница не найдена";
$lang["not_page"]["3"]="The page is not found";

$lang["ikhtisos"]["1"]="Ихтисос";
$lang["ikhtisos"]["2"]="Специальность";
$lang["ikhtisos"]["3"]="Speciality";

$lang["natija"]["1"]="Нaтиҷа";
$lang["natija"]["2"]="Результат";
$lang["natija"]["3"]="Result";

$lang["ba_in_ihtisos_kitob_mavjud_nest"]["1"]="Ба ин ихтисос китоб мавҷуд нест";
$lang["ba_in_ihtisos_kitob_mavjud_nest"]["2"]="Не найдена книга по данной специальности";
$lang["ba_in_ihtisos_kitob_mavjud_nest"]["3"]="The book on the given speciality is not found";

$lang["ba_in_kategoriya_kitob_mavjud_nest"]["1"]="Ба ин категория китоб мавҷуд нест";
$lang["ba_in_kategoriya_kitob_mavjud_nest"]["2"]="Не найдена книга по данной категории";
$lang["ba_in_kategoriya_kitob_mavjud_nest"]["3"]="The book on the given category is not found";

$lang["ba_in_harf_kitob_mavjud_nest"]["1"]="Ба ин ҳарф китоб мавҷуд нест";
$lang["ba_in_harf_kitob_mavjud_nest"]["2"]="Не найдена книга по данному алфавиту";
$lang["ba_in_harf_kitob_mavjud_nest"]["3"]="The book under the given alphabet is not found";

$lang["chunin_sahifa_mavchud_nest"]["1"]="Чунин саҳифа мавчуд нест";
$lang["chunin_sahifa_mavchud_nest"]["2"]="Такой странице не существует";
$lang["chunin_sahifa_mavchud_nest"]["3"]="To such page does not exist";

$lang["ba_in_fan_kitob_mavjud_nes"]["1"]="Ба ин фан китоб мавҷуд нест";
$lang["ba_in_fan_kitob_mavjud_nes"]["2"]="Не найдена книга по данной дисциплине";
$lang["ba_in_fan_kitob_mavjud_nes"]["3"]="The book on the given discipline is not found";

$lang["ba_in_bbk_kitob_mavjud_nes"]["1"]="Ба ин bbk китоб мавҷуд нест";
$lang["ba_in_bbk_kitob_mavjud_nes"]["2"]="Не найдена книга по такому bbk";
$lang["ba_in_bbk_kitob_mavjud_nes"]["3"]="The book on such bbk is not found";

$lang["shumo_yagon_kategoriyaro_intikhob_nakarded"]["1"]="Шумо ягон категорияро интикхоб накардед";
$lang["shumo_yagon_kategoriyaro_intikhob_nakarded"]["2"]="Вы не ввыбрали ни одну категорию";
$lang["shumo_yagon_kategoriyaro_intikhob_nakarded"]["3"]="You no chosen any category";


$lang["ba_ikhtisoshoi_munosib_ilova_namudan"]["1"]="Ба ихтисосҳои муносиб илова намудан";
$lang["ba_ikhtisoshoi_munosib_ilova_namudan"]["2"]="Добавить в соответсвующую специальность";
$lang["ba_ikhtisoshoi_munosib_ilova_namudan"]["3"]="To add in a corresponding speciality";

$lang["kodi_ikhtios"]["1"]="Коди ихтисос";
$lang["kodi_ikhtios"]["2"]="Код специальности";
$lang["kodi_ikhtios"]["3"]="Speciality code";

$lang["nomi_ikhtisos"]["1"]="Номи ихтисос";
$lang["nomi_ikhtisos"]["2"]="Наименование специальности";
$lang["nomi_ikhtisos"]["3"]="The speciality name";

$lang["wrong_search"]["1"]="Ҷустуҷӯ набояд аз 3 аломат ичро шавад, марзҳои рақами бояд бо рақам пур карда шаванд.";
$lang["wrong_search"]["2"]="Поиск должно осуществляться не менее чем трех символов, нумерованые поля должны заполняться номерами.";
$lang["wrong_search"]["3"]="Search it should be carried out not less than three symbols, The numbered fields should be filled with numbers.";

$lang["dar_borai_loihai_diplomi"]["1"]="Дар бораи лоиҳаи дипломӣ";
$lang["dar_borai_loihai_diplomi"]["2"]="О дипломном проекте";
$lang["dar_borai_loihai_diplomi"]["3"]="About the degree project";

$lang["kbtut_name"]["1"]="Донишкадаи политехникии <br>Донишгоҳи техникии Тоҷикистон <br><span>ба номи академик Муҳаммад Осимӣ</span>";
$lang["kbtut_name"]["2"]="Политехнический Институт <br>Таджикского Технического Университета <br> <span>имени академика  Мухаммад Осими</span>";
$lang["kbtut_name"]["3"]="Polytechnical Institute <br>of the Tajik Technical University <br> <span>name of the academician Muhammad Osimi</span>";

$lang["kbtut_name1"]["1"]="Донишкадаи политехникии Донишгоҳи техникии Тоҷикистон ба номи академик Муҳаммад Осимӣ";
$lang["kbtut_name1"]["2"]="Политехнический Институт Таджикского Технического Университета имени академика  Мухаммад Осими";
$lang["kbtut_name1"]["3"]="Polytechnical Institute of the Tajik Technical University name of the academician Muhammad Osimi";

$lang["not_found"]["1"]="Чустучу натича надод";
$lang["not_found"]["2"]="Поиск не дал результата";
$lang["not_found"]["3"]="Search has not given result";

$lang["sarlavha"]["1"]="Сарлавҳа";
$lang["sarlavha"]["2"]="Загаловок";
$lang["sarlavha"]["3"]="Heading";

$lang["matni_kutoh"]["1"]="Матни кӯтоҳ";
$lang["matni_kutoh"]["2"]="Короткий текст";
$lang["matni_kutoh"]["3"]="The short text";

$lang["matn"]["1"]="Матн";
$lang["matn"]["2"]="Текст";
$lang["matn"]["3"]="The text";

$lang["sana"]["1"]="Сана";
$lang["sana"]["2"]="Дата";
$lang["sana"]["3"]="Date";

$lang["ilova_namudan"]["1"]="Илова намудан";
$lang["ilova_namudan"]["2"]="Добавить";
$lang["ilova_namudan"]["3"]="To add";

$lang["taghyir_dodan"]["1"]="Тағйир додан";
$lang["taghyir_dodan"]["2"]="Изменить";
$lang["taghyir_dodan"]["3"]="To change";



$lang["update_ok"]["1"]="Бо муваффақият тағйир дода шуд";
$lang["update_ok"]["2"]="Процесс изменение удачно осуществилась";
$lang["update_ok"]["3"]="Process change it was successfully carried out";

$lang["add_ok"]["1"]="Ҳамаи маълумот бо муваффақият илова карда шуд";
$lang["add_ok"]["2"]="Вся информация успешно добавлено";
$lang["add_ok"]["3"]="All information it is successfully added";

/*$lang[""]["1"]="";$lang[""]["1"]="";$lang[""]["1"]="";$lang[""]["1"]="";$lang[""]["1"]="";$lang[""]["1"]="";$lang[""]["1"]="";*/

//Ққ Ҳҳ ӣ Ӯӯ Ҷҷ Ғғ









?>
