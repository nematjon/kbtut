﻿<?php

if(!isset($_SESSION['reg_ok']) ){header("location:../../index.php");}

$url= $_SERVER['REQUEST_URI'];

if(isset($_REQUEST['limit']))
{
	$limit=$_REQUEST['limit'];

	$url= substr($url,0,strpos($url,"limit")-1);
}
else
{ 
	$limit=0;
}

$shart="where";
///////////////////////////////////////
if(isset($_REQUEST['id_n']))
{
	
	if(is_numeric($_REQUEST['id_n']))
	{
		$id_n=$_REQUEST['id_n'];
		$shart.=" and tbm.id_naimen='$id_n'";
	}
	else
	{
		$id_n="";
	}
	
}
else
{
	$id_n="";
}

	
if(isset($_REQUEST['nom']))
{
	$nom=$_REQUEST['nom'];
}
else
{
	$nom="";
}

if(isset($_REQUEST['type']))
{
	$type=$_REQUEST['type'];
}
else
{
	$type="";
}

if(isset($_REQUEST['mua']))
{
	$mua=$_REQUEST['mua'];
}
else
{
	$mua="";
}
	
if(isset($_REQUEST['sol']))
{
	$sol=$_REQUEST['sol'];
	if(is_numeric($sol))
	$shart.=" and tbm.god_izd='$sol'";
}
else
{
	$sol="";
}
	

$from='';
$pfrom='';
if(isset($_REQUEST['joi']))
{
	$joi=$_REQUEST['joi'];
	if($joi!="")
	{
		$shart.=" and bmi.id_city = tbm.mesto_izd ";
		$from.=", books_mesto_izdatelstvo bmi";
		$pfrom.=", bmi.city";
	}	
}
else
{
	$joi="";
}


if(isset($_REQUEST['zab']))
{
	$zab=$_REQUEST['zab'];
	if($zab!="vse" && $zab!="")
	{
		$shart.=" and bl.id_lang=tbm.language and bl.id_lang='$zab'";
		$pfrom.=", bl.language";
		$from.=", books_language bl";
	}
	
}
else
{
	$zab="";
}

if(isset($_REQUEST['kat']))
{
	$kat=$_REQUEST['kat'];
	if($kat!="" and $kat!="vse")
	{
		$pfrom.=", bc.tip  as categor";
		$from.=", books_cat bc";
		$shart.=" and tbm.categor=bc.id_tip and bc.id_tip=$kat";
	}
	else
	{
	$pfrom.=", bc.tip  as categor";
	$from.=", books_cat bc";
	$shart.=" and tbm.categor=bc.id_tip";
	$kat="";
	}
}
else
	{
	$pfrom.=", bc.tip  as categor";
	$from.=", books_cat bc";
	$shart.=" and tbm.categor=bc.id_tip";
	$kat="";
}
	
if(isset($_REQUEST['isb']))
{
	$isb=$_REQUEST['isb'];
	if($isb!="" )
	$shart.=" and tbm.isbn='$isb'";
}
else
{
	$isb="";
}

if(isset($_REQUEST['bbk']))
{
	$bbk=$_REQUEST['bbk'];
	if($bbk!="")
	$shart.=" and tbm.bbk='$bbk'";
}
else
{
	$bbk="";
}
	
if(isset($_REQUEST['udk']))
{
	$udk=$_REQUEST['udk'];
	if($udk!="")
	$shart.=" and tbm.udk='$udk'";
}
else
{
	$udk="";
}
	

///////////////////////////////////////
echo "<div class='content_body' ><div class='div_search'>";	

if( ($id_n!='' && $id_n<1)  || ($nom!='' && strlen($nom)<3)  || ($mua!='' && strlen($mua)<3)  || ($sol!='' &&  strlen($sol)<4) || 
($joi!='' && strlen($joi)<3)  || ($isb!='' && strlen($isb)<3) || ($bbk!='' && strlen($bbk)<3) || ($udk!='' && strlen($udk)<3)  )
{
		echo "<div class='wrong_search'>{$lang["wrong_search"][$_SESSION['lan']]}</div>";
}




if( ($id_n!='' && $id_n>0)  || ($nom!='' && strlen($nom)>2)  || ($mua!='' && strlen($mua)>2)  || ($sol!=''  && is_numeric($sol) ) || 
($joi!='' && strlen($joi)>2)  || ($isb!='' && strlen($isb)>2) || ($bbk!='' && strlen($bbk)>2) || ($udk!='' && strlen($udk)>2)  )
{
	
	if($nom!="")
	{
		$search_trans_ru_nom=trim(translit($nom,"ru"));
		$search_trans_ru_nom=mb_convert_encoding($search_trans_ru_nom, "utf-8","auto");
		
		$search_trans_en_nom=trim(translit($nom,"en"));
		$search_trans_en_nom=mb_convert_encoding($search_trans_en_nom, "utf-8","auto");
	
	
		$search_raskladko_ru_nom=trim(raskladko($nom,"ru"));
		$search_raskladko_ru_nom=mb_convert_encoding($search_raskladko_ru_nom, "utf-8","auto");
		
		$search_raskladko_en_nom=trim(raskladko($nom,"en"));
		$search_raskladko_en_nom=mb_convert_encoding($search_raskladko_en_nom, "utf-8","auto");
		
		
		$shart.=" and (tbm.naimenovanie like '%$search_trans_ru_nom%' or tbm.naimenovanie like '%$search_trans_en_nom%' or tbm.naimenovanie like '%$search_raskladko_ru_nom%' or tbm.naimenovanie like '%$search_raskladko_en_nom%')";
	}
	
	
	if($mua!="")
	{
		$search_trans_ru_mua=trim(translit($mua,"ru"));
		$search_trans_ru_mua=mb_convert_encoding($search_trans_ru_mua, "utf-8","auto");
		
		$search_trans_en_mua=trim(translit($mua,"en"));
		$search_trans_en_mua=mb_convert_encoding($search_trans_en_mua, "utf-8","auto");
	
	
		$search_raskladko_ru_mua=trim(raskladko($mua,"ru"));
		$search_raskladko_ru_mua=mb_convert_encoding($search_raskladko_ru_mua, "utf-8","auto");
		
		$search_raskladko_en_mua=trim(raskladko($mua,"en"));
		$search_raskladko_en_mua=mb_convert_encoding($search_raskladko_en_mua, "utf-8","auto");
		
		/*if($nom!="" || $kat!="")
		{$av_sh="and";}
		else{$av_sh="or";}*/
		$shart.=" and (tbm.avtor like '%$search_trans_ru_mua%' or tbm.avtor like '%$search_trans_en_mua%' or tbm.avtor like '%$search_raskladko_ru_mua%' or tbm.avtor like '%$search_raskladko_en_mua%')";
	}
	
	
		if($joi!="")
		{
		$search_trans_ru_joi=trim(translit($joi,"ru"));
		$search_trans_ru_joi=mb_convert_encoding($search_trans_ru_joi, "utf-8","auto");
		
		$search_trans_en_joi=trim(translit($joi,"en"));
		$search_trans_en_joi=mb_convert_encoding($search_trans_en_joi, "utf-8","auto");
	
	
		$search_raskladko_ru_joi=trim(raskladko($joi,"ru"));
		$search_raskladko_ru_joi=mb_convert_encoding($search_raskladko_ru_joi, "utf-8","auto");
		
		$search_raskladko_en_joi=trim(raskladko($joi,"en"));
		$search_raskladko_en_joi=mb_convert_encoding($search_raskladko_en_joi, "utf-8","auto");
		
		if($nom!="")
		{$av_sh="and";}
		else{$av_sh="or";}
		$shart.=" $av_sh (bmi.city like '%$search_trans_ru_joi%' or bmi.city like '%$search_trans_en_joi%' or bmi.city like '%$search_raskladko_ru_joi%' or bmi.city like '%$search_raskladko_en_joi%')";
	}
	
	$shart=str_replace("where or","where",$shart);
	$shart=str_replace("where and","where",$shart);
	

	 $search_count_zapros="select tbm.id_naimen,tbm.naimenovanie,tbm.avtor,tbm.isbn,tbm.bbk,tbm.udk,tbm.god_izd,
	 (select bl.language from books_language bl where bl.id_lang=tbm.language) as language, 
	 (select sbmi.city from books_mesto_izdatelstvo sbmi where sbmi.id_city=tbm.mesto_izd) as mesto_izd $pfrom from 
	 t_books_main tbm $from
	 $shart
	 group by  tbm.naimenovanie";
	 $search_count_query=mysql_query($search_count_zapros);
	 $search_count_nr=mysql_num_rows($search_count_query);

	 $limit_sql=$limit*10;
	$search_zapros="select tbm.id_naimen,tbm.naimenovanie,tbm.avtor,tbm.isbn,tbm.bbk,tbm.udk,tbm.god_izd,
	 (select bl.language from books_language bl where bl.id_lang=tbm.language) as language, 
	 (select sbmi.city from books_mesto_izdatelstvo sbmi where sbmi.id_city=tbm.mesto_izd) as mesto_izd $pfrom from 
	 t_books_main tbm $from 
	   $shart
	 group by  tbm.naimenovanie limit $limit,10";
	 
	$search_query=mysql_query($search_zapros);
	$search_nr=mysql_num_rows($search_query);


	
	
	
	if($search_nr>0)
	{
		natijai_justuju($search_count_zapros,$limit);
		
		
		while($search_fa=mysql_fetch_array($search_query))
		{
			
			$search_n=$search_fa['id_naimen'];
			$search_ne=$search_fa['naimenovanie'];
			$avtor=$search_fa['avtor'];


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
			if($nom!="")
			{$search_ne=videlit($search_ne,$search_trans_ru_nom,$search_trans_en_nom,$search_raskladko_ru_nom,$search_raskladko_en_nom);}
			
			if($mua!="")
			{$avtor=videlit($avtor,$search_trans_ru_mua,$search_trans_en_mua,$search_raskladko_ru_mua,$search_raskladko_en_mua);}
			
			if($joi!="")
			{$search_fa['mesto_izd']=videlit($joi,$search_trans_ru_joi,$search_trans_en_joi,$search_raskladko_ru_joi,$search_raskladko_en_joi);}
			
			if($isb!="")
			{$search_fa['isbn']=videlit($search_fa['isbn'],"","","","");}
			
			if($bbk!="")
			{$search_fa['bbk']=videlit($search_fa['bbk'],"","","","");}
			
			if($udk!="")
			{$search_fa['udk']=videlit($search_fa['udk'],"","","","");}
			
			if($zab!="" and $zab!="vse")
			{$search_fa['language']=videlit($search_fa['language'],"","","","");}
						
			if($kat!="")
			{$search_fa['categor']=videlit($search_fa['categor'],"","","","");}

			if($id_n!="")
			{$id_n_class=videlit($id_n,"","","","");}
			else{$id_n_class=$search_fa['id_naimen'];}
			
			if($sol!="")
			{$search_fa['god_izd']=videlit($sol,"","","","");}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			

			

			//$search_img_put="../../../../../../rasm/";
			$search_img_put="images/books/";
			
	$poisk_img_id=$search_fa['id_naimen'];
	$poisk_img_zapros="select naimenovanie, id_naimen, avtor,kol_str, god_izd from t_books_main where id_naimen=$poisk_img_id";
	$poisk_img_query=mysql_query($poisk_img_zapros);
	$poisk_img_fa=mysql_fetch_array($poisk_img_query);
	$poisk_img_naimenovanie=$poisk_img_fa['naimenovanie'];
	$poisk_img_avtor=substr($poisk_img_fa['avtor'],0,2);
	$poisk_img_god_izd=$poisk_img_fa['god_izd'];
	$poisk_img_kol_str=$poisk_img_fa['kol_str'];
	
	$poisk_img_zapros1="select id_naimen from t_books_main where naimenovanie='$poisk_img_naimenovanie' and avtor like '$poisk_img_avtor%' and god_izd=$poisk_img_god_izd ";
	$poisk_img_query1=mysql_query($poisk_img_zapros1);	
	
if(!is_file("$search_img_put/1/"."$search_fa[id_naimen]".".jpg"))
{

	while($poisk_img_fa1=mysql_fetch_array($poisk_img_query1))
	{
		$poisk_img_file="$search_img_put/1/"."$poisk_img_fa1[id_naimen]".".jpg";
		if(file_exists($poisk_img_file))
		{
			if(file_exists($poisk_img_file))
			{
				$search_n=$poisk_img_fa1['id_naimen'];
				break;
			}
			else
			{
				$search_n=$poisk_img_fa1['id_naimen'];
			}
		}
	}	
}//if

			
			
			
			$search_rup=$search_img_put."1/".$search_n.".jpg";
			
			
			$search_imgn="class='searched_img'";
			$search_ru="<img src='".$search_rup."'/>";

			if(!file_exists("$search_rup")){$search_ru="<div $search_imgn></div>";}
			
			$search_fa['categor']=category_rutj($search_fa['categor'],"");
			
			echo "<table class='search_table_vase'>
			<tr> <td colspan='2' class='search_img'><a href='?menu=view_book_info&idb={$search_fa['id_naimen']}'>$search_ne</a></td> </tr> 
			
			 <tr> <td class='search_td_img_vase'>$search_ru</td>  
			
			<td class='search_td_info_vase'>
			<div><label>".$lang["id_nomgu"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>$id_n_class</label></div>
			<div><label>".$lang["muallif"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>$avtor</label></div>
			<div><label>".$lang["kategoriya"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['categor']}</label></div>
			<div><label>".$lang["soli_nashr"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['god_izd']}</label></div>
			<div><label>".$lang["joi_nashr"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['mesto_izd']}</label></div>
			<div><label>".$lang["zabon"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['language']}</label></div>";

	if($type!="simple")
	 {echo "<div><label>".$lang["isbn"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['isbn']}</label></div>
			<div><label>".$lang["bbk"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['bbk']}</label></div>
			<div><label>".$lang["udk"][$_SESSION['lan']].":</label> <label class='search_lable_info_vase'>{$search_fa['udk']}</label></div>";}
			
	echo   "</td></tr></table>";
			
			
		}
		


		limit_page($search_count_nr,$limit,$url,10);
	}
	else
	{
		echo "<table  class='table_message'><tr><td>{$lang["not_found"][$_SESSION['lan']]}
			<br><a href='?menu=elektroni'>{$lang["justuju"][$_SESSION['lan']]}</a></td></tr></table>";
	}
	
}
else
{
	echo "<div class='search_area'>";
	echo "<form action='' method='get'>
	<input type='hidden' name='menu' value='elektroni'/>
	<input type='hidden' name='type' value='simple'/>
	<input type='text' name='nom' class='jus_sodda'/>
	<input type='submit' value='{$lang["justuju"][$_SESSION['lan']]}' name='jus_sodda'/>
	</form> ";
	echo "</div>";
	
	//JUSTUJUI VASE
	echo "<div>";
	$jus_kat_zapros="select * from books_cat where tip<>'' order by tip";
	$jus_kat_query=mysql_query($jus_kat_zapros);
	
	$kat_sel="<option selected=\"selected\" value='vse'>1) {$lang["farq_nadorad"][$_SESSION['lan']]}</option>";
	$jus_sel_i=1;
	while($jus_kat_fa=mysql_fetch_array($jus_kat_query))
	{
		$jus_kat_fa['tip']=category_rutj($jus_kat_fa['tip'],"");
		$jus_sel_i++;
		$kat_sel.="<option value={$jus_kat_fa['id_tip']}>$jus_sel_i) {$jus_kat_fa['tip']}</option>";
	}

	$jus_len_zapros="select * from books_language order by language";	
	$jus_len_query=mysql_query($jus_len_zapros);
	$len_sel="<option selected='selected' value='vse'>1) {$lang["farq_nadorad"][$_SESSION['lan']]}</option>";
	$len_sel_i=1;
	while($jus_len_fa=mysql_fetch_array($jus_len_query))
	{
		$len_sel_i++;
		$len_sel.="<option  value='{$jus_len_fa['id_lang']}'>$len_sel_i) {$jus_len_fa['language']}</option>";
	}
		
			echo "<form action='' name='jus_vase' method='get'>
			<table  class='jus_vase_table'>
			<input type='hidden' name='menu' value='elektroni'/>
			<tr> <td class='jus_vase_td'>{$lang["id_kitob"][$_SESSION['lan']]}</td>     <td> <input type='text' name='id_n' /> </td> </tr>
			<tr> <td >{$lang["nomi_kitob"][$_SESSION['lan']]}</td>     <td> <input type='text' name='nom' /> </td> </tr>
			<tr> <td>{$lang["muallif"][$_SESSION['lan']]}</td>     <td> <input type='text' name='mua' /> </td> </tr>
			<tr> <td>{$lang["kategoriya"][$_SESSION['lan']]}</td>     <td> <select   size='5' name='kat' >$kat_sel</select> </td> </tr>
			<tr> <td>{$lang["soli_nashr"][$_SESSION['lan']]}</td>     <td> <input type='text' name='sol' /> </td> </tr>
			<tr> <td>{$lang["joi_nashr"][$_SESSION['lan']]}</td>     <td> <input type='text' name='joi' /> </td> </tr>
			<tr> <td>{$lang["zabon"][$_SESSION['lan']]}</td>     <td> <select   size='5' name='zab' >$len_sel</select> </td> </tr>
			<tr> <td>{$lang["isbn"][$_SESSION['lan']]}</td>     <td> <input type='text' name='isb' /> </td> </tr>
			<tr> <td>{$lang["bbk"][$_SESSION['lan']]}</td>     <td> <input type='text' name='bbk' /> </td> </tr>
			<tr> <td>{$lang["udk"][$_SESSION['lan']]}</td>     <td> <input type='text' name='udk' /> </td> </tr>
			
			
			
			</table>
			<p/><input type='submit' value='{$lang["justujui_vase"][$_SESSION['lan']]}' name='jus_vase'/>
			</form>";
			




	
	echo "</div>";
	
}
echo "</div></div>";
	
?>
